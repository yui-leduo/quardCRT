<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>AddTabButton</name>
    <message>
        <location filename="../lib/QtFancyTabWidget/addtabbutton.cpp" line="29"/>
        <source>New tab</source>
        <translation>Nouvel onglet</translation>
    </message>
</context>
<context>
    <name>CentralWidget</name>
    <message>
        <location filename="../src/mainwindow.ui" line="101"/>
        <location filename="../src/mainwindow.cpp" line="1123"/>
        <source>Tool Bar</source>
        <translation>Barre d&apos;outils</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="191"/>
        <location filename="../src/mainwindow.cpp" line="194"/>
        <location filename="../src/mainwindow.cpp" line="197"/>
        <location filename="../src/mainwindow.cpp" line="325"/>
        <location filename="../src/mainwindow.cpp" line="341"/>
        <location filename="../src/mainwindow.cpp" line="358"/>
        <location filename="../src/mainwindow.cpp" line="371"/>
        <location filename="../src/mainwindow.cpp" line="546"/>
        <location filename="../src/mainwindow.cpp" line="2552"/>
        <location filename="../src/mainwindow.cpp" line="2747"/>
        <location filename="../src/mainwindow.cpp" line="3115"/>
        <location filename="../src/mainwindow.cpp" line="3451"/>
        <location filename="../src/mainwindow.cpp" line="3456"/>
        <source>Warning</source>
        <translation>Attention</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="191"/>
        <source>TFTP server bind error!</source>
        <translation>Erreur de liaison du serveur TFTP !</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="194"/>
        <source>TFTP server file error!</source>
        <translation>Erreur de fichier du serveur TFTP !</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="197"/>
        <source>TFTP server network error!</source>
        <translation>Erreur réseau du serveur TFTP !</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="286"/>
        <source>Unlock Session</source>
        <translation>Déverrouiller la session</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="287"/>
        <source>Unlock current session</source>
        <translation>Déverrouiller la session en cours</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="293"/>
        <source>Move to another Tab</source>
        <translation>Déplacer vers un autre onglet</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="294"/>
        <source>Move to current session to another tab group</source>
        <translation>Déplacer la session en cours vers un autre groupe d&apos;onglets</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="303"/>
        <source>Floating Window</source>
        <translation>Fenêtre flottante</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="304"/>
        <source>Floating current session to a new window</source>
        <translation>Fenêtre flottante de la session en cours vers une nouvelle fenêtre</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="311"/>
        <source>Copy Path</source>
        <translation>Copier le chemin</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="312"/>
        <source>Copy current session working folder path</source>
        <translation>Copier le chemin du dossier de travail de la session en cours</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="325"/>
        <location filename="../src/mainwindow.cpp" line="341"/>
        <location filename="../src/mainwindow.cpp" line="358"/>
        <source>No working folder!</source>
        <translation>Pas de dossier de travail !</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="327"/>
        <source>Add Path to Bookmark</source>
        <translation>Ajouter le chemin au marque-page</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="328"/>
        <source>Add current session working folder path to bookmark</source>
        <translation>Ajouter le chemin du dossier de travail de la session en cours au marque-page</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="343"/>
        <source>Open Working Folder</source>
        <translation>Ouvrir le dossier de travail</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="344"/>
        <source>Open current session working folder in system file manager</source>
        <translation>Ouvrir le dossier de travail de la session en cours dans le gestionnaire de fichiers système</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="362"/>
        <source>Open SFTP</source>
        <translation>Ouvrir SFTP</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="363"/>
        <source>Open SFTP in a new window</source>
        <translation>Ouvrir SFTP dans une nouvelle fenêtre</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="371"/>
        <source>No SFTP channel!</source>
        <translation>Aucun canal SFTP !</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="378"/>
        <source>Save Session</source>
        <translation>Enregistrer la session</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="379"/>
        <source>Save current session to session manager</source>
        <translation>Enregistrer la session en cours dans le gestionnaire de sessions</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="389"/>
        <source>Enter Session Name</source>
        <translation>Entrer le nom de la session</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="390"/>
        <source>The session already exists, please rename the new session or cancel saving.</source>
        <translation>La session existe déjà, veuillez renommer la nouvelle session ou annuler l&apos;enregistrement.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="396"/>
        <source>Properties</source>
        <translation>Propriétés</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="397"/>
        <source>Show current session properties</source>
        <translation>Afficher les propriétés de la session en cours</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="417"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="418"/>
        <source>Close current session</source>
        <translation>Fermer la session en cours</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="423"/>
        <source>Close Others</source>
        <translation>Fermer les autres</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="424"/>
        <source>Close other sessions</source>
        <translation>Fermer les autres sessions</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="434"/>
        <source>Close to the Right</source>
        <translation>Fermer à droite</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="435"/>
        <source>Close sessions to the right</source>
        <translation>Fermer les sessions à droite</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="443"/>
        <source>Close All</source>
        <translation>Tout fermer</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="444"/>
        <source>Close all sessions</source>
        <translation>Fermer toutes les sessions</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="546"/>
        <source>Session properties error!</source>
        <translation>Erreur des propriétés de la session !</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="566"/>
        <location filename="../src/mainwindow.cpp" line="684"/>
        <location filename="../src/mainwindow.cpp" line="2455"/>
        <source>Ready</source>
        <translation>Prêt</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="777"/>
        <source>Highlight/Unhighlight</source>
        <translation>Surligner/Désurligner</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="778"/>
        <source>Highlight/Unhighlight selected text</source>
        <translation>Surligner/Désurligner le texte sélectionné</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="838"/>
        <source>Google Translate</source>
        <translation>Google Translate</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="839"/>
        <location filename="../src/mainwindow.cpp" line="852"/>
        <location filename="../src/mainwindow.cpp" line="864"/>
        <source>Translate selected text</source>
        <translation>Traduire le texte sélectionné</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="851"/>
        <source>Baidu Translate</source>
        <translation>Baidu Translate</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="863"/>
        <source>Microsoft Translate</source>
        <translation>Microsoft Translate</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="924"/>
        <source>Back to Main Window</source>
        <translation>Retour à la fenêtre principale</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="995"/>
        <location filename="../src/mainwindow.cpp" line="1019"/>
        <source>Session Manager</source>
        <translation>Gestionnaire de sessions</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="996"/>
        <source>Plugin</source>
        <translation>Plugin</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="998"/>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="999"/>
        <source>Edit</source>
        <translation>Édition</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1000"/>
        <source>View</source>
        <translation>Affichage</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1001"/>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1002"/>
        <source>Transfer</source>
        <translation>Transfert</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1003"/>
        <source>Script</source>
        <translation>Script</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1004"/>
        <source>Bookmark</source>
        <translation>Marque-page</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1005"/>
        <source>Tools</source>
        <translation>Outils</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1006"/>
        <source>Window</source>
        <translation>Fenêtre</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1007"/>
        <source>Language</source>
        <translation>Langue</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1008"/>
        <source>Theme</source>
        <translation>Thème</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1009"/>
        <location filename="../src/mainwindow.cpp" line="1229"/>
        <location filename="../src/mainwindow.cpp" line="3415"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1011"/>
        <source>New Window</source>
        <translation>Nouvelle fenêtre</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1013"/>
        <source>Open a new window &lt;Ctrl+Shift+N&gt;</source>
        <translation>Ouvrir une nouvelle fenêtre &lt;Ctrl+Shift+N&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1015"/>
        <source>Connect...</source>
        <translation>Se connecter...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1017"/>
        <source>Connect to a host &lt;Alt+C&gt;</source>
        <translation>Se connecter à un hôte &lt;Alt+C&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1021"/>
        <source>Go to the Session Manager &lt;Alt+M&gt;</source>
        <translation>Aller au gestionnaire de sessions &lt;Alt+M&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1023"/>
        <source>Quick Connect...</source>
        <translation>Connexion rapide...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1025"/>
        <source>Quick Connect to a host &lt;Alt+Q&gt;</source>
        <translation>Connexion rapide à un hôte &lt;Alt+Q&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1027"/>
        <source>Connect in Tab/Tile...</source>
        <translation>Se connecter dans l&apos;onglet/mosaïque...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1028"/>
        <source>Connect to a host in a new tab &lt;Alt+B&gt;</source>
        <translation>Se connecter à un hôte dans un nouvel onglet &lt;Alt+B&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1030"/>
        <source>Connect Local Shell</source>
        <translation>Se connecter à un shell local</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1032"/>
        <source>Connect to a local shell &lt;Alt+T&gt;</source>
        <translation>Se connecter à un shell local &lt;Alt+T&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1034"/>
        <source>Reconnect</source>
        <translation>Se reconnecter</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1036"/>
        <source>Reconnect current session</source>
        <translation>Reconnecter la session en cours</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1037"/>
        <source>Reconnect All</source>
        <translation>Reconnecter tout</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1038"/>
        <source>Reconnect all sessions &lt;Alt+A&gt;</source>
        <translation>Reconnecter toutes les sessions &lt;Alt+A&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1040"/>
        <source>Disconnect</source>
        <translation>Déconnecter</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1042"/>
        <source>Disconnect current session</source>
        <translation>Déconnecter la session en cours</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1043"/>
        <location filename="../src/mainwindow.cpp" line="1044"/>
        <source>Enter host &lt;Alt+R&gt; to connect</source>
        <translation>Entrer l&apos;hôte &lt;Alt+R&gt; pour se connecter</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1045"/>
        <source>Disconnect All</source>
        <translation>Déconnecter tout</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1046"/>
        <source>Disconnect all sessions</source>
        <translation>Déconnecter toutes les sessions</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1047"/>
        <source>Clone Session</source>
        <translation>Cloner la session</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1048"/>
        <source>Clone current session &lt;Ctrl+Shift+T&gt;</source>
        <translation>Cloner la session en cours &lt;Ctrl+Shift+T&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1050"/>
        <source>Lock Session</source>
        <translation>Verrouiller la session</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1051"/>
        <source>Lock/Unlock current session</source>
        <translation>Verrouiller/Déverrouiller la session en cours</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1052"/>
        <source>Log Session</source>
        <translation>Journaliser la session</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1053"/>
        <source>Create a log file for current session</source>
        <translation>Créer un fichier journal pour la session en cours</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1054"/>
        <source>Raw Log Session</source>
        <translation>Journal brut de la session</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1055"/>
        <source>Create a raw log file for current session</source>
        <translation>Créer un fichier journal brut pour la session en cours</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1056"/>
        <source>Hex View</source>
        <translation>Vue hexadécimale</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1057"/>
        <source>Show/Hide Hex View for current session</source>
        <translation>Afficher/Masquer la vue hexadécimale pour la session en cours</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1058"/>
        <source>Exit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1059"/>
        <source>Quit the application</source>
        <translation>Quitter l&apos;application</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1061"/>
        <source>Copy</source>
        <translation>Copier</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1064"/>
        <source>Copy the selected text to the clipboard &lt;Command+C&gt;</source>
        <translation>Copier le texte sélectionné dans le presse-papiers &lt;Command+C&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1067"/>
        <source>Copy the selected text to the clipboard &lt;Ctrl+Ins&gt;</source>
        <translation>Copier le texte sélectionné dans le presse-papiers &lt;Ctrl+Ins&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1070"/>
        <source>Paste</source>
        <translation>Coller</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1073"/>
        <source>Paste the clipboard text to the current session &lt;Command+V&gt;</source>
        <translation>Coller le texte du presse-papiers dans la session en cours &lt;Command+V&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1076"/>
        <source>Paste the clipboard text to the current session &lt;Shift+Ins&gt;</source>
        <translation>Coller le texte du presse-papiers dans la session en cours &lt;Shift+Ins&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1079"/>
        <source>Copy and Paste</source>
        <translation>Copier et coller</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1080"/>
        <source>Copy the selected text to the clipboard and paste to the current session</source>
        <translation>Copier le texte sélectionné dans le presse-papiers et coller dans la session en cours</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1081"/>
        <source>Select All</source>
        <translation>Tout sélectionner</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1083"/>
        <source>Select all text in the current session &lt;Ctrl+Shift+A&gt;</source>
        <translation>Sélectionner tout le texte dans la session en cours &lt;Ctrl+Shift+A&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1085"/>
        <source>Find...</source>
        <translation>Rechercher...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1087"/>
        <source>Find text in the current session &lt;Ctrl+F&gt;</source>
        <translation>Rechercher du texte dans la session en cours &lt;Ctrl+F&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1089"/>
        <source>Print Screen</source>
        <translation>Imprimer l&apos;écran</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1091"/>
        <source>Print current screen</source>
        <translation>Imprimer l&apos;écran en cours</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1092"/>
        <source>Screen Shot</source>
        <translation>Capture d&apos;écran</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1094"/>
        <source>Screen shot current screen &lt;Alt+P&gt;</source>
        <translation>Capture d&apos;écran de l&apos;écran en cours &lt;Alt+P&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1096"/>
        <source>Session Export</source>
        <translation>Exportation de la session</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1098"/>
        <source>Export current session to a file &lt;Alt+O&gt;</source>
        <translation>Exporter la session en cours vers un fichier &lt;Alt+O&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1100"/>
        <source>Clear Scrollback</source>
        <translation>Effacer le défilement</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1101"/>
        <source>Clear the contents of the scrollback rows</source>
        <translation>Effacer le contenu des lignes de défilement</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1102"/>
        <source>Clear Screen</source>
        <translation>Effacer l&apos;écran</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1103"/>
        <source>Clear the contents of the current screen</source>
        <translation>Effacer le contenu de l&apos;écran en cours</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1104"/>
        <source>Clear Screen and Scrollback</source>
        <translation>Effacer l&apos;écran et le défilement</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1105"/>
        <source>Clear the contents of the screen and scrollback</source>
        <translation>Effacer le contenu de l&apos;écran et du défilement</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1106"/>
        <source>Reset</source>
        <translation>Réinitialiser</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1107"/>
        <source>Reset terminal emulator</source>
        <translation>Réinitialiser l&apos;émulateur de terminal</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1109"/>
        <source>Zoom In</source>
        <translation>Agrandir</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1111"/>
        <source>Zoom In &lt;Ctrl+&quot;=&quot;&gt;</source>
        <translation>Agrandir &lt;Ctrl+&quot;=&quot;&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1113"/>
        <source>Zoom Out</source>
        <translation>Réduire</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1115"/>
        <source>Zoom Out &lt;Ctrl+&quot;-&quot;&gt;</source>
        <translation>Réduire &lt;Ctrl+&quot;-&quot;&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1117"/>
        <location filename="../src/mainwindow.cpp" line="1119"/>
        <source>Zoom Reset</source>
        <translation>Réinitialiser le zoom</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1120"/>
        <source>Menu Bar</source>
        <translation>Barre de menu</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1121"/>
        <source>Show/Hide Menu Bar &lt;Alt+U&gt;</source>
        <translation>Afficher/Masquer la barre de menu &lt;Alt+U&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1124"/>
        <source>Show/Hide Tool Bar</source>
        <translation>Afficher/Masquer la barre d&apos;outils</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1125"/>
        <source>Status Bar</source>
        <translation>Barre d&apos;état</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1126"/>
        <source>Show/Hide Status Bar</source>
        <translation>Afficher/Masquer la barre d&apos;état</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1127"/>
        <source>Command Window</source>
        <translation>Fenêtre de commande</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1128"/>
        <source>Show/Hide Command Window</source>
        <translation>Afficher/Masquer la fenêtre de commande</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1129"/>
        <source>Connect Bar</source>
        <translation>Barre de connexion</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1130"/>
        <source>Show/Hide Connect Bar</source>
        <translation>Afficher/Masquer la barre de connexion</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1131"/>
        <source>Side Window</source>
        <translation>Fenêtre latérale</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1132"/>
        <source>Show/Hide Side Window</source>
        <translation>Afficher/Masquer la fenêtre latérale</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1133"/>
        <source>Windows Transparency</source>
        <translation>Transparence des fenêtres</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1134"/>
        <source>Enable/Disable alpha transparency</source>
        <translation>Activer/Désactiver la transparence alpha</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1135"/>
        <source>Vertical Scroll Bar</source>
        <translation>Barre de défilement verticale</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1136"/>
        <source>Show/Hide Vertical Scroll Bar</source>
        <translation>Afficher/Masquer la barre de défilement verticale</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1137"/>
        <source>Allways On Top</source>
        <translation>Toujours au premier plan</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1138"/>
        <source>Show window always on top</source>
        <translation>Afficher la fenêtre toujours au premier plan</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1139"/>
        <source>Full Screen</source>
        <translation>Plein écran</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1140"/>
        <source>Toggle between full screen and normal mode &lt;Alt+Enter&gt;</source>
        <translation>Basculer entre le mode plein écran et le mode normal &lt;Alt+Entrée&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1143"/>
        <source>Session Options...</source>
        <translation>Options de la session...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1145"/>
        <source>Configure session options</source>
        <translation>Configurer les options de la session</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1146"/>
        <source>Global Options...</source>
        <translation>Options globales...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1148"/>
        <source>Configure global options</source>
        <translation>Configurer les options globales</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1149"/>
        <source>Real-time Save Options</source>
        <translation>Options d&apos;enregistrement en temps réel</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1150"/>
        <source>Real-time save session options and global options</source>
        <translation>Enregistrer en temps réel les options de session et les options globales</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1151"/>
        <source>Save Settings Now</source>
        <translation>Enregistrer les paramètres maintenant</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1152"/>
        <source>Save options configuration now</source>
        <translation>Enregistrer la configuration des options maintenant</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1154"/>
        <source>Send ASCII...</source>
        <translation>Envoyer ASCII...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1155"/>
        <source>Send ASCII file</source>
        <translation>Envoyer un fichier ASCII</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1156"/>
        <source>Receive ASCII...</source>
        <translation>Recevoir ASCII...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1157"/>
        <source>Receive ASCII file</source>
        <translation>Recevoir un fichier ASCII</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1158"/>
        <source>Send Binary...</source>
        <translation>Envoyer binaire...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1159"/>
        <source>Send Binary file</source>
        <translation>Envoyer un fichier binaire</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1160"/>
        <source>Send Xmodem...</source>
        <translation>Envoyer Xmodem...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1161"/>
        <source>Send a file using Xmodem</source>
        <translation>Envoyer un fichier en utilisant Xmodem</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1162"/>
        <source>Receive Xmodem...</source>
        <translation>Recevoir Xmodem...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1163"/>
        <source>Receive a file using Xmodem</source>
        <translation>Recevoir un fichier en utilisant Xmodem</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1164"/>
        <source>Send Ymodem...</source>
        <translation>Envoyer Ymodem...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1165"/>
        <source>Send a file using Ymodem</source>
        <translation>Envoyer un fichier en utilisant Ymodem</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1166"/>
        <source>Receive Ymodem...</source>
        <translation>Recevoir Ymodem...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1167"/>
        <source>Receive a file using Ymodem</source>
        <translation>Recevoir un fichier en utilisant Ymodem</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1168"/>
        <source>Zmodem Upload List...</source>
        <translation>Liste de téléchargement Zmodem...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1169"/>
        <source>Display Zmodem file upload list</source>
        <translation>Afficher la liste de téléchargement de fichiers Zmodem</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1170"/>
        <source>Start Zmodem Upload</source>
        <translation>Démarrer le téléchargement Zmodem</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1171"/>
        <source>Start Zmodem file upload</source>
        <translation>Démarrer le téléchargement de fichiers Zmodem</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1172"/>
        <source>Start TFTP Server</source>
        <translation>Démarrer le serveur TFTP</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1173"/>
        <source>Start/Stop the TFTP server</source>
        <translation>Démarrer/Arrêter le serveur TFTP</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1175"/>
        <source>Run...</source>
        <translation>Exécuter...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1176"/>
        <source>Run a script</source>
        <translation>Exécuter un script</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1177"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1178"/>
        <source>Cancel script execution</source>
        <translation>Annuler l&apos;exécution du script</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1179"/>
        <source>Start Recording Script</source>
        <translation>Commencer l&apos;enregistrement du script</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1180"/>
        <source>Start recording script</source>
        <translation>Commencer l&apos;enregistrement du script</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1181"/>
        <source>Stop Recording Script...</source>
        <translation>Arrêter l&apos;enregistrement du script...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1182"/>
        <source>Stop recording script</source>
        <translation>Arrêter l&apos;enregistrement du script</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1183"/>
        <source>Cancel Recording Script</source>
        <translation>Annuler l&apos;enregistrement du script</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1184"/>
        <source>Cancel recording script</source>
        <translation>Annuler l&apos;enregistrement du script</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1186"/>
        <source>Add Bookmark</source>
        <translation>Ajouter un marque-page</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1187"/>
        <source>Add a bookmark</source>
        <translation>Ajouter un marque-page</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1188"/>
        <location filename="../src/mainwindow.cpp" line="2350"/>
        <source>Remove Bookmark</source>
        <translation>Supprimer un marque-page</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1189"/>
        <source>Remove a bookmark</source>
        <translation>Supprimer un marque-page</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1190"/>
        <location filename="../src/mainwindow.cpp" line="2377"/>
        <source>Clean All Bookmark</source>
        <translation>Nettoyer tous les marque-pages</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1191"/>
        <source>Clean all bookmark</source>
        <translation>Nettoyer tous les marque-pages</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1193"/>
        <source>Keymap Manager</source>
        <translation>Gestionnaire de mappage des touches</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1194"/>
        <source>Display keymap editor</source>
        <translation>Afficher l&apos;éditeur de mappage des touches</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1195"/>
        <source>Create Public Key...</source>
        <translation>Créer une clé publique...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1196"/>
        <source>Create a public key</source>
        <translation>Créer une clé publique</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1197"/>
        <source>Publickey Manager</source>
        <translation>Gestionnaire de clés publiques</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1198"/>
        <source>Display publickey manager</source>
        <translation>Afficher le gestionnaire de clés publiques</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1242"/>
        <source>Laboratory</source>
        <translation>Laboratoire</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1245"/>
        <location filename="../src/mainwindow.cpp" line="2395"/>
        <source>SSH Scanning</source>
        <translation>Exploration SSH</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1246"/>
        <source>Display SSH scanning dialog</source>
        <translation>Afficher la boîte de dialogue d&apos;exploration SSH</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3399"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3401"/>
        <source>Commit</source>
        <translation>Validation</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3403"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3405"/>
        <source>Author</source>
        <translation>Auteur</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3407"/>
        <source>Website</source>
        <translation>Site web</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1200"/>
        <source>Tab</source>
        <translation>Onglet</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1201"/>
        <source>Arrange sessions in tabs</source>
        <translation>Organiser les sessions dans des onglets</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1202"/>
        <source>Tile</source>
        <translation>Mosaïque</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1203"/>
        <source>Arrange sessions in non-overlapping tiles</source>
        <translation>Organiser les sessions dans des mosaïques non superposées</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1204"/>
        <source>Cascade</source>
        <translation>Cascade</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1205"/>
        <source>Arrange sessions to overlap each other</source>
        <translation>Organiser les sessions pour qu&apos;elles se chevauchent</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1207"/>
        <source>Simplified Chinese</source>
        <translation>简体中文</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1208"/>
        <source>Switch to Simplified Chinese</source>
        <translation>切换到简体中文</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1209"/>
        <source>Traditional Chinese</source>
        <translation>繁體中文</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1210"/>
        <source>Switch to Traditional Chinese</source>
        <translation>切換到繁體中文</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1211"/>
        <source>Russian</source>
        <translation>Русский</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1212"/>
        <source>Switch to Russian</source>
        <translation>Переключиться на русский</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1213"/>
        <source>Korean</source>
        <translation>한국어</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1214"/>
        <source>Switch to Korean</source>
        <translation>한국어로 전환</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1215"/>
        <source>Japanese</source>
        <translation>日本語</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1216"/>
        <source>Switch to Japanese</source>
        <translation>日本語に切り替える</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1217"/>
        <source>French</source>
        <translation>français</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1218"/>
        <source>Switch to French</source>
        <translation>Passer au français</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1219"/>
        <source>Spanish</source>
        <translation>español</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1220"/>
        <source>Switch to Spanish</source>
        <translation>Cambiar a español</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1221"/>
        <source>English</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1222"/>
        <source>Switch to English</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1224"/>
        <source>Light</source>
        <translation>Clair</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1225"/>
        <source>Switch to light theme</source>
        <translation>Passer au thème clair</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1226"/>
        <source>Dark</source>
        <translation>Sombre</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1227"/>
        <source>Switch to dark theme</source>
        <translation>Passer au thème sombre</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1231"/>
        <source>Display help</source>
        <translation>Afficher l&apos;aide</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1232"/>
        <source>Check Update</source>
        <translation>Vérifier les mises à jour</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1234"/>
        <source>Check for updates</source>
        <translation>Vérifier les mises à jour</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1235"/>
        <location filename="../src/mainwindow.cpp" line="3397"/>
        <source>About</source>
        <translation>À propos de</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1237"/>
        <source>Display about dialog</source>
        <translation>Afficher la boîte de dialogue À propos de</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1238"/>
        <source>About Qt</source>
        <translation>À propos de Qt</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1240"/>
        <source>Display about Qt dialog</source>
        <translation>Afficher la boîte de dialogue À propos de Qt</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1247"/>
        <source>Plugin Info</source>
        <translation>Informations sur le plugin</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1248"/>
        <source>Display plugin information dialog</source>
        <translation>Afficher la boîte de dialogue d&apos;informations sur le plugin</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2134"/>
        <source>PrintScreen saved to %1</source>
        <translation>PrintScreen enregistré dans %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2146"/>
        <source>Save Screenshot</source>
        <translation>Enregistrer la capture d&apos;écran</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2146"/>
        <source>Image Files (*.jpg)</source>
        <translation>Fichiers image (*.jpg)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2151"/>
        <source>Screenshot saved to %1</source>
        <translation>Capture d&apos;écran enregistrée dans %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2162"/>
        <source>Save Session Export</source>
        <translation>Enregistrer l&apos;exportation de la session</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2162"/>
        <source>Text Files (*.txt);;HTML Files (*.html)</source>
        <translation>Fichiers texte (*.txt);;Fichiers HTML (*.html)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2165"/>
        <source>Text Files (*.txt)</source>
        <translation>Fichiers texte (*.txt)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2168"/>
        <source>HTML Files (*.html)</source>
        <translation>Fichiers HTML (*.html)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2177"/>
        <source>Session Export saved to %1</source>
        <translation>Exportation de la session enregistrée dans %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2179"/>
        <source>Session Export failed to save to %1</source>
        <translation>Échec de l&apos;enregistrement de l&apos;exportation de la session dans %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2323"/>
        <source>Select a directory</source>
        <translation>Sélectionner un répertoire</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2350"/>
        <source>Select a bookmark</source>
        <translation>Sélectionner un marque-page</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2377"/>
        <source>Are you sure to clean all bookmark?</source>
        <translation>Êtes-vous sûr de vouloir nettoyer tous les marque-pages ?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2395"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2552"/>
        <source>Video background is enabled, please enable animation in global options (more system resources) or change background image.</source>
        <translation>L&apos;arrière-plan vidéo est activé, veuillez activer l&apos;animation dans les options globales (plus de ressources système) ou changer l&apos;image d&apos;arrière-plan.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2747"/>
        <source>Session information get failed.</source>
        <translation>Échec de l&apos;obtention des informations de session.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2830"/>
        <source>Telnet - </source>
        <translation>Telnet - </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2831"/>
        <source>Telnet</source>
        <translation>Telnet</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2863"/>
        <source>Serial - </source>
        <translation>Série - </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2864"/>
        <source>Serial</source>
        <translation>Série</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2895"/>
        <source>Raw - </source>
        <translation>Brut - </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2896"/>
        <source>Raw</source>
        <translation>Brut</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2927"/>
        <source>NamePipe - </source>
        <translation>Tube nommé - </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2928"/>
        <source>NamePipe</source>
        <translation>Tube nommé</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2996"/>
        <location filename="../src/mainwindow.cpp" line="3000"/>
        <source>Local Shell</source>
        <translation>Shell local</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2998"/>
        <source>Local Shell - </source>
        <translation>Shell local - </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3115"/>
        <source>Are you sure to disconnect this session?</source>
        <translation>Êtes-vous sûr de vouloir déconnecter cette session ?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3417"/>
        <source>Global Shortcuts:</source>
        <translation>Raccourcis globaux :</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3418"/>
        <source>show/hide menu bar</source>
        <translation>Afficher/Masquer la barre de menu</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3419"/>
        <source>connect to LocalShell</source>
        <translation>Se connecter à LocalShell</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3420"/>
        <source>clone current session</source>
        <translation>Cloner la session actuelle</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3421"/>
        <source>switch ui to STD mode</source>
        <translation>Passer l&apos;interface utilisateur en mode STD</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3422"/>
        <source>switch ui to MINI mode</source>
        <translation>Passer l&apos;interface utilisateur en mode MINI</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3423"/>
        <source>switch to previous session</source>
        <translation>Passer à la session précédente</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3424"/>
        <source>switch to next session</source>
        <translation>Passer à la session suivante</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3425"/>
        <source>switch to session [num]</source>
        <translation>Passer à la session [num]</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3426"/>
        <source>Go to line start</source>
        <translation>Aller au début de la ligne</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3427"/>
        <source>Go to line end</source>
        <translation>Aller à la fin de la ligne</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3451"/>
        <source>There are sessions that have not yet been unlocked, please unlock them first.</source>
        <translation>Il y a des sessions qui n&apos;ont pas encore été déverrouillées, veuillez les déverrouiller d&apos;abord.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3456"/>
        <source>Are you sure to quit?</source>
        <translation>Êtes-vous sûr de vouloir quitter ?</translation>
    </message>
</context>
<context>
    <name>CommandWidget</name>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="35"/>
        <source>Send commands to active session</source>
        <translation>Envoyer des commandes à la session active</translation>
    </message>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="71"/>
        <source>ASCII</source>
        <translation>ASCII</translation>
    </message>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="84"/>
        <source>HEX</source>
        <translation>HEX</translation>
    </message>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="104"/>
        <source>time</source>
        <translation>temps</translation>
    </message>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="117"/>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="135"/>
        <source>Auto Send</source>
        <translation>Envoi automatique</translation>
    </message>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="142"/>
        <source>Send</source>
        <translation>Envoyer</translation>
    </message>
</context>
<context>
    <name>EmptyTabWidget</name>
    <message>
        <location filename="../src/sessiontab/sessiontab.cpp" line="69"/>
        <source>No session</source>
        <translation>Aucune session</translation>
    </message>
</context>
<context>
    <name>GlobalOptionsAdvancedWidget</name>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="25"/>
        <source>Config File</source>
        <translation>Fichier de configuration</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="56"/>
        <source>Translate Service</source>
        <translation>Service de traduction</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="67"/>
        <source>Google Translate</source>
        <translation>Google Translate</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="72"/>
        <source>Baidu Translate</source>
        <translation>Baidu Translate</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="77"/>
        <source>Microsoft Translate</source>
        <translation>Microsoft Translate</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="87"/>
        <source>Terminal background support animation</source>
        <translation>Le terminal prend en charge l&apos;animation d&apos;arrière-plan</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="94"/>
        <source>NativeUI(Effective after restart)</source>
        <translation>Interface utilisateur native (efficace après le redémarrage)</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="101"/>
        <source>Github Copilot</source>
        <translation>Github Copilot</translation>
    </message>
</context>
<context>
    <name>GlobalOptionsAppearanceWidget</name>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="17"/>
        <source>Color Schemes</source>
        <translation>Thèmes de couleur</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="27"/>
        <source>Font</source>
        <translation>Police de caractère</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="45"/>
        <source>Series</source>
        <translation>Série</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="65"/>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="93"/>
        <source>Background image</source>
        <translation>Image d&apos;arrière-plan</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="115"/>
        <source>Clear</source>
        <translation>Effacer</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="124"/>
        <source>Background mode</source>
        <translation>Mode d&apos;arrière-plan</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="131"/>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="143"/>
        <source>Stretch</source>
        <translation>Étirer</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="138"/>
        <source>None</source>
        <translation>Aucun</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="148"/>
        <source>Zoom</source>
        <translation>Zoomer</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="153"/>
        <source>Fit</source>
        <translation>Ajuster</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="158"/>
        <source>Center</source>
        <translation>Centrer</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="163"/>
        <source>Tile</source>
        <translation>Mosaïque</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="171"/>
        <source>Background opacity</source>
        <translation>Opacité de l&apos;arrière-plan</translation>
    </message>
</context>
<context>
    <name>GlobalOptionsGeneralWidget</name>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="19"/>
        <source>New tab mode</source>
        <translation>Mode nouvel onglet</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="33"/>
        <source>New session</source>
        <translation>Nouvelle session</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="38"/>
        <source>Clone session</source>
        <translation>Cloner la session</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="43"/>
        <source>LocalShell session</source>
        <translation>Session LocalShell</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="55"/>
        <source>New tab workpath</source>
        <translation>Nouveau chemin de travail d&apos;onglet</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="84"/>
        <source>Tab title mode</source>
        <translation>Mode de titre d&apos;onglet</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="98"/>
        <source>Brief</source>
        <translation>Bref</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="103"/>
        <source>Full</source>
        <translation>Complet</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="108"/>
        <source>Scroll</source>
        <translation>Défiler</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="126"/>
        <source>Tab Title Width</source>
        <translation>Largeur du titre de l&apos;onglet</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="155"/>
        <source>Tab Preview</source>
        <translation>Aperçu de l&apos;onglet</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="171"/>
        <source>Preview Width</source>
        <translation>Largeur de l&apos;aperçu</translation>
    </message>
</context>
<context>
    <name>GlobalOptionsTerminalWidget</name>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="17"/>
        <source>Scrollback lines</source>
        <translation>Lignes de défilement</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="34"/>
        <source>Cursor Shape</source>
        <translation>Forme du curseur</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="42"/>
        <source>Block</source>
        <translation>Bloc</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="47"/>
        <source>Underline</source>
        <translation>Souligner</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="52"/>
        <source>IBeam</source>
        <translation>IBeam</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="60"/>
        <source>Cursor Blink</source>
        <translation>Clignotement du curseur</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="70"/>
        <source>Word Characters</source>
        <translation>Caractères de mot</translation>
    </message>
</context>
<context>
    <name>GlobalOptionsWindow</name>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.ui" line="14"/>
        <source>Global Options</source>
        <translation>Options globales</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="164"/>
        <source>Select Background Image</source>
        <translation>Sélectionner une image d&apos;arrière-plan</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="164"/>
        <source>Image Files (*.png *.jpg *.jpeg *.bmp *.gif);;Video Files (*.mp4 *.avi *.mkv *.mov)</source>
        <translation>Fichiers image (*.png *.jpg *.jpeg *.bmp *.gif);;Fichiers vidéo (*.mp4 *.avi *.mkv *.mov)</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="202"/>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="207"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="202"/>
        <source>This feature needs more system resources, please use it carefully!</source>
        <translation>Cette fonctionnalité nécessite plus de ressources système, veuillez l&apos;utiliser avec précaution !</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="207"/>
        <source>This feature is not implemented yet!</source>
        <translation>Cette fonctionnalité n&apos;est pas encore implémentée !</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="229"/>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="229"/>
        <source>Appearance</source>
        <translation>Apparence</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="229"/>
        <source>Terminal</source>
        <translation>Terminal</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="229"/>
        <source>Window</source>
        <translation>Fenêtre</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="229"/>
        <source>Advanced</source>
        <translation>Avancé</translation>
    </message>
</context>
<context>
    <name>GlobalOptionsWindowWidget</name>
    <message>
        <location filename="../src/globaloptions/globaloptionswindowwidget.ui" line="17"/>
        <source>Transparent window</source>
        <translation>Fenêtre transparente</translation>
    </message>
</context>
<context>
    <name>HexViewWindow</name>
    <message>
        <source>ASCII Text...</source>
        <translation type="vanished">Texte ASCII...</translation>
    </message>
    <message>
        <source>clear</source>
        <translation type="vanished">effacer</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.ui" line="20"/>
        <source>Hex View</source>
        <translation>Vue hexadécimale</translation>
    </message>
    <message>
        <source>Information</source>
        <translation type="vanished">Information</translation>
    </message>
    <message>
        <source>Will send Hex:
</source>
        <translation type="vanished">Enverra Hex :
</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="71"/>
        <source>Copy</source>
        <translation>Copier</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="79"/>
        <source>Copy Hex</source>
        <translation>Copier Hex</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="87"/>
        <source>Dump</source>
        <translation>Déverser</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="91"/>
        <source>Save As</source>
        <translation>Enregistrer sous</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="91"/>
        <source>Binary File (*.bin)</source>
        <translation>Fichier binaire (*.bin)</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="99"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="99"/>
        <source>Failed to save file!</source>
        <translation>Échec de l&apos;enregistrement du fichier !</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="104"/>
        <source>Clear</source>
        <translation>Effacer</translation>
    </message>
</context>
<context>
    <name>Konsole::Session</name>
    <message>
        <location filename="../lib/qtermwidget/Session.cpp" line="317"/>
        <source>Bell in session &apos;%1&apos;</source>
        <translation>Sonnerie dans la session &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>Konsole::TerminalDisplay</name>
    <message>
        <location filename="../lib/qtermwidget/TerminalDisplay.cpp" line="1383"/>
        <source>Size: XXX x XXX</source>
        <translation>Taille : XXX x XXX</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/TerminalDisplay.cpp" line="1395"/>
        <source>Size: %1 x %2</source>
        <translation>Taille : %1 x %2</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/TerminalDisplay.cpp" line="2942"/>
        <source>Paste multiline text</source>
        <translation>Coller du texte multiligne</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/TerminalDisplay.cpp" line="2943"/>
        <source>Are you sure you want to paste this text?</source>
        <translation>Êtes-vous sûr de vouloir coller ce texte ?</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/TerminalDisplay.cpp" line="3426"/>
        <source>&lt;qt&gt;Output has been &lt;a href=&quot;http://en.wikipedia.org/wiki/Flow_control&quot;&gt;suspended&lt;/a&gt; by pressing Ctrl+S.  Press &lt;b&gt;Ctrl+Q&lt;/b&gt; to resume.&lt;/qt&gt;</source>
        <translation>&lt;qt&gt;La sortie a été &lt;a href=&quot;http://en.wikipedia.org/wiki/Flow_control&quot;&gt;suspendue&lt;/a&gt; en appuyant sur Ctrl+S. Appuyez sur &lt;b&gt;Ctrl+Q&lt;/b&gt; pour reprendre.&lt;/qt&gt;</translation>
    </message>
</context>
<context>
    <name>Konsole::Vt102Emulation</name>
    <message>
        <location filename="../lib/qtermwidget/Vt102Emulation.cpp" line="1113"/>
        <source>No keyboard translator available.  The information needed to convert key presses into characters to send to the terminal is missing.</source>
        <translation>Aucun traducteur de clavier disponible. Les informations nécessaires à la conversion des frappes de touches en caractères à envoyer au terminal sont manquantes.</translation>
    </message>
</context>
<context>
    <name>LockSessionWindow</name>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.ui" line="14"/>
        <source>Lock Session</source>
        <translation>Verrouiller la session</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.ui" line="20"/>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="84"/>
        <source>Enter the password that will be used to unlock the session:</source>
        <translation>Entrez le mot de passe qui sera utilisé pour déverrouiller la session :</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.ui" line="32"/>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.ui" line="50"/>
        <source>Reenter Password</source>
        <translation>Réentrez le mot de passe</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.ui" line="68"/>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="85"/>
        <source>Lock all sessions</source>
        <translation>Verrouiller toutes les sessions</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.ui" line="75"/>
        <source>Lock all sessions in tab group</source>
        <translation>Verrouiller toutes les sessions dans le groupe d&apos;onglets</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="49"/>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="55"/>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="66"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="49"/>
        <source>Passwords do not match!</source>
        <translation>Les mots de passe ne correspondent pas !</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="55"/>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="66"/>
        <source>Password cannot be empty!</source>
        <translation>Le mot de passe ne peut pas être vide !</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="98"/>
        <source>Enter the password that was used to lock the session:</source>
        <translation>Entrez le mot de passe qui a été utilisé pour verrouiller la session :</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="99"/>
        <source>Unlock all sessions</source>
        <translation>Déverrouiller toutes les sessions</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Tool Bar</source>
        <translation type="vanished">Barre d&apos;outils</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="vanished">Attention</translation>
    </message>
    <message>
        <source>TFTP server bind error!</source>
        <translation type="vanished">Erreur de liaison du serveur TFTP !</translation>
    </message>
    <message>
        <source>TFTP server file error!</source>
        <translation type="vanished">Erreur de fichier du serveur TFTP !</translation>
    </message>
    <message>
        <source>TFTP server network error!</source>
        <translation type="vanished">Erreur réseau du serveur TFTP !</translation>
    </message>
    <message>
        <source>Unlock Session</source>
        <translation type="vanished">Déverrouiller la session</translation>
    </message>
    <message>
        <source>Move to another Tab</source>
        <translation type="vanished">Déplacer vers un autre onglet</translation>
    </message>
    <message>
        <source>Floating Window</source>
        <translation type="vanished">Fenêtre flottante</translation>
    </message>
    <message>
        <source>Copy Path</source>
        <translation type="vanished">Copier le chemin</translation>
    </message>
    <message>
        <source>No working folder!</source>
        <translation type="vanished">Pas de dossier de travail !</translation>
    </message>
    <message>
        <source>Add Path to Bookmark</source>
        <translation type="vanished">Ajouter le chemin au marque-page</translation>
    </message>
    <message>
        <source>Open Working Folder</source>
        <translation type="vanished">Ouvrir le dossier de travail</translation>
    </message>
    <message>
        <source>Save Session</source>
        <translation type="vanished">Enregistrer la session</translation>
    </message>
    <message>
        <source>Enter Session Name</source>
        <translation type="vanished">Entrer le nom de la session</translation>
    </message>
    <message>
        <source>The session already exists, please rename the new session or cancel saving.</source>
        <translation type="vanished">La session existe déjà, veuillez renommer la nouvelle session ou annuler l&apos;enregistrement.</translation>
    </message>
    <message>
        <source>Properties</source>
        <translation type="vanished">Propriétés</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Fermer</translation>
    </message>
    <message>
        <source>Close Others</source>
        <translation type="vanished">Fermer les autres</translation>
    </message>
    <message>
        <source>Close to the Right</source>
        <translation type="vanished">Fermer à droite</translation>
    </message>
    <message>
        <source>Close All</source>
        <translation type="vanished">Tout fermer</translation>
    </message>
    <message>
        <source>Highlight/Unhighlight</source>
        <translation type="vanished">Surligner/Désurligner</translation>
    </message>
    <message>
        <source>Ready</source>
        <translation type="vanished">Prêt</translation>
    </message>
    <message>
        <source>Open SFTP</source>
        <translation type="vanished">Ouvrir SFTP</translation>
    </message>
    <message>
        <source>No SFTP channel!</source>
        <translation type="vanished">Aucun canal SFTP !</translation>
    </message>
    <message>
        <source>Session properties error!</source>
        <translation type="vanished">Erreur des propriétés de la session !</translation>
    </message>
    <message>
        <source>Google Translate</source>
        <translation type="vanished">Google Translate</translation>
    </message>
    <message>
        <source>Baidu Translate</source>
        <translation type="vanished">Baidu Translate</translation>
    </message>
    <message>
        <source>Microsoft Translate</source>
        <translation type="vanished">Microsoft Translate</translation>
    </message>
    <message>
        <source>Back to Main Window</source>
        <translation type="vanished">Retour à la fenêtre principale</translation>
    </message>
    <message>
        <source>Session Manager</source>
        <translation type="vanished">Gestionnaire de sessions</translation>
    </message>
    <message>
        <source>File</source>
        <translation type="vanished">Fichier</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="vanished">Édition</translation>
    </message>
    <message>
        <source>View</source>
        <translation type="vanished">Affichage</translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="vanished">Options</translation>
    </message>
    <message>
        <source>Transfer</source>
        <translation type="vanished">Transfert</translation>
    </message>
    <message>
        <source>Script</source>
        <translation type="vanished">Script</translation>
    </message>
    <message>
        <source>Bookmark</source>
        <translation type="vanished">Marque-page</translation>
    </message>
    <message>
        <source>Tools</source>
        <translation type="vanished">Outils</translation>
    </message>
    <message>
        <source>Window</source>
        <translation type="vanished">Fenêtre</translation>
    </message>
    <message>
        <source>Language</source>
        <translation type="vanished">Langue</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation type="vanished">Thème</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">Aide</translation>
    </message>
    <message>
        <source>New Window</source>
        <translation type="vanished">Nouvelle fenêtre</translation>
    </message>
    <message>
        <source>Open a new window &lt;Ctrl+Shift+N&gt;</source>
        <translation type="vanished">Ouvrir une nouvelle fenêtre &lt;Ctrl+Shift+N&gt;</translation>
    </message>
    <message>
        <source>Connect...</source>
        <translation type="vanished">Se connecter...</translation>
    </message>
    <message>
        <source>Connect to a host &lt;Alt+C&gt;</source>
        <translation type="vanished">Se connecter à un hôte &lt;Alt+C&gt;</translation>
    </message>
    <message>
        <source>Go to the Session Manager &lt;Alt+M&gt;</source>
        <translation type="vanished">Aller au gestionnaire de sessions &lt;Alt+M&gt;</translation>
    </message>
    <message>
        <source>Quick Connect...</source>
        <translation type="vanished">Connexion rapide...</translation>
    </message>
    <message>
        <source>Quick Connect to a host &lt;Alt+Q&gt;</source>
        <translation type="vanished">Connexion rapide à un hôte &lt;Alt+Q&gt;</translation>
    </message>
    <message>
        <source>Connect in Tab/Tile...</source>
        <translation type="vanished">Se connecter dans l&apos;onglet/mosaïque...</translation>
    </message>
    <message>
        <source>Connect to a host in a new tab &lt;Alt+B&gt;</source>
        <translation type="vanished">Se connecter à un hôte dans un nouvel onglet &lt;Alt+B&gt;</translation>
    </message>
    <message>
        <source>Connect Local Shell</source>
        <translation type="vanished">Se connecter à un shell local</translation>
    </message>
    <message>
        <source>Connect to a local shell &lt;Alt+T&gt;</source>
        <translation type="vanished">Se connecter à un shell local &lt;Alt+T&gt;</translation>
    </message>
    <message>
        <source>Reconnect</source>
        <translation type="vanished">Se reconnecter</translation>
    </message>
    <message>
        <source>Reconnect current session</source>
        <translation type="vanished">Reconnecter la session en cours</translation>
    </message>
    <message>
        <source>Reconnect All</source>
        <translation type="vanished">Reconnecter tout</translation>
    </message>
    <message>
        <source>Reconnect all sessions &lt;Alt+A&gt;</source>
        <translation type="vanished">Reconnecter toutes les sessions &lt;Alt+A&gt;</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation type="vanished">Déconnecter</translation>
    </message>
    <message>
        <source>Disconnect current session</source>
        <translation type="vanished">Déconnecter la session en cours</translation>
    </message>
    <message>
        <source>Enter host &lt;Alt+R&gt; to connect</source>
        <translation type="vanished">Entrer l&apos;hôte &lt;Alt+R&gt; pour se connecter</translation>
    </message>
    <message>
        <source>Disconnect All</source>
        <translation type="vanished">Déconnecter tout</translation>
    </message>
    <message>
        <source>Disconnect all sessions</source>
        <translation type="vanished">Déconnecter toutes les sessions</translation>
    </message>
    <message>
        <source>Clone Session</source>
        <translation type="vanished">Cloner la session</translation>
    </message>
    <message>
        <source>Clone current session &lt;Ctrl+Shift+T&gt;</source>
        <translation type="vanished">Cloner la session en cours &lt;Ctrl+Shift+T&gt;</translation>
    </message>
    <message>
        <source>Lock Session</source>
        <translation type="vanished">Verrouiller la session</translation>
    </message>
    <message>
        <source>Lock/Unlock current session</source>
        <translation type="vanished">Verrouiller/Déverrouiller la session en cours</translation>
    </message>
    <message>
        <source>Log Session</source>
        <translation type="vanished">Journaliser la session</translation>
    </message>
    <message>
        <source>Create a log file for current session</source>
        <translation type="vanished">Créer un fichier journal pour la session en cours</translation>
    </message>
    <message>
        <source>Raw Log Session</source>
        <translation type="vanished">Journal brut de la session</translation>
    </message>
    <message>
        <source>Create a raw log file for current session</source>
        <translation type="vanished">Créer un fichier journal brut pour la session en cours</translation>
    </message>
    <message>
        <source>Hex View</source>
        <translation type="vanished">Vue hexadécimale</translation>
    </message>
    <message>
        <source>Show/Hide Hex View for current session</source>
        <translation type="vanished">Afficher/Masquer la vue hexadécimale pour la session en cours</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="vanished">Quitter</translation>
    </message>
    <message>
        <source>Quit the application</source>
        <translation type="vanished">Quitter l&apos;application</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation type="vanished">Copier</translation>
    </message>
    <message>
        <source>Copy the selected text to the clipboard &lt;Command+C&gt;</source>
        <translation type="vanished">Copier le texte sélectionné dans le presse-papiers &lt;Command+C&gt;</translation>
    </message>
    <message>
        <source>Copy the selected text to the clipboard &lt;Ctrl+Ins&gt;</source>
        <translation type="vanished">Copier le texte sélectionné dans le presse-papiers &lt;Ctrl+Ins&gt;</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation type="vanished">Coller</translation>
    </message>
    <message>
        <source>Paste the clipboard text to the current session &lt;Command+V&gt;</source>
        <translation type="vanished">Coller le texte du presse-papiers dans la session en cours &lt;Command+V&gt;</translation>
    </message>
    <message>
        <source>Paste the clipboard text to the current session &lt;Shift+Ins&gt;</source>
        <translation type="vanished">Coller le texte du presse-papiers dans la session en cours &lt;Shift+Ins&gt;</translation>
    </message>
    <message>
        <source>Copy and Paste</source>
        <translation type="vanished">Copier et coller</translation>
    </message>
    <message>
        <source>Copy the selected text to the clipboard and paste to the current session</source>
        <translation type="vanished">Copier le texte sélectionné dans le presse-papiers et coller dans la session en cours</translation>
    </message>
    <message>
        <source>Select All</source>
        <translation type="vanished">Tout sélectionner</translation>
    </message>
    <message>
        <source>Select all text in the current session &lt;Ctrl+Shift+A&gt;</source>
        <translation type="vanished">Sélectionner tout le texte dans la session en cours &lt;Ctrl+Shift+A&gt;</translation>
    </message>
    <message>
        <source>Find...</source>
        <translation type="vanished">Rechercher...</translation>
    </message>
    <message>
        <source>Find text in the current session &lt;Ctrl+F&gt;</source>
        <translation type="vanished">Rechercher du texte dans la session en cours &lt;Ctrl+F&gt;</translation>
    </message>
    <message>
        <source>Print Screen</source>
        <translation type="vanished">Imprimer l&apos;écran</translation>
    </message>
    <message>
        <source>Print current screen</source>
        <translation type="vanished">Imprimer l&apos;écran en cours</translation>
    </message>
    <message>
        <source>Screen Shot</source>
        <translation type="vanished">Capture d&apos;écran</translation>
    </message>
    <message>
        <source>Screen shot current screen &lt;Alt+P&gt;</source>
        <translation type="vanished">Capture d&apos;écran de l&apos;écran en cours &lt;Alt+P&gt;</translation>
    </message>
    <message>
        <source>Session Export</source>
        <translation type="vanished">Exportation de la session</translation>
    </message>
    <message>
        <source>Export current session to a file &lt;Alt+O&gt;</source>
        <translation type="vanished">Exporter la session en cours vers un fichier &lt;Alt+O&gt;</translation>
    </message>
    <message>
        <source>Clear Scrollback</source>
        <translation type="vanished">Effacer le défilement</translation>
    </message>
    <message>
        <source>Clear the contents of the scrollback rows</source>
        <translation type="vanished">Effacer le contenu des lignes de défilement</translation>
    </message>
    <message>
        <source>Clear Screen</source>
        <translation type="vanished">Effacer l&apos;écran</translation>
    </message>
    <message>
        <source>Clear the contents of the current screen</source>
        <translation type="vanished">Effacer le contenu de l&apos;écran en cours</translation>
    </message>
    <message>
        <source>Clear Screen and Scrollback</source>
        <translation type="vanished">Effacer l&apos;écran et le défilement</translation>
    </message>
    <message>
        <source>Clear the contents of the screen and scrollback</source>
        <translation type="vanished">Effacer le contenu de l&apos;écran et du défilement</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation type="vanished">Réinitialiser</translation>
    </message>
    <message>
        <source>Reset terminal emulator</source>
        <translation type="vanished">Réinitialiser l&apos;émulateur de terminal</translation>
    </message>
    <message>
        <source>Zoom In</source>
        <translation type="vanished">Agrandir</translation>
    </message>
    <message>
        <source>Zoom In &lt;Ctrl+&quot;=&quot;&gt;</source>
        <translation type="vanished">Agrandir &lt;Ctrl+&quot;=&quot;&gt;</translation>
    </message>
    <message>
        <source>Zoom Out</source>
        <translation type="vanished">Réduire</translation>
    </message>
    <message>
        <source>Zoom Out &lt;Ctrl+&quot;-&quot;&gt;</source>
        <translation type="vanished">Réduire &lt;Ctrl+&quot;-&quot;&gt;</translation>
    </message>
    <message>
        <source>Zoom Reset</source>
        <translation type="vanished">Réinitialiser le zoom</translation>
    </message>
    <message>
        <source>Menu Bar</source>
        <translation type="vanished">Barre de menu</translation>
    </message>
    <message>
        <source>Show/Hide Menu Bar &lt;Alt+U&gt;</source>
        <translation type="vanished">Afficher/Masquer la barre de menu &lt;Alt+U&gt;</translation>
    </message>
    <message>
        <source>Show/Hide Tool Bar</source>
        <translation type="vanished">Afficher/Masquer la barre d&apos;outils</translation>
    </message>
    <message>
        <source>Status Bar</source>
        <translation type="vanished">Barre d&apos;état</translation>
    </message>
    <message>
        <source>Show/Hide Status Bar</source>
        <translation type="vanished">Afficher/Masquer la barre d&apos;état</translation>
    </message>
    <message>
        <source>Command Window</source>
        <translation type="vanished">Fenêtre de commande</translation>
    </message>
    <message>
        <source>Show/Hide Command Window</source>
        <translation type="vanished">Afficher/Masquer la fenêtre de commande</translation>
    </message>
    <message>
        <source>Connect Bar</source>
        <translation type="vanished">Barre de connexion</translation>
    </message>
    <message>
        <source>Show/Hide Connect Bar</source>
        <translation type="vanished">Afficher/Masquer la barre de connexion</translation>
    </message>
    <message>
        <source>Side Window</source>
        <translation type="vanished">Fenêtre latérale</translation>
    </message>
    <message>
        <source>Show/Hide Side Window</source>
        <translation type="vanished">Afficher/Masquer la fenêtre latérale</translation>
    </message>
    <message>
        <source>Windows Transparency</source>
        <translation type="vanished">Transparence des fenêtres</translation>
    </message>
    <message>
        <source>Enable/Disable alpha transparency</source>
        <translation type="vanished">Activer/Désactiver la transparence alpha</translation>
    </message>
    <message>
        <source>Vertical Scroll Bar</source>
        <translation type="vanished">Barre de défilement verticale</translation>
    </message>
    <message>
        <source>Show/Hide Vertical Scroll Bar</source>
        <translation type="vanished">Afficher/Masquer la barre de défilement verticale</translation>
    </message>
    <message>
        <source>Allways On Top</source>
        <translation type="vanished">Toujours au premier plan</translation>
    </message>
    <message>
        <source>Show window always on top</source>
        <translation type="vanished">Afficher la fenêtre toujours au premier plan</translation>
    </message>
    <message>
        <source>Full Screen</source>
        <translation type="vanished">Plein écran</translation>
    </message>
    <message>
        <source>Toggle between full screen and normal mode &lt;Alt+Enter&gt;</source>
        <translation type="vanished">Basculer entre le mode plein écran et le mode normal &lt;Alt+Entrée&gt;</translation>
    </message>
    <message>
        <source>Session Options...</source>
        <translation type="vanished">Options de la session...</translation>
    </message>
    <message>
        <source>Configure session options</source>
        <translation type="vanished">Configurer les options de la session</translation>
    </message>
    <message>
        <source>Global Options...</source>
        <translation type="vanished">Options globales...</translation>
    </message>
    <message>
        <source>Configure global options</source>
        <translation type="vanished">Configurer les options globales</translation>
    </message>
    <message>
        <source>Real-time Save Options</source>
        <translation type="vanished">Options d&apos;enregistrement en temps réel</translation>
    </message>
    <message>
        <source>Real-time save session options and global options</source>
        <translation type="vanished">Enregistrer en temps réel les options de session et les options globales</translation>
    </message>
    <message>
        <source>Save Settings Now</source>
        <translation type="vanished">Enregistrer les paramètres maintenant</translation>
    </message>
    <message>
        <source>Save options configuration now</source>
        <translation type="vanished">Enregistrer la configuration des options maintenant</translation>
    </message>
    <message>
        <source>Send ASCII...</source>
        <translation type="vanished">Envoyer ASCII...</translation>
    </message>
    <message>
        <source>Send ASCII file</source>
        <translation type="vanished">Envoyer un fichier ASCII</translation>
    </message>
    <message>
        <source>Receive ASCII...</source>
        <translation type="vanished">Recevoir ASCII...</translation>
    </message>
    <message>
        <source>Receive ASCII file</source>
        <translation type="vanished">Recevoir un fichier ASCII</translation>
    </message>
    <message>
        <source>Send Binary...</source>
        <translation type="vanished">Envoyer binaire...</translation>
    </message>
    <message>
        <source>Send Binary file</source>
        <translation type="vanished">Envoyer un fichier binaire</translation>
    </message>
    <message>
        <source>Send Xmodem...</source>
        <translation type="vanished">Envoyer Xmodem...</translation>
    </message>
    <message>
        <source>Send a file using Xmodem</source>
        <translation type="vanished">Envoyer un fichier en utilisant Xmodem</translation>
    </message>
    <message>
        <source>Receive Xmodem...</source>
        <translation type="vanished">Recevoir Xmodem...</translation>
    </message>
    <message>
        <source>Receive a file using Xmodem</source>
        <translation type="vanished">Recevoir un fichier en utilisant Xmodem</translation>
    </message>
    <message>
        <source>Send Ymodem...</source>
        <translation type="vanished">Envoyer Ymodem...</translation>
    </message>
    <message>
        <source>Send a file using Ymodem</source>
        <translation type="vanished">Envoyer un fichier en utilisant Ymodem</translation>
    </message>
    <message>
        <source>Receive Ymodem...</source>
        <translation type="vanished">Recevoir Ymodem...</translation>
    </message>
    <message>
        <source>Receive a file using Ymodem</source>
        <translation type="vanished">Recevoir un fichier en utilisant Ymodem</translation>
    </message>
    <message>
        <source>Zmodem Upload List...</source>
        <translation type="vanished">Liste de téléchargement Zmodem...</translation>
    </message>
    <message>
        <source>Display Zmodem file upload list</source>
        <translation type="vanished">Afficher la liste de téléchargement de fichiers Zmodem</translation>
    </message>
    <message>
        <source>Start Zmodem Upload</source>
        <translation type="vanished">Démarrer le téléchargement Zmodem</translation>
    </message>
    <message>
        <source>Start Zmodem file upload</source>
        <translation type="vanished">Démarrer le téléchargement de fichiers Zmodem</translation>
    </message>
    <message>
        <source>Start TFTP Server</source>
        <translation type="vanished">Démarrer le serveur TFTP</translation>
    </message>
    <message>
        <source>Start/Stop the TFTP server</source>
        <translation type="vanished">Démarrer/Arrêter le serveur TFTP</translation>
    </message>
    <message>
        <source>Run...</source>
        <translation type="vanished">Exécuter...</translation>
    </message>
    <message>
        <source>Run a script</source>
        <translation type="vanished">Exécuter un script</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Annuler</translation>
    </message>
    <message>
        <source>Cancel script execution</source>
        <translation type="vanished">Annuler l&apos;exécution du script</translation>
    </message>
    <message>
        <source>Start Recording Script</source>
        <translation type="vanished">Commencer l&apos;enregistrement du script</translation>
    </message>
    <message>
        <source>Start recording script</source>
        <translation type="vanished">Commencer l&apos;enregistrement du script</translation>
    </message>
    <message>
        <source>Stop Recording Script...</source>
        <translation type="vanished">Arrêter l&apos;enregistrement du script...</translation>
    </message>
    <message>
        <source>Stop recording script</source>
        <translation type="vanished">Arrêter l&apos;enregistrement du script</translation>
    </message>
    <message>
        <source>Cancel Recording Script</source>
        <translation type="vanished">Annuler l&apos;enregistrement du script</translation>
    </message>
    <message>
        <source>Cancel recording script</source>
        <translation type="vanished">Annuler l&apos;enregistrement du script</translation>
    </message>
    <message>
        <source>Add Bookmark</source>
        <translation type="vanished">Ajouter un marque-page</translation>
    </message>
    <message>
        <source>Add a bookmark</source>
        <translation type="vanished">Ajouter un marque-page</translation>
    </message>
    <message>
        <source>Remove Bookmark</source>
        <translation type="vanished">Supprimer un marque-page</translation>
    </message>
    <message>
        <source>Remove a bookmark</source>
        <translation type="vanished">Supprimer un marque-page</translation>
    </message>
    <message>
        <source>Clean All Bookmark</source>
        <translation type="vanished">Nettoyer tous les marque-pages</translation>
    </message>
    <message>
        <source>Clean all bookmark</source>
        <translation type="vanished">Nettoyer tous les marque-pages</translation>
    </message>
    <message>
        <source>Keymap Manager</source>
        <translation type="vanished">Gestionnaire de mappage des touches</translation>
    </message>
    <message>
        <source>Display keymap editor</source>
        <translation type="vanished">Afficher l&apos;éditeur de mappage des touches</translation>
    </message>
    <message>
        <source>Create Public Key...</source>
        <translation type="vanished">Créer une clé publique...</translation>
    </message>
    <message>
        <source>Create a public key</source>
        <translation type="vanished">Créer une clé publique</translation>
    </message>
    <message>
        <source>Publickey Manager</source>
        <translation type="vanished">Gestionnaire de clés publiques</translation>
    </message>
    <message>
        <source>Display publickey manager</source>
        <translation type="vanished">Afficher le gestionnaire de clés publiques</translation>
    </message>
    <message>
        <source>SSH Scanning</source>
        <translation type="vanished">Exploration SSH</translation>
    </message>
    <message>
        <source>Display SSH scanning dialog</source>
        <translation type="vanished">Afficher la boîte de dialogue d&apos;exploration SSH</translation>
    </message>
    <message>
        <source>Tab</source>
        <translation type="vanished">Onglet</translation>
    </message>
    <message>
        <source>Arrange sessions in tabs</source>
        <translation type="vanished">Organiser les sessions dans des onglets</translation>
    </message>
    <message>
        <source>Tile</source>
        <translation type="vanished">Mosaïque</translation>
    </message>
    <message>
        <source>Arrange sessions in non-overlapping tiles</source>
        <translation type="vanished">Organiser les sessions dans des mosaïques non superposées</translation>
    </message>
    <message>
        <source>Cascade</source>
        <translation type="vanished">Cascade</translation>
    </message>
    <message>
        <source>Arrange sessions to overlap each other</source>
        <translation type="vanished">Organiser les sessions pour qu&apos;elles se chevauchent</translation>
    </message>
    <message>
        <source>Japanese</source>
        <translation type="vanished">日本語</translation>
    </message>
    <message>
        <source>Simplified Chinese</source>
        <translation type="vanished">简体中文</translation>
    </message>
    <message>
        <source>Switch to Simplified Chinese</source>
        <translation type="vanished">切换到简体中文</translation>
    </message>
    <message>
        <source>Traditional Chinese</source>
        <translation type="vanished">繁體中文</translation>
    </message>
    <message>
        <source>Switch to Traditional Chinese</source>
        <translation type="vanished">切換到繁體中文</translation>
    </message>
    <message>
        <source>Russian</source>
        <translation type="vanished">Русский</translation>
    </message>
    <message>
        <source>Switch to Russian</source>
        <translation type="vanished">Переключиться на русский</translation>
    </message>
    <message>
        <source>Korean</source>
        <translation type="vanished">한국어</translation>
    </message>
    <message>
        <source>Switch to Korean</source>
        <translation type="vanished">한국어로 전환</translation>
    </message>
    <message>
        <source>Switch to Japanese</source>
        <translation type="vanished">日本語に切り替える</translation>
    </message>
    <message>
        <source>French</source>
        <translation type="vanished">français</translation>
    </message>
    <message>
        <source>Switch to French</source>
        <translation type="vanished">Passer au français</translation>
    </message>
    <message>
        <source>Spanish</source>
        <translation type="vanished">español</translation>
    </message>
    <message>
        <source>Switch to Spanish</source>
        <translation type="vanished">Cambiar a español</translation>
    </message>
    <message>
        <source>Light</source>
        <translation type="vanished">Clair</translation>
    </message>
    <message>
        <source>Switch to light theme</source>
        <translation type="vanished">Passer au thème clair</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation type="vanished">Sombre</translation>
    </message>
    <message>
        <source>Switch to dark theme</source>
        <translation type="vanished">Passer au thème sombre</translation>
    </message>
    <message>
        <source>Display help</source>
        <translation type="vanished">Afficher l&apos;aide</translation>
    </message>
    <message>
        <source>Check Update</source>
        <translation type="vanished">Vérifier les mises à jour</translation>
    </message>
    <message>
        <source>Check for updates</source>
        <translation type="vanished">Vérifier les mises à jour</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">À propos de</translation>
    </message>
    <message>
        <source>Display about dialog</source>
        <translation type="vanished">Afficher la boîte de dialogue À propos de</translation>
    </message>
    <message>
        <source>About Qt</source>
        <translation type="vanished">À propos de Qt</translation>
    </message>
    <message>
        <source>Display about Qt dialog</source>
        <translation type="vanished">Afficher la boîte de dialogue À propos de Qt</translation>
    </message>
    <message>
        <source>PrintScreen saved to %1</source>
        <translation type="vanished">PrintScreen enregistré dans %1</translation>
    </message>
    <message>
        <source>Save Screenshot</source>
        <translation type="vanished">Enregistrer la capture d&apos;écran</translation>
    </message>
    <message>
        <source>Image Files (*.jpg)</source>
        <translation type="vanished">Fichiers image (*.jpg)</translation>
    </message>
    <message>
        <source>Screenshot saved to %1</source>
        <translation type="vanished">Capture d&apos;écran enregistrée dans %1</translation>
    </message>
    <message>
        <source>Save Session Export</source>
        <translation type="vanished">Enregistrer l&apos;exportation de la session</translation>
    </message>
    <message>
        <source>Text Files (*.txt);;HTML Files (*.html)</source>
        <translation type="vanished">Fichiers texte (*.txt);;Fichiers HTML (*.html)</translation>
    </message>
    <message>
        <source>Text Files (*.txt)</source>
        <translation type="vanished">Fichiers texte (*.txt)</translation>
    </message>
    <message>
        <source>HTML Files (*.html)</source>
        <translation type="vanished">Fichiers HTML (*.html)</translation>
    </message>
    <message>
        <source>Session Export saved to %1</source>
        <translation type="vanished">Exportation de la session enregistrée dans %1</translation>
    </message>
    <message>
        <source>Session Export failed to save to %1</source>
        <translation type="vanished">Échec de l&apos;enregistrement de l&apos;exportation de la session dans %1</translation>
    </message>
    <message>
        <source>Select a directory</source>
        <translation type="vanished">Sélectionner un répertoire</translation>
    </message>
    <message>
        <source>Select a bookmark</source>
        <translation type="vanished">Sélectionner un marque-page</translation>
    </message>
    <message>
        <source>Are you sure to clean all bookmark?</source>
        <translation type="vanished">Êtes-vous sûr de vouloir nettoyer tous les marque-pages ?</translation>
    </message>
    <message>
        <source>Port</source>
        <translation type="vanished">Port</translation>
    </message>
    <message>
        <source>Video background is enabled, please enable animation in global options (more system resources) or change background image.</source>
        <translation type="vanished">L&apos;arrière-plan vidéo est activé, veuillez activer l&apos;animation dans les options globales (plus de ressources système) ou changer l&apos;image d&apos;arrière-plan.</translation>
    </message>
    <message>
        <source>Session information get failed.</source>
        <translation type="vanished">Échec de l&apos;obtention des informations de session.</translation>
    </message>
    <message>
        <source>Telnet - </source>
        <translation type="vanished">Telnet - </translation>
    </message>
    <message>
        <source>Telnet</source>
        <translation type="vanished">Telnet</translation>
    </message>
    <message>
        <source>Serial - </source>
        <translation type="vanished">Série - </translation>
    </message>
    <message>
        <source>Serial</source>
        <translation type="vanished">Série</translation>
    </message>
    <message>
        <source>Raw - </source>
        <translation type="vanished">Brut - </translation>
    </message>
    <message>
        <source>Raw</source>
        <translation type="vanished">Brut</translation>
    </message>
    <message>
        <source>NamePipe - </source>
        <translation type="vanished">Tube nommé - </translation>
    </message>
    <message>
        <source>NamePipe</source>
        <translation type="vanished">Tube nommé</translation>
    </message>
    <message>
        <source>Local Shell</source>
        <translation type="vanished">Shell local</translation>
    </message>
    <message>
        <source>Local Shell - </source>
        <translation type="vanished">Shell local - </translation>
    </message>
    <message>
        <source>Are you sure to disconnect this session?</source>
        <translation type="vanished">Êtes-vous sûr de vouloir déconnecter cette session ?</translation>
    </message>
    <message>
        <source>Global Shortcuts:</source>
        <translation type="vanished">Raccourcis globaux :</translation>
    </message>
    <message>
        <source>show/hide menu bar</source>
        <translation type="vanished">Afficher/Masquer la barre de menu</translation>
    </message>
    <message>
        <source>connect to LocalShell</source>
        <translation type="vanished">Se connecter à LocalShell</translation>
    </message>
    <message>
        <source>clone current session</source>
        <translation type="vanished">Cloner la session actuelle</translation>
    </message>
    <message>
        <source>switch ui to STD mode</source>
        <translation type="vanished">Passer l&apos;interface utilisateur en mode STD</translation>
    </message>
    <message>
        <source>switch ui to MINI mode</source>
        <translation type="vanished">Passer l&apos;interface utilisateur en mode MINI</translation>
    </message>
    <message>
        <source>switch to previous session</source>
        <translation type="vanished">Passer à la session précédente</translation>
    </message>
    <message>
        <source>switch to next session</source>
        <translation type="vanished">Passer à la session suivante</translation>
    </message>
    <message>
        <source>switch to session [num]</source>
        <translation type="vanished">Passer à la session [num]</translation>
    </message>
    <message>
        <source>Go to line start</source>
        <translation type="vanished">Aller au début de la ligne</translation>
    </message>
    <message>
        <source>Go to line end</source>
        <translation type="vanished">Aller à la fin de la ligne</translation>
    </message>
    <message>
        <source>There are sessions that have not yet been unlocked, please unlock them first.</source>
        <translation type="vanished">Il y a des sessions qui n&apos;ont pas encore été déverrouillées, veuillez les déverrouiller d&apos;abord.</translation>
    </message>
    <message>
        <source>Are you sure to quit?</source>
        <translation type="vanished">Êtes-vous sûr de vouloir quitter ?</translation>
    </message>
</context>
<context>
    <name>NetScanWindow</name>
    <message>
        <location filename="../src/netscanwindow/netscanwindow.ui" line="14"/>
        <source>NetScan</source>
        <translation>Exploration réseau</translation>
    </message>
</context>
<context>
    <name>OneStepWindow</name>
    <message>
        <source>Port</source>
        <translation type="obsolete">Port</translation>
    </message>
    <message>
        <source>UserName</source>
        <translation type="obsolete">Nom d&apos;utilisateur</translation>
    </message>
</context>
<context>
    <name>PluginInfoWindow</name>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.ui" line="14"/>
        <source>Plugin Info</source>
        <translation>Informations sur le plugin</translation>
    </message>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.ui" line="28"/>
        <source>API version</source>
        <translation>Version de l&apos;API</translation>
    </message>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.ui" line="38"/>
        <source>Install plugin</source>
        <translation>Installer le plugin</translation>
    </message>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="40"/>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="86"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="40"/>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="86"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="40"/>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="86"/>
        <source>API Version</source>
        <translation>Version de l&apos;API</translation>
    </message>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="40"/>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="86"/>
        <source>Enable</source>
        <translation>Activer</translation>
    </message>
</context>
<context>
    <name>PluginViewerHomeWidget</name>
    <message>
        <location filename="../src/pluginviewerwidget/pluginviewerhomewidget.cpp" line="18"/>
        <source>Welcome to use</source>
        <translation>Bienvenue à utiliser</translation>
    </message>
    <message>
        <location filename="../src/pluginviewerwidget/pluginviewerhomewidget.cpp" line="20"/>
        <source>QuardCRT plugin system!</source>
        <translation>Système de plugins QuardCRT!</translation>
    </message>
</context>
<context>
    <name>PluginViewerWidget</name>
    <message>
        <location filename="../src/pluginviewerwidget/pluginviewerwidget.cpp" line="37"/>
        <source>Home</source>
        <translation>Accueil</translation>
    </message>
</context>
<context>
    <name>QCustomFileSystemModel</name>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="155"/>
        <source>Directory</source>
        <translation>Répertoire</translation>
    </message>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="155"/>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="159"/>
        <source>Loading...</source>
        <translation>Chargement...</translation>
    </message>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="184"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="186"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="188"/>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="190"/>
        <source>Last Modified</source>
        <translation>Dernière modification</translation>
    </message>
</context>
<context>
    <name>QKeychain::DeletePasswordJobPrivate</name>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="173"/>
        <source>Could not open keystore</source>
        <translation>Impossible d&apos;ouvrir le trousseau</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="179"/>
        <source>Could not remove private key from keystore</source>
        <translation>Impossible de supprimer la clé privée du trousseau</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="584"/>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="592"/>
        <source>Unknown error</source>
        <translation>Erreur inconnue</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="613"/>
        <source>Could not open wallet: %1; %2</source>
        <translation>Impossible d&apos;ouvrir le portefeuille : %1 ; %2</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="104"/>
        <source>Password entry not found</source>
        <translation>Entrée de mot de passe introuvable</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="108"/>
        <source>Could not decrypt data</source>
        <translation>Impossible de décrypter les données</translation>
    </message>
</context>
<context>
    <name>QKeychain::Job</name>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="29"/>
        <source>No error</source>
        <translation>Aucune erreur</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="31"/>
        <source>The specified item could not be found in the keychain</source>
        <translation>L&apos;élément spécifié n&apos;a pas pu être trouvé dans le trousseau</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="33"/>
        <source>User canceled the operation</source>
        <translation>L&apos;utilisateur a annulé l&apos;opération</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="35"/>
        <source>User interaction is not allowed</source>
        <translation>L&apos;interaction utilisateur n&apos;est pas autorisée</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="37"/>
        <source>No keychain is available. You may need to restart your computer</source>
        <translation>Aucun trousseau n&apos;est disponible. Vous devrez peut-être redémarrer votre ordinateur</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="39"/>
        <source>The user name or passphrase you entered is not correct</source>
        <translation>Le nom d&apos;utilisateur ou la phrase secrète que vous avez entré n&apos;est pas correct</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="41"/>
        <source>A cryptographic verification failure has occurred</source>
        <translation>Une erreur de vérification cryptographique s&apos;est produite</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="43"/>
        <source>Function or operation not implemented</source>
        <translation>Fonction ou opération non implémentée</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="45"/>
        <source>I/O error</source>
        <translation>Erreur d&apos;E/S</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="47"/>
        <source>Already open with with write permission</source>
        <translation>Déjà ouvert avec l&apos;autorisation d&apos;écriture</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="49"/>
        <source>Invalid parameters passed to a function</source>
        <translation>Paramètres non valides transmis à une fonction</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="51"/>
        <source>Failed to allocate memory</source>
        <translation>Échec de l&apos;allocation de mémoire</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="53"/>
        <source>Bad parameter or invalid state for operation</source>
        <translation>Mauvais paramètre ou état non valide pour l&apos;opération</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="55"/>
        <source>An internal component failed</source>
        <translation>Un composant interne a échoué</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="57"/>
        <source>The specified item already exists in the keychain</source>
        <translation>L&apos;élément spécifié existe déjà dans le trousseau</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="59"/>
        <source>Unable to decode the provided data</source>
        <translation>Impossible de décoder les données fournies</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="62"/>
        <source>Unknown error</source>
        <translation>Erreur inconnue</translation>
    </message>
</context>
<context>
    <name>QKeychain::JobPrivate</name>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="294"/>
        <source>Unknown error</source>
        <translation>Erreur inconnue</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="541"/>
        <source>Access to keychain denied</source>
        <translation>Accès au trousseau refusé</translation>
    </message>
</context>
<context>
    <name>QKeychain::PlainTextStore</name>
    <message>
        <location filename="../lib/qtkeychain/plaintextstore.cpp" line="65"/>
        <source>Could not store data in settings: access error</source>
        <translation>Impossible d&apos;enregistrer les données dans les paramètres : erreur d&apos;accès</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/plaintextstore.cpp" line="67"/>
        <source>Could not store data in settings: format error</source>
        <translation>Impossible d&apos;enregistrer les données dans les paramètres : erreur de format</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/plaintextstore.cpp" line="85"/>
        <source>Could not delete data from settings: access error</source>
        <translation>Impossible de supprimer les données des paramètres : erreur d&apos;accès</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/plaintextstore.cpp" line="87"/>
        <source>Could not delete data from settings: format error</source>
        <translation>Impossible de supprimer les données des paramètres : erreur de format</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/plaintextstore.cpp" line="104"/>
        <source>Entry not found</source>
        <translation>Entrée introuvable</translation>
    </message>
</context>
<context>
    <name>QKeychain::ReadPasswordJobPrivate</name>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="52"/>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="392"/>
        <source>Entry not found</source>
        <translation>Entrée introuvable</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="60"/>
        <source>Could not open keystore</source>
        <translation>Impossible d&apos;ouvrir le trousseau</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="68"/>
        <source>Could not retrieve private key from keystore</source>
        <translation>Impossible de récupérer la clé privée du trousseau</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="75"/>
        <source>Could not create decryption cipher</source>
        <translation>Impossible de créer un chiffre de décryptage</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="204"/>
        <source>D-Bus is not running</source>
        <translation>D-Bus ne fonctionne pas</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="213"/>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="223"/>
        <source>Unknown error</source>
        <translation>Erreur inconnue</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="315"/>
        <source>No keychain service available</source>
        <translation>Aucun service de trousseau disponible</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="317"/>
        <source>Could not open wallet: %1; %2</source>
        <translation>Impossible d&apos;ouvrir le portefeuille : %1 ; %2</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="362"/>
        <source>Access to keychain denied</source>
        <translation>Accès au trousseau refusé</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="383"/>
        <source>Could not determine data type: %1; %2</source>
        <translation>Impossible de déterminer le type de données : %1 ; %2</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="401"/>
        <source>Unsupported entry type &apos;Map&apos;</source>
        <translation>Type d&apos;entrée &apos;Map&apos; non pris en charge</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="404"/>
        <source>Unknown kwallet entry type &apos;%1&apos;</source>
        <translation>Type d&apos;entrée kwallet inconnu &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="32"/>
        <source>Password entry not found</source>
        <translation>Entrée de mot de passe introuvable</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="36"/>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="139"/>
        <source>Could not decrypt data</source>
        <translation>Impossible de décrypter les données</translation>
    </message>
</context>
<context>
    <name>QKeychain::WritePasswordJobPrivate</name>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="95"/>
        <source>Could not open keystore</source>
        <translation>Impossible d&apos;ouvrir le trousseau</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="124"/>
        <source>Could not create private key generator</source>
        <translation>Impossible de créer un générateur de clé privée</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="131"/>
        <source>Could not generate new private key</source>
        <translation>Impossible de générer une nouvelle clé privée</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="139"/>
        <source>Could not retrieve private key from keystore</source>
        <translation>Impossible de récupérer la clé privée du trousseau</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="147"/>
        <source>Could not create encryption cipher</source>
        <translation>Impossible de créer un chiffre de cryptage</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="155"/>
        <source>Could not encrypt data</source>
        <translation>Impossible de crypter les données</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="444"/>
        <source>D-Bus is not running</source>
        <translation>D-Bus ne fonctionne pas</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="454"/>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="481"/>
        <source>Unknown error</source>
        <translation>Erreur inconnue</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="500"/>
        <source>Could not open wallet: %1; %2</source>
        <translation>Impossible d&apos;ouvrir le portefeuille : %1 ; %2</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="78"/>
        <source>Credential size exceeds maximum size of %1</source>
        <translation>La taille des informations d&apos;identification dépasse la taille maximale de %1</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="87"/>
        <source>Credential key exceeds maximum size of %1</source>
        <translation>La clé des informations d&apos;identification dépasse la taille maximale de %1</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="92"/>
        <source>Writing credentials failed: Win32 error code %1</source>
        <translation>L&apos;écriture des informations d&apos;identification a échoué : code d&apos;erreur Win32 %1</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="162"/>
        <source>Encryption failed</source>
        <translation>Le cryptage a échoué</translation>
    </message>
</context>
<context>
    <name>QMessageBox</name>
    <message>
        <location filename="../lib/qtermwidget/TerminalDisplay.cpp" line="2949"/>
        <source>Show Details...</source>
        <translation>Afficher les détails...</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../lib/qtermwidget/ColorScheme.cpp" line="267"/>
        <location filename="../lib/qtermwidget/ColorScheme.cpp" line="282"/>
        <source>Un-named Color Scheme</source>
        <translation>Schéma de couleur sans nom</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/ColorScheme.cpp" line="453"/>
        <source>Accessible Color Scheme</source>
        <translation>Schéma de couleur accessible</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/Filter.cpp" line="549"/>
        <source>Open Link</source>
        <translation>Ouvrir le lien</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/Filter.cpp" line="550"/>
        <source>Copy Link Address</source>
        <translation>Copier l&apos;adresse du lien</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/Filter.cpp" line="554"/>
        <source>Send Email To...</source>
        <translation>Envoyer un courriel à...</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/Filter.cpp" line="555"/>
        <source>Copy Email Address</source>
        <translation>Copier l&apos;adresse de courriel</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/Filter.cpp" line="559"/>
        <source>Open Path</source>
        <translation>Ouvrir le chemin</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/Filter.cpp" line="560"/>
        <source>Copy Path</source>
        <translation>Copier le chemin</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="254"/>
        <source>Access to keychain denied</source>
        <translation>Accès au trousseau refusé</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="256"/>
        <source>No keyring daemon</source>
        <translation>Aucun démon de trousseau</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="258"/>
        <source>Already unlocked</source>
        <translation>Déjà déverrouillé</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="260"/>
        <source>No such keyring</source>
        <translation>Aucun trousseau de clés</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="262"/>
        <source>Bad arguments</source>
        <translation>Mauvais arguments</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="264"/>
        <source>I/O error</source>
        <translation>Erreur d&apos;E/S</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="266"/>
        <source>Cancelled</source>
        <translation>Annulé</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="268"/>
        <source>Keyring already exists</source>
        <translation>Le trousseau de clés existe déjà</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="270"/>
        <source>No match</source>
        <translation>Aucune correspondance</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="275"/>
        <source>Unknown error</source>
        <translation>Erreur inconnue</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/libsecret.cpp" line="120"/>
        <source>Entry not found</source>
        <translation>Entrée introuvable</translation>
    </message>
</context>
<context>
    <name>QTermWidget</name>
    <message>
        <location filename="../lib/qtermwidget/qtermwidget.cpp" line="350"/>
        <source>Color Scheme Error</source>
        <translation>Erreur de schéma de couleur</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/qtermwidget.cpp" line="351"/>
        <source>Cannot load color scheme: %1</source>
        <translation>Impossible de charger le schéma de couleur : %1</translation>
    </message>
</context>
<context>
    <name>QuickConnectWindow</name>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="30"/>
        <source>Protocol</source>
        <translation>Protocole</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="43"/>
        <source>Serial</source>
        <translation>Série</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="48"/>
        <source>Local Shell</source>
        <translation>Shell local</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="58"/>
        <source>Named Pipe</source>
        <translation>Tuyau nommé</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="86"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="69"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="166"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="224"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="253"/>
        <source>Hostname</source>
        <translation>Nom d&apos;hôte</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="109"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="70"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="167"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="225"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="254"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="125"/>
        <source>WebSocket</source>
        <translation>WebSocket</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="133"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="233"/>
        <source>None</source>
        <translation>Aucun</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="138"/>
        <source>Insecure</source>
        <translation>Non sécurisé</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="143"/>
        <source>Secure</source>
        <translation>Sécurisé</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="161"/>
        <source>Username</source>
        <translation>Nom d&apos;utilisateur</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="171"/>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="188"/>
        <source>DataBits</source>
        <translation>Bits de données</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="225"/>
        <source>Parity</source>
        <translation>Parité</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="238"/>
        <source>Odd</source>
        <translation>Impair</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="243"/>
        <source>Even</source>
        <translation>Pair</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="261"/>
        <source>StopBits</source>
        <translation>Bits de stop</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="313"/>
        <source>Save session</source>
        <translation>Enregistrer la session</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="323"/>
        <source>Open in tab</source>
        <translation>Ouvrir dans un onglet</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="33"/>
        <source>Quick Connect</source>
        <translation>Connexion rapide</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="91"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="188"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="246"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="275"/>
        <source>e.g. 127.0.0.1</source>
        <translation>par exemple 127.0.0.1</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="99"/>
        <source>Port Name</source>
        <translation>Nom du port</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="100"/>
        <source>Baud Rate</source>
        <translation>Débit en bauds</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="109"/>
        <source>e.g. 110, 300, 600, 1200, 2400, 
4800, 9600, 14400, 19200, 38400, 
56000, 57600, 115200, 128000, 256000, 
460800, 921600</source>
        <translation>par exemple 110, 300, 600, 1200, 2400,
4800, 9600, 14400, 19200, 38400,
56000, 57600, 115200, 128000, 256000,
460800, 921600</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="141"/>
        <source>Command</source>
        <translation>Commande</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="162"/>
        <source>e.g. /bin/bash</source>
        <translation>par exemple /bin/bash</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="195"/>
        <source>Pipe Name</source>
        <translation>Nom du tube</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="217"/>
        <source>e.g. \\\.\pipe\namedpipe</source>
        <translation>par exemple \\\.\pipe\namedpipe</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="219"/>
        <source>e.g. /tmp/socket</source>
        <translation>par exemple /tmp/socket</translation>
    </message>
</context>
<context>
    <name>SFTPmenuBookmarkWidget</name>
    <message>
        <location filename="../src/sftpwindow/sftpmenubookmarkwidget.ui" line="17"/>
        <source>BookMarkName</source>
        <translation>Nom du marque-page</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpmenubookmarkwidget.ui" line="35"/>
        <source>LocalPath</source>
        <translation>Chemin local</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpmenubookmarkwidget.ui" line="62"/>
        <source>RemotePath</source>
        <translation>Chemin distant</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpmenubookmarkwidget.cpp" line="31"/>
        <source>Open Directory</source>
        <translation>ouvrir un répertoire</translation>
    </message>
</context>
<context>
    <name>SFTPmenuBookmarkWindow</name>
    <message>
        <source>Bookmark</source>
        <translation type="vanished">Marque-page</translation>
    </message>
    <message>
        <source>BookMarkName</source>
        <translation type="vanished">Nom du marque-page</translation>
    </message>
    <message>
        <source>LocalPath</source>
        <translation type="vanished">Chemin local</translation>
    </message>
    <message>
        <source>RemotePath</source>
        <translation type="vanished">Chemin distant</translation>
    </message>
    <message>
        <source>Open Directory</source>
        <translation type="vanished">ouvrir un répertoire</translation>
    </message>
</context>
<context>
    <name>SearchBar</name>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.ui" line="14"/>
        <source>SearchBar</source>
        <translation>Barre de recherche</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.ui" line="20"/>
        <source>X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.ui" line="32"/>
        <source>Find:</source>
        <translation>Trouver :</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.ui" line="42"/>
        <source>&lt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.ui" line="54"/>
        <source>&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.ui" line="66"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.cpp" line="40"/>
        <location filename="../lib/qtermwidget/SearchBar.cpp" line="130"/>
        <source>Match case</source>
        <translation>Respecter la casse</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.cpp" line="46"/>
        <location filename="../lib/qtermwidget/SearchBar.cpp" line="131"/>
        <source>Regular expression</source>
        <translation>Expression régulière</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.cpp" line="50"/>
        <location filename="../lib/qtermwidget/SearchBar.cpp" line="132"/>
        <source>Highlight all matches</source>
        <translation>Surligner toutes les correspondances</translation>
    </message>
</context>
<context>
    <name>SessionManagerTreeModel</name>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="133"/>
        <source>Telnet</source>
        <translation>Telnet</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="135"/>
        <source>Serial</source>
        <translation>Série</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="137"/>
        <source>Shell</source>
        <translation>Shell</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="139"/>
        <source>Raw</source>
        <translation>Brut</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="141"/>
        <source>NamePipe</source>
        <translation>Tube nommé</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="269"/>
        <source>Name</source>
        <translation>nom</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="271"/>
        <source>Kind</source>
        <translation>Genre</translation>
    </message>
</context>
<context>
    <name>SessionManagerTreeView</name>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="39"/>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="74"/>
        <source>Session</source>
        <translation>Session</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="84"/>
        <source>Connect Terminal</source>
        <translation>Connecter le terminal</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="89"/>
        <source>Connect in New Window</source>
        <translation>Se connecter dans une nouvelle fenêtre</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="96"/>
        <source>Connect in New Tab Group</source>
        <translation>Se connecter dans un nouveau groupe d&apos;onglets</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="103"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="109"/>
        <source>Properties</source>
        <translation>Propriétés</translation>
    </message>
</context>
<context>
    <name>SessionManagerWidget</name>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagerwidget.ui" line="43"/>
        <source>Session Manager</source>
        <translation>Gestionnaire de sessions</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagerwidget.cpp" line="67"/>
        <source>Filter by folder/session name</source>
        <translation>Filtrer par nom de dossier/session</translation>
    </message>
</context>
<context>
    <name>SessionOptionsGeneralWidget</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsgeneralwidget.ui" line="25"/>
        <source>Name</source>
        <translation>nom</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsgeneralwidget.ui" line="45"/>
        <source>Protocol</source>
        <translation>Protocole</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsgeneralwidget.ui" line="58"/>
        <source>Serial</source>
        <translation>Série</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsgeneralwidget.ui" line="63"/>
        <source>Local Shell</source>
        <translation>Shell local</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsgeneralwidget.ui" line="73"/>
        <source>Named Pipe</source>
        <translation>Tuyau nommé</translation>
    </message>
</context>
<context>
    <name>SessionOptionsLocalShellProperties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionslocalshellproperties.ui" line="19"/>
        <source>Command</source>
        <translation>Commande</translation>
    </message>
</context>
<context>
    <name>SessionOptionsLocalShellState</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionslocalshellstate.cpp" line="33"/>
        <source>Name</source>
        <translation>nom</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionslocalshellstate.ui" line="25"/>
        <source>State</source>
        <translation>État</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionslocalshellstate.ui" line="51"/>
        <source>Process Tree</source>
        <translation>Arbre des processus</translation>
    </message>
</context>
<context>
    <name>SessionOptionsNamePipeProperties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsnamepipeproperties.ui" line="25"/>
        <source>PipeName</source>
        <translation>Nom du tube</translation>
    </message>
</context>
<context>
    <name>SessionOptionsRawProperties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsrawproperties.ui" line="25"/>
        <source>Hostname</source>
        <translation>Nom d&apos;hôte</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsrawproperties.ui" line="45"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
</context>
<context>
    <name>SessionOptionsSerialProperties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="25"/>
        <source>Port Name</source>
        <translation>Nom du port</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="45"/>
        <source>Baud Rate</source>
        <translation>Débit en bauds</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="72"/>
        <source>DataBits</source>
        <translation>Bits de données</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="109"/>
        <source>Parity</source>
        <translation>Parité</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="117"/>
        <source>None</source>
        <translation>Aucun</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="122"/>
        <source>Odd</source>
        <translation>Impair</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="127"/>
        <source>Even</source>
        <translation>Pair</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="145"/>
        <source>StopBits</source>
        <translation>Bits de stop</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.cpp" line="28"/>
        <source>e.g. 110, 300, 600, 1200, 2400, 
4800, 9600, 14400, 19200, 38400, 
56000, 57600, 115200, 128000, 256000, 
460800, 921600</source>
        <translation>par exemple 110, 300, 600, 1200, 2400,
4800, 9600, 14400, 19200, 38400,
56000, 57600, 115200, 128000, 256000,
460800, 921600</translation>
    </message>
</context>
<context>
    <name>SessionOptionsSsh2Properties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsssh2properties.ui" line="25"/>
        <source>Hostname</source>
        <translation>Nom d&apos;hôte</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsssh2properties.ui" line="41"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsssh2properties.ui" line="65"/>
        <source>UserName</source>
        <translation>Nom d&apos;utilisateur</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsssh2properties.ui" line="81"/>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
</context>
<context>
    <name>SessionOptionsTelnetProperties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionstelnetproperties.ui" line="19"/>
        <source>Hostname</source>
        <translation>Nom d&apos;hôte</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionstelnetproperties.ui" line="33"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionstelnetproperties.ui" line="55"/>
        <source>None</source>
        <translation>Aucun</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionstelnetproperties.ui" line="60"/>
        <source>Insecure</source>
        <translation>Non sécurisé</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionstelnetproperties.ui" line="65"/>
        <source>Secure</source>
        <translation>Sécurisé</translation>
    </message>
</context>
<context>
    <name>SessionOptionsVNCProperties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsvncproperties.ui" line="25"/>
        <source>Hostname</source>
        <translation>Nom d&apos;hôte</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsvncproperties.ui" line="41"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsvncproperties.ui" line="65"/>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
</context>
<context>
    <name>SessionOptionsWindow</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionswindow.ui" line="14"/>
        <source>Session Options</source>
        <translation>Options de session</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionswindow.cpp" line="122"/>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionswindow.cpp" line="122"/>
        <source>Properties</source>
        <translation>Propriétés</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionswindow.cpp" line="122"/>
        <source>State</source>
        <translation>État</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionswindow.cpp" line="133"/>
        <source>Name</source>
        <translation>nom</translation>
    </message>
</context>
<context>
    <name>SessionsWindow</name>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="141"/>
        <source>Telnet Error</source>
        <translation>Erreur Telnet</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="141"/>
        <source>Telnet error:
%1.</source>
        <translation>Erreur Telnet :
%1.</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="168"/>
        <source>Serial Error</source>
        <translation>Erreur série</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="168"/>
        <source>Serial error:
%1.</source>
        <translation>Erreur série :
%1.</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="203"/>
        <source>Raw Socket Error</source>
        <translation>Erreur de socket brut</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="203"/>
        <source>Raw Socket error:
%1.</source>
        <translation>Erreur de socket brut :
%1.</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="234"/>
        <source>Name Pipe Error</source>
        <translation>Erreur de tube nommé</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="234"/>
        <source>Name Pipe error:
%1.</source>
        <translation>Erreur de tube nommé :
%1.</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="246"/>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="275"/>
        <source>SSH2 Error</source>
        <translation>Erreur SSH2</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="246"/>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="275"/>
        <source>SSH2 error:
%1.</source>
        <translation>Erreur SSH2 :
%1.</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="434"/>
        <source>Start Local Shell</source>
        <translation>Démarrer le shell local</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="434"/>
        <source>Cannot start local shell:
%1.</source>
        <translation>Impossible de démarrer le shell local :
%1.</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="573"/>
        <source>Save log...</source>
        <translation>Enregistrer le journal...</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="574"/>
        <source>log files (*.log)</source>
        <translation>Fichiers journaux (*.log)</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="578"/>
        <source>Save log</source>
        <translation>Enregistrer le journal</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="578"/>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="613"/>
        <source>Cannot write file %1:
%2.</source>
        <translation>Impossible d&apos;écrire le fichier %1 :
%2.</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="608"/>
        <source>Save Raw log...</source>
        <translation>Enregistrer le journal brut...</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="609"/>
        <source>binary files (*.bin)</source>
        <translation>Fichiers binaires (*.bin)</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="613"/>
        <source>Save Raw log</source>
        <translation>Enregistrer le journal brut</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="685"/>
        <source>Unlock Session</source>
        <translation>Déverrouiller la session</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="685"/>
        <source>Wrong password!</source>
        <translation>Mot de passe incorrect !</translation>
    </message>
</context>
<context>
    <name>SftpWindow</name>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.ui" line="14"/>
        <source>sftp</source>
        <translation>sftp</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.ui" line="72"/>
        <source>local</source>
        <translation>local</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.ui" line="130"/>
        <source>remote</source>
        <translation>à distance</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="46"/>
        <source>Bookmarks</source>
        <translation>Marque-pages</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="47"/>
        <source>Add Bookmark</source>
        <translation>Ajouter un marque-page</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="48"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="67"/>
        <source>Edit Bookmark</source>
        <translation>Modifier un marque-page</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="49"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="84"/>
        <source>Remove Bookmark</source>
        <translation>Supprimer un marque-page</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="67"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="84"/>
        <source>Bookmark Name:</source>
        <translation>Nom du marque-page :</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="109"/>
        <source>Warning</source>
        <translation>Attention</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="109"/>
        <source>Bookmark Name can not be empty!</source>
        <translation>Le nom du marque-page ne peut pas être vide !</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="196"/>
        <source>No task!</source>
        <translation>Pas de tâche !</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="200"/>
        <source>All tasks finished!</source>
        <translation>Toutes les tâches sont terminées !</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="205"/>
        <source>task %1/%2</source>
        <translation>tâche %1/%2</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="223"/>
        <source>Open Directory</source>
        <translation>ouvrir un répertoire</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="246"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="412"/>
        <source>Show/Hide Files</source>
        <translation>Afficher/masquer les fichiers</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="250"/>
        <source>Upload</source>
        <translation>Téléverser</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="301"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="476"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="305"/>
        <source>Open in System File Manager</source>
        <translation>Ouvrir dans le gestionnaire de fichiers</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="310"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="482"/>
        <source>refresh</source>
        <translation>rafraîchir</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="315"/>
        <source>Upload All</source>
        <translation>Téléverser tout</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="359"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="535"/>
        <source>Cancel Selection</source>
        <translation>Annuler la sélection</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="416"/>
        <source>Download</source>
        <translation>Télécharger</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="456"/>
        <source>File exists</source>
        <translation>Le fichier existe</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="456"/>
        <source>File %1 already exists. Do you want to overwrite it?</source>
        <translation>Le fichier %1 existe déjà. Voulez-vous l&apos;écraser ?</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="487"/>
        <source>Download All</source>
        <translation>Tout télécharger</translation>
    </message>
</context>
<context>
    <name>StartTftpSeverWindow</name>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.ui" line="14"/>
        <source>Start TFTP Sever</source>
        <translation>Démarrer le serveur TFTP</translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.ui" line="32"/>
        <source>The TFTP server uses local directories for uploading and downloading files. Please enter this information below.</source>
        <translation>Le serveur TFTP utilise des répertoires locaux pour le téléchargement et le téléversement de fichiers. Veuillez entrer ces informations ci-dessous.</translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.ui" line="65"/>
        <source>TFTP Port</source>
        <translation>Port TFTP</translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.ui" line="104"/>
        <source>Upload directory</source>
        <translation>Répertoire de téléversement</translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.ui" line="143"/>
        <source>Download directory</source>
        <translation>répertoire de téléchargement</translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.cpp" line="38"/>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.cpp" line="45"/>
        <source>Open Directory</source>
        <translation>ouvrir un répertoire</translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.cpp" line="77"/>
        <source>Warning</source>
        <translation>Attention</translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.cpp" line="77"/>
        <source>Please select a valid directory!</source>
        <translation>veuillez sélectionner un répertoire valide !</translation>
    </message>
</context>
<context>
    <name>UndoStack</name>
    <message>
        <location filename="../lib/qhexedit/commands.cpp" line="115"/>
        <source>Inserting %1 bytes</source>
        <translation>Insertion de %1 octets</translation>
    </message>
    <message>
        <location filename="../lib/qhexedit/commands.cpp" line="137"/>
        <source>Delete %1 chars</source>
        <translation>Supprimer %1 caractères</translation>
    </message>
    <message>
        <location filename="../lib/qhexedit/commands.cpp" line="162"/>
        <source>Overwrite %1 chars</source>
        <translation>Écraser %1 caractères</translation>
    </message>
</context>
<context>
    <name>keyMapManager</name>
    <message>
        <location filename="../src/keymapmanager/keymapmanager.ui" line="14"/>
        <source>keyMapManager</source>
        <translation>Gestionnaire de cartes clavier</translation>
    </message>
    <message>
        <location filename="../src/keymapmanager/keymapmanager.ui" line="20"/>
        <source>KeyBinding</source>
        <translation>Association de touches</translation>
    </message>
</context>
</TS>
