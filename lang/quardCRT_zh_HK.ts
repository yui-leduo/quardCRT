<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_HK">
<context>
    <name>AddTabButton</name>
    <message>
        <location filename="../lib/QtFancyTabWidget/addtabbutton.cpp" line="29"/>
        <source>New tab</source>
        <translation>新建標籤</translation>
    </message>
</context>
<context>
    <name>CentralWidget</name>
    <message>
        <location filename="../src/mainwindow.ui" line="101"/>
        <location filename="../src/mainwindow.cpp" line="1123"/>
        <source>Tool Bar</source>
        <translation>工具欄</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="191"/>
        <location filename="../src/mainwindow.cpp" line="194"/>
        <location filename="../src/mainwindow.cpp" line="197"/>
        <location filename="../src/mainwindow.cpp" line="325"/>
        <location filename="../src/mainwindow.cpp" line="341"/>
        <location filename="../src/mainwindow.cpp" line="358"/>
        <location filename="../src/mainwindow.cpp" line="371"/>
        <location filename="../src/mainwindow.cpp" line="546"/>
        <location filename="../src/mainwindow.cpp" line="2552"/>
        <location filename="../src/mainwindow.cpp" line="2747"/>
        <location filename="../src/mainwindow.cpp" line="3115"/>
        <location filename="../src/mainwindow.cpp" line="3451"/>
        <location filename="../src/mainwindow.cpp" line="3456"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="191"/>
        <source>TFTP server bind error!</source>
        <translation>TFTP服務器綁定錯誤！</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="194"/>
        <source>TFTP server file error!</source>
        <translation>TFTP服務器文件錯誤！</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="197"/>
        <source>TFTP server network error!</source>
        <translation>TFTP服務器網絡錯誤！</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="286"/>
        <source>Unlock Session</source>
        <translation>解鎖會話</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="287"/>
        <source>Unlock current session</source>
        <translation>解鎖當前會話</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="293"/>
        <source>Move to another Tab</source>
        <translation>移動到另一個標籤</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="294"/>
        <source>Move to current session to another tab group</source>
        <translation>將當前會話移動到另一個標籤組</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="303"/>
        <source>Floating Window</source>
        <translation>浮動窗口</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="304"/>
        <source>Floating current session to a new window</source>
        <translation>將當前會話浮動到新窗口</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="311"/>
        <source>Copy Path</source>
        <translation>複製路徑</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="312"/>
        <source>Copy current session working folder path</source>
        <translation>複製當前會話的工作文件夾路徑</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="325"/>
        <location filename="../src/mainwindow.cpp" line="341"/>
        <location filename="../src/mainwindow.cpp" line="358"/>
        <source>No working folder!</source>
        <translation>沒有工作文件夾！</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="327"/>
        <source>Add Path to Bookmark</source>
        <translation>添加路徑到書籤</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="328"/>
        <source>Add current session working folder path to bookmark</source>
        <translation>將當前會話的工作文件夾路徑添加到書籤</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="343"/>
        <source>Open Working Folder</source>
        <translation>打開工作文件夾</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="344"/>
        <source>Open current session working folder in system file manager</source>
        <translation>在系統文件管理器中打開當前會話的工作文件夾</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="362"/>
        <source>Open SFTP</source>
        <translation>打開SFTP</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="363"/>
        <source>Open SFTP in a new window</source>
        <translation>在新窗口中打開SFTP</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="371"/>
        <source>No SFTP channel!</source>
        <translation>沒有SFTP通道！</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="378"/>
        <source>Save Session</source>
        <translation>保存會話</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="379"/>
        <source>Save current session to session manager</source>
        <translation>將當前會話保存到會話管理器</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="389"/>
        <source>Enter Session Name</source>
        <translation>輸入會話名稱</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="390"/>
        <source>The session already exists, please rename the new session or cancel saving.</source>
        <translation>會話已經存在，請重新命名新會話或取消保存。</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="396"/>
        <source>Properties</source>
        <translation>屬性</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="397"/>
        <source>Show current session properties</source>
        <translation>顯示當前會話屬性</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="417"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="418"/>
        <source>Close current session</source>
        <translation>關閉當前會話</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="423"/>
        <source>Close Others</source>
        <translation>關閉其他</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="424"/>
        <source>Close other sessions</source>
        <translation>關閉其他會話</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="434"/>
        <source>Close to the Right</source>
        <translation>關閉到右側</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="435"/>
        <source>Close sessions to the right</source>
        <translation>關閉到右側的會話</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="443"/>
        <source>Close All</source>
        <translation>關閉所有</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="444"/>
        <source>Close all sessions</source>
        <translation>關閉所有會話</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="546"/>
        <source>Session properties error!</source>
        <translation>會話屬性錯誤！</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="566"/>
        <location filename="../src/mainwindow.cpp" line="684"/>
        <location filename="../src/mainwindow.cpp" line="2455"/>
        <source>Ready</source>
        <translation>就緒</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="777"/>
        <source>Highlight/Unhighlight</source>
        <translation>高亮/取消高亮</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="778"/>
        <source>Highlight/Unhighlight selected text</source>
        <translation>高亮/取消高亮所選文本</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="838"/>
        <source>Google Translate</source>
        <translation>谷歌翻譯</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="839"/>
        <location filename="../src/mainwindow.cpp" line="852"/>
        <location filename="../src/mainwindow.cpp" line="864"/>
        <source>Translate selected text</source>
        <translation>翻譯所選文本</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="851"/>
        <source>Baidu Translate</source>
        <translation>百度翻譯</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="863"/>
        <source>Microsoft Translate</source>
        <translation>微軟翻譯</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="924"/>
        <source>Back to Main Window</source>
        <translation>返回主窗口</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="995"/>
        <location filename="../src/mainwindow.cpp" line="1019"/>
        <source>Session Manager</source>
        <translation>會話管理器</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="996"/>
        <source>Plugin</source>
        <translation>插件</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="998"/>
        <source>File</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="999"/>
        <source>Edit</source>
        <translation>編輯</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1000"/>
        <source>View</source>
        <translation>查看</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1001"/>
        <source>Options</source>
        <translation>選項</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1002"/>
        <source>Transfer</source>
        <translation>傳輸</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1003"/>
        <source>Script</source>
        <translation>腳本</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1004"/>
        <source>Bookmark</source>
        <translation>書籤</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1005"/>
        <source>Tools</source>
        <translation>工具</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1006"/>
        <source>Window</source>
        <translation>窗口</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1007"/>
        <source>Language</source>
        <translation>語言</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1008"/>
        <source>Theme</source>
        <translation>主題</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1009"/>
        <location filename="../src/mainwindow.cpp" line="1229"/>
        <location filename="../src/mainwindow.cpp" line="3415"/>
        <source>Help</source>
        <translation>幫助</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1011"/>
        <source>New Window</source>
        <translation>新建窗口</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1013"/>
        <source>Open a new window &lt;Ctrl+Shift+N&gt;</source>
        <translation>打開新窗口 &lt;Ctrl+Shift+N&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1015"/>
        <source>Connect...</source>
        <translation>連接...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1017"/>
        <source>Connect to a host &lt;Alt+C&gt;</source>
        <translation>連接到主機 &lt;Alt+C&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1021"/>
        <source>Go to the Session Manager &lt;Alt+M&gt;</source>
        <translation>轉到會話管理器 &lt;Alt+M&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1023"/>
        <source>Quick Connect...</source>
        <translation>快速連接...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1025"/>
        <source>Quick Connect to a host &lt;Alt+Q&gt;</source>
        <translation>快速連接到主機 &lt;Alt+Q&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1027"/>
        <source>Connect in Tab/Tile...</source>
        <translation>在標籤/平鋪中連接...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1028"/>
        <source>Connect to a host in a new tab &lt;Alt+B&gt;</source>
        <translation>在新標籤中連接到主機 &lt;Alt+B&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1030"/>
        <source>Connect Local Shell</source>
        <translation>連接本地shell</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1032"/>
        <source>Connect to a local shell &lt;Alt+T&gt;</source>
        <translation>連接到本地shell &lt;Alt+T&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1034"/>
        <source>Reconnect</source>
        <translation>重新連接</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1036"/>
        <source>Reconnect current session</source>
        <translation>重新連接當前會話</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1037"/>
        <source>Reconnect All</source>
        <translation>重新連接所有</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1038"/>
        <source>Reconnect all sessions &lt;Alt+A&gt;</source>
        <translation>重新連接所有會話 &lt;Alt+A&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1040"/>
        <source>Disconnect</source>
        <translation>斷開連接</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1042"/>
        <source>Disconnect current session</source>
        <translation>斷開當前會話</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1043"/>
        <location filename="../src/mainwindow.cpp" line="1044"/>
        <source>Enter host &lt;Alt+R&gt; to connect</source>
        <translation>輸入主機 &lt;Alt+R&gt; 連接</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1045"/>
        <source>Disconnect All</source>
        <translation>斷開所有</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1046"/>
        <source>Disconnect all sessions</source>
        <translation>斷開所有會話</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1047"/>
        <source>Clone Session</source>
        <translation>克隆會話</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1048"/>
        <source>Clone current session &lt;Ctrl+Shift+T&gt;</source>
        <translation>克隆當前會話 &lt;Ctrl+Shift+T&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1050"/>
        <source>Lock Session</source>
        <translation>鎖定會話</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1051"/>
        <source>Lock/Unlock current session</source>
        <translation>鎖定/解鎖當前會話</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1052"/>
        <source>Log Session</source>
        <translation>記錄日誌</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1053"/>
        <source>Create a log file for current session</source>
        <translation>為當前會話創建日誌文件</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1054"/>
        <source>Raw Log Session</source>
        <translation>記錄原始日誌</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1055"/>
        <source>Create a raw log file for current session</source>
        <translation>為當前會話創建原始日誌文件</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1056"/>
        <source>Hex View</source>
        <translation>十六進制視圖</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1057"/>
        <source>Show/Hide Hex View for current session</source>
        <translation>顯示/隱藏當前會話的十六進制視圖</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1058"/>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1059"/>
        <source>Quit the application</source>
        <translation>退出應用程序</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1061"/>
        <source>Copy</source>
        <translation>複製</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1064"/>
        <source>Copy the selected text to the clipboard &lt;Command+C&gt;</source>
        <translation>將所選文本複製到剪貼板 &lt;Command+C&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1067"/>
        <source>Copy the selected text to the clipboard &lt;Ctrl+Ins&gt;</source>
        <translation>將所選文本複製到剪貼板 &lt;Ctrl+Ins&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1070"/>
        <source>Paste</source>
        <translation>粘貼</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1073"/>
        <source>Paste the clipboard text to the current session &lt;Command+V&gt;</source>
        <translation>將剪貼板文本粘貼到當前會話 &lt;Command+V&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1076"/>
        <source>Paste the clipboard text to the current session &lt;Shift+Ins&gt;</source>
        <translation>將剪貼板文本粘貼到當前會話 &lt;Shift+Ins&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1079"/>
        <source>Copy and Paste</source>
        <translation>複製和粘貼</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1080"/>
        <source>Copy the selected text to the clipboard and paste to the current session</source>
        <translation>將所選文本複製到剪貼板並粘貼到當前會話</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1081"/>
        <source>Select All</source>
        <translation>全選</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1083"/>
        <source>Select all text in the current session &lt;Ctrl+Shift+A&gt;</source>
        <translation>選擇當前會話中的所有文本 &lt;Ctrl+Shift+A&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1085"/>
        <source>Find...</source>
        <translation>查找...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1087"/>
        <source>Find text in the current session &lt;Ctrl+F&gt;</source>
        <translation>在當前會話中查找文本 &lt;Ctrl+F&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1089"/>
        <source>Print Screen</source>
        <translation>打印屏幕</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1091"/>
        <source>Print current screen</source>
        <translation>打印當前屏幕</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1092"/>
        <source>Screen Shot</source>
        <translation>屏幕截圖</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1094"/>
        <source>Screen shot current screen &lt;Alt+P&gt;</source>
        <translation>截圖當前屏幕 &lt;Alt+P&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1096"/>
        <source>Session Export</source>
        <translation>會話導出</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1098"/>
        <source>Export current session to a file &lt;Alt+O&gt;</source>
        <translation>將當前會話導出到文件 &lt;Alt+O&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1100"/>
        <source>Clear Scrollback</source>
        <translation>清除滾動內容</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1101"/>
        <source>Clear the contents of the scrollback rows</source>
        <translation>清除滾動行的內容</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1102"/>
        <source>Clear Screen</source>
        <translation>清除屏幕</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1103"/>
        <source>Clear the contents of the current screen</source>
        <translation>清除當前屏幕的內容</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1104"/>
        <source>Clear Screen and Scrollback</source>
        <translation>清除屏幕和滾動內容</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1105"/>
        <source>Clear the contents of the screen and scrollback</source>
        <translation>清除屏幕和滾動內容的內容</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1106"/>
        <source>Reset</source>
        <translation>重置</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1107"/>
        <source>Reset terminal emulator</source>
        <translation>重置終端仿真器</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1109"/>
        <source>Zoom In</source>
        <translation>放大</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1111"/>
        <source>Zoom In &lt;Ctrl+&quot;=&quot;&gt;</source>
        <translation>放大 &lt;Ctrl+&quot;=&quot;&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1113"/>
        <source>Zoom Out</source>
        <translation>縮小</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1115"/>
        <source>Zoom Out &lt;Ctrl+&quot;-&quot;&gt;</source>
        <translation>縮小 &lt;Ctrl+&quot;-&quot;&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1117"/>
        <location filename="../src/mainwindow.cpp" line="1119"/>
        <source>Zoom Reset</source>
        <translation>重置縮放</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1120"/>
        <source>Menu Bar</source>
        <translation>菜單欄</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1121"/>
        <source>Show/Hide Menu Bar &lt;Alt+U&gt;</source>
        <translation>顯示/隱藏菜單欄 &lt;Alt+U&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1124"/>
        <source>Show/Hide Tool Bar</source>
        <translation>顯示/隱藏工具欄</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1125"/>
        <source>Status Bar</source>
        <translation>狀態欄</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1126"/>
        <source>Show/Hide Status Bar</source>
        <translation>顯示/隱藏狀態欄</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1127"/>
        <source>Command Window</source>
        <translation>命令窗口</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1128"/>
        <source>Show/Hide Command Window</source>
        <translation>顯示/隱藏命令窗口</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1129"/>
        <source>Connect Bar</source>
        <translation>連接欄</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1130"/>
        <source>Show/Hide Connect Bar</source>
        <translation>顯示/隱藏連接欄</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1131"/>
        <source>Side Window</source>
        <translation>側邊窗口</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1132"/>
        <source>Show/Hide Side Window</source>
        <translation>顯示/隱藏側邊窗口</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1133"/>
        <source>Windows Transparency</source>
        <translation>窗口透明</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1134"/>
        <source>Enable/Disable alpha transparency</source>
        <translation>啟用/禁用alpha透明度</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1135"/>
        <source>Vertical Scroll Bar</source>
        <translation>垂直滾動條</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1136"/>
        <source>Show/Hide Vertical Scroll Bar</source>
        <translation>顯示/隱藏垂直滾動條</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1137"/>
        <source>Allways On Top</source>
        <translation>總在最前</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1138"/>
        <source>Show window always on top</source>
        <translation>總是顯示窗口在最上面</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1139"/>
        <source>Full Screen</source>
        <translation>全屏</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1140"/>
        <source>Toggle between full screen and normal mode &lt;Alt+Enter&gt;</source>
        <translation>在全屏模式和正常模式之間切換 &lt;Alt+Enter&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1143"/>
        <source>Session Options...</source>
        <translation>會話選項...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1145"/>
        <source>Configure session options</source>
        <translation>配置會話選項</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1146"/>
        <source>Global Options...</source>
        <translation>全局選項...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1148"/>
        <source>Configure global options</source>
        <translation>配置全局選項</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1149"/>
        <source>Real-time Save Options</source>
        <translation>實時保存選項</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1150"/>
        <source>Real-time save session options and global options</source>
        <translation>實時保存會話選項和全局選項</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1151"/>
        <source>Save Settings Now</source>
        <translation>立即保存設置</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1152"/>
        <source>Save options configuration now</source>
        <translation>立即保存選項配置</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1154"/>
        <source>Send ASCII...</source>
        <translation>發送ASCII...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1155"/>
        <source>Send ASCII file</source>
        <translation>發送ASCII文件</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1156"/>
        <source>Receive ASCII...</source>
        <translation>接收ASCII...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1157"/>
        <source>Receive ASCII file</source>
        <translation>接收ASCII文件</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1158"/>
        <source>Send Binary...</source>
        <translation>發送二進制...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1159"/>
        <source>Send Binary file</source>
        <translation>發送二進制文件</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1160"/>
        <source>Send Xmodem...</source>
        <translation>發送Xmodem...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1161"/>
        <source>Send a file using Xmodem</source>
        <translation>使用Xmodem發送文件</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1162"/>
        <source>Receive Xmodem...</source>
        <translation>接收Xmodem...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1163"/>
        <source>Receive a file using Xmodem</source>
        <translation>使用Xmodem接收文件</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1164"/>
        <source>Send Ymodem...</source>
        <translation>發送Ymodem...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1165"/>
        <source>Send a file using Ymodem</source>
        <translation>使用Ymodem發送文件</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1166"/>
        <source>Receive Ymodem...</source>
        <translation>接收Ymodem...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1167"/>
        <source>Receive a file using Ymodem</source>
        <translation>使用Ymodem接收文件</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1168"/>
        <source>Zmodem Upload List...</source>
        <translation>Zmodem上傳列表...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1169"/>
        <source>Display Zmodem file upload list</source>
        <translation>顯示Zmodem文件上傳列表</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1170"/>
        <source>Start Zmodem Upload</source>
        <translation>開始Zmodem上傳</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1171"/>
        <source>Start Zmodem file upload</source>
        <translation>開始Zmodem文件上傳</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1172"/>
        <source>Start TFTP Server</source>
        <translation>啟動TFTP服務器</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1173"/>
        <source>Start/Stop the TFTP server</source>
        <translation>啟動/停止TFTP服務器</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1175"/>
        <source>Run...</source>
        <translation>運行...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1176"/>
        <source>Run a script</source>
        <translation>運行腳本</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1177"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1178"/>
        <source>Cancel script execution</source>
        <translation>取消腳本執行</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1179"/>
        <source>Start Recording Script</source>
        <translation>開始記錄腳本</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1180"/>
        <source>Start recording script</source>
        <translation>開始記錄腳本</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1181"/>
        <source>Stop Recording Script...</source>
        <translation>停止記錄腳本...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1182"/>
        <source>Stop recording script</source>
        <translation>停止記錄腳本</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1183"/>
        <source>Cancel Recording Script</source>
        <translation>取消記錄腳本</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1184"/>
        <source>Cancel recording script</source>
        <translation>取消記錄腳本</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1186"/>
        <source>Add Bookmark</source>
        <translation>添加書籤</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1187"/>
        <source>Add a bookmark</source>
        <translation>添加書籤</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1188"/>
        <location filename="../src/mainwindow.cpp" line="2350"/>
        <source>Remove Bookmark</source>
        <translation>刪除書籤</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1189"/>
        <source>Remove a bookmark</source>
        <translation>刪除書籤</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1190"/>
        <location filename="../src/mainwindow.cpp" line="2377"/>
        <source>Clean All Bookmark</source>
        <translation>清除所有書籤</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1191"/>
        <source>Clean all bookmark</source>
        <translation>清除所有書籤</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1193"/>
        <source>Keymap Manager</source>
        <translation>鍵盤映射管理器</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1194"/>
        <source>Display keymap editor</source>
        <translation>顯示鍵盤映射編輯器</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1195"/>
        <source>Create Public Key...</source>
        <translation>創建公鑰...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1196"/>
        <source>Create a public key</source>
        <translation>創建公鑰</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1197"/>
        <source>Publickey Manager</source>
        <translation>公鑰管理器</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1198"/>
        <source>Display publickey manager</source>
        <translation>顯示公鑰管理器</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1242"/>
        <source>Laboratory</source>
        <translation>實驗室</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1245"/>
        <location filename="../src/mainwindow.cpp" line="2395"/>
        <source>SSH Scanning</source>
        <translation>SSH掃描</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1246"/>
        <source>Display SSH scanning dialog</source>
        <translation>顯示SSH掃描對話框</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3399"/>
        <source>Version</source>
        <translation>版本</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3401"/>
        <source>Commit</source>
        <translation>提交</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3403"/>
        <source>Date</source>
        <translation>日期</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3405"/>
        <source>Author</source>
        <translation>作者</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3407"/>
        <source>Website</source>
        <translation>網站</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1200"/>
        <source>Tab</source>
        <translation>標籤</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1201"/>
        <source>Arrange sessions in tabs</source>
        <translation>在標籤中排列會話</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1202"/>
        <source>Tile</source>
        <translation>平鋪</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1203"/>
        <source>Arrange sessions in non-overlapping tiles</source>
        <translation>在不重疊的平鋪中排列會話</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1204"/>
        <source>Cascade</source>
        <translation>級聯</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1205"/>
        <source>Arrange sessions to overlap each other</source>
        <translation>安排會話互相重疊</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1207"/>
        <source>Simplified Chinese</source>
        <translation>简体中文</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1208"/>
        <source>Switch to Simplified Chinese</source>
        <translation>切换到简体中文</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1209"/>
        <source>Traditional Chinese</source>
        <translation>繁體中文</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1210"/>
        <source>Switch to Traditional Chinese</source>
        <translation>切換到繁體中文</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1211"/>
        <source>Russian</source>
        <translation>Русский</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1212"/>
        <source>Switch to Russian</source>
        <translation>Переключиться на русский</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1213"/>
        <source>Korean</source>
        <translation>한국어</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1214"/>
        <source>Switch to Korean</source>
        <translation>한국어로 전환</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1215"/>
        <source>Japanese</source>
        <translation>日本語</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1216"/>
        <source>Switch to Japanese</source>
        <translation>日本語に切り替える</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1217"/>
        <source>French</source>
        <translation>français</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1218"/>
        <source>Switch to French</source>
        <translation>Passer au français</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1219"/>
        <source>Spanish</source>
        <translation>español</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1220"/>
        <source>Switch to Spanish</source>
        <translation>Cambiar a español</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1221"/>
        <source>English</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1222"/>
        <source>Switch to English</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1224"/>
        <source>Light</source>
        <translation>明亮</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1225"/>
        <source>Switch to light theme</source>
        <translation>切換到明亮的主題</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1226"/>
        <source>Dark</source>
        <translation>暗黑</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1227"/>
        <source>Switch to dark theme</source>
        <translation>切換到暗黑主題</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1231"/>
        <source>Display help</source>
        <translation>顯示幫助</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1232"/>
        <source>Check Update</source>
        <translation>檢查更新</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1234"/>
        <source>Check for updates</source>
        <translation>檢查更新</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1235"/>
        <location filename="../src/mainwindow.cpp" line="3397"/>
        <source>About</source>
        <translation>關於</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1237"/>
        <source>Display about dialog</source>
        <translation>顯示關於對話框</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1238"/>
        <source>About Qt</source>
        <translation>關於Qt</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1240"/>
        <source>Display about Qt dialog</source>
        <translation>顯示關於Qt對話框</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1247"/>
        <source>Plugin Info</source>
        <translation>插件信息</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1248"/>
        <source>Display plugin information dialog</source>
        <translation>顯示插件信息對話框</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2134"/>
        <source>PrintScreen saved to %1</source>
        <translation>PrintScreen保存到%1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2146"/>
        <source>Save Screenshot</source>
        <translation>保存屏幕截圖</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2146"/>
        <source>Image Files (*.jpg)</source>
        <translation>圖像文件(*.jpg)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2151"/>
        <source>Screenshot saved to %1</source>
        <translation>屏幕截圖保存到%1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2162"/>
        <source>Save Session Export</source>
        <translation>保存會話導出</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2162"/>
        <source>Text Files (*.txt);;HTML Files (*.html)</source>
        <translation>文本文件(*.txt);;HTML文件(*.html)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2165"/>
        <source>Text Files (*.txt)</source>
        <translation>文本文件(*.txt)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2168"/>
        <source>HTML Files (*.html)</source>
        <translation>HTML文件(*.html)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2177"/>
        <source>Session Export saved to %1</source>
        <translation>會話導出保存到%1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2179"/>
        <source>Session Export failed to save to %1</source>
        <translation>會話導出保存到%1失敗</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2323"/>
        <source>Select a directory</source>
        <translation>選擇一個目錄</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2350"/>
        <source>Select a bookmark</source>
        <translation>選擇一個書籤</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2377"/>
        <source>Are you sure to clean all bookmark?</source>
        <translation>您確定要清除所有書籤嗎？</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2395"/>
        <source>Port</source>
        <translation>端口</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2552"/>
        <source>Video background is enabled, please enable animation in global options (more system resources) or change background image.</source>
        <translation>視頻背景已啟用，請在全局選項中啟用動畫（更多系統資源）或更改背景圖像。</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2747"/>
        <source>Session information get failed.</source>
        <translation>會話信息獲取失敗。</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2830"/>
        <source>Telnet - </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2831"/>
        <source>Telnet</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2863"/>
        <source>Serial - </source>
        <translation>串口 - </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2864"/>
        <source>Serial</source>
        <translation>串口</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2895"/>
        <source>Raw - </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2896"/>
        <source>Raw</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2927"/>
        <source>NamePipe - </source>
        <translation>命名管道 - </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2928"/>
        <source>NamePipe</source>
        <translation>命名管道</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2996"/>
        <location filename="../src/mainwindow.cpp" line="3000"/>
        <source>Local Shell</source>
        <translation>本地Shell</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2998"/>
        <source>Local Shell - </source>
        <translation>本地Shell - </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3115"/>
        <source>Are you sure to disconnect this session?</source>
        <translation>您確定要斷開此會話嗎？</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3417"/>
        <source>Global Shortcuts:</source>
        <translation>全局快捷鍵：</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3418"/>
        <source>show/hide menu bar</source>
        <translation>顯示/隱藏菜單欄</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3419"/>
        <source>connect to LocalShell</source>
        <translation>連接到本地Shell</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3420"/>
        <source>clone current session</source>
        <translation>克隆當前會話</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3421"/>
        <source>switch ui to STD mode</source>
        <translation>切換ui到STD模式</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3422"/>
        <source>switch ui to MINI mode</source>
        <translation>切換ui到MINI模式</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3423"/>
        <source>switch to previous session</source>
        <translation>切換到上一個會話</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3424"/>
        <source>switch to next session</source>
        <translation>切換到下一個會話</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3425"/>
        <source>switch to session [num]</source>
        <translation>切換到會話[num]</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3426"/>
        <source>Go to line start</source>
        <translation>跳轉到行首</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3427"/>
        <source>Go to line end</source>
        <translation>跳轉到行尾</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3451"/>
        <source>There are sessions that have not yet been unlocked, please unlock them first.</source>
        <translation>有會話尚未解鎖，請先解鎖它們。</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3456"/>
        <source>Are you sure to quit?</source>
        <translation>您確定要退出嗎？</translation>
    </message>
</context>
<context>
    <name>CommandWidget</name>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="35"/>
        <source>Send commands to active session</source>
        <translation>發送命令到活動會話</translation>
    </message>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="71"/>
        <source>ASCII</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="84"/>
        <source>HEX</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="104"/>
        <source>time</source>
        <translation>時間</translation>
    </message>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="117"/>
        <source>ms</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="135"/>
        <source>Auto Send</source>
        <translation>自動發送</translation>
    </message>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="142"/>
        <source>Send</source>
        <translation>發送</translation>
    </message>
</context>
<context>
    <name>EmptyTabWidget</name>
    <message>
        <location filename="../src/sessiontab/sessiontab.cpp" line="69"/>
        <source>No session</source>
        <translation>沒有會話</translation>
    </message>
</context>
<context>
    <name>GlobalOptionsAdvancedWidget</name>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="25"/>
        <source>Config File</source>
        <translation>配置文件</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="56"/>
        <source>Translate Service</source>
        <translation>翻譯服務</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="67"/>
        <source>Google Translate</source>
        <translation>谷歌翻譯</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="72"/>
        <source>Baidu Translate</source>
        <translation>百度翻譯</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="77"/>
        <source>Microsoft Translate</source>
        <translation>微軟翻譯</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="87"/>
        <source>Terminal background support animation</source>
        <translation>終端背景支持動畫</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="94"/>
        <source>NativeUI(Effective after restart)</source>
        <translation>原生UI(重啟後生效)</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="101"/>
        <source>Github Copilot</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>GlobalOptionsAppearanceWidget</name>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="17"/>
        <source>Color Schemes</source>
        <translation>配色方案</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="27"/>
        <source>Font</source>
        <translation>字體</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="45"/>
        <source>Series</source>
        <translation>系列</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="65"/>
        <source>Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="93"/>
        <source>Background image</source>
        <translation>背景圖片</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="115"/>
        <source>Clear</source>
        <translation>清除</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="124"/>
        <source>Background mode</source>
        <translation>背景模式</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="131"/>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="143"/>
        <source>Stretch</source>
        <translation>拉伸</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="138"/>
        <source>None</source>
        <translation>無</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="148"/>
        <source>Zoom</source>
        <translation>縮放</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="153"/>
        <source>Fit</source>
        <translation>適應</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="158"/>
        <source>Center</source>
        <translation>居中</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="163"/>
        <source>Tile</source>
        <translation>平鋪</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="171"/>
        <source>Background opacity</source>
        <translation>背景透明度</translation>
    </message>
</context>
<context>
    <name>GlobalOptionsGeneralWidget</name>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="19"/>
        <source>New tab mode</source>
        <translation>新標籤模式</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="33"/>
        <source>New session</source>
        <translation>新會話</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="38"/>
        <source>Clone session</source>
        <translation>克隆會話</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="43"/>
        <source>LocalShell session</source>
        <translation>本地Shell會話</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="55"/>
        <source>New tab workpath</source>
        <translation>新建標籤工作路徑</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="84"/>
        <source>Tab title mode</source>
        <translation>標籤標題模式</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="98"/>
        <source>Brief</source>
        <translation>簡潔</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="103"/>
        <source>Full</source>
        <translation>完整</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="108"/>
        <source>Scroll</source>
        <translation>滾動</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="126"/>
        <source>Tab Title Width</source>
        <translation>標籤標題寬度</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="155"/>
        <source>Tab Preview</source>
        <translation>標籤預覽</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="171"/>
        <source>Preview Width</source>
        <translation>預覽寬度</translation>
    </message>
</context>
<context>
    <name>GlobalOptionsTerminalWidget</name>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="17"/>
        <source>Scrollback lines</source>
        <translation>滾動行數</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="34"/>
        <source>Cursor Shape</source>
        <translation>光標形狀</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="42"/>
        <source>Block</source>
        <translation>塊</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="47"/>
        <source>Underline</source>
        <translation>下劃線</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="52"/>
        <source>IBeam</source>
        <translation>IBeam</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="60"/>
        <source>Cursor Blink</source>
        <translation>光標閃爍</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="70"/>
        <source>Word Characters</source>
        <translation>單詞字符</translation>
    </message>
</context>
<context>
    <name>GlobalOptionsWindow</name>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.ui" line="14"/>
        <source>Global Options</source>
        <translation>全局選項</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="164"/>
        <source>Select Background Image</source>
        <translation>選擇背景圖片</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="164"/>
        <source>Image Files (*.png *.jpg *.jpeg *.bmp *.gif);;Video Files (*.mp4 *.avi *.mkv *.mov)</source>
        <translation>圖片文件 (*.png *.jpg *.jpeg *.bmp *.gif);;視頻文件 (*.mp4 *.avi *.mkv *.mov)</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="202"/>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="207"/>
        <source>Information</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="202"/>
        <source>This feature needs more system resources, please use it carefully!</source>
        <translation>此功能需要更多系統資源，請謹慎使用！</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="207"/>
        <source>This feature is not implemented yet!</source>
        <translation>此功能尚未實現！</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="229"/>
        <source>General</source>
        <translation>常規</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="229"/>
        <source>Appearance</source>
        <translation>外觀</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="229"/>
        <source>Terminal</source>
        <translation>終端</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="229"/>
        <source>Window</source>
        <translation>窗口</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="229"/>
        <source>Advanced</source>
        <translation>高級</translation>
    </message>
</context>
<context>
    <name>GlobalOptionsWindowWidget</name>
    <message>
        <location filename="../src/globaloptions/globaloptionswindowwidget.ui" line="17"/>
        <source>Transparent window</source>
        <translation>透明窗口</translation>
    </message>
</context>
<context>
    <name>HexViewWindow</name>
    <message>
        <source>ASCII Text...</source>
        <translation type="vanished">ASCII文本...</translation>
    </message>
    <message>
        <source>clear</source>
        <translation type="vanished">清除</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.ui" line="20"/>
        <source>Hex View</source>
        <translation>十六進制視圖</translation>
    </message>
    <message>
        <source>Information</source>
        <translation type="vanished">信息</translation>
    </message>
    <message>
        <source>Will send Hex:
</source>
        <translation type="vanished">將發送十六進制:
</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="71"/>
        <source>Copy</source>
        <translation>複製</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="79"/>
        <source>Copy Hex</source>
        <translation>複製HEX</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="87"/>
        <source>Dump</source>
        <translation>轉儲</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="91"/>
        <source>Save As</source>
        <translation>另存為</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="91"/>
        <source>Binary File (*.bin)</source>
        <translation>二進制文件 (*.bin)</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="99"/>
        <source>Error</source>
        <translation>錯誤</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="99"/>
        <source>Failed to save file!</source>
        <translation>無法保存文件！</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="104"/>
        <source>Clear</source>
        <translation>清除</translation>
    </message>
</context>
<context>
    <name>Konsole::Session</name>
    <message>
        <location filename="../lib/qtermwidget/Session.cpp" line="317"/>
        <source>Bell in session &apos;%1&apos;</source>
        <translation>會話 &apos;%1&apos; 中的鈴聲</translation>
    </message>
</context>
<context>
    <name>Konsole::TerminalDisplay</name>
    <message>
        <location filename="../lib/qtermwidget/TerminalDisplay.cpp" line="1383"/>
        <source>Size: XXX x XXX</source>
        <translation>大小: XXX x XXX</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/TerminalDisplay.cpp" line="1395"/>
        <source>Size: %1 x %2</source>
        <translation>大小: %1 x %2</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/TerminalDisplay.cpp" line="2942"/>
        <source>Paste multiline text</source>
        <translation>粘貼多行文本</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/TerminalDisplay.cpp" line="2943"/>
        <source>Are you sure you want to paste this text?</source>
        <translation>您確定要粘貼此文本嗎？</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/TerminalDisplay.cpp" line="3426"/>
        <source>&lt;qt&gt;Output has been &lt;a href=&quot;http://en.wikipedia.org/wiki/Flow_control&quot;&gt;suspended&lt;/a&gt; by pressing Ctrl+S.  Press &lt;b&gt;Ctrl+Q&lt;/b&gt; to resume.&lt;/qt&gt;</source>
        <translation>&lt;qt&gt;通過按Ctrl+S已經&lt;a href=&quot;http://en.wikipedia.org/wiki/Flow_control&quot;&gt;暫停&lt;/a&gt;輸出。 按&lt;b&gt;Ctrl+Q&lt;/b&gt;恢復。&lt;/qt&gt;</translation>
    </message>
</context>
<context>
    <name>Konsole::Vt102Emulation</name>
    <message>
        <location filename="../lib/qtermwidget/Vt102Emulation.cpp" line="1113"/>
        <source>No keyboard translator available.  The information needed to convert key presses into characters to send to the terminal is missing.</source>
        <translation>沒有可用的鍵盤轉換器。 缺少將按鍵轉換為要發送到終端的字符所需的信息。</translation>
    </message>
</context>
<context>
    <name>LockSessionWindow</name>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.ui" line="14"/>
        <source>Lock Session</source>
        <translation>鎖定會話</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.ui" line="20"/>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="84"/>
        <source>Enter the password that will be used to unlock the session:</source>
        <translation>輸入將用於解鎖會話的密碼:</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.ui" line="32"/>
        <source>Password</source>
        <translation>密碼</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.ui" line="50"/>
        <source>Reenter Password</source>
        <translation>重新輸入密碼</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.ui" line="68"/>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="85"/>
        <source>Lock all sessions</source>
        <translation>鎖定所有會話</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.ui" line="75"/>
        <source>Lock all sessions in tab group</source>
        <translation>鎖定標籤組中的所有會話</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="49"/>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="55"/>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="66"/>
        <source>Error</source>
        <translation>錯誤</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="49"/>
        <source>Passwords do not match!</source>
        <translation>密碼不匹配！</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="55"/>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="66"/>
        <source>Password cannot be empty!</source>
        <translation>密碼不能為空！</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="98"/>
        <source>Enter the password that was used to lock the session:</source>
        <translation>輸入用於鎖定會話的密碼:</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="99"/>
        <source>Unlock all sessions</source>
        <translation>解鎖所有會話</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Tool Bar</source>
        <translation type="vanished">工具欄</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="vanished">警告</translation>
    </message>
    <message>
        <source>TFTP server bind error!</source>
        <translation type="vanished">TFTP服務器綁定錯誤！</translation>
    </message>
    <message>
        <source>TFTP server file error!</source>
        <translation type="vanished">TFTP服務器文件錯誤！</translation>
    </message>
    <message>
        <source>TFTP server network error!</source>
        <translation type="vanished">TFTP服務器網絡錯誤！</translation>
    </message>
    <message>
        <source>Unlock Session</source>
        <translation type="vanished">解鎖會話</translation>
    </message>
    <message>
        <source>Move to another Tab</source>
        <translation type="vanished">移動到另一個標籤</translation>
    </message>
    <message>
        <source>Floating Window</source>
        <translation type="vanished">浮動窗口</translation>
    </message>
    <message>
        <source>Copy Path</source>
        <translation type="vanished">複製路徑</translation>
    </message>
    <message>
        <source>No working folder!</source>
        <translation type="vanished">沒有工作文件夾！</translation>
    </message>
    <message>
        <source>Add Path to Bookmark</source>
        <translation type="vanished">添加路徑到書籤</translation>
    </message>
    <message>
        <source>Open Working Folder</source>
        <translation type="vanished">打開工作文件夾</translation>
    </message>
    <message>
        <source>Save Session</source>
        <translation type="vanished">保存會話</translation>
    </message>
    <message>
        <source>Enter Session Name</source>
        <translation type="vanished">輸入會話名稱</translation>
    </message>
    <message>
        <source>The session already exists, please rename the new session or cancel saving.</source>
        <translation type="vanished">會話已經存在，請重新命名新會話或取消保存。</translation>
    </message>
    <message>
        <source>Properties</source>
        <translation type="vanished">屬性</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">關閉</translation>
    </message>
    <message>
        <source>Close Others</source>
        <translation type="vanished">關閉其他</translation>
    </message>
    <message>
        <source>Close to the Right</source>
        <translation type="vanished">關閉到右側</translation>
    </message>
    <message>
        <source>Close All</source>
        <translation type="vanished">關閉所有</translation>
    </message>
    <message>
        <source>Highlight/Unhighlight</source>
        <translation type="vanished">高亮/取消高亮</translation>
    </message>
    <message>
        <source>Ready</source>
        <translation type="vanished">就緒</translation>
    </message>
    <message>
        <source>Open SFTP</source>
        <translation type="vanished">打開SFTP</translation>
    </message>
    <message>
        <source>No SFTP channel!</source>
        <translation type="vanished">沒有SFTP通道！</translation>
    </message>
    <message>
        <source>Session properties error!</source>
        <translation type="vanished">會話屬性錯誤！</translation>
    </message>
    <message>
        <source>Google Translate</source>
        <translation type="vanished">谷歌翻譯</translation>
    </message>
    <message>
        <source>Baidu Translate</source>
        <translation type="vanished">百度翻譯</translation>
    </message>
    <message>
        <source>Microsoft Translate</source>
        <translation type="vanished">微軟翻譯</translation>
    </message>
    <message>
        <source>Back to Main Window</source>
        <translation type="vanished">返回主窗口</translation>
    </message>
    <message>
        <source>Session Manager</source>
        <translation type="vanished">會話管理器</translation>
    </message>
    <message>
        <source>File</source>
        <translation type="vanished">文件</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="vanished">編輯</translation>
    </message>
    <message>
        <source>View</source>
        <translation type="vanished">查看</translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="vanished">選項</translation>
    </message>
    <message>
        <source>Transfer</source>
        <translation type="vanished">傳輸</translation>
    </message>
    <message>
        <source>Script</source>
        <translation type="vanished">腳本</translation>
    </message>
    <message>
        <source>Bookmark</source>
        <translation type="vanished">書籤</translation>
    </message>
    <message>
        <source>Tools</source>
        <translation type="vanished">工具</translation>
    </message>
    <message>
        <source>Window</source>
        <translation type="vanished">窗口</translation>
    </message>
    <message>
        <source>Language</source>
        <translation type="vanished">語言</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation type="vanished">主題</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">幫助</translation>
    </message>
    <message>
        <source>New Window</source>
        <translation type="vanished">新建窗口</translation>
    </message>
    <message>
        <source>Open a new window &lt;Ctrl+Shift+N&gt;</source>
        <translation type="vanished">打開新窗口 &lt;Ctrl+Shift+N&gt;</translation>
    </message>
    <message>
        <source>Connect...</source>
        <translation type="vanished">連接...</translation>
    </message>
    <message>
        <source>Connect to a host &lt;Alt+C&gt;</source>
        <translation type="vanished">連接到主機 &lt;Alt+C&gt;</translation>
    </message>
    <message>
        <source>Go to the Session Manager &lt;Alt+M&gt;</source>
        <translation type="vanished">轉到會話管理器 &lt;Alt+M&gt;</translation>
    </message>
    <message>
        <source>Quick Connect...</source>
        <translation type="vanished">快速連接...</translation>
    </message>
    <message>
        <source>Quick Connect to a host &lt;Alt+Q&gt;</source>
        <translation type="vanished">快速連接到主機 &lt;Alt+Q&gt;</translation>
    </message>
    <message>
        <source>Connect in Tab/Tile...</source>
        <translation type="vanished">在標籤/平鋪中連接...</translation>
    </message>
    <message>
        <source>Connect to a host in a new tab &lt;Alt+B&gt;</source>
        <translation type="vanished">在新標籤中連接到主機 &lt;Alt+B&gt;</translation>
    </message>
    <message>
        <source>Connect Local Shell</source>
        <translation type="vanished">連接本地shell</translation>
    </message>
    <message>
        <source>Connect to a local shell &lt;Alt+T&gt;</source>
        <translation type="vanished">連接到本地shell &lt;Alt+T&gt;</translation>
    </message>
    <message>
        <source>Reconnect</source>
        <translation type="vanished">重新連接</translation>
    </message>
    <message>
        <source>Reconnect current session</source>
        <translation type="vanished">重新連接當前會話</translation>
    </message>
    <message>
        <source>Reconnect All</source>
        <translation type="vanished">重新連接所有</translation>
    </message>
    <message>
        <source>Reconnect all sessions &lt;Alt+A&gt;</source>
        <translation type="vanished">重新連接所有會話 &lt;Alt+A&gt;</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation type="vanished">斷開連接</translation>
    </message>
    <message>
        <source>Disconnect current session</source>
        <translation type="vanished">斷開當前會話</translation>
    </message>
    <message>
        <source>Enter host &lt;Alt+R&gt; to connect</source>
        <translation type="vanished">輸入主機 &lt;Alt+R&gt; 連接</translation>
    </message>
    <message>
        <source>Disconnect All</source>
        <translation type="vanished">斷開所有</translation>
    </message>
    <message>
        <source>Disconnect all sessions</source>
        <translation type="vanished">斷開所有會話</translation>
    </message>
    <message>
        <source>Clone Session</source>
        <translation type="vanished">克隆會話</translation>
    </message>
    <message>
        <source>Clone current session &lt;Ctrl+Shift+T&gt;</source>
        <translation type="vanished">克隆當前會話 &lt;Ctrl+Shift+T&gt;</translation>
    </message>
    <message>
        <source>Lock Session</source>
        <translation type="vanished">鎖定會話</translation>
    </message>
    <message>
        <source>Lock/Unlock current session</source>
        <translation type="vanished">鎖定/解鎖當前會話</translation>
    </message>
    <message>
        <source>Log Session</source>
        <translation type="vanished">記錄日誌</translation>
    </message>
    <message>
        <source>Create a log file for current session</source>
        <translation type="vanished">為當前會話創建日誌文件</translation>
    </message>
    <message>
        <source>Raw Log Session</source>
        <translation type="vanished">記錄原始日誌</translation>
    </message>
    <message>
        <source>Create a raw log file for current session</source>
        <translation type="vanished">為當前會話創建原始日誌文件</translation>
    </message>
    <message>
        <source>Hex View</source>
        <translation type="vanished">十六進制視圖</translation>
    </message>
    <message>
        <source>Show/Hide Hex View for current session</source>
        <translation type="vanished">顯示/隱藏當前會話的十六進制視圖</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="vanished">退出</translation>
    </message>
    <message>
        <source>Quit the application</source>
        <translation type="vanished">退出應用程序</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation type="vanished">複製</translation>
    </message>
    <message>
        <source>Copy the selected text to the clipboard &lt;Command+C&gt;</source>
        <translation type="vanished">將所選文本複製到剪貼板 &lt;Command+C&gt;</translation>
    </message>
    <message>
        <source>Copy the selected text to the clipboard &lt;Ctrl+Ins&gt;</source>
        <translation type="vanished">將所選文本複製到剪貼板 &lt;Ctrl+Ins&gt;</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation type="vanished">粘貼</translation>
    </message>
    <message>
        <source>Paste the clipboard text to the current session &lt;Command+V&gt;</source>
        <translation type="vanished">將剪貼板文本粘貼到當前會話 &lt;Command+V&gt;</translation>
    </message>
    <message>
        <source>Paste the clipboard text to the current session &lt;Shift+Ins&gt;</source>
        <translation type="vanished">將剪貼板文本粘貼到當前會話 &lt;Shift+Ins&gt;</translation>
    </message>
    <message>
        <source>Copy and Paste</source>
        <translation type="vanished">複製和粘貼</translation>
    </message>
    <message>
        <source>Copy the selected text to the clipboard and paste to the current session</source>
        <translation type="vanished">將所選文本複製到剪貼板並粘貼到當前會話</translation>
    </message>
    <message>
        <source>Select All</source>
        <translation type="vanished">全選</translation>
    </message>
    <message>
        <source>Select all text in the current session &lt;Ctrl+Shift+A&gt;</source>
        <translation type="vanished">選擇當前會話中的所有文本 &lt;Ctrl+Shift+A&gt;</translation>
    </message>
    <message>
        <source>Find...</source>
        <translation type="vanished">查找...</translation>
    </message>
    <message>
        <source>Find text in the current session &lt;Ctrl+F&gt;</source>
        <translation type="vanished">在當前會話中查找文本 &lt;Ctrl+F&gt;</translation>
    </message>
    <message>
        <source>Print Screen</source>
        <translation type="vanished">打印屏幕</translation>
    </message>
    <message>
        <source>Print current screen</source>
        <translation type="vanished">打印當前屏幕</translation>
    </message>
    <message>
        <source>Screen Shot</source>
        <translation type="vanished">屏幕截圖</translation>
    </message>
    <message>
        <source>Screen shot current screen &lt;Alt+P&gt;</source>
        <translation type="vanished">截圖當前屏幕 &lt;Alt+P&gt;</translation>
    </message>
    <message>
        <source>Session Export</source>
        <translation type="vanished">會話導出</translation>
    </message>
    <message>
        <source>Export current session to a file &lt;Alt+O&gt;</source>
        <translation type="vanished">將當前會話導出到文件 &lt;Alt+O&gt;</translation>
    </message>
    <message>
        <source>Clear Scrollback</source>
        <translation type="vanished">清除滾動內容</translation>
    </message>
    <message>
        <source>Clear the contents of the scrollback rows</source>
        <translation type="vanished">清除滾動行的內容</translation>
    </message>
    <message>
        <source>Clear Screen</source>
        <translation type="vanished">清除屏幕</translation>
    </message>
    <message>
        <source>Clear the contents of the current screen</source>
        <translation type="vanished">清除當前屏幕的內容</translation>
    </message>
    <message>
        <source>Clear Screen and Scrollback</source>
        <translation type="vanished">清除屏幕和滾動內容</translation>
    </message>
    <message>
        <source>Clear the contents of the screen and scrollback</source>
        <translation type="vanished">清除屏幕和滾動內容的內容</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation type="vanished">重置</translation>
    </message>
    <message>
        <source>Reset terminal emulator</source>
        <translation type="vanished">重置終端仿真器</translation>
    </message>
    <message>
        <source>Zoom In</source>
        <translation type="vanished">放大</translation>
    </message>
    <message>
        <source>Zoom In &lt;Ctrl+&quot;=&quot;&gt;</source>
        <translation type="vanished">放大 &lt;Ctrl+&quot;=&quot;&gt;</translation>
    </message>
    <message>
        <source>Zoom Out</source>
        <translation type="vanished">縮小</translation>
    </message>
    <message>
        <source>Zoom Out &lt;Ctrl+&quot;-&quot;&gt;</source>
        <translation type="vanished">縮小 &lt;Ctrl+&quot;-&quot;&gt;</translation>
    </message>
    <message>
        <source>Zoom Reset</source>
        <translation type="vanished">重置縮放</translation>
    </message>
    <message>
        <source>Menu Bar</source>
        <translation type="vanished">菜單欄</translation>
    </message>
    <message>
        <source>Show/Hide Menu Bar &lt;Alt+U&gt;</source>
        <translation type="vanished">顯示/隱藏菜單欄 &lt;Alt+U&gt;</translation>
    </message>
    <message>
        <source>Show/Hide Tool Bar</source>
        <translation type="vanished">顯示/隱藏工具欄</translation>
    </message>
    <message>
        <source>Status Bar</source>
        <translation type="vanished">狀態欄</translation>
    </message>
    <message>
        <source>Show/Hide Status Bar</source>
        <translation type="vanished">顯示/隱藏狀態欄</translation>
    </message>
    <message>
        <source>Command Window</source>
        <translation type="vanished">命令窗口</translation>
    </message>
    <message>
        <source>Show/Hide Command Window</source>
        <translation type="vanished">顯示/隱藏命令窗口</translation>
    </message>
    <message>
        <source>Connect Bar</source>
        <translation type="vanished">連接欄</translation>
    </message>
    <message>
        <source>Show/Hide Connect Bar</source>
        <translation type="vanished">顯示/隱藏連接欄</translation>
    </message>
    <message>
        <source>Side Window</source>
        <translation type="vanished">側邊窗口</translation>
    </message>
    <message>
        <source>Show/Hide Side Window</source>
        <translation type="vanished">顯示/隱藏側邊窗口</translation>
    </message>
    <message>
        <source>Windows Transparency</source>
        <translation type="vanished">窗口透明</translation>
    </message>
    <message>
        <source>Enable/Disable alpha transparency</source>
        <translation type="vanished">啟用/禁用alpha透明度</translation>
    </message>
    <message>
        <source>Vertical Scroll Bar</source>
        <translation type="vanished">垂直滾動條</translation>
    </message>
    <message>
        <source>Show/Hide Vertical Scroll Bar</source>
        <translation type="vanished">顯示/隱藏垂直滾動條</translation>
    </message>
    <message>
        <source>Allways On Top</source>
        <translation type="vanished">總在最前</translation>
    </message>
    <message>
        <source>Show window always on top</source>
        <translation type="vanished">總是顯示窗口在最上面</translation>
    </message>
    <message>
        <source>Full Screen</source>
        <translation type="vanished">全屏</translation>
    </message>
    <message>
        <source>Toggle between full screen and normal mode &lt;Alt+Enter&gt;</source>
        <translation type="vanished">在全屏模式和正常模式之間切換 &lt;Alt+Enter&gt;</translation>
    </message>
    <message>
        <source>Session Options...</source>
        <translation type="vanished">會話選項...</translation>
    </message>
    <message>
        <source>Configure session options</source>
        <translation type="vanished">配置會話選項</translation>
    </message>
    <message>
        <source>Global Options...</source>
        <translation type="vanished">全局選項...</translation>
    </message>
    <message>
        <source>Configure global options</source>
        <translation type="vanished">配置全局選項</translation>
    </message>
    <message>
        <source>Real-time Save Options</source>
        <translation type="vanished">實時保存選項</translation>
    </message>
    <message>
        <source>Real-time save session options and global options</source>
        <translation type="vanished">實時保存會話選項和全局選項</translation>
    </message>
    <message>
        <source>Save Settings Now</source>
        <translation type="vanished">立即保存設置</translation>
    </message>
    <message>
        <source>Save options configuration now</source>
        <translation type="vanished">立即保存選項配置</translation>
    </message>
    <message>
        <source>Send ASCII...</source>
        <translation type="vanished">發送ASCII...</translation>
    </message>
    <message>
        <source>Send ASCII file</source>
        <translation type="vanished">發送ASCII文件</translation>
    </message>
    <message>
        <source>Receive ASCII...</source>
        <translation type="vanished">接收ASCII...</translation>
    </message>
    <message>
        <source>Receive ASCII file</source>
        <translation type="vanished">接收ASCII文件</translation>
    </message>
    <message>
        <source>Send Binary...</source>
        <translation type="vanished">發送二進制...</translation>
    </message>
    <message>
        <source>Send Binary file</source>
        <translation type="vanished">發送二進制文件</translation>
    </message>
    <message>
        <source>Send Xmodem...</source>
        <translation type="vanished">發送Xmodem...</translation>
    </message>
    <message>
        <source>Send a file using Xmodem</source>
        <translation type="vanished">使用Xmodem發送文件</translation>
    </message>
    <message>
        <source>Receive Xmodem...</source>
        <translation type="vanished">接收Xmodem...</translation>
    </message>
    <message>
        <source>Receive a file using Xmodem</source>
        <translation type="vanished">使用Xmodem接收文件</translation>
    </message>
    <message>
        <source>Send Ymodem...</source>
        <translation type="vanished">發送Ymodem...</translation>
    </message>
    <message>
        <source>Send a file using Ymodem</source>
        <translation type="vanished">使用Ymodem發送文件</translation>
    </message>
    <message>
        <source>Receive Ymodem...</source>
        <translation type="vanished">接收Ymodem...</translation>
    </message>
    <message>
        <source>Receive a file using Ymodem</source>
        <translation type="vanished">使用Ymodem接收文件</translation>
    </message>
    <message>
        <source>Zmodem Upload List...</source>
        <translation type="vanished">Zmodem上傳列表...</translation>
    </message>
    <message>
        <source>Display Zmodem file upload list</source>
        <translation type="vanished">顯示Zmodem文件上傳列表</translation>
    </message>
    <message>
        <source>Start Zmodem Upload</source>
        <translation type="vanished">開始Zmodem上傳</translation>
    </message>
    <message>
        <source>Start Zmodem file upload</source>
        <translation type="vanished">開始Zmodem文件上傳</translation>
    </message>
    <message>
        <source>Start TFTP Server</source>
        <translation type="vanished">啟動TFTP服務器</translation>
    </message>
    <message>
        <source>Start/Stop the TFTP server</source>
        <translation type="vanished">啟動/停止TFTP服務器</translation>
    </message>
    <message>
        <source>Run...</source>
        <translation type="vanished">運行...</translation>
    </message>
    <message>
        <source>Run a script</source>
        <translation type="vanished">運行腳本</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Cancel script execution</source>
        <translation type="vanished">取消腳本執行</translation>
    </message>
    <message>
        <source>Start Recording Script</source>
        <translation type="vanished">開始記錄腳本</translation>
    </message>
    <message>
        <source>Start recording script</source>
        <translation type="vanished">開始記錄腳本</translation>
    </message>
    <message>
        <source>Stop Recording Script...</source>
        <translation type="vanished">停止記錄腳本...</translation>
    </message>
    <message>
        <source>Stop recording script</source>
        <translation type="vanished">停止記錄腳本</translation>
    </message>
    <message>
        <source>Cancel Recording Script</source>
        <translation type="vanished">取消記錄腳本</translation>
    </message>
    <message>
        <source>Cancel recording script</source>
        <translation type="vanished">取消記錄腳本</translation>
    </message>
    <message>
        <source>Add Bookmark</source>
        <translation type="vanished">添加書籤</translation>
    </message>
    <message>
        <source>Add a bookmark</source>
        <translation type="vanished">添加書籤</translation>
    </message>
    <message>
        <source>Remove Bookmark</source>
        <translation type="vanished">刪除書籤</translation>
    </message>
    <message>
        <source>Remove a bookmark</source>
        <translation type="vanished">刪除書籤</translation>
    </message>
    <message>
        <source>Clean All Bookmark</source>
        <translation type="vanished">清除所有書籤</translation>
    </message>
    <message>
        <source>Clean all bookmark</source>
        <translation type="vanished">清除所有書籤</translation>
    </message>
    <message>
        <source>Keymap Manager</source>
        <translation type="vanished">鍵盤映射管理器</translation>
    </message>
    <message>
        <source>Display keymap editor</source>
        <translation type="vanished">顯示鍵盤映射編輯器</translation>
    </message>
    <message>
        <source>Create Public Key...</source>
        <translation type="vanished">創建公鑰...</translation>
    </message>
    <message>
        <source>Create a public key</source>
        <translation type="vanished">創建公鑰</translation>
    </message>
    <message>
        <source>Publickey Manager</source>
        <translation type="vanished">公鑰管理器</translation>
    </message>
    <message>
        <source>Display publickey manager</source>
        <translation type="vanished">顯示公鑰管理器</translation>
    </message>
    <message>
        <source>SSH Scanning</source>
        <translation type="vanished">SSH掃描</translation>
    </message>
    <message>
        <source>Display SSH scanning dialog</source>
        <translation type="vanished">顯示SSH掃描對話框</translation>
    </message>
    <message>
        <source>Tab</source>
        <translation type="vanished">標籤</translation>
    </message>
    <message>
        <source>Arrange sessions in tabs</source>
        <translation type="vanished">在標籤中排列會話</translation>
    </message>
    <message>
        <source>Tile</source>
        <translation type="vanished">平鋪</translation>
    </message>
    <message>
        <source>Arrange sessions in non-overlapping tiles</source>
        <translation type="vanished">在不重疊的平鋪中排列會話</translation>
    </message>
    <message>
        <source>Cascade</source>
        <translation type="vanished">級聯</translation>
    </message>
    <message>
        <source>Arrange sessions to overlap each other</source>
        <translation type="vanished">安排會話互相重疊</translation>
    </message>
    <message>
        <source>Japanese</source>
        <translation type="vanished">日本語</translation>
    </message>
    <message>
        <source>Simplified Chinese</source>
        <translation type="vanished">简体中文</translation>
    </message>
    <message>
        <source>Switch to Simplified Chinese</source>
        <translation type="vanished">切换到简体中文</translation>
    </message>
    <message>
        <source>Traditional Chinese</source>
        <translation type="vanished">繁體中文</translation>
    </message>
    <message>
        <source>Switch to Traditional Chinese</source>
        <translation type="vanished">切換到繁體中文</translation>
    </message>
    <message>
        <source>Russian</source>
        <translation type="vanished">Русский</translation>
    </message>
    <message>
        <source>Switch to Russian</source>
        <translation type="vanished">Переключиться на русский</translation>
    </message>
    <message>
        <source>Korean</source>
        <translation type="vanished">한국어</translation>
    </message>
    <message>
        <source>Switch to Korean</source>
        <translation type="vanished">한국어로 전환</translation>
    </message>
    <message>
        <source>Switch to Japanese</source>
        <translation type="vanished">日本語に切り替える</translation>
    </message>
    <message>
        <source>French</source>
        <translation type="vanished">français</translation>
    </message>
    <message>
        <source>Switch to French</source>
        <translation type="vanished">Passer au français</translation>
    </message>
    <message>
        <source>Spanish</source>
        <translation type="vanished">español</translation>
    </message>
    <message>
        <source>Switch to Spanish</source>
        <translation type="vanished">Cambiar a español</translation>
    </message>
    <message>
        <source>Light</source>
        <translation type="vanished">明亮</translation>
    </message>
    <message>
        <source>Switch to light theme</source>
        <translation type="vanished">切換到明亮的主題</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation type="vanished">暗黑</translation>
    </message>
    <message>
        <source>Switch to dark theme</source>
        <translation type="vanished">切換到暗黑主題</translation>
    </message>
    <message>
        <source>Display help</source>
        <translation type="vanished">顯示幫助</translation>
    </message>
    <message>
        <source>Check Update</source>
        <translation type="vanished">檢查更新</translation>
    </message>
    <message>
        <source>Check for updates</source>
        <translation type="vanished">檢查更新</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">關於</translation>
    </message>
    <message>
        <source>Display about dialog</source>
        <translation type="vanished">顯示關於對話框</translation>
    </message>
    <message>
        <source>About Qt</source>
        <translation type="vanished">關於Qt</translation>
    </message>
    <message>
        <source>Display about Qt dialog</source>
        <translation type="vanished">顯示關於Qt對話框</translation>
    </message>
    <message>
        <source>PrintScreen saved to %1</source>
        <translation type="vanished">PrintScreen保存到%1</translation>
    </message>
    <message>
        <source>Save Screenshot</source>
        <translation type="vanished">保存屏幕截圖</translation>
    </message>
    <message>
        <source>Image Files (*.jpg)</source>
        <translation type="vanished">圖像文件(*.jpg)</translation>
    </message>
    <message>
        <source>Screenshot saved to %1</source>
        <translation type="vanished">屏幕截圖保存到%1</translation>
    </message>
    <message>
        <source>Save Session Export</source>
        <translation type="vanished">保存會話導出</translation>
    </message>
    <message>
        <source>Text Files (*.txt);;HTML Files (*.html)</source>
        <translation type="vanished">文本文件(*.txt);;HTML文件(*.html)</translation>
    </message>
    <message>
        <source>Text Files (*.txt)</source>
        <translation type="vanished">文本文件(*.txt)</translation>
    </message>
    <message>
        <source>HTML Files (*.html)</source>
        <translation type="vanished">HTML文件(*.html)</translation>
    </message>
    <message>
        <source>Session Export saved to %1</source>
        <translation type="vanished">會話導出保存到%1</translation>
    </message>
    <message>
        <source>Session Export failed to save to %1</source>
        <translation type="vanished">會話導出保存到%1失敗</translation>
    </message>
    <message>
        <source>Select a directory</source>
        <translation type="vanished">選擇一個目錄</translation>
    </message>
    <message>
        <source>Select a bookmark</source>
        <translation type="vanished">選擇一個書籤</translation>
    </message>
    <message>
        <source>Are you sure to clean all bookmark?</source>
        <translation type="vanished">您確定要清除所有書籤嗎？</translation>
    </message>
    <message>
        <source>Port</source>
        <translation type="vanished">端口</translation>
    </message>
    <message>
        <source>Video background is enabled, please enable animation in global options (more system resources) or change background image.</source>
        <translation type="vanished">視頻背景已啟用，請在全局選項中啟用動畫（更多系統資源）或更改背景圖像。</translation>
    </message>
    <message>
        <source>Session information get failed.</source>
        <translation type="vanished">會話信息獲取失敗。</translation>
    </message>
    <message>
        <source>Serial - </source>
        <translation type="vanished">串口 - </translation>
    </message>
    <message>
        <source>Serial</source>
        <translation type="vanished">串口</translation>
    </message>
    <message>
        <source>NamePipe - </source>
        <translation type="vanished">命名管道 - </translation>
    </message>
    <message>
        <source>NamePipe</source>
        <translation type="vanished">命名管道</translation>
    </message>
    <message>
        <source>Local Shell</source>
        <translation type="vanished">本地Shell</translation>
    </message>
    <message>
        <source>Local Shell - </source>
        <translation type="vanished">本地Shell - </translation>
    </message>
    <message>
        <source>Are you sure to disconnect this session?</source>
        <translation type="vanished">您確定要斷開此會話嗎？</translation>
    </message>
    <message>
        <source>Global Shortcuts:</source>
        <translation type="vanished">全局快捷鍵：</translation>
    </message>
    <message>
        <source>show/hide menu bar</source>
        <translation type="vanished">顯示/隱藏菜單欄</translation>
    </message>
    <message>
        <source>connect to LocalShell</source>
        <translation type="vanished">連接到本地Shell</translation>
    </message>
    <message>
        <source>clone current session</source>
        <translation type="vanished">克隆當前會話</translation>
    </message>
    <message>
        <source>switch ui to STD mode</source>
        <translation type="vanished">切換ui到STD模式</translation>
    </message>
    <message>
        <source>switch ui to MINI mode</source>
        <translation type="vanished">切換ui到MINI模式</translation>
    </message>
    <message>
        <source>switch to previous session</source>
        <translation type="vanished">切換到上一個會話</translation>
    </message>
    <message>
        <source>switch to next session</source>
        <translation type="vanished">切換到下一個會話</translation>
    </message>
    <message>
        <source>switch to session [num]</source>
        <translation type="vanished">切換到會話[num]</translation>
    </message>
    <message>
        <source>Go to line start</source>
        <translation type="vanished">跳轉到行首</translation>
    </message>
    <message>
        <source>Go to line end</source>
        <translation type="vanished">跳轉到行尾</translation>
    </message>
    <message>
        <source>There are sessions that have not yet been unlocked, please unlock them first.</source>
        <translation type="vanished">有會話尚未解鎖，請先解鎖它們。</translation>
    </message>
    <message>
        <source>Are you sure to quit?</source>
        <translation type="vanished">您確定要退出嗎？</translation>
    </message>
</context>
<context>
    <name>NetScanWindow</name>
    <message>
        <location filename="../src/netscanwindow/netscanwindow.ui" line="14"/>
        <source>NetScan</source>
        <translation>網絡掃描</translation>
    </message>
</context>
<context>
    <name>OneStepWindow</name>
    <message>
        <source>Port</source>
        <translation type="obsolete">端口</translation>
    </message>
    <message>
        <source>UserName</source>
        <translation type="obsolete">用戶名</translation>
    </message>
</context>
<context>
    <name>PluginInfoWindow</name>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.ui" line="14"/>
        <source>Plugin Info</source>
        <translation>插件信息</translation>
    </message>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.ui" line="28"/>
        <source>API version</source>
        <translation>API版本</translation>
    </message>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.ui" line="38"/>
        <source>Install plugin</source>
        <translation>安裝插件</translation>
    </message>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="40"/>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="86"/>
        <source>Name</source>
        <translation>名稱</translation>
    </message>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="40"/>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="86"/>
        <source>Version</source>
        <translation>版本</translation>
    </message>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="40"/>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="86"/>
        <source>API Version</source>
        <translation>API版本</translation>
    </message>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="40"/>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="86"/>
        <source>Enable</source>
        <translation>啟用</translation>
    </message>
</context>
<context>
    <name>PluginViewerHomeWidget</name>
    <message>
        <location filename="../src/pluginviewerwidget/pluginviewerhomewidget.cpp" line="18"/>
        <source>Welcome to use</source>
        <translation>歡迎使用</translation>
    </message>
    <message>
        <location filename="../src/pluginviewerwidget/pluginviewerhomewidget.cpp" line="20"/>
        <source>QuardCRT plugin system!</source>
        <translation>QuardCRT插件系統！</translation>
    </message>
</context>
<context>
    <name>PluginViewerWidget</name>
    <message>
        <location filename="../src/pluginviewerwidget/pluginviewerwidget.cpp" line="37"/>
        <source>Home</source>
        <translation>主頁</translation>
    </message>
</context>
<context>
    <name>QCustomFileSystemModel</name>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="155"/>
        <source>Directory</source>
        <translation>目錄</translation>
    </message>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="155"/>
        <source>File</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="159"/>
        <source>Loading...</source>
        <translation>正在加載...</translation>
    </message>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="184"/>
        <source>Name</source>
        <translation>名稱</translation>
    </message>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="186"/>
        <source>Type</source>
        <translation>類型</translation>
    </message>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="188"/>
        <source>Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="190"/>
        <source>Last Modified</source>
        <translation>最後修改</translation>
    </message>
</context>
<context>
    <name>QKeychain::DeletePasswordJobPrivate</name>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="173"/>
        <source>Could not open keystore</source>
        <translation>無法打開密鑰庫</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="179"/>
        <source>Could not remove private key from keystore</source>
        <translation>無法從密鑰庫中刪除私鑰</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="584"/>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="592"/>
        <source>Unknown error</source>
        <translation>未知錯誤</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="613"/>
        <source>Could not open wallet: %1; %2</source>
        <translation>無法打開錢包：%1；%2</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="104"/>
        <source>Password entry not found</source>
        <translation>找不到密碼條目</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="108"/>
        <source>Could not decrypt data</source>
        <translation>無法解密數據</translation>
    </message>
</context>
<context>
    <name>QKeychain::Job</name>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="29"/>
        <source>No error</source>
        <translation>無錯誤</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="31"/>
        <source>The specified item could not be found in the keychain</source>
        <translation>在鑰匙圈中找不到指定的項目</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="33"/>
        <source>User canceled the operation</source>
        <translation>用戶取消了操作</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="35"/>
        <source>User interaction is not allowed</source>
        <translation>不允許用戶交互</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="37"/>
        <source>No keychain is available. You may need to restart your computer</source>
        <translation>沒有可用的鑰匙圈。您可能需要重新啟動計算機</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="39"/>
        <source>The user name or passphrase you entered is not correct</source>
        <translation>您輸入的用戶名或密碼不正確</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="41"/>
        <source>A cryptographic verification failure has occurred</source>
        <translation>發生了加密驗證失敗</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="43"/>
        <source>Function or operation not implemented</source>
        <translation>功能或操作未實現</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="45"/>
        <source>I/O error</source>
        <translation>輸入/輸出錯誤</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="47"/>
        <source>Already open with with write permission</source>
        <translation>已經以寫入權限打開</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="49"/>
        <source>Invalid parameters passed to a function</source>
        <translation>向函數傳遞了無效的參數</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="51"/>
        <source>Failed to allocate memory</source>
        <translation>無法分配內存</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="53"/>
        <source>Bad parameter or invalid state for operation</source>
        <translation>操作的參數錯誤或狀態無效</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="55"/>
        <source>An internal component failed</source>
        <translation>內部組件失敗</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="57"/>
        <source>The specified item already exists in the keychain</source>
        <translation>鑰匙圈中已經存在指定的項目</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="59"/>
        <source>Unable to decode the provided data</source>
        <translation>無法解碼提供的數據</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="62"/>
        <source>Unknown error</source>
        <translation>未知錯誤</translation>
    </message>
</context>
<context>
    <name>QKeychain::JobPrivate</name>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="294"/>
        <source>Unknown error</source>
        <translation>未知錯誤</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="541"/>
        <source>Access to keychain denied</source>
        <translation>訪問鑰匙圈被拒絕</translation>
    </message>
</context>
<context>
    <name>QKeychain::PlainTextStore</name>
    <message>
        <location filename="../lib/qtkeychain/plaintextstore.cpp" line="65"/>
        <source>Could not store data in settings: access error</source>
        <translation>無法在設置中存儲數據：訪問錯誤</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/plaintextstore.cpp" line="67"/>
        <source>Could not store data in settings: format error</source>
        <translation>無法在設置中存儲數據：格式錯誤</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/plaintextstore.cpp" line="85"/>
        <source>Could not delete data from settings: access error</source>
        <translation>無法從設置中刪除數據：訪問錯誤</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/plaintextstore.cpp" line="87"/>
        <source>Could not delete data from settings: format error</source>
        <translation>無法從設置中刪除數據：格式錯誤</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/plaintextstore.cpp" line="104"/>
        <source>Entry not found</source>
        <translation>條目未找到</translation>
    </message>
</context>
<context>
    <name>QKeychain::ReadPasswordJobPrivate</name>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="52"/>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="392"/>
        <source>Entry not found</source>
        <translation>條目未找到</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="60"/>
        <source>Could not open keystore</source>
        <translation>無法打開密鑰庫</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="68"/>
        <source>Could not retrieve private key from keystore</source>
        <translation>無法從密鑰庫中檢索私鑰</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="75"/>
        <source>Could not create decryption cipher</source>
        <translation>無法創建解密密碼</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="204"/>
        <source>D-Bus is not running</source>
        <translation>D-Bus未運行</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="213"/>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="223"/>
        <source>Unknown error</source>
        <translation>未知錯誤</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="315"/>
        <source>No keychain service available</source>
        <translation>沒有可用的鑰匙圈服務</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="317"/>
        <source>Could not open wallet: %1; %2</source>
        <translation>無法打開wallet：%1；%2</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="362"/>
        <source>Access to keychain denied</source>
        <translation>訪問鑰匙圈被拒絕</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="383"/>
        <source>Could not determine data type: %1; %2</source>
        <translation>無法確定數據類型：%1；%2</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="401"/>
        <source>Unsupported entry type &apos;Map&apos;</source>
        <translation>不支持的條目類型&apos;Map&apos;</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="404"/>
        <source>Unknown kwallet entry type &apos;%1&apos;</source>
        <translation>未知的kwallet條目類型&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="32"/>
        <source>Password entry not found</source>
        <translation>找不到密碼條目</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="36"/>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="139"/>
        <source>Could not decrypt data</source>
        <translation>無法解密數據</translation>
    </message>
</context>
<context>
    <name>QKeychain::WritePasswordJobPrivate</name>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="95"/>
        <source>Could not open keystore</source>
        <translation>無法打開密鑰庫</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="124"/>
        <source>Could not create private key generator</source>
        <translation>無法創建私鑰生成器</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="131"/>
        <source>Could not generate new private key</source>
        <translation>無法生成新的私鑰</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="139"/>
        <source>Could not retrieve private key from keystore</source>
        <translation>無法從密鑰庫中檢索私鑰</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="147"/>
        <source>Could not create encryption cipher</source>
        <translation>無法創建加密密碼</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="155"/>
        <source>Could not encrypt data</source>
        <translation>無法加密數據</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="444"/>
        <source>D-Bus is not running</source>
        <translation>D-Bus未運行</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="454"/>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="481"/>
        <source>Unknown error</source>
        <translation>未知錯誤</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="500"/>
        <source>Could not open wallet: %1; %2</source>
        <translation>無法打開wallet：%1；%2</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="78"/>
        <source>Credential size exceeds maximum size of %1</source>
        <translation>憑據大小超過最大大小%1</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="87"/>
        <source>Credential key exceeds maximum size of %1</source>
        <translation>憑據密鑰超過最大大小%1</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="92"/>
        <source>Writing credentials failed: Win32 error code %1</source>
        <translation>寫入憑據失敗：Win32錯誤碼%1</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="162"/>
        <source>Encryption failed</source>
        <translation>加密失敗</translation>
    </message>
</context>
<context>
    <name>QMessageBox</name>
    <message>
        <location filename="../lib/qtermwidget/TerminalDisplay.cpp" line="2949"/>
        <source>Show Details...</source>
        <translation>顯示詳情...</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../lib/qtermwidget/ColorScheme.cpp" line="267"/>
        <location filename="../lib/qtermwidget/ColorScheme.cpp" line="282"/>
        <source>Un-named Color Scheme</source>
        <translation>未命名的配色方案</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/ColorScheme.cpp" line="453"/>
        <source>Accessible Color Scheme</source>
        <translation>無障礙配色方案</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/Filter.cpp" line="549"/>
        <source>Open Link</source>
        <translation>打開鏈接</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/Filter.cpp" line="550"/>
        <source>Copy Link Address</source>
        <translation>複製鏈接</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/Filter.cpp" line="554"/>
        <source>Send Email To...</source>
        <translation>發送郵件給...</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/Filter.cpp" line="555"/>
        <source>Copy Email Address</source>
        <translation>複製郵件地址</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/Filter.cpp" line="559"/>
        <source>Open Path</source>
        <translation>打開路徑</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/Filter.cpp" line="560"/>
        <source>Copy Path</source>
        <translation>複製路徑</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="254"/>
        <source>Access to keychain denied</source>
        <translation>訪問鑰匙圈被拒絕</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="256"/>
        <source>No keyring daemon</source>
        <translation>沒有鑰匙圈守護程序</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="258"/>
        <source>Already unlocked</source>
        <translation>已解鎖</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="260"/>
        <source>No such keyring</source>
        <translation>沒有這樣的鑰匙圈</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="262"/>
        <source>Bad arguments</source>
        <translation>參數錯誤</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="264"/>
        <source>I/O error</source>
        <translation>輸入/輸出錯誤</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="266"/>
        <source>Cancelled</source>
        <translation>已取消</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="268"/>
        <source>Keyring already exists</source>
        <translation>鑰匙圈已經存在</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="270"/>
        <source>No match</source>
        <translation>沒有匹配</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="275"/>
        <source>Unknown error</source>
        <translation>未知錯誤</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/libsecret.cpp" line="120"/>
        <source>Entry not found</source>
        <translation>條目未找到</translation>
    </message>
</context>
<context>
    <name>QTermWidget</name>
    <message>
        <location filename="../lib/qtermwidget/qtermwidget.cpp" line="350"/>
        <source>Color Scheme Error</source>
        <translation>配色方案錯誤</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/qtermwidget.cpp" line="351"/>
        <source>Cannot load color scheme: %1</source>
        <translation>無法加載配色方案：%1</translation>
    </message>
</context>
<context>
    <name>QuickConnectWindow</name>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="30"/>
        <source>Protocol</source>
        <translation>協議</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="43"/>
        <source>Serial</source>
        <translation>串口</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="48"/>
        <source>Local Shell</source>
        <translation>本地Shell</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="58"/>
        <source>Named Pipe</source>
        <translation>命名管道</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="86"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="69"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="166"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="224"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="253"/>
        <source>Hostname</source>
        <translation>主機名</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="109"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="70"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="167"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="225"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="254"/>
        <source>Port</source>
        <translation>端口</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="125"/>
        <source>WebSocket</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="133"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="233"/>
        <source>None</source>
        <translation>無</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="138"/>
        <source>Insecure</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="143"/>
        <source>Secure</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="161"/>
        <source>Username</source>
        <translation>用戶名</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="171"/>
        <source>Password</source>
        <translation>密碼</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="188"/>
        <source>DataBits</source>
        <translation>數據位</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="225"/>
        <source>Parity</source>
        <translation>校驗位</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="238"/>
        <source>Odd</source>
        <translation>奇校驗</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="243"/>
        <source>Even</source>
        <translation>偶校驗</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="261"/>
        <source>StopBits</source>
        <translation>停止位</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="313"/>
        <source>Save session</source>
        <translation>保存會話</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="323"/>
        <source>Open in tab</source>
        <translation>在標籤中打開</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="33"/>
        <source>Quick Connect</source>
        <translation>快速連接</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="91"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="188"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="246"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="275"/>
        <source>e.g. 127.0.0.1</source>
        <translation>例如： 127.0.0.1</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="99"/>
        <source>Port Name</source>
        <translation>端口名稱</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="100"/>
        <source>Baud Rate</source>
        <translation>波特率</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="109"/>
        <source>e.g. 110, 300, 600, 1200, 2400, 
4800, 9600, 14400, 19200, 38400, 
56000, 57600, 115200, 128000, 256000, 
460800, 921600</source>
        <translation>例如： 110, 300, 600, 1200, 2400,
4800, 9600, 14400, 19200, 38400,
56000, 57600, 115200, 128000, 256000,
460800, 921600</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="141"/>
        <source>Command</source>
        <translation>命令</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="162"/>
        <source>e.g. /bin/bash</source>
        <translation>例如： /bin/bash</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="195"/>
        <source>Pipe Name</source>
        <translation>管道名稱</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="217"/>
        <source>e.g. \\\.\pipe\namedpipe</source>
        <translation>例如： \\\.\pipe\namedpipe</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="219"/>
        <source>e.g. /tmp/socket</source>
        <translation>例如： /tmp/socket</translation>
    </message>
</context>
<context>
    <name>SFTPmenuBookmarkWidget</name>
    <message>
        <location filename="../src/sftpwindow/sftpmenubookmarkwidget.ui" line="17"/>
        <source>BookMarkName</source>
        <translation>書籤名稱</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpmenubookmarkwidget.ui" line="35"/>
        <source>LocalPath</source>
        <translation>本地路徑</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpmenubookmarkwidget.ui" line="62"/>
        <source>RemotePath</source>
        <translation>遠程路徑</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpmenubookmarkwidget.cpp" line="31"/>
        <source>Open Directory</source>
        <translation>打開目錄</translation>
    </message>
</context>
<context>
    <name>SFTPmenuBookmarkWindow</name>
    <message>
        <source>Bookmark</source>
        <translation type="vanished">書籤</translation>
    </message>
    <message>
        <source>BookMarkName</source>
        <translation type="vanished">書籤名稱</translation>
    </message>
    <message>
        <source>LocalPath</source>
        <translation type="vanished">本地路徑</translation>
    </message>
    <message>
        <source>RemotePath</source>
        <translation type="vanished">遠程路徑</translation>
    </message>
    <message>
        <source>Open Directory</source>
        <translation type="vanished">打開目錄</translation>
    </message>
</context>
<context>
    <name>SearchBar</name>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.ui" line="14"/>
        <source>SearchBar</source>
        <translation>搜索欄</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.ui" line="20"/>
        <source>X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.ui" line="32"/>
        <source>Find:</source>
        <translation>查找：</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.ui" line="42"/>
        <source>&lt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.ui" line="54"/>
        <source>&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.ui" line="66"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.cpp" line="40"/>
        <location filename="../lib/qtermwidget/SearchBar.cpp" line="130"/>
        <source>Match case</source>
        <translation>區分大小寫</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.cpp" line="46"/>
        <location filename="../lib/qtermwidget/SearchBar.cpp" line="131"/>
        <source>Regular expression</source>
        <translation>正則表達式</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.cpp" line="50"/>
        <location filename="../lib/qtermwidget/SearchBar.cpp" line="132"/>
        <source>Highlight all matches</source>
        <translation>高亮所有匹配項</translation>
    </message>
</context>
<context>
    <name>SessionManagerTreeModel</name>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="133"/>
        <source>Telnet</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="135"/>
        <source>Serial</source>
        <translation>串口</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="137"/>
        <source>Shell</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="139"/>
        <source>Raw</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="141"/>
        <source>NamePipe</source>
        <translation>命名管道</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="269"/>
        <source>Name</source>
        <translation>名稱</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="271"/>
        <source>Kind</source>
        <translation>類型</translation>
    </message>
</context>
<context>
    <name>SessionManagerTreeView</name>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="39"/>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="74"/>
        <source>Session</source>
        <translation>會話</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="84"/>
        <source>Connect Terminal</source>
        <translation>連接終端</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="89"/>
        <source>Connect in New Window</source>
        <translation>在新窗口中連接</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="96"/>
        <source>Connect in New Tab Group</source>
        <translation>在新標籤組中連接</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="103"/>
        <source>Delete</source>
        <translation>刪除</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="109"/>
        <source>Properties</source>
        <translation>屬性</translation>
    </message>
</context>
<context>
    <name>SessionManagerWidget</name>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagerwidget.ui" line="43"/>
        <source>Session Manager</source>
        <translation>會話管理器</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagerwidget.cpp" line="67"/>
        <source>Filter by folder/session name</source>
        <translation>按文件夾/會話名稱過濾</translation>
    </message>
</context>
<context>
    <name>SessionOptionsGeneralWidget</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsgeneralwidget.ui" line="25"/>
        <source>Name</source>
        <translation>名稱</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsgeneralwidget.ui" line="45"/>
        <source>Protocol</source>
        <translation>協議</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsgeneralwidget.ui" line="58"/>
        <source>Serial</source>
        <translation>串口</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsgeneralwidget.ui" line="63"/>
        <source>Local Shell</source>
        <translation>本地Shell</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsgeneralwidget.ui" line="73"/>
        <source>Named Pipe</source>
        <translation>命名管道</translation>
    </message>
</context>
<context>
    <name>SessionOptionsLocalShellProperties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionslocalshellproperties.ui" line="19"/>
        <source>Command</source>
        <translation>命令</translation>
    </message>
</context>
<context>
    <name>SessionOptionsLocalShellState</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionslocalshellstate.cpp" line="33"/>
        <source>Name</source>
        <translation>名稱</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionslocalshellstate.ui" line="25"/>
        <source>State</source>
        <translation>狀態</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionslocalshellstate.ui" line="51"/>
        <source>Process Tree</source>
        <translation>進程樹</translation>
    </message>
</context>
<context>
    <name>SessionOptionsNamePipeProperties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsnamepipeproperties.ui" line="25"/>
        <source>PipeName</source>
        <translation>管道名稱</translation>
    </message>
</context>
<context>
    <name>SessionOptionsRawProperties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsrawproperties.ui" line="25"/>
        <source>Hostname</source>
        <translation>主機名</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsrawproperties.ui" line="45"/>
        <source>Port</source>
        <translation>端口</translation>
    </message>
</context>
<context>
    <name>SessionOptionsSerialProperties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="25"/>
        <source>Port Name</source>
        <translation>端口名稱</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="45"/>
        <source>Baud Rate</source>
        <translation> 波特率</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="72"/>
        <source>DataBits</source>
        <translation>數據位</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="109"/>
        <source>Parity</source>
        <translation>校驗位</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="117"/>
        <source>None</source>
        <translation>無</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="122"/>
        <source>Odd</source>
        <translation>奇校驗</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="127"/>
        <source>Even</source>
        <translation>偶校驗</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="145"/>
        <source>StopBits</source>
        <translation>停止位</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.cpp" line="28"/>
        <source>e.g. 110, 300, 600, 1200, 2400, 
4800, 9600, 14400, 19200, 38400, 
56000, 57600, 115200, 128000, 256000, 
460800, 921600</source>
        <translation>例如： 110, 300, 600, 1200, 2400,
4800, 9600, 14400, 19200, 38400,
56000, 57600, 115200, 128000, 256000,
460800, 921600</translation>
    </message>
</context>
<context>
    <name>SessionOptionsSsh2Properties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsssh2properties.ui" line="25"/>
        <source>Hostname</source>
        <translation>主機名</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsssh2properties.ui" line="41"/>
        <source>Port</source>
        <translation>端口</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsssh2properties.ui" line="65"/>
        <source>UserName</source>
        <translation>用戶名</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsssh2properties.ui" line="81"/>
        <source>Password</source>
        <translation>密碼</translation>
    </message>
</context>
<context>
    <name>SessionOptionsTelnetProperties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionstelnetproperties.ui" line="19"/>
        <source>Hostname</source>
        <translation>主機名</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionstelnetproperties.ui" line="33"/>
        <source>Port</source>
        <translation>端口</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionstelnetproperties.ui" line="55"/>
        <source>None</source>
        <translation>無</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionstelnetproperties.ui" line="60"/>
        <source>Insecure</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionstelnetproperties.ui" line="65"/>
        <source>Secure</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SessionOptionsVNCProperties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsvncproperties.ui" line="25"/>
        <source>Hostname</source>
        <translation>主機名</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsvncproperties.ui" line="41"/>
        <source>Port</source>
        <translation>端口</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsvncproperties.ui" line="65"/>
        <source>Password</source>
        <translation>密碼</translation>
    </message>
</context>
<context>
    <name>SessionOptionsWindow</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionswindow.ui" line="14"/>
        <source>Session Options</source>
        <translation>會話選項</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionswindow.cpp" line="122"/>
        <source>General</source>
        <translation>常規</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionswindow.cpp" line="122"/>
        <source>Properties</source>
        <translation>屬性</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionswindow.cpp" line="122"/>
        <source>State</source>
        <translation>狀態</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionswindow.cpp" line="133"/>
        <source>Name</source>
        <translation>名稱</translation>
    </message>
</context>
<context>
    <name>SessionsWindow</name>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="141"/>
        <source>Telnet Error</source>
        <translation>telnet錯誤</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="141"/>
        <source>Telnet error:
%1.</source>
        <translation>telnet錯誤：
%1。</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="168"/>
        <source>Serial Error</source>
        <translation>串口錯誤</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="168"/>
        <source>Serial error:
%1.</source>
        <translation>串口錯誤：
%1。</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="203"/>
        <source>Raw Socket Error</source>
        <translation>原始套接字錯誤</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="203"/>
        <source>Raw Socket error:
%1.</source>
        <translation>原始套接字錯誤：
%1。</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="234"/>
        <source>Name Pipe Error</source>
        <translation>命名管道錯誤</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="234"/>
        <source>Name Pipe error:
%1.</source>
        <translation>命名管道錯誤：
%1。</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="246"/>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="275"/>
        <source>SSH2 Error</source>
        <translation>SSH2錯誤</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="246"/>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="275"/>
        <source>SSH2 error:
%1.</source>
        <translation>SSH2錯誤：
%1。</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="434"/>
        <source>Start Local Shell</source>
        <translation>啟動本地Shell</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="434"/>
        <source>Cannot start local shell:
%1.</source>
        <translation>無法啟動本地shell：
%1。</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="573"/>
        <source>Save log...</source>
        <translation>保存日誌...</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="574"/>
        <source>log files (*.log)</source>
        <translation>日誌文件(*.log)</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="578"/>
        <source>Save log</source>
        <translation>保存日誌</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="578"/>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="613"/>
        <source>Cannot write file %1:
%2.</source>
        <translation>無法寫入文件%1：
%2。</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="608"/>
        <source>Save Raw log...</source>
        <translation>保存原始日誌...</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="609"/>
        <source>binary files (*.bin)</source>
        <translation>二進制文件(*.bin)</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="613"/>
        <source>Save Raw log</source>
        <translation>保存原始日誌</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="685"/>
        <source>Unlock Session</source>
        <translation>解鎖會話</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="685"/>
        <source>Wrong password!</source>
        <translation>密碼錯誤！</translation>
    </message>
</context>
<context>
    <name>SftpWindow</name>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.ui" line="14"/>
        <source>sftp</source>
        <translation>SFTP</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.ui" line="72"/>
        <source>local</source>
        <translation>本地</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.ui" line="130"/>
        <source>remote</source>
        <translation>遠程</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="46"/>
        <source>Bookmarks</source>
        <translation>書籤</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="47"/>
        <source>Add Bookmark</source>
        <translation>添加書籤</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="48"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="67"/>
        <source>Edit Bookmark</source>
        <translation>編輯書籤</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="49"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="84"/>
        <source>Remove Bookmark</source>
        <translation>刪除書籤</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="67"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="84"/>
        <source>Bookmark Name:</source>
        <translation>書籤名稱：</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="109"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="109"/>
        <source>Bookmark Name can not be empty!</source>
        <translation>書籤名稱不能為空！</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="196"/>
        <source>No task!</source>
        <translation>沒有任務！</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="200"/>
        <source>All tasks finished!</source>
        <translation>所有任務已完成！</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="205"/>
        <source>task %1/%2</source>
        <translation>任務%1/%2</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="223"/>
        <source>Open Directory</source>
        <translation>打開目錄</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="246"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="412"/>
        <source>Show/Hide Files</source>
        <translation>顯示/隱藏文件</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="250"/>
        <source>Upload</source>
        <translation>上傳</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="301"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="476"/>
        <source>Open</source>
        <translation>打開</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="305"/>
        <source>Open in System File Manager</source>
        <translation>在系統文件管理器中打開</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="310"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="482"/>
        <source>refresh</source>
        <translation>刷新</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="315"/>
        <source>Upload All</source>
        <translation>上傳全部</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="359"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="535"/>
        <source>Cancel Selection</source>
        <translation>取消選擇</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="416"/>
        <source>Download</source>
        <translation>下載</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="456"/>
        <source>File exists</source>
        <translation>文件已存在</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="456"/>
        <source>File %1 already exists. Do you want to overwrite it?</source>
        <translation>文件%1已經存在。 您要覆蓋它嗎？</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="487"/>
        <source>Download All</source>
        <translation>下載全部</translation>
    </message>
</context>
<context>
    <name>StartTftpSeverWindow</name>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.ui" line="14"/>
        <source>Start TFTP Sever</source>
        <translation>啟動TFTP服務器</translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.ui" line="32"/>
        <source>The TFTP server uses local directories for uploading and downloading files. Please enter this information below.</source>
        <translation>TFTP服務器使用本地目錄上傳和下載文件。 請在下面輸入此信息。</translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.ui" line="65"/>
        <source>TFTP Port</source>
        <translation>TFTP端口</translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.ui" line="104"/>
        <source>Upload directory</source>
        <translation>上傳目錄</translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.ui" line="143"/>
        <source>Download directory</source>
        <translation>下載目錄</translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.cpp" line="38"/>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.cpp" line="45"/>
        <source>Open Directory</source>
        <translation>打開目錄</translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.cpp" line="77"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.cpp" line="77"/>
        <source>Please select a valid directory!</source>
        <translation>請選擇一個有效的目錄！</translation>
    </message>
</context>
<context>
    <name>UndoStack</name>
    <message>
        <location filename="../lib/qhexedit/commands.cpp" line="115"/>
        <source>Inserting %1 bytes</source>
        <translation>插入%1字節</translation>
    </message>
    <message>
        <location filename="../lib/qhexedit/commands.cpp" line="137"/>
        <source>Delete %1 chars</source>
        <translation>刪除%1字符</translation>
    </message>
    <message>
        <location filename="../lib/qhexedit/commands.cpp" line="162"/>
        <source>Overwrite %1 chars</source>
        <translation>覆蓋%1字符</translation>
    </message>
</context>
<context>
    <name>keyMapManager</name>
    <message>
        <location filename="../src/keymapmanager/keymapmanager.ui" line="14"/>
        <source>keyMapManager</source>
        <translation>按鍵映射管理器</translation>
    </message>
    <message>
        <location filename="../src/keymapmanager/keymapmanager.ui" line="20"/>
        <source>KeyBinding</source>
        <translation>按鍵綁定</translation>
    </message>
</context>
</TS>
