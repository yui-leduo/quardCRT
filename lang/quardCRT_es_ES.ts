<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>AddTabButton</name>
    <message>
        <location filename="../lib/QtFancyTabWidget/addtabbutton.cpp" line="29"/>
        <source>New tab</source>
        <translation>Nueva pestaña</translation>
    </message>
</context>
<context>
    <name>CentralWidget</name>
    <message>
        <location filename="../src/mainwindow.ui" line="101"/>
        <location filename="../src/mainwindow.cpp" line="1123"/>
        <source>Tool Bar</source>
        <translation>Barra de herramientas</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="191"/>
        <location filename="../src/mainwindow.cpp" line="194"/>
        <location filename="../src/mainwindow.cpp" line="197"/>
        <location filename="../src/mainwindow.cpp" line="325"/>
        <location filename="../src/mainwindow.cpp" line="341"/>
        <location filename="../src/mainwindow.cpp" line="358"/>
        <location filename="../src/mainwindow.cpp" line="371"/>
        <location filename="../src/mainwindow.cpp" line="546"/>
        <location filename="../src/mainwindow.cpp" line="2552"/>
        <location filename="../src/mainwindow.cpp" line="2747"/>
        <location filename="../src/mainwindow.cpp" line="3115"/>
        <location filename="../src/mainwindow.cpp" line="3451"/>
        <location filename="../src/mainwindow.cpp" line="3456"/>
        <source>Warning</source>
        <translation>Advertencia</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="191"/>
        <source>TFTP server bind error!</source>
        <translation>Error de enlace del servidor TFTP!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="194"/>
        <source>TFTP server file error!</source>
        <translation>Error de archivo del servidor TFTP!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="197"/>
        <source>TFTP server network error!</source>
        <translation>Error de red del servidor TFTP!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="286"/>
        <source>Unlock Session</source>
        <translation>Desbloquear sesión</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="287"/>
        <source>Unlock current session</source>
        <translation>Desbloquear sesión actual</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="293"/>
        <source>Move to another Tab</source>
        <translation>Mover a otra pestaña</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="294"/>
        <source>Move to current session to another tab group</source>
        <translation>Mover la sesión actual a otro grupo de pestañas</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="303"/>
        <source>Floating Window</source>
        <translation>Ventana flotante</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="304"/>
        <source>Floating current session to a new window</source>
        <translation>Flotar la sesión actual en una nueva ventana</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="311"/>
        <source>Copy Path</source>
        <translation>Copiar ruta</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="312"/>
        <source>Copy current session working folder path</source>
        <translation>Copiar la ruta de la carpeta de trabajo de la sesión actual</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="325"/>
        <location filename="../src/mainwindow.cpp" line="341"/>
        <location filename="../src/mainwindow.cpp" line="358"/>
        <source>No working folder!</source>
        <translation>¡No hay carpeta de trabajo!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="327"/>
        <source>Add Path to Bookmark</source>
        <translation>Añadir ruta a marcador</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="328"/>
        <source>Add current session working folder path to bookmark</source>
        <translation>Añadir la ruta de la carpeta de trabajo de la sesión actual al marcador</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="343"/>
        <source>Open Working Folder</source>
        <translation>Abrir carpeta de trabajo</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="344"/>
        <source>Open current session working folder in system file manager</source>
        <translation>Abrir la carpeta de trabajo de la sesión actual en el administrador de archivos del sistema</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="362"/>
        <source>Open SFTP</source>
        <translation>Abrir SFTP</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="363"/>
        <source>Open SFTP in a new window</source>
        <translation>Abrir SFTP en una nueva ventana</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="371"/>
        <source>No SFTP channel!</source>
        <translation>¡No hay canal SFTP!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="378"/>
        <source>Save Session</source>
        <translation>Guardar sesión</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="379"/>
        <source>Save current session to session manager</source>
        <translation>Guardar la sesión actual en el administrador de sesiones</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="389"/>
        <source>Enter Session Name</source>
        <translation>Introduzca el nombre de la sesión</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="390"/>
        <source>The session already exists, please rename the new session or cancel saving.</source>
        <translation>La sesión ya existe, por favor cambie el nombre de la nueva sesión o cancele el guardado.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="396"/>
        <source>Properties</source>
        <translation>Propiedades</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="397"/>
        <source>Show current session properties</source>
        <translation>Mostrar las propiedades de la sesión actual</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="417"/>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="418"/>
        <source>Close current session</source>
        <translation>Cerrar sesión actual</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="423"/>
        <source>Close Others</source>
        <translation>Cerrar otros</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="424"/>
        <source>Close other sessions</source>
        <translation>Cerrar otras sesiones</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="434"/>
        <source>Close to the Right</source>
        <translation>Cerrar a la derecha</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="435"/>
        <source>Close sessions to the right</source>
        <translation>Cerrar sesiones a la derecha</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="443"/>
        <source>Close All</source>
        <translation>Cerrar todo</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="444"/>
        <source>Close all sessions</source>
        <translation>Cerrar todas las sesiones</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="546"/>
        <source>Session properties error!</source>
        <translation>¡Error de propiedades de sesión!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="566"/>
        <location filename="../src/mainwindow.cpp" line="684"/>
        <location filename="../src/mainwindow.cpp" line="2455"/>
        <source>Ready</source>
        <translation>Preparado</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="777"/>
        <source>Highlight/Unhighlight</source>
        <translation>Resaltar/Desresaltar</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="778"/>
        <source>Highlight/Unhighlight selected text</source>
        <translation>Resaltar/Desresaltar texto seleccionado</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="838"/>
        <source>Google Translate</source>
        <translation>Google Translate</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="839"/>
        <location filename="../src/mainwindow.cpp" line="852"/>
        <location filename="../src/mainwindow.cpp" line="864"/>
        <source>Translate selected text</source>
        <translation>Traducir texto seleccionado</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="851"/>
        <source>Baidu Translate</source>
        <translation>Baidu Translate</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="863"/>
        <source>Microsoft Translate</source>
        <translation>Microsoft Translate</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="924"/>
        <source>Back to Main Window</source>
        <translation>Volver a la ventana principal</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="995"/>
        <location filename="../src/mainwindow.cpp" line="1019"/>
        <source>Session Manager</source>
        <translation>Administrador de sesiones</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="996"/>
        <source>Plugin</source>
        <translation>Plugin</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="998"/>
        <source>File</source>
        <translation>Archivo</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="999"/>
        <source>Edit</source>
        <translation>Editar</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1000"/>
        <source>View</source>
        <translation>Ver</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1001"/>
        <source>Options</source>
        <translation>Opciones</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1002"/>
        <source>Transfer</source>
        <translation>Transferir</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1003"/>
        <source>Script</source>
        <translation>Script</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1004"/>
        <source>Bookmark</source>
        <translation>Marcador</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1005"/>
        <source>Tools</source>
        <translation>Herramientas</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1006"/>
        <source>Window</source>
        <translation>Ventana</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1007"/>
        <source>Language</source>
        <translation>Idioma</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1008"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1009"/>
        <location filename="../src/mainwindow.cpp" line="1229"/>
        <location filename="../src/mainwindow.cpp" line="3415"/>
        <source>Help</source>
        <translation>Ayuda</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1011"/>
        <source>New Window</source>
        <translation>Nueva ventana</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1013"/>
        <source>Open a new window &lt;Ctrl+Shift+N&gt;</source>
        <translation>Abrir una nueva ventana &lt;Ctrl+Shift+N&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1015"/>
        <source>Connect...</source>
        <translation>Conectar...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1017"/>
        <source>Connect to a host &lt;Alt+C&gt;</source>
        <translation>Conectar a un host &lt;Alt+C&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1021"/>
        <source>Go to the Session Manager &lt;Alt+M&gt;</source>
        <translation>Ir al administrador de sesiones &lt;Alt+M&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1023"/>
        <source>Quick Connect...</source>
        <translation>Conexión rápida...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1025"/>
        <source>Quick Connect to a host &lt;Alt+Q&gt;</source>
        <translation>Conexión rápida a un host &lt;Alt+Q&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1027"/>
        <source>Connect in Tab/Tile...</source>
        <translation>Conectar en pestaña/mosaico...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1028"/>
        <source>Connect to a host in a new tab &lt;Alt+B&gt;</source>
        <translation>Conectar a un host en una nueva pestaña &lt;Alt+B&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1030"/>
        <source>Connect Local Shell</source>
        <translation>Conectar a shell local</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1032"/>
        <source>Connect to a local shell &lt;Alt+T&gt;</source>
        <translation>Conectar a un shell local &lt;Alt+T&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1034"/>
        <source>Reconnect</source>
        <translation>Reconectar</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1036"/>
        <source>Reconnect current session</source>
        <translation>Reconectar sesión actual</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1037"/>
        <source>Reconnect All</source>
        <translation>Reconectar todo</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1038"/>
        <source>Reconnect all sessions &lt;Alt+A&gt;</source>
        <translation>Reconectar todas las sesiones &lt;Alt+A&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1040"/>
        <source>Disconnect</source>
        <translation>Desconectar</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1042"/>
        <source>Disconnect current session</source>
        <translation>Desconectar sesión actual</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1043"/>
        <location filename="../src/mainwindow.cpp" line="1044"/>
        <source>Enter host &lt;Alt+R&gt; to connect</source>
        <translation>Introduzca el host &lt;Alt+R&gt; para conectar</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1045"/>
        <source>Disconnect All</source>
        <translation>Desconectar todo</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1046"/>
        <source>Disconnect all sessions</source>
        <translation>Desconectar todas las sesiones</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1047"/>
        <source>Clone Session</source>
        <translation>Clonar sesión</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1048"/>
        <source>Clone current session &lt;Ctrl+Shift+T&gt;</source>
        <translation>Clonar sesión actual &lt;Ctrl+Shift+T&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1050"/>
        <source>Lock Session</source>
        <translation>Bloquear sesión</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1051"/>
        <source>Lock/Unlock current session</source>
        <translation>Bloquear/Desbloquear sesión actual</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1052"/>
        <source>Log Session</source>
        <translation>Registro de sesión</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1053"/>
        <source>Create a log file for current session</source>
        <translation>Crear un archivo de registro para la sesión actual</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1054"/>
        <source>Raw Log Session</source>
        <translation>Registro de sesión en bruto</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1055"/>
        <source>Create a raw log file for current session</source>
        <translation>Crear un archivo de registro en bruto para la sesión actual</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1056"/>
        <source>Hex View</source>
        <translation>Vista hexadecimal</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1057"/>
        <source>Show/Hide Hex View for current session</source>
        <translation>Mostrar/Ocultar vista hexadecimal para la sesión actual</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1058"/>
        <source>Exit</source>
        <translation>Salir</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1059"/>
        <source>Quit the application</source>
        <translation>Salir de la aplicación</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1061"/>
        <source>Copy</source>
        <translation>Copiar</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1064"/>
        <source>Copy the selected text to the clipboard &lt;Command+C&gt;</source>
        <translation>Copiar el texto seleccionado al portapapeles &lt;Command+C&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1067"/>
        <source>Copy the selected text to the clipboard &lt;Ctrl+Ins&gt;</source>
        <translation>Copiar el texto seleccionado al portapapeles &lt;Ctrl+Ins&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1070"/>
        <source>Paste</source>
        <translation>Pegar</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1073"/>
        <source>Paste the clipboard text to the current session &lt;Command+V&gt;</source>
        <translation>Pegar el texto del portapapeles a la sesión actual &lt;Command+V&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1076"/>
        <source>Paste the clipboard text to the current session &lt;Shift+Ins&gt;</source>
        <translation>Pegar el texto del portapapeles a la sesión actual &lt;Shift+Ins&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1079"/>
        <source>Copy and Paste</source>
        <translation>Copiar y pegar</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1080"/>
        <source>Copy the selected text to the clipboard and paste to the current session</source>
        <translation>Copiar el texto seleccionado al portapapeles y pegar a la sesión actual</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1081"/>
        <source>Select All</source>
        <translation>Seleccionar todo</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1083"/>
        <source>Select all text in the current session &lt;Ctrl+Shift+A&gt;</source>
        <translation>Seleccionar todo el texto en la sesión actual &lt;Ctrl+Shift+A&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1085"/>
        <source>Find...</source>
        <translation>Buscar...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1087"/>
        <source>Find text in the current session &lt;Ctrl+F&gt;</source>
        <translation>Buscar texto en la sesión actual &lt;Ctrl+F&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1089"/>
        <source>Print Screen</source>
        <translation>Imprimir pantalla</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1091"/>
        <source>Print current screen</source>
        <translation>Imprimir pantalla actual</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1092"/>
        <source>Screen Shot</source>
        <translation>Captura de pantalla</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1094"/>
        <source>Screen shot current screen &lt;Alt+P&gt;</source>
        <translation>Captura de pantalla de la pantalla actual &lt;Alt+P&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1096"/>
        <source>Session Export</source>
        <translation>Exportar sesión</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1098"/>
        <source>Export current session to a file &lt;Alt+O&gt;</source>
        <translation>Exportar sesión actual a un archivo &lt;Alt+O&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1100"/>
        <source>Clear Scrollback</source>
        <translation>Borrar desplazamiento</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1101"/>
        <source>Clear the contents of the scrollback rows</source>
        <translation>Borrar el contenido de las filas de desplazamiento</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1102"/>
        <source>Clear Screen</source>
        <translation>Borrar pantalla</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1103"/>
        <source>Clear the contents of the current screen</source>
        <translation>Borrar el contenido de la pantalla actual</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1104"/>
        <source>Clear Screen and Scrollback</source>
        <translation>Borrar pantalla y desplazamiento</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1105"/>
        <source>Clear the contents of the screen and scrollback</source>
        <translation>Borrar el contenido de la pantalla y el desplazamiento</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1106"/>
        <source>Reset</source>
        <translation>Reiniciar</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1107"/>
        <source>Reset terminal emulator</source>
        <translation>Reiniciar emulador de terminal</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1109"/>
        <source>Zoom In</source>
        <translation>Acercar</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1111"/>
        <source>Zoom In &lt;Ctrl+&quot;=&quot;&gt;</source>
        <translation>Acercar &lt;Ctrl+&quot;=&quot;&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1113"/>
        <source>Zoom Out</source>
        <translation>Alejar</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1115"/>
        <source>Zoom Out &lt;Ctrl+&quot;-&quot;&gt;</source>
        <translation>Alejar &lt;Ctrl+&quot;-&quot;&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1117"/>
        <location filename="../src/mainwindow.cpp" line="1119"/>
        <source>Zoom Reset</source>
        <translation>Restablecer zoom</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1120"/>
        <source>Menu Bar</source>
        <translation>Barra de menús</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1121"/>
        <source>Show/Hide Menu Bar &lt;Alt+U&gt;</source>
        <translation>Mostrar/Ocultar barra de menús &lt;Alt+U&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1124"/>
        <source>Show/Hide Tool Bar</source>
        <translation>Mostrar/Ocultar barra de herramientas</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1125"/>
        <source>Status Bar</source>
        <translation>Barra de estado</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1126"/>
        <source>Show/Hide Status Bar</source>
        <translation>Mostrar/Ocultar barra de estado</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1127"/>
        <source>Command Window</source>
        <translation>Ventana de comandos</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1128"/>
        <source>Show/Hide Command Window</source>
        <translation>Mostrar/Ocultar ventana de comandos</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1129"/>
        <source>Connect Bar</source>
        <translation>Barra de conexión</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1130"/>
        <source>Show/Hide Connect Bar</source>
        <translation>Mostrar/Ocultar barra de conexión</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1131"/>
        <source>Side Window</source>
        <translation>Ventana lateral</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1132"/>
        <source>Show/Hide Side Window</source>
        <translation>Mostrar/Ocultar ventana lateral</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1133"/>
        <source>Windows Transparency</source>
        <translation>Transparencia de ventanas</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1134"/>
        <source>Enable/Disable alpha transparency</source>
        <translation>Habilitar/Deshabilitar transparencia alfa</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1135"/>
        <source>Vertical Scroll Bar</source>
        <translation>Barra de desplazamiento vertical</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1136"/>
        <source>Show/Hide Vertical Scroll Bar</source>
        <translation>Mostrar/Ocultar barra de desplazamiento vertical</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1137"/>
        <source>Allways On Top</source>
        <translation>Siempre encima</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1138"/>
        <source>Show window always on top</source>
        <translation>Mostrar ventana siempre encima</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1139"/>
        <source>Full Screen</source>
        <translation>Pantalla completa</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1140"/>
        <source>Toggle between full screen and normal mode &lt;Alt+Enter&gt;</source>
        <translation>Alternar entre pantalla completa y modo normal &lt;Alt+Enter&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1143"/>
        <source>Session Options...</source>
        <translation>Opciones de sesión...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1145"/>
        <source>Configure session options</source>
        <translation>Configurar opciones de sesión</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1146"/>
        <source>Global Options...</source>
        <translation>Opciones globales...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1148"/>
        <source>Configure global options</source>
        <translation>Configurar opciones globales</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1149"/>
        <source>Real-time Save Options</source>
        <translation>Opciones de guardado en tiempo real</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1150"/>
        <source>Real-time save session options and global options</source>
        <translation>Opciones de guardado en tiempo real de la sesión y opciones globales</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1151"/>
        <source>Save Settings Now</source>
        <translation>Guardar configuración ahora</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1152"/>
        <source>Save options configuration now</source>
        <translation>Guardar configuración de opciones ahora</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1154"/>
        <source>Send ASCII...</source>
        <translation>Enviar ASCII...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1155"/>
        <source>Send ASCII file</source>
        <translation>Enviar archivo ASCII</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1156"/>
        <source>Receive ASCII...</source>
        <translation>Recibir ASCII...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1157"/>
        <source>Receive ASCII file</source>
        <translation>Recibir archivo ASCII</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1158"/>
        <source>Send Binary...</source>
        <translation>Enviar binario...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1159"/>
        <source>Send Binary file</source>
        <translation>Enviar archivo binario</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1160"/>
        <source>Send Xmodem...</source>
        <translation>Enviar Xmodem...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1161"/>
        <source>Send a file using Xmodem</source>
        <translation>Enviar un archivo usando Xmodem</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1162"/>
        <source>Receive Xmodem...</source>
        <translation>Recibir Xmodem...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1163"/>
        <source>Receive a file using Xmodem</source>
        <translation>Recibir un archivo usando Xmodem</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1164"/>
        <source>Send Ymodem...</source>
        <translation>Enviar Ymodem...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1165"/>
        <source>Send a file using Ymodem</source>
        <translation>Enviar un archivo usando Ymodem</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1166"/>
        <source>Receive Ymodem...</source>
        <translation>Recibir Ymodem...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1167"/>
        <source>Receive a file using Ymodem</source>
        <translation>Recibir un archivo usando Ymodem</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1168"/>
        <source>Zmodem Upload List...</source>
        <translation>Lista de subida Zmodem...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1169"/>
        <source>Display Zmodem file upload list</source>
        <translation>Mostrar lista de subida de archivos Zmodem</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1170"/>
        <source>Start Zmodem Upload</source>
        <translation>Iniciar subida Zmodem</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1171"/>
        <source>Start Zmodem file upload</source>
        <translation>Iniciar subida de archivo Zmodem</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1172"/>
        <source>Start TFTP Server</source>
        <translation>Iniciar servidor TFTP</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1173"/>
        <source>Start/Stop the TFTP server</source>
        <translation>Iniciar/Detener el servidor TFTP</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1175"/>
        <source>Run...</source>
        <translation>Ejecutar...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1176"/>
        <source>Run a script</source>
        <translation>Ejecutar un script</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1177"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1178"/>
        <source>Cancel script execution</source>
        <translation>Cancelar ejecución de script</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1179"/>
        <source>Start Recording Script</source>
        <translation>Iniciar grabación de script</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1180"/>
        <source>Start recording script</source>
        <translation>Iniciar grabación de script</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1181"/>
        <source>Stop Recording Script...</source>
        <translation>Detener grabación de script...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1182"/>
        <source>Stop recording script</source>
        <translation>Detener grabación de script</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1183"/>
        <source>Cancel Recording Script</source>
        <translation>Cancelar grabación de script</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1184"/>
        <source>Cancel recording script</source>
        <translation>Cancelar grabación de script</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1186"/>
        <source>Add Bookmark</source>
        <translation>Añadir marcador</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1187"/>
        <source>Add a bookmark</source>
        <translation>Añadir un marcador</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1188"/>
        <location filename="../src/mainwindow.cpp" line="2350"/>
        <source>Remove Bookmark</source>
        <translation>Eliminar marcador</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1189"/>
        <source>Remove a bookmark</source>
        <translation>Eliminar un marcador</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1190"/>
        <location filename="../src/mainwindow.cpp" line="2377"/>
        <source>Clean All Bookmark</source>
        <translation>Limpiar todos los marcadores</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1191"/>
        <source>Clean all bookmark</source>
        <translation>Limpiar todos los marcadores</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1193"/>
        <source>Keymap Manager</source>
        <translation>Administrador de mapas de teclas</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1194"/>
        <source>Display keymap editor</source>
        <translation>Mostrar editor de mapas de teclas</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1195"/>
        <source>Create Public Key...</source>
        <translation>Crear clave pública...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1196"/>
        <source>Create a public key</source>
        <translation>Crear una clave pública</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1197"/>
        <source>Publickey Manager</source>
        <translation>Administrador de claves públicas</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1198"/>
        <source>Display publickey manager</source>
        <translation>Mostrar administrador de claves públicas</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1242"/>
        <source>Laboratory</source>
        <translation>Laboratorio</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1245"/>
        <location filename="../src/mainwindow.cpp" line="2395"/>
        <source>SSH Scanning</source>
        <translation>Escaneo SSH</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1246"/>
        <source>Display SSH scanning dialog</source>
        <translation>Mostrar diálogo de escaneo SSH</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3399"/>
        <source>Version</source>
        <translation>Versión</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3401"/>
        <source>Commit</source>
        <translation>Confirmar</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3403"/>
        <source>Date</source>
        <translation>Fecha</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3405"/>
        <source>Author</source>
        <translation>Autor</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3407"/>
        <source>Website</source>
        <translation>Sitio web</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1200"/>
        <source>Tab</source>
        <translation>Pestaña</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1201"/>
        <source>Arrange sessions in tabs</source>
        <translation>Organizar sesiones en pestañas</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1202"/>
        <source>Tile</source>
        <translation>Mosaico</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1203"/>
        <source>Arrange sessions in non-overlapping tiles</source>
        <translation>Organizar sesiones en mosaicos no superpuestos</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1204"/>
        <source>Cascade</source>
        <translation>Cascada</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1205"/>
        <source>Arrange sessions to overlap each other</source>
        <translation>Organizar sesiones para que se superpongan entre sí</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1207"/>
        <source>Simplified Chinese</source>
        <translation>简体中文</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1208"/>
        <source>Switch to Simplified Chinese</source>
        <translation>切换到简体中文</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1209"/>
        <source>Traditional Chinese</source>
        <translation>繁體中文</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1210"/>
        <source>Switch to Traditional Chinese</source>
        <translation>切換到繁體中文</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1211"/>
        <source>Russian</source>
        <translation>Русский</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1212"/>
        <source>Switch to Russian</source>
        <translation>Переключиться на русский</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1213"/>
        <source>Korean</source>
        <translation>한국어</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1214"/>
        <source>Switch to Korean</source>
        <translation>한국어로 전환</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1215"/>
        <source>Japanese</source>
        <translation>日本語</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1216"/>
        <source>Switch to Japanese</source>
        <translation>日本語に切り替える</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1217"/>
        <source>French</source>
        <translation>français</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1218"/>
        <source>Switch to French</source>
        <translation>Passer au français</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1219"/>
        <source>Spanish</source>
        <translation>español</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1220"/>
        <source>Switch to Spanish</source>
        <translation>Cambiar a español</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1221"/>
        <source>English</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1222"/>
        <source>Switch to English</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1224"/>
        <source>Light</source>
        <translation>Claro</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1225"/>
        <source>Switch to light theme</source>
        <translation>Cambiar a tema claro</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1226"/>
        <source>Dark</source>
        <translation>Oscuro</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1227"/>
        <source>Switch to dark theme</source>
        <translation>Cambiar a tema oscuro</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1231"/>
        <source>Display help</source>
        <translation>Mostrar ayuda</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1232"/>
        <source>Check Update</source>
        <translation>Comprobar actualizaciones</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1234"/>
        <source>Check for updates</source>
        <translation>Buscar actualizaciones</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1235"/>
        <location filename="../src/mainwindow.cpp" line="3397"/>
        <source>About</source>
        <translation>Acerca de</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1237"/>
        <source>Display about dialog</source>
        <translation>Mostrar diálogo acerca de</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1238"/>
        <source>About Qt</source>
        <translation>Acerca de Qt</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1240"/>
        <source>Display about Qt dialog</source>
        <translation>Mostrar diálogo acerca de Qt</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1247"/>
        <source>Plugin Info</source>
        <translation>Información del complemento</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1248"/>
        <source>Display plugin information dialog</source>
        <translation>Mostrar diálogo de información del complemento</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2134"/>
        <source>PrintScreen saved to %1</source>
        <translation>Imprimir pantalla guardada en %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2146"/>
        <source>Save Screenshot</source>
        <translation>Guardar captura de pantalla</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2146"/>
        <source>Image Files (*.jpg)</source>
        <translation>Archivos de imagen (*.jpg)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2151"/>
        <source>Screenshot saved to %1</source>
        <translation>Captura de pantalla guardada en %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2162"/>
        <source>Save Session Export</source>
        <translation>Guardar exportación de sesión</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2162"/>
        <source>Text Files (*.txt);;HTML Files (*.html)</source>
        <translation>Archivos de texto (*.txt);;Archivos HTML (*.html)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2165"/>
        <source>Text Files (*.txt)</source>
        <translation>Archivos de texto (*.txt)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2168"/>
        <source>HTML Files (*.html)</source>
        <translation>Archivos HTML (*.html)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2177"/>
        <source>Session Export saved to %1</source>
        <translation>Exportación de sesión guardada en %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2179"/>
        <source>Session Export failed to save to %1</source>
        <translation>Error al guardar la exportación de sesión en %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2323"/>
        <source>Select a directory</source>
        <translation>Seleccionar un directorio</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2350"/>
        <source>Select a bookmark</source>
        <translation>Seleccionar un marcador</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2377"/>
        <source>Are you sure to clean all bookmark?</source>
        <translation>¿Está seguro de limpiar todos los marcadores?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2395"/>
        <source>Port</source>
        <translation>Puerto</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2552"/>
        <source>Video background is enabled, please enable animation in global options (more system resources) or change background image.</source>
        <translation>El fondo de video está habilitado, habilite la animación en las opciones globales (más recursos del sistema) o cambie la imagen de fondo.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2747"/>
        <source>Session information get failed.</source>
        <translation>Error al obtener la información de la sesión.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2830"/>
        <source>Telnet - </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2831"/>
        <source>Telnet</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2863"/>
        <source>Serial - </source>
        <translation>Serie - </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2864"/>
        <source>Serial</source>
        <translation>Serie</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2895"/>
        <source>Raw - </source>
        <translation>Crudo - </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2896"/>
        <source>Raw</source>
        <translation>Crudo</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2927"/>
        <source>NamePipe - </source>
        <translation>Nombre de tubería - </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2928"/>
        <source>NamePipe</source>
        <translation>Nombre de tubería</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2996"/>
        <location filename="../src/mainwindow.cpp" line="3000"/>
        <source>Local Shell</source>
        <translation>Shell local</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2998"/>
        <source>Local Shell - </source>
        <translation>Shell local - </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3115"/>
        <source>Are you sure to disconnect this session?</source>
        <translation>¿Está seguro de desconectar esta sesión?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3417"/>
        <source>Global Shortcuts:</source>
        <translation>Atajos globales:</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3418"/>
        <source>show/hide menu bar</source>
        <translation>mostrar/ocultar barra de menús</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3419"/>
        <source>connect to LocalShell</source>
        <translation>conectar a Shell local</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3420"/>
        <source>clone current session</source>
        <translation>clonar sesión actual</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3421"/>
        <source>switch ui to STD mode</source>
        <translation>cambiar la interfaz de usuario al modo STD</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3422"/>
        <source>switch ui to MINI mode</source>
        <translation>cambiar la interfaz de usuario al modo MINI</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3423"/>
        <source>switch to previous session</source>
        <translation>cambiar a la sesión anterior</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3424"/>
        <source>switch to next session</source>
        <translation>cambiar a la siguiente sesión</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3425"/>
        <source>switch to session [num]</source>
        <translation>cambiar a la sesión [num]</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3426"/>
        <source>Go to line start</source>
        <translation>Ir al inicio de la línea</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3427"/>
        <source>Go to line end</source>
        <translation>Ir al final de la línea</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3451"/>
        <source>There are sessions that have not yet been unlocked, please unlock them first.</source>
        <translation>Hay sesiones que aún no se han desbloqueado, desbloquéelas primero.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3456"/>
        <source>Are you sure to quit?</source>
        <translation>¿Está seguro de salir?</translation>
    </message>
</context>
<context>
    <name>CommandWidget</name>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="35"/>
        <source>Send commands to active session</source>
        <translation>Enviar comandos a la sesión activa</translation>
    </message>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="71"/>
        <source>ASCII</source>
        <translation>ASCII</translation>
    </message>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="84"/>
        <source>HEX</source>
        <translation>HEX</translation>
    </message>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="104"/>
        <source>time</source>
        <translation>tiempo</translation>
    </message>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="117"/>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="135"/>
        <source>Auto Send</source>
        <translation>Autoenviar</translation>
    </message>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="142"/>
        <source>Send</source>
        <translation>Enviar</translation>
    </message>
</context>
<context>
    <name>EmptyTabWidget</name>
    <message>
        <location filename="../src/sessiontab/sessiontab.cpp" line="69"/>
        <source>No session</source>
        <translation>No hay sesión</translation>
    </message>
</context>
<context>
    <name>GlobalOptionsAdvancedWidget</name>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="25"/>
        <source>Config File</source>
        <translation>Archivo de configuración</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="56"/>
        <source>Translate Service</source>
        <translation>Servicio de traducción</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="67"/>
        <source>Google Translate</source>
        <translation>Google Translate</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="72"/>
        <source>Baidu Translate</source>
        <translation>Baidu Translate</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="77"/>
        <source>Microsoft Translate</source>
        <translation>Microsoft Translate</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="87"/>
        <source>Terminal background support animation</source>
        <translation>El fondo de la terminal admite animación</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="94"/>
        <source>NativeUI(Effective after restart)</source>
        <translation>Interfaz nativa (efectivo después de reiniciar)</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="101"/>
        <source>Github Copilot</source>
        <translation>Github Copilot</translation>
    </message>
</context>
<context>
    <name>GlobalOptionsAppearanceWidget</name>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="17"/>
        <source>Color Schemes</source>
        <translation>Esquemas de color</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="27"/>
        <source>Font</source>
        <translation>Fuente</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="45"/>
        <source>Series</source>
        <translation>Serie</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="65"/>
        <source>Size</source>
        <translation>Tamaño</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="93"/>
        <source>Background image</source>
        <translation>Imagen de fondo</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="115"/>
        <source>Clear</source>
        <translation>Borrar</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="124"/>
        <source>Background mode</source>
        <translation>Modo de fondo</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="131"/>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="143"/>
        <source>Stretch</source>
        <translation>Estirar</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="138"/>
        <source>None</source>
        <translation>Ninguno</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="148"/>
        <source>Zoom</source>
        <translation>Zoom</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="153"/>
        <source>Fit</source>
        <translation>Ajustar</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="158"/>
        <source>Center</source>
        <translation>Centrar</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="163"/>
        <source>Tile</source>
        <translation>Mosaico</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="171"/>
        <source>Background opacity</source>
        <translation>Opacidad de fondo</translation>
    </message>
</context>
<context>
    <name>GlobalOptionsGeneralWidget</name>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="19"/>
        <source>New tab mode</source>
        <translation>Modo de nueva pestaña</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="33"/>
        <source>New session</source>
        <translation>Nueva sesión</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="38"/>
        <source>Clone session</source>
        <translation>Clonar sesión</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="43"/>
        <source>LocalShell session</source>
        <translation>Sesión LocalShell</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="55"/>
        <source>New tab workpath</source>
        <translation>Nueva pestaña de trabajo</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="84"/>
        <source>Tab title mode</source>
        <translation>Modo de título de pestaña</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="98"/>
        <source>Brief</source>
        <translation>Breve</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="103"/>
        <source>Full</source>
        <translation>Completo</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="108"/>
        <source>Scroll</source>
        <translation>Desplazamiento</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="126"/>
        <source>Tab Title Width</source>
        <translation>Ancho del título de la pestaña</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="155"/>
        <source>Tab Preview</source>
        <translation>Vista previa de la pestaña</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="171"/>
        <source>Preview Width</source>
        <translation>Ancho de la vista previa</translation>
    </message>
</context>
<context>
    <name>GlobalOptionsTerminalWidget</name>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="17"/>
        <source>Scrollback lines</source>
        <translation>Líneas de desplazamiento</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="34"/>
        <source>Cursor Shape</source>
        <translation>Forma del cursor</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="42"/>
        <source>Block</source>
        <translation>Bloquear</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="47"/>
        <source>Underline</source>
        <translation>Subrayar</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="52"/>
        <source>IBeam</source>
        <translation>IBeam</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="60"/>
        <source>Cursor Blink</source>
        <translation>Parpadeo del cursor</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="70"/>
        <source>Word Characters</source>
        <translation>Caracteres de palabras</translation>
    </message>
</context>
<context>
    <name>GlobalOptionsWindow</name>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.ui" line="14"/>
        <source>Global Options</source>
        <translation>Opciones globales</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="164"/>
        <source>Select Background Image</source>
        <translation>Seleccionar imagen de fondo</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="164"/>
        <source>Image Files (*.png *.jpg *.jpeg *.bmp *.gif);;Video Files (*.mp4 *.avi *.mkv *.mov)</source>
        <translation>Archivos de imagen (*.png *.jpg *.jpeg *.bmp *.gif);;Archivos de vídeo (*.mp4 *.avi *.mkv *.mov)</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="202"/>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="207"/>
        <source>Information</source>
        <translation>Información</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="202"/>
        <source>This feature needs more system resources, please use it carefully!</source>
        <translation>¡Esta función necesita más recursos del sistema, úsela con cuidado!</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="207"/>
        <source>This feature is not implemented yet!</source>
        <translation>¡Esta función aún no está implementada!</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="229"/>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="229"/>
        <source>Appearance</source>
        <translation>Apariencia</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="229"/>
        <source>Terminal</source>
        <translation>Terminal</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="229"/>
        <source>Window</source>
        <translation>Ventana</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="229"/>
        <source>Advanced</source>
        <translation>Avanzado</translation>
    </message>
</context>
<context>
    <name>GlobalOptionsWindowWidget</name>
    <message>
        <location filename="../src/globaloptions/globaloptionswindowwidget.ui" line="17"/>
        <source>Transparent window</source>
        <translation>Ventana transparente</translation>
    </message>
</context>
<context>
    <name>HexViewWindow</name>
    <message>
        <source>ASCII Text...</source>
        <translation type="vanished">Texto ASCII...</translation>
    </message>
    <message>
        <source>clear</source>
        <translation type="vanished">borrar</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.ui" line="20"/>
        <source>Hex View</source>
        <translation>Vista hexadecimal</translation>
    </message>
    <message>
        <source>Information</source>
        <translation type="vanished">Información</translation>
    </message>
    <message>
        <source>Will send Hex:
</source>
        <translation type="vanished">Se enviará Hex:
</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="71"/>
        <source>Copy</source>
        <translation>Copiar</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="79"/>
        <source>Copy Hex</source>
        <translation>Copiar Hex</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="87"/>
        <source>Dump</source>
        <translation>Volcar</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="91"/>
        <source>Save As</source>
        <translation>Guardar como</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="91"/>
        <source>Binary File (*.bin)</source>
        <translation>Archivo binario (*.bin)</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="99"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="99"/>
        <source>Failed to save file!</source>
        <translation>¡Error al guardar el archivo!</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="104"/>
        <source>Clear</source>
        <translation>Borrar</translation>
    </message>
</context>
<context>
    <name>Konsole::Session</name>
    <message>
        <location filename="../lib/qtermwidget/Session.cpp" line="317"/>
        <source>Bell in session &apos;%1&apos;</source>
        <translation>Campana en la sesión &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>Konsole::TerminalDisplay</name>
    <message>
        <location filename="../lib/qtermwidget/TerminalDisplay.cpp" line="1383"/>
        <source>Size: XXX x XXX</source>
        <translation>Tamaño: XXX x XXX</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/TerminalDisplay.cpp" line="1395"/>
        <source>Size: %1 x %2</source>
        <translation>Tamaño: %1 x %2</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/TerminalDisplay.cpp" line="2942"/>
        <source>Paste multiline text</source>
        <translation>Pegar texto de varias líneas</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/TerminalDisplay.cpp" line="2943"/>
        <source>Are you sure you want to paste this text?</source>
        <translation>¿Está seguro de que desea pegar este texto?</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/TerminalDisplay.cpp" line="3426"/>
        <source>&lt;qt&gt;Output has been &lt;a href=&quot;http://en.wikipedia.org/wiki/Flow_control&quot;&gt;suspended&lt;/a&gt; by pressing Ctrl+S.  Press &lt;b&gt;Ctrl+Q&lt;/b&gt; to resume.&lt;/qt&gt;</source>
        <translation>&lt;qt&gt;La salida ha sido &lt;a href=&quot;http://en.wikipedia.org/wiki/Flow_control&quot;&gt;suspendida&lt;/a&gt; al presionar Ctrl+S.  Presione &lt;b&gt;Ctrl+Q&lt;/b&gt; para reanudar.&lt;/qt&gt;</translation>
    </message>
</context>
<context>
    <name>Konsole::Vt102Emulation</name>
    <message>
        <location filename="../lib/qtermwidget/Vt102Emulation.cpp" line="1113"/>
        <source>No keyboard translator available.  The information needed to convert key presses into characters to send to the terminal is missing.</source>
        <translation>No hay traductor de teclado disponible.  Falta la información necesaria para convertir las pulsaciones de teclas en caracteres para enviar al terminal.</translation>
    </message>
</context>
<context>
    <name>LockSessionWindow</name>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.ui" line="14"/>
        <source>Lock Session</source>
        <translation>Bloquear sesión</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.ui" line="20"/>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="84"/>
        <source>Enter the password that will be used to unlock the session:</source>
        <translation>Introduzca la contraseña que se utilizará para desbloquear la sesión:</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.ui" line="32"/>
        <source>Password</source>
        <translation>Contraseña</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.ui" line="50"/>
        <source>Reenter Password</source>
        <translation>Vuelva a introducir la contraseña</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.ui" line="68"/>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="85"/>
        <source>Lock all sessions</source>
        <translation>Bloquear todas las sesiones</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.ui" line="75"/>
        <source>Lock all sessions in tab group</source>
        <translation>Bloquear todas las sesiones en el grupo de pestañas</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="49"/>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="55"/>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="66"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="49"/>
        <source>Passwords do not match!</source>
        <translation>¡Las contraseñas no coinciden!</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="55"/>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="66"/>
        <source>Password cannot be empty!</source>
        <translation>¡La contraseña no puede estar vacía!</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="98"/>
        <source>Enter the password that was used to lock the session:</source>
        <translation>Introduzca la contraseña que se utilizó para bloquear la sesión:</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="99"/>
        <source>Unlock all sessions</source>
        <translation>Desbloquear todas las sesiones</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Tool Bar</source>
        <translation type="vanished">Barra de herramientas</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="vanished">Advertencia</translation>
    </message>
    <message>
        <source>TFTP server bind error!</source>
        <translation type="vanished">Error de enlace del servidor TFTP!</translation>
    </message>
    <message>
        <source>TFTP server file error!</source>
        <translation type="vanished">Error de archivo del servidor TFTP!</translation>
    </message>
    <message>
        <source>TFTP server network error!</source>
        <translation type="vanished">Error de red del servidor TFTP!</translation>
    </message>
    <message>
        <source>Unlock Session</source>
        <translation type="vanished">Desbloquear sesión</translation>
    </message>
    <message>
        <source>Move to another Tab</source>
        <translation type="vanished">Mover a otra pestaña</translation>
    </message>
    <message>
        <source>Floating Window</source>
        <translation type="vanished">Ventana flotante</translation>
    </message>
    <message>
        <source>Copy Path</source>
        <translation type="vanished">Copiar ruta</translation>
    </message>
    <message>
        <source>No working folder!</source>
        <translation type="vanished">¡No hay carpeta de trabajo!</translation>
    </message>
    <message>
        <source>Add Path to Bookmark</source>
        <translation type="vanished">Añadir ruta a marcador</translation>
    </message>
    <message>
        <source>Open Working Folder</source>
        <translation type="vanished">Abrir carpeta de trabajo</translation>
    </message>
    <message>
        <source>Save Session</source>
        <translation type="vanished">Guardar sesión</translation>
    </message>
    <message>
        <source>Enter Session Name</source>
        <translation type="vanished">Introduzca el nombre de la sesión</translation>
    </message>
    <message>
        <source>The session already exists, please rename the new session or cancel saving.</source>
        <translation type="vanished">La sesión ya existe, por favor cambie el nombre de la nueva sesión o cancele el guardado.</translation>
    </message>
    <message>
        <source>Properties</source>
        <translation type="vanished">Propiedades</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Cerrar</translation>
    </message>
    <message>
        <source>Close Others</source>
        <translation type="vanished">Cerrar otros</translation>
    </message>
    <message>
        <source>Close to the Right</source>
        <translation type="vanished">Cerrar a la derecha</translation>
    </message>
    <message>
        <source>Close All</source>
        <translation type="vanished">Cerrar todo</translation>
    </message>
    <message>
        <source>Highlight/Unhighlight</source>
        <translation type="vanished">Resaltar/Desresaltar</translation>
    </message>
    <message>
        <source>Ready</source>
        <translation type="vanished">Preparado</translation>
    </message>
    <message>
        <source>Open SFTP</source>
        <translation type="vanished">Abrir SFTP</translation>
    </message>
    <message>
        <source>No SFTP channel!</source>
        <translation type="vanished">¡No hay canal SFTP!</translation>
    </message>
    <message>
        <source>Session properties error!</source>
        <translation type="vanished">¡Error de propiedades de sesión!</translation>
    </message>
    <message>
        <source>Google Translate</source>
        <translation type="vanished">Google Translate</translation>
    </message>
    <message>
        <source>Baidu Translate</source>
        <translation type="vanished">Baidu Translate</translation>
    </message>
    <message>
        <source>Microsoft Translate</source>
        <translation type="vanished">Microsoft Translate</translation>
    </message>
    <message>
        <source>Back to Main Window</source>
        <translation type="vanished">Volver a la ventana principal</translation>
    </message>
    <message>
        <source>Session Manager</source>
        <translation type="vanished">Administrador de sesiones</translation>
    </message>
    <message>
        <source>File</source>
        <translation type="vanished">Archivo</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="vanished">Editar</translation>
    </message>
    <message>
        <source>View</source>
        <translation type="vanished">Ver</translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="vanished">Opciones</translation>
    </message>
    <message>
        <source>Transfer</source>
        <translation type="vanished">Transferir</translation>
    </message>
    <message>
        <source>Script</source>
        <translation type="vanished">Script</translation>
    </message>
    <message>
        <source>Bookmark</source>
        <translation type="vanished">Marcador</translation>
    </message>
    <message>
        <source>Tools</source>
        <translation type="vanished">Herramientas</translation>
    </message>
    <message>
        <source>Window</source>
        <translation type="vanished">Ventana</translation>
    </message>
    <message>
        <source>Language</source>
        <translation type="vanished">Idioma</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation type="vanished">Tema</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">Ayuda</translation>
    </message>
    <message>
        <source>New Window</source>
        <translation type="vanished">Nueva ventana</translation>
    </message>
    <message>
        <source>Open a new window &lt;Ctrl+Shift+N&gt;</source>
        <translation type="vanished">Abrir una nueva ventana &lt;Ctrl+Shift+N&gt;</translation>
    </message>
    <message>
        <source>Connect...</source>
        <translation type="vanished">Conectar...</translation>
    </message>
    <message>
        <source>Connect to a host &lt;Alt+C&gt;</source>
        <translation type="vanished">Conectar a un host &lt;Alt+C&gt;</translation>
    </message>
    <message>
        <source>Go to the Session Manager &lt;Alt+M&gt;</source>
        <translation type="vanished">Ir al administrador de sesiones &lt;Alt+M&gt;</translation>
    </message>
    <message>
        <source>Quick Connect...</source>
        <translation type="vanished">Conexión rápida...</translation>
    </message>
    <message>
        <source>Quick Connect to a host &lt;Alt+Q&gt;</source>
        <translation type="vanished">Conexión rápida a un host &lt;Alt+Q&gt;</translation>
    </message>
    <message>
        <source>Connect in Tab/Tile...</source>
        <translation type="vanished">Conectar en pestaña/mosaico...</translation>
    </message>
    <message>
        <source>Connect to a host in a new tab &lt;Alt+B&gt;</source>
        <translation type="vanished">Conectar a un host en una nueva pestaña &lt;Alt+B&gt;</translation>
    </message>
    <message>
        <source>Connect Local Shell</source>
        <translation type="vanished">Conectar a shell local</translation>
    </message>
    <message>
        <source>Connect to a local shell &lt;Alt+T&gt;</source>
        <translation type="vanished">Conectar a un shell local &lt;Alt+T&gt;</translation>
    </message>
    <message>
        <source>Reconnect</source>
        <translation type="vanished">Reconectar</translation>
    </message>
    <message>
        <source>Reconnect current session</source>
        <translation type="vanished">Reconectar sesión actual</translation>
    </message>
    <message>
        <source>Reconnect All</source>
        <translation type="vanished">Reconectar todo</translation>
    </message>
    <message>
        <source>Reconnect all sessions &lt;Alt+A&gt;</source>
        <translation type="vanished">Reconectar todas las sesiones &lt;Alt+A&gt;</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation type="vanished">Desconectar</translation>
    </message>
    <message>
        <source>Disconnect current session</source>
        <translation type="vanished">Desconectar sesión actual</translation>
    </message>
    <message>
        <source>Enter host &lt;Alt+R&gt; to connect</source>
        <translation type="vanished">Introduzca el host &lt;Alt+R&gt; para conectar</translation>
    </message>
    <message>
        <source>Disconnect All</source>
        <translation type="vanished">Desconectar todo</translation>
    </message>
    <message>
        <source>Disconnect all sessions</source>
        <translation type="vanished">Desconectar todas las sesiones</translation>
    </message>
    <message>
        <source>Clone Session</source>
        <translation type="vanished">Clonar sesión</translation>
    </message>
    <message>
        <source>Clone current session &lt;Ctrl+Shift+T&gt;</source>
        <translation type="vanished">Clonar sesión actual &lt;Ctrl+Shift+T&gt;</translation>
    </message>
    <message>
        <source>Lock Session</source>
        <translation type="vanished">Bloquear sesión</translation>
    </message>
    <message>
        <source>Lock/Unlock current session</source>
        <translation type="vanished">Bloquear/Desbloquear sesión actual</translation>
    </message>
    <message>
        <source>Log Session</source>
        <translation type="vanished">Registro de sesión</translation>
    </message>
    <message>
        <source>Create a log file for current session</source>
        <translation type="vanished">Crear un archivo de registro para la sesión actual</translation>
    </message>
    <message>
        <source>Raw Log Session</source>
        <translation type="vanished">Registro de sesión en bruto</translation>
    </message>
    <message>
        <source>Create a raw log file for current session</source>
        <translation type="vanished">Crear un archivo de registro en bruto para la sesión actual</translation>
    </message>
    <message>
        <source>Hex View</source>
        <translation type="vanished">Vista hexadecimal</translation>
    </message>
    <message>
        <source>Show/Hide Hex View for current session</source>
        <translation type="vanished">Mostrar/Ocultar vista hexadecimal para la sesión actual</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="vanished">Salir</translation>
    </message>
    <message>
        <source>Quit the application</source>
        <translation type="vanished">Salir de la aplicación</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation type="vanished">Copiar</translation>
    </message>
    <message>
        <source>Copy the selected text to the clipboard &lt;Command+C&gt;</source>
        <translation type="vanished">Copiar el texto seleccionado al portapapeles &lt;Command+C&gt;</translation>
    </message>
    <message>
        <source>Copy the selected text to the clipboard &lt;Ctrl+Ins&gt;</source>
        <translation type="vanished">Copiar el texto seleccionado al portapapeles &lt;Ctrl+Ins&gt;</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation type="vanished">Pegar</translation>
    </message>
    <message>
        <source>Paste the clipboard text to the current session &lt;Command+V&gt;</source>
        <translation type="vanished">Pegar el texto del portapapeles a la sesión actual &lt;Command+V&gt;</translation>
    </message>
    <message>
        <source>Paste the clipboard text to the current session &lt;Shift+Ins&gt;</source>
        <translation type="vanished">Pegar el texto del portapapeles a la sesión actual &lt;Shift+Ins&gt;</translation>
    </message>
    <message>
        <source>Copy and Paste</source>
        <translation type="vanished">Copiar y pegar</translation>
    </message>
    <message>
        <source>Copy the selected text to the clipboard and paste to the current session</source>
        <translation type="vanished">Copiar el texto seleccionado al portapapeles y pegar a la sesión actual</translation>
    </message>
    <message>
        <source>Select All</source>
        <translation type="vanished">Seleccionar todo</translation>
    </message>
    <message>
        <source>Select all text in the current session &lt;Ctrl+Shift+A&gt;</source>
        <translation type="vanished">Seleccionar todo el texto en la sesión actual &lt;Ctrl+Shift+A&gt;</translation>
    </message>
    <message>
        <source>Find...</source>
        <translation type="vanished">Buscar...</translation>
    </message>
    <message>
        <source>Find text in the current session &lt;Ctrl+F&gt;</source>
        <translation type="vanished">Buscar texto en la sesión actual &lt;Ctrl+F&gt;</translation>
    </message>
    <message>
        <source>Print Screen</source>
        <translation type="vanished">Imprimir pantalla</translation>
    </message>
    <message>
        <source>Print current screen</source>
        <translation type="vanished">Imprimir pantalla actual</translation>
    </message>
    <message>
        <source>Screen Shot</source>
        <translation type="vanished">Captura de pantalla</translation>
    </message>
    <message>
        <source>Screen shot current screen &lt;Alt+P&gt;</source>
        <translation type="vanished">Captura de pantalla de la pantalla actual &lt;Alt+P&gt;</translation>
    </message>
    <message>
        <source>Session Export</source>
        <translation type="vanished">Exportar sesión</translation>
    </message>
    <message>
        <source>Export current session to a file &lt;Alt+O&gt;</source>
        <translation type="vanished">Exportar sesión actual a un archivo &lt;Alt+O&gt;</translation>
    </message>
    <message>
        <source>Clear Scrollback</source>
        <translation type="vanished">Borrar desplazamiento</translation>
    </message>
    <message>
        <source>Clear the contents of the scrollback rows</source>
        <translation type="vanished">Borrar el contenido de las filas de desplazamiento</translation>
    </message>
    <message>
        <source>Clear Screen</source>
        <translation type="vanished">Borrar pantalla</translation>
    </message>
    <message>
        <source>Clear the contents of the current screen</source>
        <translation type="vanished">Borrar el contenido de la pantalla actual</translation>
    </message>
    <message>
        <source>Clear Screen and Scrollback</source>
        <translation type="vanished">Borrar pantalla y desplazamiento</translation>
    </message>
    <message>
        <source>Clear the contents of the screen and scrollback</source>
        <translation type="vanished">Borrar el contenido de la pantalla y el desplazamiento</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation type="vanished">Reiniciar</translation>
    </message>
    <message>
        <source>Reset terminal emulator</source>
        <translation type="vanished">Reiniciar emulador de terminal</translation>
    </message>
    <message>
        <source>Zoom In</source>
        <translation type="vanished">Acercar</translation>
    </message>
    <message>
        <source>Zoom In &lt;Ctrl+&quot;=&quot;&gt;</source>
        <translation type="vanished">Acercar &lt;Ctrl+&quot;=&quot;&gt;</translation>
    </message>
    <message>
        <source>Zoom Out</source>
        <translation type="vanished">Alejar</translation>
    </message>
    <message>
        <source>Zoom Out &lt;Ctrl+&quot;-&quot;&gt;</source>
        <translation type="vanished">Alejar &lt;Ctrl+&quot;-&quot;&gt;</translation>
    </message>
    <message>
        <source>Zoom Reset</source>
        <translation type="vanished">Restablecer zoom</translation>
    </message>
    <message>
        <source>Menu Bar</source>
        <translation type="vanished">Barra de menús</translation>
    </message>
    <message>
        <source>Show/Hide Menu Bar &lt;Alt+U&gt;</source>
        <translation type="vanished">Mostrar/Ocultar barra de menús &lt;Alt+U&gt;</translation>
    </message>
    <message>
        <source>Show/Hide Tool Bar</source>
        <translation type="vanished">Mostrar/Ocultar barra de herramientas</translation>
    </message>
    <message>
        <source>Status Bar</source>
        <translation type="vanished">Barra de estado</translation>
    </message>
    <message>
        <source>Show/Hide Status Bar</source>
        <translation type="vanished">Mostrar/Ocultar barra de estado</translation>
    </message>
    <message>
        <source>Command Window</source>
        <translation type="vanished">Ventana de comandos</translation>
    </message>
    <message>
        <source>Show/Hide Command Window</source>
        <translation type="vanished">Mostrar/Ocultar ventana de comandos</translation>
    </message>
    <message>
        <source>Connect Bar</source>
        <translation type="vanished">Barra de conexión</translation>
    </message>
    <message>
        <source>Show/Hide Connect Bar</source>
        <translation type="vanished">Mostrar/Ocultar barra de conexión</translation>
    </message>
    <message>
        <source>Side Window</source>
        <translation type="vanished">Ventana lateral</translation>
    </message>
    <message>
        <source>Show/Hide Side Window</source>
        <translation type="vanished">Mostrar/Ocultar ventana lateral</translation>
    </message>
    <message>
        <source>Windows Transparency</source>
        <translation type="vanished">Transparencia de ventanas</translation>
    </message>
    <message>
        <source>Enable/Disable alpha transparency</source>
        <translation type="vanished">Habilitar/Deshabilitar transparencia alfa</translation>
    </message>
    <message>
        <source>Vertical Scroll Bar</source>
        <translation type="vanished">Barra de desplazamiento vertical</translation>
    </message>
    <message>
        <source>Show/Hide Vertical Scroll Bar</source>
        <translation type="vanished">Mostrar/Ocultar barra de desplazamiento vertical</translation>
    </message>
    <message>
        <source>Allways On Top</source>
        <translation type="vanished">Siempre encima</translation>
    </message>
    <message>
        <source>Show window always on top</source>
        <translation type="vanished">Mostrar ventana siempre encima</translation>
    </message>
    <message>
        <source>Full Screen</source>
        <translation type="vanished">Pantalla completa</translation>
    </message>
    <message>
        <source>Toggle between full screen and normal mode &lt;Alt+Enter&gt;</source>
        <translation type="vanished">Alternar entre pantalla completa y modo normal &lt;Alt+Enter&gt;</translation>
    </message>
    <message>
        <source>Session Options...</source>
        <translation type="vanished">Opciones de sesión...</translation>
    </message>
    <message>
        <source>Configure session options</source>
        <translation type="vanished">Configurar opciones de sesión</translation>
    </message>
    <message>
        <source>Global Options...</source>
        <translation type="vanished">Opciones globales...</translation>
    </message>
    <message>
        <source>Configure global options</source>
        <translation type="vanished">Configurar opciones globales</translation>
    </message>
    <message>
        <source>Real-time Save Options</source>
        <translation type="vanished">Opciones de guardado en tiempo real</translation>
    </message>
    <message>
        <source>Real-time save session options and global options</source>
        <translation type="vanished">Opciones de guardado en tiempo real de la sesión y opciones globales</translation>
    </message>
    <message>
        <source>Save Settings Now</source>
        <translation type="vanished">Guardar configuración ahora</translation>
    </message>
    <message>
        <source>Save options configuration now</source>
        <translation type="vanished">Guardar configuración de opciones ahora</translation>
    </message>
    <message>
        <source>Send ASCII...</source>
        <translation type="vanished">Enviar ASCII...</translation>
    </message>
    <message>
        <source>Send ASCII file</source>
        <translation type="vanished">Enviar archivo ASCII</translation>
    </message>
    <message>
        <source>Receive ASCII...</source>
        <translation type="vanished">Recibir ASCII...</translation>
    </message>
    <message>
        <source>Receive ASCII file</source>
        <translation type="vanished">Recibir archivo ASCII</translation>
    </message>
    <message>
        <source>Send Binary...</source>
        <translation type="vanished">Enviar binario...</translation>
    </message>
    <message>
        <source>Send Binary file</source>
        <translation type="vanished">Enviar archivo binario</translation>
    </message>
    <message>
        <source>Send Xmodem...</source>
        <translation type="vanished">Enviar Xmodem...</translation>
    </message>
    <message>
        <source>Send a file using Xmodem</source>
        <translation type="vanished">Enviar un archivo usando Xmodem</translation>
    </message>
    <message>
        <source>Receive Xmodem...</source>
        <translation type="vanished">Recibir Xmodem...</translation>
    </message>
    <message>
        <source>Receive a file using Xmodem</source>
        <translation type="vanished">Recibir un archivo usando Xmodem</translation>
    </message>
    <message>
        <source>Send Ymodem...</source>
        <translation type="vanished">Enviar Ymodem...</translation>
    </message>
    <message>
        <source>Send a file using Ymodem</source>
        <translation type="vanished">Enviar un archivo usando Ymodem</translation>
    </message>
    <message>
        <source>Receive Ymodem...</source>
        <translation type="vanished">Recibir Ymodem...</translation>
    </message>
    <message>
        <source>Receive a file using Ymodem</source>
        <translation type="vanished">Recibir un archivo usando Ymodem</translation>
    </message>
    <message>
        <source>Zmodem Upload List...</source>
        <translation type="vanished">Lista de subida Zmodem...</translation>
    </message>
    <message>
        <source>Display Zmodem file upload list</source>
        <translation type="vanished">Mostrar lista de subida de archivos Zmodem</translation>
    </message>
    <message>
        <source>Start Zmodem Upload</source>
        <translation type="vanished">Iniciar subida Zmodem</translation>
    </message>
    <message>
        <source>Start Zmodem file upload</source>
        <translation type="vanished">Iniciar subida de archivo Zmodem</translation>
    </message>
    <message>
        <source>Start TFTP Server</source>
        <translation type="vanished">Iniciar servidor TFTP</translation>
    </message>
    <message>
        <source>Start/Stop the TFTP server</source>
        <translation type="vanished">Iniciar/Detener el servidor TFTP</translation>
    </message>
    <message>
        <source>Run...</source>
        <translation type="vanished">Ejecutar...</translation>
    </message>
    <message>
        <source>Run a script</source>
        <translation type="vanished">Ejecutar un script</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Cancelar</translation>
    </message>
    <message>
        <source>Cancel script execution</source>
        <translation type="vanished">Cancelar ejecución de script</translation>
    </message>
    <message>
        <source>Start Recording Script</source>
        <translation type="vanished">Iniciar grabación de script</translation>
    </message>
    <message>
        <source>Start recording script</source>
        <translation type="vanished">Iniciar grabación de script</translation>
    </message>
    <message>
        <source>Stop Recording Script...</source>
        <translation type="vanished">Detener grabación de script...</translation>
    </message>
    <message>
        <source>Stop recording script</source>
        <translation type="vanished">Detener grabación de script</translation>
    </message>
    <message>
        <source>Cancel Recording Script</source>
        <translation type="vanished">Cancelar grabación de script</translation>
    </message>
    <message>
        <source>Cancel recording script</source>
        <translation type="vanished">Cancelar grabación de script</translation>
    </message>
    <message>
        <source>Add Bookmark</source>
        <translation type="vanished">Añadir marcador</translation>
    </message>
    <message>
        <source>Add a bookmark</source>
        <translation type="vanished">Añadir un marcador</translation>
    </message>
    <message>
        <source>Remove Bookmark</source>
        <translation type="vanished">Eliminar marcador</translation>
    </message>
    <message>
        <source>Remove a bookmark</source>
        <translation type="vanished">Eliminar un marcador</translation>
    </message>
    <message>
        <source>Clean All Bookmark</source>
        <translation type="vanished">Limpiar todos los marcadores</translation>
    </message>
    <message>
        <source>Clean all bookmark</source>
        <translation type="vanished">Limpiar todos los marcadores</translation>
    </message>
    <message>
        <source>Keymap Manager</source>
        <translation type="vanished">Administrador de mapas de teclas</translation>
    </message>
    <message>
        <source>Display keymap editor</source>
        <translation type="vanished">Mostrar editor de mapas de teclas</translation>
    </message>
    <message>
        <source>Create Public Key...</source>
        <translation type="vanished">Crear clave pública...</translation>
    </message>
    <message>
        <source>Create a public key</source>
        <translation type="vanished">Crear una clave pública</translation>
    </message>
    <message>
        <source>Publickey Manager</source>
        <translation type="vanished">Administrador de claves públicas</translation>
    </message>
    <message>
        <source>Display publickey manager</source>
        <translation type="vanished">Mostrar administrador de claves públicas</translation>
    </message>
    <message>
        <source>SSH Scanning</source>
        <translation type="vanished">Escaneo SSH</translation>
    </message>
    <message>
        <source>Display SSH scanning dialog</source>
        <translation type="vanished">Mostrar diálogo de escaneo SSH</translation>
    </message>
    <message>
        <source>Tab</source>
        <translation type="vanished">Pestaña</translation>
    </message>
    <message>
        <source>Arrange sessions in tabs</source>
        <translation type="vanished">Organizar sesiones en pestañas</translation>
    </message>
    <message>
        <source>Tile</source>
        <translation type="vanished">Mosaico</translation>
    </message>
    <message>
        <source>Arrange sessions in non-overlapping tiles</source>
        <translation type="vanished">Organizar sesiones en mosaicos no superpuestos</translation>
    </message>
    <message>
        <source>Cascade</source>
        <translation type="vanished">Cascada</translation>
    </message>
    <message>
        <source>Arrange sessions to overlap each other</source>
        <translation type="vanished">Organizar sesiones para que se superpongan entre sí</translation>
    </message>
    <message>
        <source>Japanese</source>
        <translation type="vanished">日本語</translation>
    </message>
    <message>
        <source>Simplified Chinese</source>
        <translation type="vanished">简体中文</translation>
    </message>
    <message>
        <source>Switch to Simplified Chinese</source>
        <translation type="vanished">切换到简体中文</translation>
    </message>
    <message>
        <source>Traditional Chinese</source>
        <translation type="vanished">繁體中文</translation>
    </message>
    <message>
        <source>Switch to Traditional Chinese</source>
        <translation type="vanished">切換到繁體中文</translation>
    </message>
    <message>
        <source>Russian</source>
        <translation type="vanished">Русский</translation>
    </message>
    <message>
        <source>Switch to Russian</source>
        <translation type="vanished">Переключиться на русский</translation>
    </message>
    <message>
        <source>Korean</source>
        <translation type="vanished">한국어</translation>
    </message>
    <message>
        <source>Switch to Korean</source>
        <translation type="vanished">한국어로 전환</translation>
    </message>
    <message>
        <source>Switch to Japanese</source>
        <translation type="vanished">日本語に切り替える</translation>
    </message>
    <message>
        <source>French</source>
        <translation type="vanished">français</translation>
    </message>
    <message>
        <source>Switch to French</source>
        <translation type="vanished">Passer au français</translation>
    </message>
    <message>
        <source>Spanish</source>
        <translation type="vanished">español</translation>
    </message>
    <message>
        <source>Switch to Spanish</source>
        <translation type="vanished">Cambiar a español</translation>
    </message>
    <message>
        <source>Light</source>
        <translation type="vanished">Claro</translation>
    </message>
    <message>
        <source>Switch to light theme</source>
        <translation type="vanished">Cambiar a tema claro</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation type="vanished">Oscuro</translation>
    </message>
    <message>
        <source>Switch to dark theme</source>
        <translation type="vanished">Cambiar a tema oscuro</translation>
    </message>
    <message>
        <source>Display help</source>
        <translation type="vanished">Mostrar ayuda</translation>
    </message>
    <message>
        <source>Check Update</source>
        <translation type="vanished">Comprobar actualizaciones</translation>
    </message>
    <message>
        <source>Check for updates</source>
        <translation type="vanished">Buscar actualizaciones</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">Acerca de</translation>
    </message>
    <message>
        <source>Display about dialog</source>
        <translation type="vanished">Mostrar diálogo acerca de</translation>
    </message>
    <message>
        <source>About Qt</source>
        <translation type="vanished">Acerca de Qt</translation>
    </message>
    <message>
        <source>Display about Qt dialog</source>
        <translation type="vanished">Mostrar diálogo acerca de Qt</translation>
    </message>
    <message>
        <source>PrintScreen saved to %1</source>
        <translation type="vanished">Imprimir pantalla guardada en %1</translation>
    </message>
    <message>
        <source>Save Screenshot</source>
        <translation type="vanished">Guardar captura de pantalla</translation>
    </message>
    <message>
        <source>Image Files (*.jpg)</source>
        <translation type="vanished">Archivos de imagen (*.jpg)</translation>
    </message>
    <message>
        <source>Screenshot saved to %1</source>
        <translation type="vanished">Captura de pantalla guardada en %1</translation>
    </message>
    <message>
        <source>Save Session Export</source>
        <translation type="vanished">Guardar exportación de sesión</translation>
    </message>
    <message>
        <source>Text Files (*.txt);;HTML Files (*.html)</source>
        <translation type="vanished">Archivos de texto (*.txt);;Archivos HTML (*.html)</translation>
    </message>
    <message>
        <source>Text Files (*.txt)</source>
        <translation type="vanished">Archivos de texto (*.txt)</translation>
    </message>
    <message>
        <source>HTML Files (*.html)</source>
        <translation type="vanished">Archivos HTML (*.html)</translation>
    </message>
    <message>
        <source>Session Export saved to %1</source>
        <translation type="vanished">Exportación de sesión guardada en %1</translation>
    </message>
    <message>
        <source>Session Export failed to save to %1</source>
        <translation type="vanished">Error al guardar la exportación de sesión en %1</translation>
    </message>
    <message>
        <source>Select a directory</source>
        <translation type="vanished">Seleccionar un directorio</translation>
    </message>
    <message>
        <source>Select a bookmark</source>
        <translation type="vanished">Seleccionar un marcador</translation>
    </message>
    <message>
        <source>Are you sure to clean all bookmark?</source>
        <translation type="vanished">¿Está seguro de limpiar todos los marcadores?</translation>
    </message>
    <message>
        <source>Port</source>
        <translation type="vanished">Puerto</translation>
    </message>
    <message>
        <source>Video background is enabled, please enable animation in global options (more system resources) or change background image.</source>
        <translation type="vanished">El fondo de video está habilitado, habilite la animación en las opciones globales (más recursos del sistema) o cambie la imagen de fondo.</translation>
    </message>
    <message>
        <source>Session information get failed.</source>
        <translation type="vanished">Error al obtener la información de la sesión.</translation>
    </message>
    <message>
        <source>Serial - </source>
        <translation type="vanished">Serie - </translation>
    </message>
    <message>
        <source>Serial</source>
        <translation type="vanished">Serie</translation>
    </message>
    <message>
        <source>Raw - </source>
        <translation type="vanished">Crudo - </translation>
    </message>
    <message>
        <source>Raw</source>
        <translation type="vanished">Crudo</translation>
    </message>
    <message>
        <source>NamePipe - </source>
        <translation type="vanished">Nombre de tubería - </translation>
    </message>
    <message>
        <source>NamePipe</source>
        <translation type="vanished">Nombre de tubería</translation>
    </message>
    <message>
        <source>Local Shell</source>
        <translation type="vanished">Shell local</translation>
    </message>
    <message>
        <source>Local Shell - </source>
        <translation type="vanished">Shell local - </translation>
    </message>
    <message>
        <source>Are you sure to disconnect this session?</source>
        <translation type="vanished">¿Está seguro de desconectar esta sesión?</translation>
    </message>
    <message>
        <source>Global Shortcuts:</source>
        <translation type="vanished">Atajos globales:</translation>
    </message>
    <message>
        <source>show/hide menu bar</source>
        <translation type="vanished">mostrar/ocultar barra de menús</translation>
    </message>
    <message>
        <source>connect to LocalShell</source>
        <translation type="vanished">conectar a Shell local</translation>
    </message>
    <message>
        <source>clone current session</source>
        <translation type="vanished">clonar sesión actual</translation>
    </message>
    <message>
        <source>switch ui to STD mode</source>
        <translation type="vanished">cambiar la interfaz de usuario al modo STD</translation>
    </message>
    <message>
        <source>switch ui to MINI mode</source>
        <translation type="vanished">cambiar la interfaz de usuario al modo MINI</translation>
    </message>
    <message>
        <source>switch to previous session</source>
        <translation type="vanished">cambiar a la sesión anterior</translation>
    </message>
    <message>
        <source>switch to next session</source>
        <translation type="vanished">cambiar a la siguiente sesión</translation>
    </message>
    <message>
        <source>switch to session [num]</source>
        <translation type="vanished">cambiar a la sesión [num]</translation>
    </message>
    <message>
        <source>Go to line start</source>
        <translation type="vanished">Ir al inicio de la línea</translation>
    </message>
    <message>
        <source>Go to line end</source>
        <translation type="vanished">Ir al final de la línea</translation>
    </message>
    <message>
        <source>There are sessions that have not yet been unlocked, please unlock them first.</source>
        <translation type="vanished">Hay sesiones que aún no se han desbloqueado, desbloquéelas primero.</translation>
    </message>
    <message>
        <source>Are you sure to quit?</source>
        <translation type="vanished">¿Está seguro de salir?</translation>
    </message>
</context>
<context>
    <name>NetScanWindow</name>
    <message>
        <location filename="../src/netscanwindow/netscanwindow.ui" line="14"/>
        <source>NetScan</source>
        <translation>NetScan</translation>
    </message>
</context>
<context>
    <name>OneStepWindow</name>
    <message>
        <source>Port</source>
        <translation type="obsolete">Puerto</translation>
    </message>
    <message>
        <source>UserName</source>
        <translation type="obsolete">Nombre de usuario</translation>
    </message>
</context>
<context>
    <name>PluginInfoWindow</name>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.ui" line="14"/>
        <source>Plugin Info</source>
        <translation>Información del complemento</translation>
    </message>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.ui" line="28"/>
        <source>API version</source>
        <translation>Versión de API</translation>
    </message>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.ui" line="38"/>
        <source>Install plugin</source>
        <translation>Instalar complemento</translation>
    </message>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="40"/>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="86"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="40"/>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="86"/>
        <source>Version</source>
        <translation>Versión</translation>
    </message>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="40"/>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="86"/>
        <source>API Version</source>
        <translation>Versión de API</translation>
    </message>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="40"/>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="86"/>
        <source>Enable</source>
        <translation>Habilitar</translation>
    </message>
</context>
<context>
    <name>PluginViewerHomeWidget</name>
    <message>
        <location filename="../src/pluginviewerwidget/pluginviewerhomewidget.cpp" line="18"/>
        <source>Welcome to use</source>
        <translation>¡Bienvenido a utilizar el</translation>
    </message>
    <message>
        <location filename="../src/pluginviewerwidget/pluginviewerhomewidget.cpp" line="20"/>
        <source>QuardCRT plugin system!</source>
        <translation>sistema de complementos QuardCRT!</translation>
    </message>
</context>
<context>
    <name>PluginViewerWidget</name>
    <message>
        <location filename="../src/pluginviewerwidget/pluginviewerwidget.cpp" line="37"/>
        <source>Home</source>
        <translation>Inicio</translation>
    </message>
</context>
<context>
    <name>QCustomFileSystemModel</name>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="155"/>
        <source>Directory</source>
        <translation>Directorio</translation>
    </message>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="155"/>
        <source>File</source>
        <translation>Archivo</translation>
    </message>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="159"/>
        <source>Loading...</source>
        <translation>Cargando...</translation>
    </message>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="184"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="186"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="188"/>
        <source>Size</source>
        <translation>Tamaño</translation>
    </message>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="190"/>
        <source>Last Modified</source>
        <translation>Última modificación</translation>
    </message>
</context>
<context>
    <name>QKeychain::DeletePasswordJobPrivate</name>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="173"/>
        <source>Could not open keystore</source>
        <translation>No se pudo abrir el almacén de claves</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="179"/>
        <source>Could not remove private key from keystore</source>
        <translation>No se pudo eliminar la clave privada del almacén de claves</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="584"/>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="592"/>
        <source>Unknown error</source>
        <translation>Error desconocido</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="613"/>
        <source>Could not open wallet: %1; %2</source>
        <translation>No se pudo abrir la billetera: %1; %2</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="104"/>
        <source>Password entry not found</source>
        <translation>Entrada de contraseña no encontrada</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="108"/>
        <source>Could not decrypt data</source>
        <translation>No se pudo descifrar los datos</translation>
    </message>
</context>
<context>
    <name>QKeychain::Job</name>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="29"/>
        <source>No error</source>
        <translation>Sin errores</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="31"/>
        <source>The specified item could not be found in the keychain</source>
        <translation>No se pudo encontrar el elemento especificado en el llavero</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="33"/>
        <source>User canceled the operation</source>
        <translation>El usuario canceló la operación</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="35"/>
        <source>User interaction is not allowed</source>
        <translation>No se permite la interacción del usuario</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="37"/>
        <source>No keychain is available. You may need to restart your computer</source>
        <translation>No hay llavero disponible. Es posible que deba reiniciar su computadora</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="39"/>
        <source>The user name or passphrase you entered is not correct</source>
        <translation>El nombre de usuario o la frase de contraseña que ingresó no es correcta</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="41"/>
        <source>A cryptographic verification failure has occurred</source>
        <translation>Se produjo un error de verificación criptográfica</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="43"/>
        <source>Function or operation not implemented</source>
        <translation>Función u operación no implementada</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="45"/>
        <source>I/O error</source>
        <translation>Error de E/S</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="47"/>
        <source>Already open with with write permission</source>
        <translation>Ya abierto con permiso de escritura</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="49"/>
        <source>Invalid parameters passed to a function</source>
        <translation>Parámetros no válidos pasados ​​a una función</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="51"/>
        <source>Failed to allocate memory</source>
        <translation>Error al asignar memoria</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="53"/>
        <source>Bad parameter or invalid state for operation</source>
        <translation>Parámetro incorrecto o estado no válido para la operación</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="55"/>
        <source>An internal component failed</source>
        <translation>Falló un componente interno</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="57"/>
        <source>The specified item already exists in the keychain</source>
        <translation>El elemento especificado ya existe en el llavero</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="59"/>
        <source>Unable to decode the provided data</source>
        <translation>No se puede decodificar los datos proporcionados</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="62"/>
        <source>Unknown error</source>
        <translation>Error desconocido</translation>
    </message>
</context>
<context>
    <name>QKeychain::JobPrivate</name>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="294"/>
        <source>Unknown error</source>
        <translation>Error desconocido</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="541"/>
        <source>Access to keychain denied</source>
        <translation>Acceso al llavero denegado</translation>
    </message>
</context>
<context>
    <name>QKeychain::PlainTextStore</name>
    <message>
        <location filename="../lib/qtkeychain/plaintextstore.cpp" line="65"/>
        <source>Could not store data in settings: access error</source>
        <translation>No se pudieron almacenar los datos en la configuración: error de acceso</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/plaintextstore.cpp" line="67"/>
        <source>Could not store data in settings: format error</source>
        <translation>No se pudieron almacenar los datos en la configuración: error de formato</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/plaintextstore.cpp" line="85"/>
        <source>Could not delete data from settings: access error</source>
        <translation>No se pudieron eliminar los datos de la configuración: error de acceso</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/plaintextstore.cpp" line="87"/>
        <source>Could not delete data from settings: format error</source>
        <translation>No se pudieron eliminar los datos de la configuración: error de formato</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/plaintextstore.cpp" line="104"/>
        <source>Entry not found</source>
        <translation>Entrada no encontrada</translation>
    </message>
</context>
<context>
    <name>QKeychain::ReadPasswordJobPrivate</name>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="52"/>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="392"/>
        <source>Entry not found</source>
        <translation>Entrada no encontrada</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="60"/>
        <source>Could not open keystore</source>
        <translation>No se pudo abrir el almacén de claves</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="68"/>
        <source>Could not retrieve private key from keystore</source>
        <translation>No se pudo recuperar la clave privada del almacén de claves</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="75"/>
        <source>Could not create decryption cipher</source>
        <translation>No se pudo crear el cifrado de descifrado</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="204"/>
        <source>D-Bus is not running</source>
        <translation>D-Bus no se está ejecutando</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="213"/>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="223"/>
        <source>Unknown error</source>
        <translation>Error desconocido</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="315"/>
        <source>No keychain service available</source>
        <translation>No hay servicio de llavero disponible</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="317"/>
        <source>Could not open wallet: %1; %2</source>
        <translation>No se pudo abrir la billetera: %1; %2</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="362"/>
        <source>Access to keychain denied</source>
        <translation>Acceso al llavero denegado</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="383"/>
        <source>Could not determine data type: %1; %2</source>
        <translation>No se pudo determinar el tipo de datos: %1; %2</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="401"/>
        <source>Unsupported entry type &apos;Map&apos;</source>
        <translation>Tipo de entrada no compatible &apos;Map&apos;</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="404"/>
        <source>Unknown kwallet entry type &apos;%1&apos;</source>
        <translation>Tipo de entrada kwallet desconocido &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="32"/>
        <source>Password entry not found</source>
        <translation>Entrada de contraseña no encontrada</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="36"/>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="139"/>
        <source>Could not decrypt data</source>
        <translation>No se pudo descifrar los datos</translation>
    </message>
</context>
<context>
    <name>QKeychain::WritePasswordJobPrivate</name>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="95"/>
        <source>Could not open keystore</source>
        <translation>No se pudo abrir el almacén de claves</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="124"/>
        <source>Could not create private key generator</source>
        <translation>No se pudo crear el generador de claves privadas</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="131"/>
        <source>Could not generate new private key</source>
        <translation>No se pudo generar una nueva clave privada</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="139"/>
        <source>Could not retrieve private key from keystore</source>
        <translation>No se pudo recuperar la clave privada del almacén de claves</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="147"/>
        <source>Could not create encryption cipher</source>
        <translation>No se pudo crear el cifrado de cifrado</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="155"/>
        <source>Could not encrypt data</source>
        <translation>No se pudieron cifrar los datos</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="444"/>
        <source>D-Bus is not running</source>
        <translation>D-Bus no se está ejecutando</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="454"/>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="481"/>
        <source>Unknown error</source>
        <translation>Error desconocido</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="500"/>
        <source>Could not open wallet: %1; %2</source>
        <translation>No se pudo abrir la billetera: %1; %2</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="78"/>
        <source>Credential size exceeds maximum size of %1</source>
        <translation>El tamaño de la credencial supera el tamaño máximo de %1</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="87"/>
        <source>Credential key exceeds maximum size of %1</source>
        <translation>La clave de credencial supera el tamaño máximo de %1</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="92"/>
        <source>Writing credentials failed: Win32 error code %1</source>
        <translation>Error al escribir las credenciales: código de error Win32 %1</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="162"/>
        <source>Encryption failed</source>
        <translation>Cifrado fallido</translation>
    </message>
</context>
<context>
    <name>QMessageBox</name>
    <message>
        <location filename="../lib/qtermwidget/TerminalDisplay.cpp" line="2949"/>
        <source>Show Details...</source>
        <translation>Mostrar detalles...</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../lib/qtermwidget/ColorScheme.cpp" line="267"/>
        <location filename="../lib/qtermwidget/ColorScheme.cpp" line="282"/>
        <source>Un-named Color Scheme</source>
        <translation>Esquema de color sin nombre</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/ColorScheme.cpp" line="453"/>
        <source>Accessible Color Scheme</source>
        <translation>Esquema de color accesible</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/Filter.cpp" line="549"/>
        <source>Open Link</source>
        <translation>Abrir enlace</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/Filter.cpp" line="550"/>
        <source>Copy Link Address</source>
        <translation>Copiar dirección del enlace</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/Filter.cpp" line="554"/>
        <source>Send Email To...</source>
        <translation>Enviar correo electrónico a...</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/Filter.cpp" line="555"/>
        <source>Copy Email Address</source>
        <translation>Copiar dirección de correo electrónico</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/Filter.cpp" line="559"/>
        <source>Open Path</source>
        <translation>Abrir ruta</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/Filter.cpp" line="560"/>
        <source>Copy Path</source>
        <translation>Copiar ruta</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="254"/>
        <source>Access to keychain denied</source>
        <translation>Acceso al llavero denegado</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="256"/>
        <source>No keyring daemon</source>
        <translation>No hay demonio de llavero</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="258"/>
        <source>Already unlocked</source>
        <translation>Ya desbloqueado</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="260"/>
        <source>No such keyring</source>
        <translation>No hay tal llavero</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="262"/>
        <source>Bad arguments</source>
        <translation>Argumentos incorrectos</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="264"/>
        <source>I/O error</source>
        <translation>Error de E/S</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="266"/>
        <source>Cancelled</source>
        <translation>Cancelado</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="268"/>
        <source>Keyring already exists</source>
        <translation>El llavero ya existe</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="270"/>
        <source>No match</source>
        <translation>No hay coincidencia</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="275"/>
        <source>Unknown error</source>
        <translation>Error desconocido</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/libsecret.cpp" line="120"/>
        <source>Entry not found</source>
        <translation>Entrada no encontrada</translation>
    </message>
</context>
<context>
    <name>QTermWidget</name>
    <message>
        <location filename="../lib/qtermwidget/qtermwidget.cpp" line="350"/>
        <source>Color Scheme Error</source>
        <translation>Error de esquema de color</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/qtermwidget.cpp" line="351"/>
        <source>Cannot load color scheme: %1</source>
        <translation>No se puede cargar el esquema de color: %1</translation>
    </message>
</context>
<context>
    <name>QuickConnectWindow</name>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="30"/>
        <source>Protocol</source>
        <translation>Protocolo</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="43"/>
        <source>Serial</source>
        <translation>Serie</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="48"/>
        <source>Local Shell</source>
        <translation>Shell local</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="58"/>
        <source>Named Pipe</source>
        <translation>Tubería con nombre</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="86"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="69"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="166"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="224"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="253"/>
        <source>Hostname</source>
        <translation>Nombre de host</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="109"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="70"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="167"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="225"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="254"/>
        <source>Port</source>
        <translation>Puerto</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="125"/>
        <source>WebSocket</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="133"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="233"/>
        <source>None</source>
        <translation>Ninguno</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="138"/>
        <source>Insecure</source>
        <translation>Inseguro</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="143"/>
        <source>Secure</source>
        <translation>Seguro</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="161"/>
        <source>Username</source>
        <translation>Nombre de usuario</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="171"/>
        <source>Password</source>
        <translation>Contraseña</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="188"/>
        <source>DataBits</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="225"/>
        <source>Parity</source>
        <translation>Paridad</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="238"/>
        <source>Odd</source>
        <translation>Impar</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="243"/>
        <source>Even</source>
        <translation>Par</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="261"/>
        <source>StopBits</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="313"/>
        <source>Save session</source>
        <translation>Guardar sesión</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="323"/>
        <source>Open in tab</source>
        <translation>Abrir en pestaña</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="33"/>
        <source>Quick Connect</source>
        <translation>Conexión rápida</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="91"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="188"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="246"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="275"/>
        <source>e.g. 127.0.0.1</source>
        <translation>ej. 127.0.0.1</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="99"/>
        <source>Port Name</source>
        <translation>Nombre de puerto</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="100"/>
        <source>Baud Rate</source>
        <translation>Tasa de baudios</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="109"/>
        <source>e.g. 110, 300, 600, 1200, 2400, 
4800, 9600, 14400, 19200, 38400, 
56000, 57600, 115200, 128000, 256000, 
460800, 921600</source>
        <translation>ej. 110, 300, 600, 1200, 2400,
4800, 9600, 14400, 19200, 38400,
56000, 57600, 115200, 128000, 256000,
460800, 921600</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="141"/>
        <source>Command</source>
        <translation>Comando</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="162"/>
        <source>e.g. /bin/bash</source>
        <translation>ej. /bin/bash</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="195"/>
        <source>Pipe Name</source>
        <translation>Nombre de tubería</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="217"/>
        <source>e.g. \\\.\pipe\namedpipe</source>
        <translation>ej. \\\.\pipe\namedpipe</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="219"/>
        <source>e.g. /tmp/socket</source>
        <translation>ej. /tmp/socket</translation>
    </message>
</context>
<context>
    <name>SFTPmenuBookmarkWidget</name>
    <message>
        <location filename="../src/sftpwindow/sftpmenubookmarkwidget.ui" line="17"/>
        <source>BookMarkName</source>
        <translation>Nombre del marcador</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpmenubookmarkwidget.ui" line="35"/>
        <source>LocalPath</source>
        <translation>Ruta local</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpmenubookmarkwidget.ui" line="62"/>
        <source>RemotePath</source>
        <translation>Ruta remota</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpmenubookmarkwidget.cpp" line="31"/>
        <source>Open Directory</source>
        <translation>Abrir directorio</translation>
    </message>
</context>
<context>
    <name>SFTPmenuBookmarkWindow</name>
    <message>
        <source>Bookmark</source>
        <translation type="vanished">Marcador</translation>
    </message>
    <message>
        <source>BookMarkName</source>
        <translation type="vanished">Nombre del marcador</translation>
    </message>
    <message>
        <source>LocalPath</source>
        <translation type="vanished">Ruta local</translation>
    </message>
    <message>
        <source>RemotePath</source>
        <translation type="vanished">Ruta remota</translation>
    </message>
    <message>
        <source>Open Directory</source>
        <translation type="vanished">Abrir directorio</translation>
    </message>
</context>
<context>
    <name>SearchBar</name>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.ui" line="14"/>
        <source>SearchBar</source>
        <translation>Barra de búsqueda</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.ui" line="20"/>
        <source>X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.ui" line="32"/>
        <source>Find:</source>
        <translation>Buscar:</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.ui" line="42"/>
        <source>&lt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.ui" line="54"/>
        <source>&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.ui" line="66"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.cpp" line="40"/>
        <location filename="../lib/qtermwidget/SearchBar.cpp" line="130"/>
        <source>Match case</source>
        <translation>Coincidir mayúsculas y minúsculas</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.cpp" line="46"/>
        <location filename="../lib/qtermwidget/SearchBar.cpp" line="131"/>
        <source>Regular expression</source>
        <translation>Expresión regular</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.cpp" line="50"/>
        <location filename="../lib/qtermwidget/SearchBar.cpp" line="132"/>
        <source>Highlight all matches</source>
        <translation>Resaltar todas las coincidencias</translation>
    </message>
</context>
<context>
    <name>SessionManagerTreeModel</name>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="133"/>
        <source>Telnet</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="135"/>
        <source>Serial</source>
        <translation>Serie</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="137"/>
        <source>Shell</source>
        <translation>Shell</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="139"/>
        <source>Raw</source>
        <translation>Crudo</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="141"/>
        <source>NamePipe</source>
        <translation>Nombre de tubería</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="269"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="271"/>
        <source>Kind</source>
        <translation>Tipo</translation>
    </message>
</context>
<context>
    <name>SessionManagerTreeView</name>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="39"/>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="74"/>
        <source>Session</source>
        <translation>Sesión</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="84"/>
        <source>Connect Terminal</source>
        <translation>Conectar terminal</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="89"/>
        <source>Connect in New Window</source>
        <translation>Conectar en nueva ventana</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="96"/>
        <source>Connect in New Tab Group</source>
        <translation>Conectar en nuevo grupo de pestañas</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="103"/>
        <source>Delete</source>
        <translation>Borrar</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="109"/>
        <source>Properties</source>
        <translation>Propiedades</translation>
    </message>
</context>
<context>
    <name>SessionManagerWidget</name>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagerwidget.ui" line="43"/>
        <source>Session Manager</source>
        <translation>Administrador de sesiones</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagerwidget.cpp" line="67"/>
        <source>Filter by folder/session name</source>
        <translation>Filtrar por carpeta/nombre de sesión</translation>
    </message>
</context>
<context>
    <name>SessionOptionsGeneralWidget</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsgeneralwidget.ui" line="25"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsgeneralwidget.ui" line="45"/>
        <source>Protocol</source>
        <translation>Protocolo</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsgeneralwidget.ui" line="58"/>
        <source>Serial</source>
        <translation>Serie</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsgeneralwidget.ui" line="63"/>
        <source>Local Shell</source>
        <translation>Shell local</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsgeneralwidget.ui" line="73"/>
        <source>Named Pipe</source>
        <translation>Tubería con nombre</translation>
    </message>
</context>
<context>
    <name>SessionOptionsLocalShellProperties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionslocalshellproperties.ui" line="19"/>
        <source>Command</source>
        <translation>Comando</translation>
    </message>
</context>
<context>
    <name>SessionOptionsLocalShellState</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionslocalshellstate.cpp" line="33"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionslocalshellstate.ui" line="25"/>
        <source>State</source>
        <translation>Estado</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionslocalshellstate.ui" line="51"/>
        <source>Process Tree</source>
        <translation>Árbol de procesos</translation>
    </message>
</context>
<context>
    <name>SessionOptionsNamePipeProperties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsnamepipeproperties.ui" line="25"/>
        <source>PipeName</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SessionOptionsRawProperties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsrawproperties.ui" line="25"/>
        <source>Hostname</source>
        <translation>Nombre de host</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsrawproperties.ui" line="45"/>
        <source>Port</source>
        <translation>Puerto</translation>
    </message>
</context>
<context>
    <name>SessionOptionsSerialProperties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="25"/>
        <source>Port Name</source>
        <translation>Nombre de puerto</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="45"/>
        <source>Baud Rate</source>
        <translation>Tasa de baudios</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="72"/>
        <source>DataBits</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="109"/>
        <source>Parity</source>
        <translation>Paridad</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="117"/>
        <source>None</source>
        <translation>Ninguno</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="122"/>
        <source>Odd</source>
        <translation>Impar</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="127"/>
        <source>Even</source>
        <translation>Par</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="145"/>
        <source>StopBits</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.cpp" line="28"/>
        <source>e.g. 110, 300, 600, 1200, 2400, 
4800, 9600, 14400, 19200, 38400, 
56000, 57600, 115200, 128000, 256000, 
460800, 921600</source>
        <translation>ej. 110, 300, 600, 1200, 2400,
4800, 9600, 14400, 19200, 38400,
56000, 57600, 115200, 128000, 256000,
460800, 921600</translation>
    </message>
</context>
<context>
    <name>SessionOptionsSsh2Properties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsssh2properties.ui" line="25"/>
        <source>Hostname</source>
        <translation>Nombre de host</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsssh2properties.ui" line="41"/>
        <source>Port</source>
        <translation>Puerto</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsssh2properties.ui" line="65"/>
        <source>UserName</source>
        <translation>Nombre de usuario</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsssh2properties.ui" line="81"/>
        <source>Password</source>
        <translation>Contraseña</translation>
    </message>
</context>
<context>
    <name>SessionOptionsTelnetProperties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionstelnetproperties.ui" line="19"/>
        <source>Hostname</source>
        <translation>Nombre de host</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionstelnetproperties.ui" line="33"/>
        <source>Port</source>
        <translation>Puerto</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionstelnetproperties.ui" line="55"/>
        <source>None</source>
        <translation>Ninguno</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionstelnetproperties.ui" line="60"/>
        <source>Insecure</source>
        <translation>Inseguro</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionstelnetproperties.ui" line="65"/>
        <source>Secure</source>
        <translation>Seguro</translation>
    </message>
</context>
<context>
    <name>SessionOptionsVNCProperties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsvncproperties.ui" line="25"/>
        <source>Hostname</source>
        <translation>Nombre de host</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsvncproperties.ui" line="41"/>
        <source>Port</source>
        <translation>Puerto</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsvncproperties.ui" line="65"/>
        <source>Password</source>
        <translation>Contraseña</translation>
    </message>
</context>
<context>
    <name>SessionOptionsWindow</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionswindow.ui" line="14"/>
        <source>Session Options</source>
        <translation>Opciones de sesión</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionswindow.cpp" line="122"/>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionswindow.cpp" line="122"/>
        <source>Properties</source>
        <translation>Propiedades</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionswindow.cpp" line="122"/>
        <source>State</source>
        <translation>Estado</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionswindow.cpp" line="133"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
</context>
<context>
    <name>SessionsWindow</name>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="141"/>
        <source>Telnet Error</source>
        <translation>Error de Telnet</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="141"/>
        <source>Telnet error:
%1.</source>
        <translation>Error de Telnet:
%1.</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="168"/>
        <source>Serial Error</source>
        <translation>Error de serie</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="168"/>
        <source>Serial error:
%1.</source>
        <translation>Error de serie:
%1.</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="203"/>
        <source>Raw Socket Error</source>
        <translation>Error de socket sin formato</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="203"/>
        <source>Raw Socket error:
%1.</source>
        <translation>Error de socket sin formato:
%1.</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="234"/>
        <source>Name Pipe Error</source>
        <translation>Error de nombre de tubería</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="234"/>
        <source>Name Pipe error:
%1.</source>
        <translation>Error de nombre de tubería:
%1.</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="246"/>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="275"/>
        <source>SSH2 Error</source>
        <translation>Error de SSH2</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="246"/>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="275"/>
        <source>SSH2 error:
%1.</source>
        <translation>Error de SSH2:
%1.</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="434"/>
        <source>Start Local Shell</source>
        <translation>Iniciar shell local</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="434"/>
        <source>Cannot start local shell:
%1.</source>
        <translation>No se puede iniciar el shell local:
%1.</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="573"/>
        <source>Save log...</source>
        <translation>Guardar registro...</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="574"/>
        <source>log files (*.log)</source>
        <translation>archivos de registro (*.log)</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="578"/>
        <source>Save log</source>
        <translation>Guardar registro</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="578"/>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="613"/>
        <source>Cannot write file %1:
%2.</source>
        <translation>No se puede escribir el archivo %1:
%2.</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="608"/>
        <source>Save Raw log...</source>
        <translation>Guardar registro sin formato...</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="609"/>
        <source>binary files (*.bin)</source>
        <translation>archivos binarios (*.bin)</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="613"/>
        <source>Save Raw log</source>
        <translation>Guardar registro sin formato</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="685"/>
        <source>Unlock Session</source>
        <translation>Desbloquear sesión</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="685"/>
        <source>Wrong password!</source>
        <translation>¡Contraseña incorrecta!</translation>
    </message>
</context>
<context>
    <name>SftpWindow</name>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.ui" line="14"/>
        <source>sftp</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.ui" line="72"/>
        <source>local</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.ui" line="130"/>
        <source>remote</source>
        <translation>rremoto</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="46"/>
        <source>Bookmarks</source>
        <translation>Marcadores</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="47"/>
        <source>Add Bookmark</source>
        <translation>Añadir marcador</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="48"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="67"/>
        <source>Edit Bookmark</source>
        <translation>Editar marcador</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="49"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="84"/>
        <source>Remove Bookmark</source>
        <translation>Eliminar marcador</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="67"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="84"/>
        <source>Bookmark Name:</source>
        <translation>Nombre del marcador:</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="109"/>
        <source>Warning</source>
        <translation>Advertencia</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="109"/>
        <source>Bookmark Name can not be empty!</source>
        <translation>¡El nombre del marcador no puede estar vacío!</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="196"/>
        <source>No task!</source>
        <translation>¡Sin tarea!</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="200"/>
        <source>All tasks finished!</source>
        <translation>¡Todas las tareas terminadas!</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="205"/>
        <source>task %1/%2</source>
        <translation>tarea %1/%2</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="223"/>
        <source>Open Directory</source>
        <translation>Abrir directorio</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="246"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="412"/>
        <source>Show/Hide Files</source>
        <translation>Mostrar/Ocultar archivos</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="250"/>
        <source>Upload</source>
        <translation>Cargar</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="301"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="476"/>
        <source>Open</source>
        <translation>Abrir</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="305"/>
        <source>Open in System File Manager</source>
        <translation>Abrir en el administrador de archivos</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="310"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="482"/>
        <source>refresh</source>
        <translation>actualizar</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="315"/>
        <source>Upload All</source>
        <translation>Cargar todo</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="359"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="535"/>
        <source>Cancel Selection</source>
        <translation>Cancelar selección</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="416"/>
        <source>Download</source>
        <translation>Descargar</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="456"/>
        <source>File exists</source>
        <translation>El archivo existe</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="456"/>
        <source>File %1 already exists. Do you want to overwrite it?</source>
        <translation>El archivo %1 ya existe. ¿Quieres sobrescribirlo?</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="487"/>
        <source>Download All</source>
        <translation>Descargar todo</translation>
    </message>
</context>
<context>
    <name>StartTftpSeverWindow</name>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.ui" line="14"/>
        <source>Start TFTP Sever</source>
        <translation>Iniciar servidor TFTP</translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.ui" line="32"/>
        <source>The TFTP server uses local directories for uploading and downloading files. Please enter this information below.</source>
        <translation>El servidor TFTP utiliza directorios locales para cargar y descargar archivos. Por favor ingrese esta información a continuación.</translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.ui" line="65"/>
        <source>TFTP Port</source>
        <translation>Puerto TFTP</translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.ui" line="104"/>
        <source>Upload directory</source>
        <translation>Directorio de carga</translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.ui" line="143"/>
        <source>Download directory</source>
        <translation>Directorio de descarga</translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.cpp" line="38"/>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.cpp" line="45"/>
        <source>Open Directory</source>
        <translation>Abrir directorio</translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.cpp" line="77"/>
        <source>Warning</source>
        <translation>Advertencia</translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.cpp" line="77"/>
        <source>Please select a valid directory!</source>
        <translation>¡Por favor seleccione un directorio válido!</translation>
    </message>
</context>
<context>
    <name>UndoStack</name>
    <message>
        <location filename="../lib/qhexedit/commands.cpp" line="115"/>
        <source>Inserting %1 bytes</source>
        <translation>Insertando %1 bytes</translation>
    </message>
    <message>
        <location filename="../lib/qhexedit/commands.cpp" line="137"/>
        <source>Delete %1 chars</source>
        <translation>Borrar %1 caracteres</translation>
    </message>
    <message>
        <location filename="../lib/qhexedit/commands.cpp" line="162"/>
        <source>Overwrite %1 chars</source>
        <translation>Sobrescribir %1 caracteres</translation>
    </message>
</context>
<context>
    <name>keyMapManager</name>
    <message>
        <location filename="../src/keymapmanager/keymapmanager.ui" line="14"/>
        <source>keyMapManager</source>
        <translation>Administrador de mapas de teclado</translation>
    </message>
    <message>
        <location filename="../src/keymapmanager/keymapmanager.ui" line="20"/>
        <source>KeyBinding</source>
        <translation>Asignación de teclas</translation>
    </message>
</context>
</TS>
