<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>AddTabButton</name>
    <message>
        <location filename="../lib/QtFancyTabWidget/addtabbutton.cpp" line="29"/>
        <source>New tab</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>CentralWidget</name>
    <message>
        <location filename="../src/mainwindow.ui" line="101"/>
        <location filename="../src/mainwindow.cpp" line="1123"/>
        <source>Tool Bar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="191"/>
        <location filename="../src/mainwindow.cpp" line="194"/>
        <location filename="../src/mainwindow.cpp" line="197"/>
        <location filename="../src/mainwindow.cpp" line="325"/>
        <location filename="../src/mainwindow.cpp" line="341"/>
        <location filename="../src/mainwindow.cpp" line="358"/>
        <location filename="../src/mainwindow.cpp" line="371"/>
        <location filename="../src/mainwindow.cpp" line="546"/>
        <location filename="../src/mainwindow.cpp" line="2552"/>
        <location filename="../src/mainwindow.cpp" line="2747"/>
        <location filename="../src/mainwindow.cpp" line="3115"/>
        <location filename="../src/mainwindow.cpp" line="3451"/>
        <location filename="../src/mainwindow.cpp" line="3456"/>
        <source>Warning</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="191"/>
        <source>TFTP server bind error!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="194"/>
        <source>TFTP server file error!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="197"/>
        <source>TFTP server network error!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="286"/>
        <source>Unlock Session</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="287"/>
        <source>Unlock current session</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="293"/>
        <source>Move to another Tab</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="294"/>
        <source>Move to current session to another tab group</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="303"/>
        <source>Floating Window</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="304"/>
        <source>Floating current session to a new window</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="311"/>
        <source>Copy Path</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="312"/>
        <source>Copy current session working folder path</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="325"/>
        <location filename="../src/mainwindow.cpp" line="341"/>
        <location filename="../src/mainwindow.cpp" line="358"/>
        <source>No working folder!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="327"/>
        <source>Add Path to Bookmark</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="328"/>
        <source>Add current session working folder path to bookmark</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="343"/>
        <source>Open Working Folder</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="344"/>
        <source>Open current session working folder in system file manager</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="362"/>
        <source>Open SFTP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="363"/>
        <source>Open SFTP in a new window</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="371"/>
        <source>No SFTP channel!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="378"/>
        <source>Save Session</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="379"/>
        <source>Save current session to session manager</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="389"/>
        <source>Enter Session Name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="390"/>
        <source>The session already exists, please rename the new session or cancel saving.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="396"/>
        <source>Properties</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="397"/>
        <source>Show current session properties</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="417"/>
        <source>Close</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="418"/>
        <source>Close current session</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="423"/>
        <source>Close Others</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="424"/>
        <source>Close other sessions</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="434"/>
        <source>Close to the Right</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="435"/>
        <source>Close sessions to the right</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="443"/>
        <source>Close All</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="444"/>
        <source>Close all sessions</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="546"/>
        <source>Session properties error!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="566"/>
        <location filename="../src/mainwindow.cpp" line="684"/>
        <location filename="../src/mainwindow.cpp" line="2455"/>
        <source>Ready</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="777"/>
        <source>Highlight/Unhighlight</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="778"/>
        <source>Highlight/Unhighlight selected text</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="838"/>
        <source>Google Translate</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="839"/>
        <location filename="../src/mainwindow.cpp" line="852"/>
        <location filename="../src/mainwindow.cpp" line="864"/>
        <source>Translate selected text</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="851"/>
        <source>Baidu Translate</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="863"/>
        <source>Microsoft Translate</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="924"/>
        <source>Back to Main Window</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="995"/>
        <location filename="../src/mainwindow.cpp" line="1019"/>
        <source>Session Manager</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="996"/>
        <source>Plugin</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="998"/>
        <source>File</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="999"/>
        <source>Edit</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1000"/>
        <source>View</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1001"/>
        <source>Options</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1002"/>
        <source>Transfer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1003"/>
        <source>Script</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1004"/>
        <source>Bookmark</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1005"/>
        <source>Tools</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1006"/>
        <source>Window</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1007"/>
        <source>Language</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1008"/>
        <source>Theme</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1009"/>
        <location filename="../src/mainwindow.cpp" line="1229"/>
        <location filename="../src/mainwindow.cpp" line="3415"/>
        <source>Help</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1011"/>
        <source>New Window</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1013"/>
        <source>Open a new window &lt;Ctrl+Shift+N&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1015"/>
        <source>Connect...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1017"/>
        <source>Connect to a host &lt;Alt+C&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1021"/>
        <source>Go to the Session Manager &lt;Alt+M&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1023"/>
        <source>Quick Connect...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1025"/>
        <source>Quick Connect to a host &lt;Alt+Q&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1027"/>
        <source>Connect in Tab/Tile...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1028"/>
        <source>Connect to a host in a new tab &lt;Alt+B&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1030"/>
        <source>Connect Local Shell</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1032"/>
        <source>Connect to a local shell &lt;Alt+T&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1034"/>
        <source>Reconnect</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1036"/>
        <source>Reconnect current session</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1037"/>
        <source>Reconnect All</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1038"/>
        <source>Reconnect all sessions &lt;Alt+A&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1040"/>
        <source>Disconnect</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1042"/>
        <source>Disconnect current session</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1043"/>
        <location filename="../src/mainwindow.cpp" line="1044"/>
        <source>Enter host &lt;Alt+R&gt; to connect</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1045"/>
        <source>Disconnect All</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1046"/>
        <source>Disconnect all sessions</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1047"/>
        <source>Clone Session</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1048"/>
        <source>Clone current session &lt;Ctrl+Shift+T&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1050"/>
        <source>Lock Session</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1051"/>
        <source>Lock/Unlock current session</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1052"/>
        <source>Log Session</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1053"/>
        <source>Create a log file for current session</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1054"/>
        <source>Raw Log Session</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1055"/>
        <source>Create a raw log file for current session</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1056"/>
        <source>Hex View</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1057"/>
        <source>Show/Hide Hex View for current session</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1058"/>
        <source>Exit</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1059"/>
        <source>Quit the application</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1061"/>
        <source>Copy</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1064"/>
        <source>Copy the selected text to the clipboard &lt;Command+C&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1067"/>
        <source>Copy the selected text to the clipboard &lt;Ctrl+Ins&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1070"/>
        <source>Paste</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1073"/>
        <source>Paste the clipboard text to the current session &lt;Command+V&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1076"/>
        <source>Paste the clipboard text to the current session &lt;Shift+Ins&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1079"/>
        <source>Copy and Paste</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1080"/>
        <source>Copy the selected text to the clipboard and paste to the current session</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1081"/>
        <source>Select All</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1083"/>
        <source>Select all text in the current session &lt;Ctrl+Shift+A&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1085"/>
        <source>Find...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1087"/>
        <source>Find text in the current session &lt;Ctrl+F&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1089"/>
        <source>Print Screen</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1091"/>
        <source>Print current screen</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1092"/>
        <source>Screen Shot</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1094"/>
        <source>Screen shot current screen &lt;Alt+P&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1096"/>
        <source>Session Export</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1098"/>
        <source>Export current session to a file &lt;Alt+O&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1100"/>
        <source>Clear Scrollback</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1101"/>
        <source>Clear the contents of the scrollback rows</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1102"/>
        <source>Clear Screen</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1103"/>
        <source>Clear the contents of the current screen</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1104"/>
        <source>Clear Screen and Scrollback</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1105"/>
        <source>Clear the contents of the screen and scrollback</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1106"/>
        <source>Reset</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1107"/>
        <source>Reset terminal emulator</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1109"/>
        <source>Zoom In</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1111"/>
        <source>Zoom In &lt;Ctrl+&quot;=&quot;&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1113"/>
        <source>Zoom Out</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1115"/>
        <source>Zoom Out &lt;Ctrl+&quot;-&quot;&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1117"/>
        <location filename="../src/mainwindow.cpp" line="1119"/>
        <source>Zoom Reset</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1120"/>
        <source>Menu Bar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1121"/>
        <source>Show/Hide Menu Bar &lt;Alt+U&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1124"/>
        <source>Show/Hide Tool Bar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1125"/>
        <source>Status Bar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1126"/>
        <source>Show/Hide Status Bar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1127"/>
        <source>Command Window</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1128"/>
        <source>Show/Hide Command Window</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1129"/>
        <source>Connect Bar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1130"/>
        <source>Show/Hide Connect Bar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1131"/>
        <source>Side Window</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1132"/>
        <source>Show/Hide Side Window</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1133"/>
        <source>Windows Transparency</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1134"/>
        <source>Enable/Disable alpha transparency</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1135"/>
        <source>Vertical Scroll Bar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1136"/>
        <source>Show/Hide Vertical Scroll Bar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1137"/>
        <source>Allways On Top</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1138"/>
        <source>Show window always on top</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1139"/>
        <source>Full Screen</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1140"/>
        <source>Toggle between full screen and normal mode &lt;Alt+Enter&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1143"/>
        <source>Session Options...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1145"/>
        <source>Configure session options</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1146"/>
        <source>Global Options...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1148"/>
        <source>Configure global options</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1149"/>
        <source>Real-time Save Options</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1150"/>
        <source>Real-time save session options and global options</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1151"/>
        <source>Save Settings Now</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1152"/>
        <source>Save options configuration now</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1154"/>
        <source>Send ASCII...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1155"/>
        <source>Send ASCII file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1156"/>
        <source>Receive ASCII...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1157"/>
        <source>Receive ASCII file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1158"/>
        <source>Send Binary...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1159"/>
        <source>Send Binary file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1160"/>
        <source>Send Xmodem...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1161"/>
        <source>Send a file using Xmodem</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1162"/>
        <source>Receive Xmodem...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1163"/>
        <source>Receive a file using Xmodem</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1164"/>
        <source>Send Ymodem...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1165"/>
        <source>Send a file using Ymodem</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1166"/>
        <source>Receive Ymodem...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1167"/>
        <source>Receive a file using Ymodem</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1168"/>
        <source>Zmodem Upload List...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1169"/>
        <source>Display Zmodem file upload list</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1170"/>
        <source>Start Zmodem Upload</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1171"/>
        <source>Start Zmodem file upload</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1172"/>
        <source>Start TFTP Server</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1173"/>
        <source>Start/Stop the TFTP server</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1175"/>
        <source>Run...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1176"/>
        <source>Run a script</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1177"/>
        <source>Cancel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1178"/>
        <source>Cancel script execution</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1179"/>
        <source>Start Recording Script</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1180"/>
        <source>Start recording script</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1181"/>
        <source>Stop Recording Script...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1182"/>
        <source>Stop recording script</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1183"/>
        <source>Cancel Recording Script</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1184"/>
        <source>Cancel recording script</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1186"/>
        <source>Add Bookmark</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1187"/>
        <source>Add a bookmark</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1188"/>
        <location filename="../src/mainwindow.cpp" line="2350"/>
        <source>Remove Bookmark</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1189"/>
        <source>Remove a bookmark</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1190"/>
        <location filename="../src/mainwindow.cpp" line="2377"/>
        <source>Clean All Bookmark</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1191"/>
        <source>Clean all bookmark</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1193"/>
        <source>Keymap Manager</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1194"/>
        <source>Display keymap editor</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1195"/>
        <source>Create Public Key...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1196"/>
        <source>Create a public key</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1197"/>
        <source>Publickey Manager</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1198"/>
        <source>Display publickey manager</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1242"/>
        <source>Laboratory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1245"/>
        <location filename="../src/mainwindow.cpp" line="2395"/>
        <source>SSH Scanning</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1246"/>
        <source>Display SSH scanning dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3399"/>
        <source>Version</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3401"/>
        <source>Commit</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3403"/>
        <source>Date</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3405"/>
        <source>Author</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3407"/>
        <source>Website</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1200"/>
        <source>Tab</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1201"/>
        <source>Arrange sessions in tabs</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1202"/>
        <source>Tile</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1203"/>
        <source>Arrange sessions in non-overlapping tiles</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1204"/>
        <source>Cascade</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1205"/>
        <source>Arrange sessions to overlap each other</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1207"/>
        <source>Simplified Chinese</source>
        <translation>简体中文</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1208"/>
        <source>Switch to Simplified Chinese</source>
        <translation>切换到简体中文</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1209"/>
        <source>Traditional Chinese</source>
        <translation>繁體中文</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1210"/>
        <source>Switch to Traditional Chinese</source>
        <translation>切換到繁體中文</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1211"/>
        <source>Russian</source>
        <translation>Русский</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1212"/>
        <source>Switch to Russian</source>
        <translation>Переключиться на русский</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1213"/>
        <source>Korean</source>
        <translation>한국어</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1214"/>
        <source>Switch to Korean</source>
        <translation>한국어로 전환</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1215"/>
        <source>Japanese</source>
        <translation>日本語</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1216"/>
        <source>Switch to Japanese</source>
        <translation>日本語に切り替える</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1217"/>
        <source>French</source>
        <translation>français</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1218"/>
        <source>Switch to French</source>
        <translation>Passer au français</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1219"/>
        <source>Spanish</source>
        <translation>español</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1220"/>
        <source>Switch to Spanish</source>
        <translation>Cambiar a español</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1221"/>
        <source>English</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1222"/>
        <source>Switch to English</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1224"/>
        <source>Light</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1225"/>
        <source>Switch to light theme</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1226"/>
        <source>Dark</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1227"/>
        <source>Switch to dark theme</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1231"/>
        <source>Display help</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1232"/>
        <source>Check Update</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1234"/>
        <source>Check for updates</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1235"/>
        <location filename="../src/mainwindow.cpp" line="3397"/>
        <source>About</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1237"/>
        <source>Display about dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1238"/>
        <source>About Qt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1240"/>
        <source>Display about Qt dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1247"/>
        <source>Plugin Info</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1248"/>
        <source>Display plugin information dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2134"/>
        <source>PrintScreen saved to %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2146"/>
        <source>Save Screenshot</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2146"/>
        <source>Image Files (*.jpg)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2151"/>
        <source>Screenshot saved to %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2162"/>
        <source>Save Session Export</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2162"/>
        <source>Text Files (*.txt);;HTML Files (*.html)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2165"/>
        <source>Text Files (*.txt)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2168"/>
        <source>HTML Files (*.html)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2177"/>
        <source>Session Export saved to %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2179"/>
        <source>Session Export failed to save to %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2323"/>
        <source>Select a directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2350"/>
        <source>Select a bookmark</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2377"/>
        <source>Are you sure to clean all bookmark?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2395"/>
        <source>Port</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2552"/>
        <source>Video background is enabled, please enable animation in global options (more system resources) or change background image.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2747"/>
        <source>Session information get failed.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2830"/>
        <source>Telnet - </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2831"/>
        <source>Telnet</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2863"/>
        <source>Serial - </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2864"/>
        <source>Serial</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2895"/>
        <source>Raw - </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2896"/>
        <source>Raw</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2927"/>
        <source>NamePipe - </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2928"/>
        <source>NamePipe</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2996"/>
        <location filename="../src/mainwindow.cpp" line="3000"/>
        <source>Local Shell</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2998"/>
        <source>Local Shell - </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3115"/>
        <source>Are you sure to disconnect this session?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3417"/>
        <source>Global Shortcuts:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3418"/>
        <source>show/hide menu bar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3419"/>
        <source>connect to LocalShell</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3420"/>
        <source>clone current session</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3421"/>
        <source>switch ui to STD mode</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3422"/>
        <source>switch ui to MINI mode</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3423"/>
        <source>switch to previous session</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3424"/>
        <source>switch to next session</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3425"/>
        <source>switch to session [num]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3426"/>
        <source>Go to line start</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3427"/>
        <source>Go to line end</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3451"/>
        <source>There are sessions that have not yet been unlocked, please unlock them first.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3456"/>
        <source>Are you sure to quit?</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>CommandWidget</name>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="35"/>
        <source>Send commands to active session</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="71"/>
        <source>ASCII</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="84"/>
        <source>HEX</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="104"/>
        <source>time</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="117"/>
        <source>ms</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="135"/>
        <source>Auto Send</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="142"/>
        <source>Send</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>EmptyTabWidget</name>
    <message>
        <location filename="../src/sessiontab/sessiontab.cpp" line="69"/>
        <source>No session</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>GlobalOptionsAdvancedWidget</name>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="25"/>
        <source>Config File</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="56"/>
        <source>Translate Service</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="67"/>
        <source>Google Translate</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="72"/>
        <source>Baidu Translate</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="77"/>
        <source>Microsoft Translate</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="87"/>
        <source>Terminal background support animation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="94"/>
        <source>NativeUI(Effective after restart)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="101"/>
        <source>Github Copilot</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>GlobalOptionsAppearanceWidget</name>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="17"/>
        <source>Color Schemes</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="27"/>
        <source>Font</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="45"/>
        <source>Series</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="65"/>
        <source>Size</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="93"/>
        <source>Background image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="115"/>
        <source>Clear</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="124"/>
        <source>Background mode</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="131"/>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="143"/>
        <source>Stretch</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="138"/>
        <source>None</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="148"/>
        <source>Zoom</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="153"/>
        <source>Fit</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="158"/>
        <source>Center</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="163"/>
        <source>Tile</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="171"/>
        <source>Background opacity</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>GlobalOptionsGeneralWidget</name>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="19"/>
        <source>New tab mode</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="33"/>
        <source>New session</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="38"/>
        <source>Clone session</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="43"/>
        <source>LocalShell session</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="55"/>
        <source>New tab workpath</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="84"/>
        <source>Tab title mode</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="98"/>
        <source>Brief</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="103"/>
        <source>Full</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="108"/>
        <source>Scroll</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="126"/>
        <source>Tab Title Width</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="155"/>
        <source>Tab Preview</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="171"/>
        <source>Preview Width</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>GlobalOptionsTerminalWidget</name>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="17"/>
        <source>Scrollback lines</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="34"/>
        <source>Cursor Shape</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="42"/>
        <source>Block</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="47"/>
        <source>Underline</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="52"/>
        <source>IBeam</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="60"/>
        <source>Cursor Blink</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="70"/>
        <source>Word Characters</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>GlobalOptionsWindow</name>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="164"/>
        <source>Image Files (*.png *.jpg *.jpeg *.bmp *.gif);;Video Files (*.mp4 *.avi *.mkv *.mov)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="202"/>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="207"/>
        <source>Information</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="202"/>
        <source>This feature needs more system resources, please use it carefully!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="207"/>
        <source>This feature is not implemented yet!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="229"/>
        <source>General</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="229"/>
        <source>Appearance</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="229"/>
        <source>Terminal</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="229"/>
        <source>Window</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="229"/>
        <source>Advanced</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="164"/>
        <source>Select Background Image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.ui" line="14"/>
        <source>Global Options</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>GlobalOptionsWindowWidget</name>
    <message>
        <location filename="../src/globaloptions/globaloptionswindowwidget.ui" line="17"/>
        <source>Transparent window</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>HexViewWindow</name>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.ui" line="20"/>
        <source>Hex View</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="71"/>
        <source>Copy</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="79"/>
        <source>Copy Hex</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="87"/>
        <source>Dump</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="91"/>
        <source>Save As</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="91"/>
        <source>Binary File (*.bin)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="99"/>
        <source>Error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="99"/>
        <source>Failed to save file!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="104"/>
        <source>Clear</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Konsole::Session</name>
    <message>
        <location filename="../lib/qtermwidget/Session.cpp" line="317"/>
        <source>Bell in session &apos;%1&apos;</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Konsole::TerminalDisplay</name>
    <message>
        <location filename="../lib/qtermwidget/TerminalDisplay.cpp" line="1383"/>
        <source>Size: XXX x XXX</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/TerminalDisplay.cpp" line="1395"/>
        <source>Size: %1 x %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/TerminalDisplay.cpp" line="2942"/>
        <source>Paste multiline text</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/TerminalDisplay.cpp" line="2943"/>
        <source>Are you sure you want to paste this text?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/TerminalDisplay.cpp" line="3426"/>
        <source>&lt;qt&gt;Output has been &lt;a href=&quot;http://en.wikipedia.org/wiki/Flow_control&quot;&gt;suspended&lt;/a&gt; by pressing Ctrl+S.  Press &lt;b&gt;Ctrl+Q&lt;/b&gt; to resume.&lt;/qt&gt;</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Konsole::Vt102Emulation</name>
    <message>
        <location filename="../lib/qtermwidget/Vt102Emulation.cpp" line="1113"/>
        <source>No keyboard translator available.  The information needed to convert key presses into characters to send to the terminal is missing.</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LockSessionWindow</name>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.ui" line="14"/>
        <source>Lock Session</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.ui" line="20"/>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="84"/>
        <source>Enter the password that will be used to unlock the session:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.ui" line="32"/>
        <source>Password</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.ui" line="50"/>
        <source>Reenter Password</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.ui" line="68"/>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="85"/>
        <source>Lock all sessions</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.ui" line="75"/>
        <source>Lock all sessions in tab group</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="49"/>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="55"/>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="66"/>
        <source>Error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="49"/>
        <source>Passwords do not match!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="55"/>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="66"/>
        <source>Password cannot be empty!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="98"/>
        <source>Enter the password that was used to lock the session:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="99"/>
        <source>Unlock all sessions</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>NetScanWindow</name>
    <message>
        <location filename="../src/netscanwindow/netscanwindow.ui" line="14"/>
        <source>NetScan</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>PluginInfoWindow</name>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.ui" line="14"/>
        <source>Plugin Info</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.ui" line="28"/>
        <source>API version</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.ui" line="38"/>
        <source>Install plugin</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="40"/>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="86"/>
        <source>Name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="40"/>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="86"/>
        <source>Version</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="40"/>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="86"/>
        <source>API Version</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="40"/>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="86"/>
        <source>Enable</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>PluginViewerHomeWidget</name>
    <message>
        <location filename="../src/pluginviewerwidget/pluginviewerhomewidget.cpp" line="18"/>
        <source>Welcome to use</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/pluginviewerwidget/pluginviewerhomewidget.cpp" line="20"/>
        <source>QuardCRT plugin system!</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>PluginViewerWidget</name>
    <message>
        <location filename="../src/pluginviewerwidget/pluginviewerwidget.cpp" line="37"/>
        <source>Home</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QCustomFileSystemModel</name>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="155"/>
        <source>Directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="155"/>
        <source>File</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="159"/>
        <source>Loading...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="184"/>
        <source>Name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="186"/>
        <source>Type</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="188"/>
        <source>Size</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="190"/>
        <source>Last Modified</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QKeychain::DeletePasswordJobPrivate</name>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="173"/>
        <source>Could not open keystore</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="179"/>
        <source>Could not remove private key from keystore</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="584"/>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="592"/>
        <source>Unknown error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="613"/>
        <source>Could not open wallet: %1; %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="104"/>
        <source>Password entry not found</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="108"/>
        <source>Could not decrypt data</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QKeychain::Job</name>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="29"/>
        <source>No error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="31"/>
        <source>The specified item could not be found in the keychain</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="33"/>
        <source>User canceled the operation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="35"/>
        <source>User interaction is not allowed</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="37"/>
        <source>No keychain is available. You may need to restart your computer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="39"/>
        <source>The user name or passphrase you entered is not correct</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="41"/>
        <source>A cryptographic verification failure has occurred</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="43"/>
        <source>Function or operation not implemented</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="45"/>
        <source>I/O error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="47"/>
        <source>Already open with with write permission</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="49"/>
        <source>Invalid parameters passed to a function</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="51"/>
        <source>Failed to allocate memory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="53"/>
        <source>Bad parameter or invalid state for operation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="55"/>
        <source>An internal component failed</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="57"/>
        <source>The specified item already exists in the keychain</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="59"/>
        <source>Unable to decode the provided data</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="62"/>
        <source>Unknown error</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QKeychain::JobPrivate</name>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="294"/>
        <source>Unknown error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="541"/>
        <source>Access to keychain denied</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QKeychain::PlainTextStore</name>
    <message>
        <location filename="../lib/qtkeychain/plaintextstore.cpp" line="65"/>
        <source>Could not store data in settings: access error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/plaintextstore.cpp" line="67"/>
        <source>Could not store data in settings: format error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/plaintextstore.cpp" line="85"/>
        <source>Could not delete data from settings: access error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/plaintextstore.cpp" line="87"/>
        <source>Could not delete data from settings: format error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/plaintextstore.cpp" line="104"/>
        <source>Entry not found</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QKeychain::ReadPasswordJobPrivate</name>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="52"/>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="392"/>
        <source>Entry not found</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="60"/>
        <source>Could not open keystore</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="68"/>
        <source>Could not retrieve private key from keystore</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="75"/>
        <source>Could not create decryption cipher</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="204"/>
        <source>D-Bus is not running</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="213"/>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="223"/>
        <source>Unknown error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="315"/>
        <source>No keychain service available</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="317"/>
        <source>Could not open wallet: %1; %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="362"/>
        <source>Access to keychain denied</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="383"/>
        <source>Could not determine data type: %1; %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="401"/>
        <source>Unsupported entry type &apos;Map&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="404"/>
        <source>Unknown kwallet entry type &apos;%1&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="32"/>
        <source>Password entry not found</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="36"/>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="139"/>
        <source>Could not decrypt data</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QKeychain::WritePasswordJobPrivate</name>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="95"/>
        <source>Could not open keystore</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="124"/>
        <source>Could not create private key generator</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="131"/>
        <source>Could not generate new private key</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="139"/>
        <source>Could not retrieve private key from keystore</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="147"/>
        <source>Could not create encryption cipher</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="155"/>
        <source>Could not encrypt data</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="444"/>
        <source>D-Bus is not running</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="454"/>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="481"/>
        <source>Unknown error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="500"/>
        <source>Could not open wallet: %1; %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="78"/>
        <source>Credential size exceeds maximum size of %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="87"/>
        <source>Credential key exceeds maximum size of %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="92"/>
        <source>Writing credentials failed: Win32 error code %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="162"/>
        <source>Encryption failed</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QMessageBox</name>
    <message>
        <location filename="../lib/qtermwidget/TerminalDisplay.cpp" line="2949"/>
        <source>Show Details...</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../lib/qtermwidget/ColorScheme.cpp" line="267"/>
        <location filename="../lib/qtermwidget/ColorScheme.cpp" line="282"/>
        <source>Un-named Color Scheme</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/ColorScheme.cpp" line="453"/>
        <source>Accessible Color Scheme</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/Filter.cpp" line="549"/>
        <source>Open Link</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/Filter.cpp" line="550"/>
        <source>Copy Link Address</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/Filter.cpp" line="554"/>
        <source>Send Email To...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/Filter.cpp" line="555"/>
        <source>Copy Email Address</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/Filter.cpp" line="559"/>
        <source>Open Path</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/Filter.cpp" line="560"/>
        <source>Copy Path</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="254"/>
        <source>Access to keychain denied</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="256"/>
        <source>No keyring daemon</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="258"/>
        <source>Already unlocked</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="260"/>
        <source>No such keyring</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="262"/>
        <source>Bad arguments</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="264"/>
        <source>I/O error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="266"/>
        <source>Cancelled</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="268"/>
        <source>Keyring already exists</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="270"/>
        <source>No match</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="275"/>
        <source>Unknown error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/libsecret.cpp" line="120"/>
        <source>Entry not found</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QTermWidget</name>
    <message>
        <location filename="../lib/qtermwidget/qtermwidget.cpp" line="350"/>
        <source>Color Scheme Error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/qtermwidget.cpp" line="351"/>
        <source>Cannot load color scheme: %1</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QuickConnectWindow</name>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="30"/>
        <source>Protocol</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="43"/>
        <source>Serial</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="48"/>
        <source>Local Shell</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="58"/>
        <source>Named Pipe</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="86"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="69"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="166"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="224"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="253"/>
        <source>Hostname</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="109"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="70"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="167"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="225"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="254"/>
        <source>Port</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="125"/>
        <source>WebSocket</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="133"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="233"/>
        <source>None</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="138"/>
        <source>Insecure</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="143"/>
        <source>Secure</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="161"/>
        <source>Username</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="171"/>
        <source>Password</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="188"/>
        <source>DataBits</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="225"/>
        <source>Parity</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="238"/>
        <source>Odd</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="243"/>
        <source>Even</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="261"/>
        <source>StopBits</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="313"/>
        <source>Save session</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="323"/>
        <source>Open in tab</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="33"/>
        <source>Quick Connect</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="91"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="188"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="246"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="275"/>
        <source>e.g. 127.0.0.1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="99"/>
        <source>Port Name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="100"/>
        <source>Baud Rate</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="109"/>
        <source>e.g. 110, 300, 600, 1200, 2400, 
4800, 9600, 14400, 19200, 38400, 
56000, 57600, 115200, 128000, 256000, 
460800, 921600</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="141"/>
        <source>Command</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="162"/>
        <source>e.g. /bin/bash</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="195"/>
        <source>Pipe Name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="217"/>
        <source>e.g. \\\.\pipe\namedpipe</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="219"/>
        <source>e.g. /tmp/socket</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SFTPmenuBookmarkWidget</name>
    <message>
        <location filename="../src/sftpwindow/sftpmenubookmarkwidget.ui" line="17"/>
        <source>BookMarkName</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpmenubookmarkwidget.ui" line="35"/>
        <source>LocalPath</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpmenubookmarkwidget.ui" line="62"/>
        <source>RemotePath</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpmenubookmarkwidget.cpp" line="31"/>
        <source>Open Directory</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SearchBar</name>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.ui" line="14"/>
        <source>SearchBar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.ui" line="20"/>
        <source>X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.ui" line="32"/>
        <source>Find:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.ui" line="42"/>
        <source>&lt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.ui" line="54"/>
        <source>&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.ui" line="66"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.cpp" line="40"/>
        <location filename="../lib/qtermwidget/SearchBar.cpp" line="130"/>
        <source>Match case</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.cpp" line="46"/>
        <location filename="../lib/qtermwidget/SearchBar.cpp" line="131"/>
        <source>Regular expression</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.cpp" line="50"/>
        <location filename="../lib/qtermwidget/SearchBar.cpp" line="132"/>
        <source>Highlight all matches</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SessionManagerTreeModel</name>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="133"/>
        <source>Telnet</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="135"/>
        <source>Serial</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="137"/>
        <source>Shell</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="139"/>
        <source>Raw</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="141"/>
        <source>NamePipe</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="269"/>
        <source>Name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="271"/>
        <source>Kind</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SessionManagerTreeView</name>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="39"/>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="74"/>
        <source>Session</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="84"/>
        <source>Connect Terminal</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="89"/>
        <source>Connect in New Window</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="96"/>
        <source>Connect in New Tab Group</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="103"/>
        <source>Delete</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="109"/>
        <source>Properties</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SessionManagerWidget</name>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagerwidget.ui" line="43"/>
        <source>Session Manager</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagerwidget.cpp" line="67"/>
        <source>Filter by folder/session name</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SessionOptionsGeneralWidget</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsgeneralwidget.ui" line="25"/>
        <source>Name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsgeneralwidget.ui" line="45"/>
        <source>Protocol</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsgeneralwidget.ui" line="58"/>
        <source>Serial</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsgeneralwidget.ui" line="63"/>
        <source>Local Shell</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsgeneralwidget.ui" line="73"/>
        <source>Named Pipe</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SessionOptionsLocalShellProperties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionslocalshellproperties.ui" line="19"/>
        <source>Command</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SessionOptionsLocalShellState</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionslocalshellstate.cpp" line="33"/>
        <source>Name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionslocalshellstate.ui" line="25"/>
        <source>State</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionslocalshellstate.ui" line="51"/>
        <source>Process Tree</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SessionOptionsNamePipeProperties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsnamepipeproperties.ui" line="25"/>
        <source>PipeName</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SessionOptionsRawProperties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsrawproperties.ui" line="25"/>
        <source>Hostname</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsrawproperties.ui" line="45"/>
        <source>Port</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SessionOptionsSerialProperties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="25"/>
        <source>Port Name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="45"/>
        <source>Baud Rate</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="72"/>
        <source>DataBits</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="109"/>
        <source>Parity</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="117"/>
        <source>None</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="122"/>
        <source>Odd</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="127"/>
        <source>Even</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="145"/>
        <source>StopBits</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.cpp" line="28"/>
        <source>e.g. 110, 300, 600, 1200, 2400, 
4800, 9600, 14400, 19200, 38400, 
56000, 57600, 115200, 128000, 256000, 
460800, 921600</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SessionOptionsSsh2Properties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsssh2properties.ui" line="25"/>
        <source>Hostname</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsssh2properties.ui" line="41"/>
        <source>Port</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsssh2properties.ui" line="65"/>
        <source>UserName</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsssh2properties.ui" line="81"/>
        <source>Password</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SessionOptionsTelnetProperties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionstelnetproperties.ui" line="19"/>
        <source>Hostname</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionstelnetproperties.ui" line="33"/>
        <source>Port</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionstelnetproperties.ui" line="55"/>
        <source>None</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionstelnetproperties.ui" line="60"/>
        <source>Insecure</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionstelnetproperties.ui" line="65"/>
        <source>Secure</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SessionOptionsVNCProperties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsvncproperties.ui" line="25"/>
        <source>Hostname</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsvncproperties.ui" line="41"/>
        <source>Port</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsvncproperties.ui" line="65"/>
        <source>Password</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SessionOptionsWindow</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionswindow.ui" line="14"/>
        <source>Session Options</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionswindow.cpp" line="122"/>
        <source>General</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionswindow.cpp" line="122"/>
        <source>Properties</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionswindow.cpp" line="122"/>
        <source>State</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionswindow.cpp" line="133"/>
        <source>Name</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SessionsWindow</name>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="141"/>
        <source>Telnet Error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="141"/>
        <source>Telnet error:
%1.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="168"/>
        <source>Serial Error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="168"/>
        <source>Serial error:
%1.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="203"/>
        <source>Raw Socket Error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="203"/>
        <source>Raw Socket error:
%1.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="234"/>
        <source>Name Pipe Error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="234"/>
        <source>Name Pipe error:
%1.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="246"/>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="275"/>
        <source>SSH2 Error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="246"/>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="275"/>
        <source>SSH2 error:
%1.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="434"/>
        <source>Start Local Shell</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="434"/>
        <source>Cannot start local shell:
%1.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="573"/>
        <source>Save log...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="574"/>
        <source>log files (*.log)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="578"/>
        <source>Save log</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="578"/>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="613"/>
        <source>Cannot write file %1:
%2.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="608"/>
        <source>Save Raw log...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="609"/>
        <source>binary files (*.bin)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="613"/>
        <source>Save Raw log</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="685"/>
        <source>Unlock Session</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="685"/>
        <source>Wrong password!</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SftpWindow</name>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.ui" line="14"/>
        <source>sftp</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.ui" line="72"/>
        <source>local</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.ui" line="130"/>
        <source>remote</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="46"/>
        <source>Bookmarks</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="47"/>
        <source>Add Bookmark</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="48"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="67"/>
        <source>Edit Bookmark</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="49"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="84"/>
        <source>Remove Bookmark</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="67"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="84"/>
        <source>Bookmark Name:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="109"/>
        <source>Warning</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="109"/>
        <source>Bookmark Name can not be empty!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="196"/>
        <source>No task!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="200"/>
        <source>All tasks finished!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="205"/>
        <source>task %1/%2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="223"/>
        <source>Open Directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="246"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="412"/>
        <source>Show/Hide Files</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="250"/>
        <source>Upload</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="301"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="476"/>
        <source>Open</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="305"/>
        <source>Open in System File Manager</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="310"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="482"/>
        <source>refresh</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="315"/>
        <source>Upload All</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="359"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="535"/>
        <source>Cancel Selection</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="416"/>
        <source>Download</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="456"/>
        <source>File exists</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="456"/>
        <source>File %1 already exists. Do you want to overwrite it?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="487"/>
        <source>Download All</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>StartTftpSeverWindow</name>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.ui" line="14"/>
        <source>Start TFTP Sever</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.ui" line="32"/>
        <source>The TFTP server uses local directories for uploading and downloading files. Please enter this information below.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.ui" line="65"/>
        <source>TFTP Port</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.ui" line="104"/>
        <source>Upload directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.ui" line="143"/>
        <source>Download directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.cpp" line="38"/>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.cpp" line="45"/>
        <source>Open Directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.cpp" line="77"/>
        <source>Warning</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.cpp" line="77"/>
        <source>Please select a valid directory!</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>UndoStack</name>
    <message>
        <location filename="../lib/qhexedit/commands.cpp" line="115"/>
        <source>Inserting %1 bytes</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qhexedit/commands.cpp" line="137"/>
        <source>Delete %1 chars</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qhexedit/commands.cpp" line="162"/>
        <source>Overwrite %1 chars</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>keyMapManager</name>
    <message>
        <location filename="../src/keymapmanager/keymapmanager.ui" line="14"/>
        <source>keyMapManager</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/keymapmanager/keymapmanager.ui" line="20"/>
        <source>KeyBinding</source>
        <translation></translation>
    </message>
</context>
</TS>
