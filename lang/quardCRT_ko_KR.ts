<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ko_KR">
<context>
    <name>AddTabButton</name>
    <message>
        <location filename="../lib/QtFancyTabWidget/addtabbutton.cpp" line="29"/>
        <source>New tab</source>
        <translation>새 탭</translation>
    </message>
</context>
<context>
    <name>CentralWidget</name>
    <message>
        <location filename="../src/mainwindow.ui" line="101"/>
        <location filename="../src/mainwindow.cpp" line="1123"/>
        <source>Tool Bar</source>
        <translation>툴바</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="191"/>
        <location filename="../src/mainwindow.cpp" line="194"/>
        <location filename="../src/mainwindow.cpp" line="197"/>
        <location filename="../src/mainwindow.cpp" line="325"/>
        <location filename="../src/mainwindow.cpp" line="341"/>
        <location filename="../src/mainwindow.cpp" line="358"/>
        <location filename="../src/mainwindow.cpp" line="371"/>
        <location filename="../src/mainwindow.cpp" line="546"/>
        <location filename="../src/mainwindow.cpp" line="2552"/>
        <location filename="../src/mainwindow.cpp" line="2747"/>
        <location filename="../src/mainwindow.cpp" line="3115"/>
        <location filename="../src/mainwindow.cpp" line="3451"/>
        <location filename="../src/mainwindow.cpp" line="3456"/>
        <source>Warning</source>
        <translation>경고</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="191"/>
        <source>TFTP server bind error!</source>
        <translation>TFTP 서버 바인딩 오류!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="194"/>
        <source>TFTP server file error!</source>
        <translation>TFTP 서버 파일 오류!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="197"/>
        <source>TFTP server network error!</source>
        <translation>TFTP 서버 네트워크 오류!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="286"/>
        <source>Unlock Session</source>
        <translation>세션 잠금 해제</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="287"/>
        <source>Unlock current session</source>
        <translation>현재 세션 잠금 해제</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="293"/>
        <source>Move to another Tab</source>
        <translation>다른 탭으로 이동</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="294"/>
        <source>Move to current session to another tab group</source>
        <translation>현재 세션을 다른 탭 그룹으로 이동</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="303"/>
        <source>Floating Window</source>
        <translation>플로팅 창</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="304"/>
        <source>Floating current session to a new window</source>
        <translation>현재 세션을 새 창으로 플로팅</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="311"/>
        <source>Copy Path</source>
        <translation>경로 복사</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="312"/>
        <source>Copy current session working folder path</source>
        <translation>현재 세션 작업 폴더 경로 복사</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="325"/>
        <location filename="../src/mainwindow.cpp" line="341"/>
        <location filename="../src/mainwindow.cpp" line="358"/>
        <source>No working folder!</source>
        <translation>작업 폴더 없음!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="327"/>
        <source>Add Path to Bookmark</source>
        <translation>북마크에 경로 추가</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="328"/>
        <source>Add current session working folder path to bookmark</source>
        <translation>현재 세션 작업 폴더 경로를 북마크에 추가</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="343"/>
        <source>Open Working Folder</source>
        <translation>작업 폴더 열기</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="344"/>
        <source>Open current session working folder in system file manager</source>
        <translation>시스템 파일 관리자에서 현재 세션 작업 폴더 열기</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="362"/>
        <source>Open SFTP</source>
        <translation>SFTP 열기</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="363"/>
        <source>Open SFTP in a new window</source>
        <translation>새 창에서 SFTP 열기</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="371"/>
        <source>No SFTP channel!</source>
        <translation>SFTP 채널 없음!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="378"/>
        <source>Save Session</source>
        <translation>세션 저장</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="379"/>
        <source>Save current session to session manager</source>
        <translation>현재 세션을 세션 관리자에 저장</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="389"/>
        <source>Enter Session Name</source>
        <translation>세션 이름 입력</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="390"/>
        <source>The session already exists, please rename the new session or cancel saving.</source>
        <translation>세션이 이미 있습니다. 새 세션의 이름을 바꾸거나 저장을 취소하십시오.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="396"/>
        <source>Properties</source>
        <translation>속성</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="397"/>
        <source>Show current session properties</source>
        <translation>현재 세션 속성 표시</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="417"/>
        <source>Close</source>
        <translation>닫기</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="418"/>
        <source>Close current session</source>
        <translation>현재 세션 닫기</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="423"/>
        <source>Close Others</source>
        <translation>다른 탭 닫기</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="424"/>
        <source>Close other sessions</source>
        <translation>다른 세션 닫기</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="434"/>
        <source>Close to the Right</source>
        <translation>오른쪽 탭 닫기</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="435"/>
        <source>Close sessions to the right</source>
        <translation>오른쪽 세션 닫기</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="443"/>
        <source>Close All</source>
        <translation>모든 탭 닫기</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="444"/>
        <source>Close all sessions</source>
        <translation>모든 세션 닫기</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="546"/>
        <source>Session properties error!</source>
        <translation>세션 속성 오류!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="566"/>
        <location filename="../src/mainwindow.cpp" line="684"/>
        <location filename="../src/mainwindow.cpp" line="2455"/>
        <source>Ready</source>
        <translation>준비 완료</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="777"/>
        <source>Highlight/Unhighlight</source>
        <translation>강조/강조 해제</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="778"/>
        <source>Highlight/Unhighlight selected text</source>
        <translation>선택한 텍스트 강조/강조 해제</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="838"/>
        <source>Google Translate</source>
        <translation>Google 번역</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="839"/>
        <location filename="../src/mainwindow.cpp" line="852"/>
        <location filename="../src/mainwindow.cpp" line="864"/>
        <source>Translate selected text</source>
        <translation>선택한 텍스트 번역</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="851"/>
        <source>Baidu Translate</source>
        <translation>Baidu 번역</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="863"/>
        <source>Microsoft Translate</source>
        <translation>Microsoft 번역</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="924"/>
        <source>Back to Main Window</source>
        <translation>메인 창으로 돌아가기</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="995"/>
        <location filename="../src/mainwindow.cpp" line="1019"/>
        <source>Session Manager</source>
        <translation>세션 관리자</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="996"/>
        <source>Plugin</source>
        <translation>플러그인</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="998"/>
        <source>File</source>
        <translation>파일</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="999"/>
        <source>Edit</source>
        <translation>편집</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1000"/>
        <source>View</source>
        <translation>보기</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1001"/>
        <source>Options</source>
        <translation>옵션</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1002"/>
        <source>Transfer</source>
        <translation>전송</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1003"/>
        <source>Script</source>
        <translation>스크립트</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1004"/>
        <source>Bookmark</source>
        <translation>북마크</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1005"/>
        <source>Tools</source>
        <translation>도구</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1006"/>
        <source>Window</source>
        <translation>창</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1007"/>
        <source>Language</source>
        <translation>언어</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1008"/>
        <source>Theme</source>
        <translation>테마</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1009"/>
        <location filename="../src/mainwindow.cpp" line="1229"/>
        <location filename="../src/mainwindow.cpp" line="3415"/>
        <source>Help</source>
        <translation>도움말</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1011"/>
        <source>New Window</source>
        <translation>새 창</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1013"/>
        <source>Open a new window &lt;Ctrl+Shift+N&gt;</source>
        <translation>새 창 열기 &lt;Ctrl+Shift+N&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1015"/>
        <source>Connect...</source>
        <translation>연결...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1017"/>
        <source>Connect to a host &lt;Alt+C&gt;</source>
        <translation>호스트에 연결 &lt;Alt+C&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1021"/>
        <source>Go to the Session Manager &lt;Alt+M&gt;</source>
        <translation>세션 관리자로 이동 &lt;Alt+M&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1023"/>
        <source>Quick Connect...</source>
        <translation>빠른 연결...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1025"/>
        <source>Quick Connect to a host &lt;Alt+Q&gt;</source>
        <translation>호스트에 빠르게 연결 &lt;Alt+Q&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1027"/>
        <source>Connect in Tab/Tile...</source>
        <translation>탭/타일에 연결...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1028"/>
        <source>Connect to a host in a new tab &lt;Alt+B&gt;</source>
        <translation>새 탭에 호스트에 연결 &lt;Alt+B&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1030"/>
        <source>Connect Local Shell</source>
        <translation>로컬 쉘 연결</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1032"/>
        <source>Connect to a local shell &lt;Alt+T&gt;</source>
        <translation>로컬 쉘에 연결 &lt;Alt+T&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1034"/>
        <source>Reconnect</source>
        <translation>재연결</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1036"/>
        <source>Reconnect current session</source>
        <translation>현재 세션 재연결</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1037"/>
        <source>Reconnect All</source>
        <translation>모두 재연결</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1038"/>
        <source>Reconnect all sessions &lt;Alt+A&gt;</source>
        <translation>모든 세션 재연결 &lt;Alt+A&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1040"/>
        <source>Disconnect</source>
        <translation>연결 해제</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1042"/>
        <source>Disconnect current session</source>
        <translation>현재 세션 연결 해제</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1043"/>
        <location filename="../src/mainwindow.cpp" line="1044"/>
        <source>Enter host &lt;Alt+R&gt; to connect</source>
        <translation>연결할 호스트 입력 &lt;Alt+R&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1045"/>
        <source>Disconnect All</source>
        <translation>모두 연결 해제</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1046"/>
        <source>Disconnect all sessions</source>
        <translation>모든 세션 연결 해제</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1047"/>
        <source>Clone Session</source>
        <translation>세션 복제</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1048"/>
        <source>Clone current session &lt;Ctrl+Shift+T&gt;</source>
        <translation>현재 세션 복제 &lt;Ctrl+Shift+T&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1050"/>
        <source>Lock Session</source>
        <translation>세션 잠금</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1051"/>
        <source>Lock/Unlock current session</source>
        <translation>현재 세션 잠금/잠금 해제</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1052"/>
        <source>Log Session</source>
        <translation>세션 로그</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1053"/>
        <source>Create a log file for current session</source>
        <translation>현재 세션의 로그 파일 생성</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1054"/>
        <source>Raw Log Session</source>
        <translation>세션 로그(원본)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1055"/>
        <source>Create a raw log file for current session</source>
        <translation>현재 세션의 원본 로그 파일 생성</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1056"/>
        <source>Hex View</source>
        <translation>16진수 보기</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1057"/>
        <source>Show/Hide Hex View for current session</source>
        <translation>현재 세션의 16진수 보기 표시/숨기기</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1058"/>
        <source>Exit</source>
        <translation>종료</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1059"/>
        <source>Quit the application</source>
        <translation>애플리케이션 종료</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1061"/>
        <source>Copy</source>
        <translation>복사</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1064"/>
        <source>Copy the selected text to the clipboard &lt;Command+C&gt;</source>
        <translation>선택한 텍스트를 클립보드에 복사 &lt;Command+C&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1067"/>
        <source>Copy the selected text to the clipboard &lt;Ctrl+Ins&gt;</source>
        <translation>선택한 텍스트를 클립보드에 복사 &lt;Ctrl+Ins&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1070"/>
        <source>Paste</source>
        <translation>붙여넣기</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1073"/>
        <source>Paste the clipboard text to the current session &lt;Command+V&gt;</source>
        <translation>클립보드 텍스트를 현재 세션에 붙여넣기 &lt;Command+V&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1076"/>
        <source>Paste the clipboard text to the current session &lt;Shift+Ins&gt;</source>
        <translation>클립보드 텍스트를 현재 세션에 붙여넣기 &lt;Shift+Ins&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1079"/>
        <source>Copy and Paste</source>
        <translation>복사 및 붙여넣기</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1080"/>
        <source>Copy the selected text to the clipboard and paste to the current session</source>
        <translation>선택한 텍스트를 클립보드에 복사하고 현재 세션에 붙여넣기</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1081"/>
        <source>Select All</source>
        <translation>모두 선택</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1083"/>
        <source>Select all text in the current session &lt;Ctrl+Shift+A&gt;</source>
        <translation>현재 세션의 모든 텍스트 선택 &lt;Ctrl+Shift+A&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1085"/>
        <source>Find...</source>
        <translation>찾기...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1087"/>
        <source>Find text in the current session &lt;Ctrl+F&gt;</source>
        <translation>현재 세션에서 텍스트 찾기 &lt;Ctrl+F&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1089"/>
        <source>Print Screen</source>
        <translation>스크린 샷</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1091"/>
        <source>Print current screen</source>
        <translation>현재 화면 인쇄</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1092"/>
        <source>Screen Shot</source>
        <translation>스크린 샷</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1094"/>
        <source>Screen shot current screen &lt;Alt+P&gt;</source>
        <translation>현재 화면 스크린 샷 &lt;Alt+P&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1096"/>
        <source>Session Export</source>
        <translation>세션 내보내기</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1098"/>
        <source>Export current session to a file &lt;Alt+O&gt;</source>
        <translation>현재 세션을 파일로 내보내기 &lt;Alt+O&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1100"/>
        <source>Clear Scrollback</source>
        <translation>스크롤백 지우기</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1101"/>
        <source>Clear the contents of the scrollback rows</source>
        <translation>스크롤백 행의 내용 지우기</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1102"/>
        <source>Clear Screen</source>
        <translation>화면 지우기</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1103"/>
        <source>Clear the contents of the current screen</source>
        <translation>현재 화면의 내용 지우기</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1104"/>
        <source>Clear Screen and Scrollback</source>
        <translation>화면과 스크롤백 지우기</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1105"/>
        <source>Clear the contents of the screen and scrollback</source>
        <translation>화면과 스크롤백의 내용 지우기</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1106"/>
        <source>Reset</source>
        <translation>재설정</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1107"/>
        <source>Reset terminal emulator</source>
        <translation>터미널 에뮬레이터 재설정</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1109"/>
        <source>Zoom In</source>
        <translation>확대</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1111"/>
        <source>Zoom In &lt;Ctrl+&quot;=&quot;&gt;</source>
        <translation>확대 &lt;Ctrl+&quot;=&quot;&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1113"/>
        <source>Zoom Out</source>
        <translation>축소</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1115"/>
        <source>Zoom Out &lt;Ctrl+&quot;-&quot;&gt;</source>
        <translation>축소 &lt;Ctrl+&quot;-&quot;&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1117"/>
        <location filename="../src/mainwindow.cpp" line="1119"/>
        <source>Zoom Reset</source>
        <translation>확대/축소 초기화</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1120"/>
        <source>Menu Bar</source>
        <translation>메뉴 바</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1121"/>
        <source>Show/Hide Menu Bar &lt;Alt+U&gt;</source>
        <translation>메뉴 바 표시/숨기기 &lt;Alt+U&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1124"/>
        <source>Show/Hide Tool Bar</source>
        <translation>툴바 표시/숨기기</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1125"/>
        <source>Status Bar</source>
        <translation>상태 바</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1126"/>
        <source>Show/Hide Status Bar</source>
        <translation>상태 바 표시/숨기기</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1127"/>
        <source>Command Window</source>
        <translation>명령 창</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1128"/>
        <source>Show/Hide Command Window</source>
        <translation>명령 창 표시/숨기기</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1129"/>
        <source>Connect Bar</source>
        <translation>연결 바</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1130"/>
        <source>Show/Hide Connect Bar</source>
        <translation>연결 바 표시/숨기기</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1131"/>
        <source>Side Window</source>
        <translation>사이드 창</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1132"/>
        <source>Show/Hide Side Window</source>
        <translation>사이드 창 표시/숨기기</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1133"/>
        <source>Windows Transparency</source>
        <translation>창 투명도</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1134"/>
        <source>Enable/Disable alpha transparency</source>
        <translation>알파 투명도 사용/사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1135"/>
        <source>Vertical Scroll Bar</source>
        <translation>수직 스크롤 바</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1136"/>
        <source>Show/Hide Vertical Scroll Bar</source>
        <translation>수직 스크롤 바 표시/숨기기</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1137"/>
        <source>Allways On Top</source>
        <translation>항상 위에 표시</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1138"/>
        <source>Show window always on top</source>
        <translation>창을 항상 위에 표시</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1139"/>
        <source>Full Screen</source>
        <translation>전체 화면</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1140"/>
        <source>Toggle between full screen and normal mode &lt;Alt+Enter&gt;</source>
        <translation>전체 화면 모드와 일반 모드 전환 &lt;Alt+Enter&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1143"/>
        <source>Session Options...</source>
        <translation>세션 옵션...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1145"/>
        <source>Configure session options</source>
        <translation>세션 옵션 구성</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1146"/>
        <source>Global Options...</source>
        <translation>전역 옵션...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1148"/>
        <source>Configure global options</source>
        <translation>전역 옵션 구성</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1149"/>
        <source>Real-time Save Options</source>
        <translation>실시간 저장 옵션</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1150"/>
        <source>Real-time save session options and global options</source>
        <translation>세션 옵션과 전역 옵션을 실시간으로 저장</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1151"/>
        <source>Save Settings Now</source>
        <translation>설정 지금 저장</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1152"/>
        <source>Save options configuration now</source>
        <translation>옵션 구성 지금 저장</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1154"/>
        <source>Send ASCII...</source>
        <translation>ASCII 전송...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1155"/>
        <source>Send ASCII file</source>
        <translation>ASCII 파일 전송</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1156"/>
        <source>Receive ASCII...</source>
        <translation>ASCII 수신...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1157"/>
        <source>Receive ASCII file</source>
        <translation>ASCII 파일 수신</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1158"/>
        <source>Send Binary...</source>
        <translation>바이너리 전송...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1159"/>
        <source>Send Binary file</source>
        <translation>바이너리 파일 전송</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1160"/>
        <source>Send Xmodem...</source>
        <translation>Xmodem 전송...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1161"/>
        <source>Send a file using Xmodem</source>
        <translation>Xmodem을 사용하여 파일 전송</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1162"/>
        <source>Receive Xmodem...</source>
        <translation>Xmodem 수신...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1163"/>
        <source>Receive a file using Xmodem</source>
        <translation>Xmodem을 사용하여 파일 수신</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1164"/>
        <source>Send Ymodem...</source>
        <translation>Ymodem 전송...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1165"/>
        <source>Send a file using Ymodem</source>
        <translation>Ymodem을 사용하여 파일 전송</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1166"/>
        <source>Receive Ymodem...</source>
        <translation>Ymodem 수신...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1167"/>
        <source>Receive a file using Ymodem</source>
        <translation>Ymodem을 사용하여 파일 수신</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1168"/>
        <source>Zmodem Upload List...</source>
        <translation>Zmodem 파일 업로드 목록...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1169"/>
        <source>Display Zmodem file upload list</source>
        <translation>Zmodem 파일 업로드 목록 표시</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1170"/>
        <source>Start Zmodem Upload</source>
        <translation>Zmodem 파일 업로드 시작</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1171"/>
        <source>Start Zmodem file upload</source>
        <translation>Zmodem 파일 업로드 시작</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1172"/>
        <source>Start TFTP Server</source>
        <translation>TFTP 서버 시작</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1173"/>
        <source>Start/Stop the TFTP server</source>
        <translation>TFTP 서버 시작/중지</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1175"/>
        <source>Run...</source>
        <translation>실행...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1176"/>
        <source>Run a script</source>
        <translation>스크립트 실행</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1177"/>
        <source>Cancel</source>
        <translation>취소</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1178"/>
        <source>Cancel script execution</source>
        <translation>스크립트 실행 취소</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1179"/>
        <source>Start Recording Script</source>
        <translation>스크립트 녹화 시작</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1180"/>
        <source>Start recording script</source>
        <translation>스크립트 녹화 시작</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1181"/>
        <source>Stop Recording Script...</source>
        <translation>스크립트 녹화 중지...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1182"/>
        <source>Stop recording script</source>
        <translation>스크립트 녹화 중지</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1183"/>
        <source>Cancel Recording Script</source>
        <translation>스크립트 녹화 취소</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1184"/>
        <source>Cancel recording script</source>
        <translation>스크립트 녹화 취소</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1186"/>
        <source>Add Bookmark</source>
        <translation>북마크 추가</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1187"/>
        <source>Add a bookmark</source>
        <translation>북마크 추가</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1188"/>
        <location filename="../src/mainwindow.cpp" line="2350"/>
        <source>Remove Bookmark</source>
        <translation>북마크 제거</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1189"/>
        <source>Remove a bookmark</source>
        <translation>북마크 제거</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1190"/>
        <location filename="../src/mainwindow.cpp" line="2377"/>
        <source>Clean All Bookmark</source>
        <translation>모든 북마크 지우기</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1191"/>
        <source>Clean all bookmark</source>
        <translation>모든 북마크 지우기</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1193"/>
        <source>Keymap Manager</source>
        <translation>키맵 관리자</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1194"/>
        <source>Display keymap editor</source>
        <translation>키맵 편집기 표시</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1195"/>
        <source>Create Public Key...</source>
        <translation>공개 키 생성...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1196"/>
        <source>Create a public key</source>
        <translation>공개 키 생성</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1197"/>
        <source>Publickey Manager</source>
        <translation>공개 키 관리자</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1198"/>
        <source>Display publickey manager</source>
        <translation>공개 키 관리자 표시</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1242"/>
        <source>Laboratory</source>
        <translation>실험실</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1245"/>
        <location filename="../src/mainwindow.cpp" line="2395"/>
        <source>SSH Scanning</source>
        <translation>SSH 스캔</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1246"/>
        <source>Display SSH scanning dialog</source>
        <translation>SSH 스캔 대화 상자 표시</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3399"/>
        <source>Version</source>
        <translation>버전</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3401"/>
        <source>Commit</source>
        <translation>커밋</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3403"/>
        <source>Date</source>
        <translation>날짜</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3405"/>
        <source>Author</source>
        <translation>작성자</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3407"/>
        <source>Website</source>
        <translation>웹사이트</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1200"/>
        <source>Tab</source>
        <translation>탭</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1201"/>
        <source>Arrange sessions in tabs</source>
        <translation>탭에 세션 정렬</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1202"/>
        <source>Tile</source>
        <translation>타일</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1203"/>
        <source>Arrange sessions in non-overlapping tiles</source>
        <translation>겹치지 않는 타일에 세션 정렬</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1204"/>
        <source>Cascade</source>
        <translation>계단식</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1205"/>
        <source>Arrange sessions to overlap each other</source>
        <translation>서로 겹치도록 세션 정렬</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1207"/>
        <source>Simplified Chinese</source>
        <translation>简体中文</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1208"/>
        <source>Switch to Simplified Chinese</source>
        <translation>切换到简体中文</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1209"/>
        <source>Traditional Chinese</source>
        <translation>繁體中文</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1210"/>
        <source>Switch to Traditional Chinese</source>
        <translation>切換到繁體中文</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1211"/>
        <source>Russian</source>
        <translation>Русский</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1212"/>
        <source>Switch to Russian</source>
        <translation>Переключиться на русский</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1213"/>
        <source>Korean</source>
        <translation>한국어</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1214"/>
        <source>Switch to Korean</source>
        <translation>한국어로 전환</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1215"/>
        <source>Japanese</source>
        <translation>日本語</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1216"/>
        <source>Switch to Japanese</source>
        <translation>日本語に切り替える</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1217"/>
        <source>French</source>
        <translation>français</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1218"/>
        <source>Switch to French</source>
        <translation>Passer au français</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1219"/>
        <source>Spanish</source>
        <translation>español</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1220"/>
        <source>Switch to Spanish</source>
        <translation>Cambiar a español</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1221"/>
        <source>English</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1222"/>
        <source>Switch to English</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1224"/>
        <source>Light</source>
        <translation>밝게</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1225"/>
        <source>Switch to light theme</source>
        <translation>밝은 테마로 전환</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1226"/>
        <source>Dark</source>
        <translation>어둡게</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1227"/>
        <source>Switch to dark theme</source>
        <translation>어두운 테마로 전환</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1231"/>
        <source>Display help</source>
        <translation>도움말 표시</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1232"/>
        <source>Check Update</source>
        <translation>업데이트 확인</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1234"/>
        <source>Check for updates</source>
        <translation>업데이트 확인</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1235"/>
        <location filename="../src/mainwindow.cpp" line="3397"/>
        <source>About</source>
        <translation>정보</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1237"/>
        <source>Display about dialog</source>
        <translation>정보 대화 상자 표시</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1238"/>
        <source>About Qt</source>
        <translation>Qt 정보</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1240"/>
        <source>Display about Qt dialog</source>
        <translation>Qt 정보 대화 상자 표시</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1247"/>
        <source>Plugin Info</source>
        <translation>플러그인 정보</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1248"/>
        <source>Display plugin information dialog</source>
        <translation>플러그인 정보 대화 상자 표시</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2134"/>
        <source>PrintScreen saved to %1</source>
        <translation>스크린 샷 저장 위치: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2146"/>
        <source>Save Screenshot</source>
        <translation>스크린 샷 저장</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2146"/>
        <source>Image Files (*.jpg)</source>
        <translation>이미지 파일 (*.jpg)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2151"/>
        <source>Screenshot saved to %1</source>
        <translation>스크린 샷 저장 위치: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2162"/>
        <source>Save Session Export</source>
        <translation>세션 내보내기 저장</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2162"/>
        <source>Text Files (*.txt);;HTML Files (*.html)</source>
        <translation>텍스트 파일 (*.txt);;HTML 파일 (*.html)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2165"/>
        <source>Text Files (*.txt)</source>
        <translation>텍스트 파일 (*.txt)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2168"/>
        <source>HTML Files (*.html)</source>
        <translation>HTML 파일 (*.html)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2177"/>
        <source>Session Export saved to %1</source>
        <translation>세션 내보내기 저장 위치: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2179"/>
        <source>Session Export failed to save to %1</source>
        <translation>세션 내보내기 저장 위치: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2323"/>
        <source>Select a directory</source>
        <translation>디렉터리 선택</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2350"/>
        <source>Select a bookmark</source>
        <translation>북마크 선택</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2377"/>
        <source>Are you sure to clean all bookmark?</source>
        <translation>모든 북마크를 지우시겠습니까?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2395"/>
        <source>Port</source>
        <translation>포트</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2552"/>
        <source>Video background is enabled, please enable animation in global options (more system resources) or change background image.</source>
        <translation>비디오 배경이 활성화되었습니다. 전역 옵션에서 애니메이션을 활성화하거나(시스템 리소스가 더 필요함) 배경 이미지를 변경하십시오.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2747"/>
        <source>Session information get failed.</source>
        <translation>세션 정보 가져오기 실패.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2830"/>
        <source>Telnet - </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2831"/>
        <source>Telnet</source>
        <translation>텔넷</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2863"/>
        <source>Serial - </source>
        <translation>시리얼 - </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2864"/>
        <source>Serial</source>
        <translation>시리얼</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2895"/>
        <source>Raw - </source>
        <translation>Raw - </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2896"/>
        <source>Raw</source>
        <translation>Raw</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2927"/>
        <source>NamePipe - </source>
        <translation>네임 파이프 - </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2928"/>
        <source>NamePipe</source>
        <translation>네임 파이프</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2996"/>
        <location filename="../src/mainwindow.cpp" line="3000"/>
        <source>Local Shell</source>
        <translation>로컬 쉘</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2998"/>
        <source>Local Shell - </source>
        <translation>로컬 쉘 - </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3115"/>
        <source>Are you sure to disconnect this session?</source>
        <translation>이 세션을 연결 해제하시겠습니까?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3417"/>
        <source>Global Shortcuts:</source>
        <translation>전역 단축키:</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3418"/>
        <source>show/hide menu bar</source>
        <translation>메뉴 바 표시/숨기기</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3419"/>
        <source>connect to LocalShell</source>
        <translation>로컬 쉘에 연결</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3420"/>
        <source>clone current session</source>
        <translation>현재 세션 복제</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3421"/>
        <source>switch ui to STD mode</source>
        <translation>STD 모드로 UI 전환</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3422"/>
        <source>switch ui to MINI mode</source>
        <translation>MINI 모드로 UI 전환</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3423"/>
        <source>switch to previous session</source>
        <translation>이전 세션으로 전환</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3424"/>
        <source>switch to next session</source>
        <translation>다음 세션으로 전환</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3425"/>
        <source>switch to session [num]</source>
        <translation>세션 [num]으로 전환</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3426"/>
        <source>Go to line start</source>
        <translation>줄 시작으로 이동</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3427"/>
        <source>Go to line end</source>
        <translation>줄 끝으로 이동</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3451"/>
        <source>There are sessions that have not yet been unlocked, please unlock them first.</source>
        <translation>아직 잠금 해제되지 않은 세션이 있습니다. 먼저 잠금을 해제하십시오.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3456"/>
        <source>Are you sure to quit?</source>
        <translation>종료하시겠습니까?</translation>
    </message>
</context>
<context>
    <name>CommandWidget</name>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="35"/>
        <source>Send commands to active session</source>
        <translation>활성 세션에 명령 보내기</translation>
    </message>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="71"/>
        <source>ASCII</source>
        <translation>ASCII</translation>
    </message>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="84"/>
        <source>HEX</source>
        <translation>HEX</translation>
    </message>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="104"/>
        <source>time</source>
        <translation>시간</translation>
    </message>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="117"/>
        <source>ms</source>
        <translation>밀리초</translation>
    </message>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="135"/>
        <source>Auto Send</source>
        <translation>자동 전송</translation>
    </message>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="142"/>
        <source>Send</source>
        <translation>보내기</translation>
    </message>
</context>
<context>
    <name>EmptyTabWidget</name>
    <message>
        <location filename="../src/sessiontab/sessiontab.cpp" line="69"/>
        <source>No session</source>
        <translation>세션 없음</translation>
    </message>
</context>
<context>
    <name>GlobalOptionsAdvancedWidget</name>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="25"/>
        <source>Config File</source>
        <translation>설정 파일</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="56"/>
        <source>Translate Service</source>
        <translation>번역 서비스</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="67"/>
        <source>Google Translate</source>
        <translation>Google 번역</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="72"/>
        <source>Baidu Translate</source>
        <translation>Baidu 번역</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="77"/>
        <source>Microsoft Translate</source>
        <translation>Microsoft 번역</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="87"/>
        <source>Terminal background support animation</source>
        <translation>터미널 배경 지원 애니메이션</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="94"/>
        <source>NativeUI(Effective after restart)</source>
        <translation>NativeUI(재시작 후 적용)</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="101"/>
        <source>Github Copilot</source>
        <translation>Github Copilot</translation>
    </message>
</context>
<context>
    <name>GlobalOptionsAppearanceWidget</name>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="17"/>
        <source>Color Schemes</source>
        <translation>색상 테마</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="27"/>
        <source>Font</source>
        <translation>글꼴</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="45"/>
        <source>Series</source>
        <translation>시리즈</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="65"/>
        <source>Size</source>
        <translation>크기</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="93"/>
        <source>Background image</source>
        <translation>배경 이미지</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="115"/>
        <source>Clear</source>
        <translation>지우기</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="124"/>
        <source>Background mode</source>
        <translation>배경 모드</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="131"/>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="143"/>
        <source>Stretch</source>
        <translation>늘리기</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="138"/>
        <source>None</source>
        <translation>없음</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="148"/>
        <source>Zoom</source>
        <translation>확대</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="153"/>
        <source>Fit</source>
        <translation>맞춤</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="158"/>
        <source>Center</source>
        <translation>가운데</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="163"/>
        <source>Tile</source>
        <translation>타일</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="171"/>
        <source>Background opacity</source>
        <translation>배경 불투명도</translation>
    </message>
</context>
<context>
    <name>GlobalOptionsGeneralWidget</name>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="19"/>
        <source>New tab mode</source>
        <translation>새 탭 모드</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="33"/>
        <source>New session</source>
        <translation>새 세션</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="38"/>
        <source>Clone session</source>
        <translation>세션 복제</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="43"/>
        <source>LocalShell session</source>
        <translation>로컬 쉘 세션</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="55"/>
        <source>New tab workpath</source>
        <translation>새 탭 작업 경로</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="84"/>
        <source>Tab title mode</source>
        <translation>탭 제목 모드</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="98"/>
        <source>Brief</source>
        <translation>간략히</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="103"/>
        <source>Full</source>
        <translation>전체</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="108"/>
        <source>Scroll</source>
        <translation>스크롤</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="126"/>
        <source>Tab Title Width</source>
        <translation>탭 제목 너비</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="155"/>
        <source>Tab Preview</source>
        <translation>탭 미리보기</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="171"/>
        <source>Preview Width</source>
        <translation>미리보기 너비</translation>
    </message>
</context>
<context>
    <name>GlobalOptionsTerminalWidget</name>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="17"/>
        <source>Scrollback lines</source>
        <translation>스크롤백 라인 수</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="34"/>
        <source>Cursor Shape</source>
        <translation>커서 모양</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="42"/>
        <source>Block</source>
        <translation>블록</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="47"/>
        <source>Underline</source>
        <translation>밑줄</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="52"/>
        <source>IBeam</source>
        <translation>I 빔</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="60"/>
        <source>Cursor Blink</source>
        <translation>커서 깜빡임</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="70"/>
        <source>Word Characters</source>
        <translation>단어 문자</translation>
    </message>
</context>
<context>
    <name>GlobalOptionsWindow</name>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.ui" line="14"/>
        <source>Global Options</source>
        <translation>전역 옵션</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="164"/>
        <source>Select Background Image</source>
        <translation>배경 이미지 선택</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="164"/>
        <source>Image Files (*.png *.jpg *.jpeg *.bmp *.gif);;Video Files (*.mp4 *.avi *.mkv *.mov)</source>
        <translation>이미지 파일 (*.png *.jpg *.jpeg *.bmp *.gif);;비디오 파일 (*.mp4 *.avi *.mkv *.mov)</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="202"/>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="207"/>
        <source>Information</source>
        <translation>정보</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="202"/>
        <source>This feature needs more system resources, please use it carefully!</source>
        <translation>이 기능은 더 많은 시스템 리소스가 필요합니다. 주의해서 사용하십시오!</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="207"/>
        <source>This feature is not implemented yet!</source>
        <translation>이 기능은 아직 구현되지 않았습니다!</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="229"/>
        <source>General</source>
        <translation>일반</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="229"/>
        <source>Appearance</source>
        <translation>외관</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="229"/>
        <source>Terminal</source>
        <translation>터미널</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="229"/>
        <source>Window</source>
        <translation>창</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="229"/>
        <source>Advanced</source>
        <translation>고급</translation>
    </message>
</context>
<context>
    <name>GlobalOptionsWindowWidget</name>
    <message>
        <location filename="../src/globaloptions/globaloptionswindowwidget.ui" line="17"/>
        <source>Transparent window</source>
        <translation>투명 창</translation>
    </message>
</context>
<context>
    <name>HexViewWindow</name>
    <message>
        <source>ASCII Text...</source>
        <translation type="vanished">ASCII 텍스트...</translation>
    </message>
    <message>
        <source>clear</source>
        <translation type="vanished">지우기</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.ui" line="20"/>
        <source>Hex View</source>
        <translation>16진수 보기</translation>
    </message>
    <message>
        <source>Information</source>
        <translation type="vanished">정보</translation>
    </message>
    <message>
        <source>Will send Hex:
</source>
        <translation type="vanished">16진수를 보냅니다:
</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="71"/>
        <source>Copy</source>
        <translation>복사</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="79"/>
        <source>Copy Hex</source>
        <translation>16진수 복사</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="87"/>
        <source>Dump</source>
        <translation>덤프</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="91"/>
        <source>Save As</source>
        <translation>다른 이름으로 저장</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="91"/>
        <source>Binary File (*.bin)</source>
        <translation>바이너리 파일 (*.bin)</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="99"/>
        <source>Error</source>
        <translation>오류</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="99"/>
        <source>Failed to save file!</source>
        <translation>파일 저장 실패!</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="104"/>
        <source>Clear</source>
        <translation>지우기</translation>
    </message>
</context>
<context>
    <name>Konsole::Session</name>
    <message>
        <location filename="../lib/qtermwidget/Session.cpp" line="317"/>
        <source>Bell in session &apos;%1&apos;</source>
        <translation>&apos;%1&apos; 세션에서 벨</translation>
    </message>
</context>
<context>
    <name>Konsole::TerminalDisplay</name>
    <message>
        <location filename="../lib/qtermwidget/TerminalDisplay.cpp" line="1383"/>
        <source>Size: XXX x XXX</source>
        <translation>크기: XXX x XXX</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/TerminalDisplay.cpp" line="1395"/>
        <source>Size: %1 x %2</source>
        <translation>크기: %1 x %2</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/TerminalDisplay.cpp" line="2942"/>
        <source>Paste multiline text</source>
        <translation>여러 줄 텍스트 붙여넣기</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/TerminalDisplay.cpp" line="2943"/>
        <source>Are you sure you want to paste this text?</source>
        <translation>이 텍스트를 붙여넣으시겠습니까?</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/TerminalDisplay.cpp" line="3426"/>
        <source>&lt;qt&gt;Output has been &lt;a href=&quot;http://en.wikipedia.org/wiki/Flow_control&quot;&gt;suspended&lt;/a&gt; by pressing Ctrl+S.  Press &lt;b&gt;Ctrl+Q&lt;/b&gt; to resume.&lt;/qt&gt;</source>
        <translation>&lt;qt&gt;Ctrl+S를 눌러 출력이 &lt;a href=&quot;http://en.wikipedia.org/wiki/Flow_control&quot;&gt;중지&lt;/a&gt;되었습니다. 재개하려면 &lt;b&gt;Ctrl+Q&lt;/b&gt;를 누르십시오.&lt;/qt&gt;</translation>
    </message>
</context>
<context>
    <name>Konsole::Vt102Emulation</name>
    <message>
        <location filename="../lib/qtermwidget/Vt102Emulation.cpp" line="1113"/>
        <source>No keyboard translator available.  The information needed to convert key presses into characters to send to the terminal is missing.</source>
        <translation>사용 가능한 키보드 변환기가 없습니다. 키를 문자로 변환하여 터미널로 보내는 데 필요한 정보가 없습니다.</translation>
    </message>
</context>
<context>
    <name>LockSessionWindow</name>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.ui" line="14"/>
        <source>Lock Session</source>
        <translation>세션 잠금</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.ui" line="20"/>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="84"/>
        <source>Enter the password that will be used to unlock the session:</source>
        <translation>세션 잠금을 해제할 비밀번호를 입력하십시오:</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.ui" line="32"/>
        <source>Password</source>
        <translation>비밀번호</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.ui" line="50"/>
        <source>Reenter Password</source>
        <translation>비밀번호 재입력</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.ui" line="68"/>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="85"/>
        <source>Lock all sessions</source>
        <translation>모든 세션 잠금</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.ui" line="75"/>
        <source>Lock all sessions in tab group</source>
        <translation>탭 그룹의 모든 세션 잠금</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="49"/>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="55"/>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="66"/>
        <source>Error</source>
        <translation>오류</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="49"/>
        <source>Passwords do not match!</source>
        <translation>비밀번호가 일치하지 않습니다!</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="55"/>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="66"/>
        <source>Password cannot be empty!</source>
        <translation>비밀번호를 비워 둘 수 없습니다!</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="98"/>
        <source>Enter the password that was used to lock the session:</source>
        <translation>세션을 잠그는 데 사용된 비밀번호를 입력하십시오:</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="99"/>
        <source>Unlock all sessions</source>
        <translation>모든 세션 잠금 해제</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Tool Bar</source>
        <translation type="vanished">툴바</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="vanished">경고</translation>
    </message>
    <message>
        <source>TFTP server bind error!</source>
        <translation type="vanished">TFTP 서버 바인딩 오류!</translation>
    </message>
    <message>
        <source>TFTP server file error!</source>
        <translation type="vanished">TFTP 서버 파일 오류!</translation>
    </message>
    <message>
        <source>TFTP server network error!</source>
        <translation type="vanished">TFTP 서버 네트워크 오류!</translation>
    </message>
    <message>
        <source>Unlock Session</source>
        <translation type="vanished">세션 잠금 해제</translation>
    </message>
    <message>
        <source>Move to another Tab</source>
        <translation type="vanished">다른 탭으로 이동</translation>
    </message>
    <message>
        <source>Floating Window</source>
        <translation type="vanished">플로팅 창</translation>
    </message>
    <message>
        <source>Copy Path</source>
        <translation type="vanished">경로 복사</translation>
    </message>
    <message>
        <source>No working folder!</source>
        <translation type="vanished">작업 폴더 없음!</translation>
    </message>
    <message>
        <source>Add Path to Bookmark</source>
        <translation type="vanished">북마크에 경로 추가</translation>
    </message>
    <message>
        <source>Open Working Folder</source>
        <translation type="vanished">작업 폴더 열기</translation>
    </message>
    <message>
        <source>Save Session</source>
        <translation type="vanished">세션 저장</translation>
    </message>
    <message>
        <source>Enter Session Name</source>
        <translation type="vanished">세션 이름 입력</translation>
    </message>
    <message>
        <source>The session already exists, please rename the new session or cancel saving.</source>
        <translation type="vanished">세션이 이미 있습니다. 새 세션의 이름을 바꾸거나 저장을 취소하십시오.</translation>
    </message>
    <message>
        <source>Properties</source>
        <translation type="vanished">속성</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">닫기</translation>
    </message>
    <message>
        <source>Close Others</source>
        <translation type="vanished">다른 탭 닫기</translation>
    </message>
    <message>
        <source>Close to the Right</source>
        <translation type="vanished">오른쪽 탭 닫기</translation>
    </message>
    <message>
        <source>Close All</source>
        <translation type="vanished">모든 탭 닫기</translation>
    </message>
    <message>
        <source>Highlight/Unhighlight</source>
        <translation type="vanished">강조/강조 해제</translation>
    </message>
    <message>
        <source>Ready</source>
        <translation type="vanished">준비 완료</translation>
    </message>
    <message>
        <source>Open SFTP</source>
        <translation type="vanished">SFTP 열기</translation>
    </message>
    <message>
        <source>No SFTP channel!</source>
        <translation type="vanished">SFTP 채널 없음!</translation>
    </message>
    <message>
        <source>Session properties error!</source>
        <translation type="vanished">세션 속성 오류!</translation>
    </message>
    <message>
        <source>Google Translate</source>
        <translation type="vanished">Google 번역</translation>
    </message>
    <message>
        <source>Baidu Translate</source>
        <translation type="vanished">Baidu 번역</translation>
    </message>
    <message>
        <source>Microsoft Translate</source>
        <translation type="vanished">Microsoft 번역</translation>
    </message>
    <message>
        <source>Back to Main Window</source>
        <translation type="vanished">메인 창으로 돌아가기</translation>
    </message>
    <message>
        <source>Session Manager</source>
        <translation type="vanished">세션 관리자</translation>
    </message>
    <message>
        <source>File</source>
        <translation type="vanished">파일</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="vanished">편집</translation>
    </message>
    <message>
        <source>View</source>
        <translation type="vanished">보기</translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="vanished">옵션</translation>
    </message>
    <message>
        <source>Transfer</source>
        <translation type="vanished">전송</translation>
    </message>
    <message>
        <source>Script</source>
        <translation type="vanished">스크립트</translation>
    </message>
    <message>
        <source>Bookmark</source>
        <translation type="vanished">북마크</translation>
    </message>
    <message>
        <source>Tools</source>
        <translation type="vanished">도구</translation>
    </message>
    <message>
        <source>Window</source>
        <translation type="vanished">창</translation>
    </message>
    <message>
        <source>Language</source>
        <translation type="vanished">언어</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation type="vanished">테마</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">도움말</translation>
    </message>
    <message>
        <source>New Window</source>
        <translation type="vanished">새 창</translation>
    </message>
    <message>
        <source>Open a new window &lt;Ctrl+Shift+N&gt;</source>
        <translation type="vanished">새 창 열기 &lt;Ctrl+Shift+N&gt;</translation>
    </message>
    <message>
        <source>Connect...</source>
        <translation type="vanished">연결...</translation>
    </message>
    <message>
        <source>Connect to a host &lt;Alt+C&gt;</source>
        <translation type="vanished">호스트에 연결 &lt;Alt+C&gt;</translation>
    </message>
    <message>
        <source>Go to the Session Manager &lt;Alt+M&gt;</source>
        <translation type="vanished">세션 관리자로 이동 &lt;Alt+M&gt;</translation>
    </message>
    <message>
        <source>Quick Connect...</source>
        <translation type="vanished">빠른 연결...</translation>
    </message>
    <message>
        <source>Quick Connect to a host &lt;Alt+Q&gt;</source>
        <translation type="vanished">호스트에 빠르게 연결 &lt;Alt+Q&gt;</translation>
    </message>
    <message>
        <source>Connect in Tab/Tile...</source>
        <translation type="vanished">탭/타일에 연결...</translation>
    </message>
    <message>
        <source>Connect to a host in a new tab &lt;Alt+B&gt;</source>
        <translation type="vanished">새 탭에 호스트에 연결 &lt;Alt+B&gt;</translation>
    </message>
    <message>
        <source>Connect Local Shell</source>
        <translation type="vanished">로컬 쉘 연결</translation>
    </message>
    <message>
        <source>Connect to a local shell &lt;Alt+T&gt;</source>
        <translation type="vanished">로컬 쉘에 연결 &lt;Alt+T&gt;</translation>
    </message>
    <message>
        <source>Reconnect</source>
        <translation type="vanished">재연결</translation>
    </message>
    <message>
        <source>Reconnect current session</source>
        <translation type="vanished">현재 세션 재연결</translation>
    </message>
    <message>
        <source>Reconnect All</source>
        <translation type="vanished">모두 재연결</translation>
    </message>
    <message>
        <source>Reconnect all sessions &lt;Alt+A&gt;</source>
        <translation type="vanished">모든 세션 재연결 &lt;Alt+A&gt;</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation type="vanished">연결 해제</translation>
    </message>
    <message>
        <source>Disconnect current session</source>
        <translation type="vanished">현재 세션 연결 해제</translation>
    </message>
    <message>
        <source>Enter host &lt;Alt+R&gt; to connect</source>
        <translation type="vanished">연결할 호스트 입력 &lt;Alt+R&gt;</translation>
    </message>
    <message>
        <source>Disconnect All</source>
        <translation type="vanished">모두 연결 해제</translation>
    </message>
    <message>
        <source>Disconnect all sessions</source>
        <translation type="vanished">모든 세션 연결 해제</translation>
    </message>
    <message>
        <source>Clone Session</source>
        <translation type="vanished">세션 복제</translation>
    </message>
    <message>
        <source>Clone current session &lt;Ctrl+Shift+T&gt;</source>
        <translation type="vanished">현재 세션 복제 &lt;Ctrl+Shift+T&gt;</translation>
    </message>
    <message>
        <source>Lock Session</source>
        <translation type="vanished">세션 잠금</translation>
    </message>
    <message>
        <source>Lock/Unlock current session</source>
        <translation type="vanished">현재 세션 잠금/잠금 해제</translation>
    </message>
    <message>
        <source>Log Session</source>
        <translation type="vanished">세션 로그</translation>
    </message>
    <message>
        <source>Create a log file for current session</source>
        <translation type="vanished">현재 세션의 로그 파일 생성</translation>
    </message>
    <message>
        <source>Raw Log Session</source>
        <translation type="vanished">세션 로그(원본)</translation>
    </message>
    <message>
        <source>Create a raw log file for current session</source>
        <translation type="vanished">현재 세션의 원본 로그 파일 생성</translation>
    </message>
    <message>
        <source>Hex View</source>
        <translation type="vanished">16진수 보기</translation>
    </message>
    <message>
        <source>Show/Hide Hex View for current session</source>
        <translation type="vanished">현재 세션의 16진수 보기 표시/숨기기</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="vanished">종료</translation>
    </message>
    <message>
        <source>Quit the application</source>
        <translation type="vanished">애플리케이션 종료</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation type="vanished">복사</translation>
    </message>
    <message>
        <source>Copy the selected text to the clipboard &lt;Command+C&gt;</source>
        <translation type="vanished">선택한 텍스트를 클립보드에 복사 &lt;Command+C&gt;</translation>
    </message>
    <message>
        <source>Copy the selected text to the clipboard &lt;Ctrl+Ins&gt;</source>
        <translation type="vanished">선택한 텍스트를 클립보드에 복사 &lt;Ctrl+Ins&gt;</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation type="vanished">붙여넣기</translation>
    </message>
    <message>
        <source>Paste the clipboard text to the current session &lt;Command+V&gt;</source>
        <translation type="vanished">클립보드 텍스트를 현재 세션에 붙여넣기 &lt;Command+V&gt;</translation>
    </message>
    <message>
        <source>Paste the clipboard text to the current session &lt;Shift+Ins&gt;</source>
        <translation type="vanished">클립보드 텍스트를 현재 세션에 붙여넣기 &lt;Shift+Ins&gt;</translation>
    </message>
    <message>
        <source>Copy and Paste</source>
        <translation type="vanished">복사 및 붙여넣기</translation>
    </message>
    <message>
        <source>Copy the selected text to the clipboard and paste to the current session</source>
        <translation type="vanished">선택한 텍스트를 클립보드에 복사하고 현재 세션에 붙여넣기</translation>
    </message>
    <message>
        <source>Select All</source>
        <translation type="vanished">모두 선택</translation>
    </message>
    <message>
        <source>Select all text in the current session &lt;Ctrl+Shift+A&gt;</source>
        <translation type="vanished">현재 세션의 모든 텍스트 선택 &lt;Ctrl+Shift+A&gt;</translation>
    </message>
    <message>
        <source>Find...</source>
        <translation type="vanished">찾기...</translation>
    </message>
    <message>
        <source>Find text in the current session &lt;Ctrl+F&gt;</source>
        <translation type="vanished">현재 세션에서 텍스트 찾기 &lt;Ctrl+F&gt;</translation>
    </message>
    <message>
        <source>Print Screen</source>
        <translation type="vanished">스크린 샷</translation>
    </message>
    <message>
        <source>Print current screen</source>
        <translation type="vanished">현재 화면 인쇄</translation>
    </message>
    <message>
        <source>Screen Shot</source>
        <translation type="vanished">스크린 샷</translation>
    </message>
    <message>
        <source>Screen shot current screen &lt;Alt+P&gt;</source>
        <translation type="vanished">현재 화면 스크린 샷 &lt;Alt+P&gt;</translation>
    </message>
    <message>
        <source>Session Export</source>
        <translation type="vanished">세션 내보내기</translation>
    </message>
    <message>
        <source>Export current session to a file &lt;Alt+O&gt;</source>
        <translation type="vanished">현재 세션을 파일로 내보내기 &lt;Alt+O&gt;</translation>
    </message>
    <message>
        <source>Clear Scrollback</source>
        <translation type="vanished">스크롤백 지우기</translation>
    </message>
    <message>
        <source>Clear the contents of the scrollback rows</source>
        <translation type="vanished">스크롤백 행의 내용 지우기</translation>
    </message>
    <message>
        <source>Clear Screen</source>
        <translation type="vanished">화면 지우기</translation>
    </message>
    <message>
        <source>Clear the contents of the current screen</source>
        <translation type="vanished">현재 화면의 내용 지우기</translation>
    </message>
    <message>
        <source>Clear Screen and Scrollback</source>
        <translation type="vanished">화면과 스크롤백 지우기</translation>
    </message>
    <message>
        <source>Clear the contents of the screen and scrollback</source>
        <translation type="vanished">화면과 스크롤백의 내용 지우기</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation type="vanished">재설정</translation>
    </message>
    <message>
        <source>Reset terminal emulator</source>
        <translation type="vanished">터미널 에뮬레이터 재설정</translation>
    </message>
    <message>
        <source>Zoom In</source>
        <translation type="vanished">확대</translation>
    </message>
    <message>
        <source>Zoom In &lt;Ctrl+&quot;=&quot;&gt;</source>
        <translation type="vanished">확대 &lt;Ctrl+&quot;=&quot;&gt;</translation>
    </message>
    <message>
        <source>Zoom Out</source>
        <translation type="vanished">축소</translation>
    </message>
    <message>
        <source>Zoom Out &lt;Ctrl+&quot;-&quot;&gt;</source>
        <translation type="vanished">축소 &lt;Ctrl+&quot;-&quot;&gt;</translation>
    </message>
    <message>
        <source>Zoom Reset</source>
        <translation type="vanished">확대/축소 초기화</translation>
    </message>
    <message>
        <source>Menu Bar</source>
        <translation type="vanished">메뉴 바</translation>
    </message>
    <message>
        <source>Show/Hide Menu Bar &lt;Alt+U&gt;</source>
        <translation type="vanished">메뉴 바 표시/숨기기 &lt;Alt+U&gt;</translation>
    </message>
    <message>
        <source>Show/Hide Tool Bar</source>
        <translation type="vanished">툴바 표시/숨기기</translation>
    </message>
    <message>
        <source>Status Bar</source>
        <translation type="vanished">상태 바</translation>
    </message>
    <message>
        <source>Show/Hide Status Bar</source>
        <translation type="vanished">상태 바 표시/숨기기</translation>
    </message>
    <message>
        <source>Command Window</source>
        <translation type="vanished">명령 창</translation>
    </message>
    <message>
        <source>Show/Hide Command Window</source>
        <translation type="vanished">명령 창 표시/숨기기</translation>
    </message>
    <message>
        <source>Connect Bar</source>
        <translation type="vanished">연결 바</translation>
    </message>
    <message>
        <source>Show/Hide Connect Bar</source>
        <translation type="vanished">연결 바 표시/숨기기</translation>
    </message>
    <message>
        <source>Side Window</source>
        <translation type="vanished">사이드 창</translation>
    </message>
    <message>
        <source>Show/Hide Side Window</source>
        <translation type="vanished">사이드 창 표시/숨기기</translation>
    </message>
    <message>
        <source>Windows Transparency</source>
        <translation type="vanished">창 투명도</translation>
    </message>
    <message>
        <source>Enable/Disable alpha transparency</source>
        <translation type="vanished">알파 투명도 사용/사용 안 함</translation>
    </message>
    <message>
        <source>Vertical Scroll Bar</source>
        <translation type="vanished">수직 스크롤 바</translation>
    </message>
    <message>
        <source>Show/Hide Vertical Scroll Bar</source>
        <translation type="vanished">수직 스크롤 바 표시/숨기기</translation>
    </message>
    <message>
        <source>Allways On Top</source>
        <translation type="vanished">항상 위에 표시</translation>
    </message>
    <message>
        <source>Show window always on top</source>
        <translation type="vanished">창을 항상 위에 표시</translation>
    </message>
    <message>
        <source>Full Screen</source>
        <translation type="vanished">전체 화면</translation>
    </message>
    <message>
        <source>Toggle between full screen and normal mode &lt;Alt+Enter&gt;</source>
        <translation type="vanished">전체 화면 모드와 일반 모드 전환 &lt;Alt+Enter&gt;</translation>
    </message>
    <message>
        <source>Session Options...</source>
        <translation type="vanished">세션 옵션...</translation>
    </message>
    <message>
        <source>Configure session options</source>
        <translation type="vanished">세션 옵션 구성</translation>
    </message>
    <message>
        <source>Global Options...</source>
        <translation type="vanished">전역 옵션...</translation>
    </message>
    <message>
        <source>Configure global options</source>
        <translation type="vanished">전역 옵션 구성</translation>
    </message>
    <message>
        <source>Real-time Save Options</source>
        <translation type="vanished">실시간 저장 옵션</translation>
    </message>
    <message>
        <source>Real-time save session options and global options</source>
        <translation type="vanished">세션 옵션과 전역 옵션을 실시간으로 저장</translation>
    </message>
    <message>
        <source>Save Settings Now</source>
        <translation type="vanished">설정 지금 저장</translation>
    </message>
    <message>
        <source>Save options configuration now</source>
        <translation type="vanished">옵션 구성 지금 저장</translation>
    </message>
    <message>
        <source>Send ASCII...</source>
        <translation type="vanished">ASCII 전송...</translation>
    </message>
    <message>
        <source>Send ASCII file</source>
        <translation type="vanished">ASCII 파일 전송</translation>
    </message>
    <message>
        <source>Receive ASCII...</source>
        <translation type="vanished">ASCII 수신...</translation>
    </message>
    <message>
        <source>Receive ASCII file</source>
        <translation type="vanished">ASCII 파일 수신</translation>
    </message>
    <message>
        <source>Send Binary...</source>
        <translation type="vanished">바이너리 전송...</translation>
    </message>
    <message>
        <source>Send Binary file</source>
        <translation type="vanished">바이너리 파일 전송</translation>
    </message>
    <message>
        <source>Send Xmodem...</source>
        <translation type="vanished">Xmodem 전송...</translation>
    </message>
    <message>
        <source>Send a file using Xmodem</source>
        <translation type="vanished">Xmodem을 사용하여 파일 전송</translation>
    </message>
    <message>
        <source>Receive Xmodem...</source>
        <translation type="vanished">Xmodem 수신...</translation>
    </message>
    <message>
        <source>Receive a file using Xmodem</source>
        <translation type="vanished">Xmodem을 사용하여 파일 수신</translation>
    </message>
    <message>
        <source>Send Ymodem...</source>
        <translation type="vanished">Ymodem 전송...</translation>
    </message>
    <message>
        <source>Send a file using Ymodem</source>
        <translation type="vanished">Ymodem을 사용하여 파일 전송</translation>
    </message>
    <message>
        <source>Receive Ymodem...</source>
        <translation type="vanished">Ymodem 수신...</translation>
    </message>
    <message>
        <source>Receive a file using Ymodem</source>
        <translation type="vanished">Ymodem을 사용하여 파일 수신</translation>
    </message>
    <message>
        <source>Zmodem Upload List...</source>
        <translation type="vanished">Zmodem 파일 업로드 목록...</translation>
    </message>
    <message>
        <source>Display Zmodem file upload list</source>
        <translation type="vanished">Zmodem 파일 업로드 목록 표시</translation>
    </message>
    <message>
        <source>Start Zmodem Upload</source>
        <translation type="vanished">Zmodem 파일 업로드 시작</translation>
    </message>
    <message>
        <source>Start Zmodem file upload</source>
        <translation type="vanished">Zmodem 파일 업로드 시작</translation>
    </message>
    <message>
        <source>Start TFTP Server</source>
        <translation type="vanished">TFTP 서버 시작</translation>
    </message>
    <message>
        <source>Start/Stop the TFTP server</source>
        <translation type="vanished">TFTP 서버 시작/중지</translation>
    </message>
    <message>
        <source>Run...</source>
        <translation type="vanished">실행...</translation>
    </message>
    <message>
        <source>Run a script</source>
        <translation type="vanished">스크립트 실행</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">취소</translation>
    </message>
    <message>
        <source>Cancel script execution</source>
        <translation type="vanished">스크립트 실행 취소</translation>
    </message>
    <message>
        <source>Start Recording Script</source>
        <translation type="vanished">스크립트 녹화 시작</translation>
    </message>
    <message>
        <source>Start recording script</source>
        <translation type="vanished">스크립트 녹화 시작</translation>
    </message>
    <message>
        <source>Stop Recording Script...</source>
        <translation type="vanished">스크립트 녹화 중지...</translation>
    </message>
    <message>
        <source>Stop recording script</source>
        <translation type="vanished">스크립트 녹화 중지</translation>
    </message>
    <message>
        <source>Cancel Recording Script</source>
        <translation type="vanished">스크립트 녹화 취소</translation>
    </message>
    <message>
        <source>Cancel recording script</source>
        <translation type="vanished">스크립트 녹화 취소</translation>
    </message>
    <message>
        <source>Add Bookmark</source>
        <translation type="vanished">북마크 추가</translation>
    </message>
    <message>
        <source>Add a bookmark</source>
        <translation type="vanished">북마크 추가</translation>
    </message>
    <message>
        <source>Remove Bookmark</source>
        <translation type="vanished">북마크 제거</translation>
    </message>
    <message>
        <source>Remove a bookmark</source>
        <translation type="vanished">북마크 제거</translation>
    </message>
    <message>
        <source>Clean All Bookmark</source>
        <translation type="vanished">모든 북마크 지우기</translation>
    </message>
    <message>
        <source>Clean all bookmark</source>
        <translation type="vanished">모든 북마크 지우기</translation>
    </message>
    <message>
        <source>Keymap Manager</source>
        <translation type="vanished">키맵 관리자</translation>
    </message>
    <message>
        <source>Display keymap editor</source>
        <translation type="vanished">키맵 편집기 표시</translation>
    </message>
    <message>
        <source>Create Public Key...</source>
        <translation type="vanished">공개 키 생성...</translation>
    </message>
    <message>
        <source>Create a public key</source>
        <translation type="vanished">공개 키 생성</translation>
    </message>
    <message>
        <source>Publickey Manager</source>
        <translation type="vanished">공개 키 관리자</translation>
    </message>
    <message>
        <source>Display publickey manager</source>
        <translation type="vanished">공개 키 관리자 표시</translation>
    </message>
    <message>
        <source>SSH Scanning</source>
        <translation type="vanished">SSH 스캔</translation>
    </message>
    <message>
        <source>Display SSH scanning dialog</source>
        <translation type="vanished">SSH 스캔 대화 상자 표시</translation>
    </message>
    <message>
        <source>Tab</source>
        <translation type="vanished">탭</translation>
    </message>
    <message>
        <source>Arrange sessions in tabs</source>
        <translation type="vanished">탭에 세션 정렬</translation>
    </message>
    <message>
        <source>Tile</source>
        <translation type="vanished">타일</translation>
    </message>
    <message>
        <source>Arrange sessions in non-overlapping tiles</source>
        <translation type="vanished">겹치지 않는 타일에 세션 정렬</translation>
    </message>
    <message>
        <source>Cascade</source>
        <translation type="vanished">계단식</translation>
    </message>
    <message>
        <source>Arrange sessions to overlap each other</source>
        <translation type="vanished">서로 겹치도록 세션 정렬</translation>
    </message>
    <message>
        <source>Japanese</source>
        <translation type="vanished">日本語</translation>
    </message>
    <message>
        <source>Simplified Chinese</source>
        <translation type="vanished">简体中文</translation>
    </message>
    <message>
        <source>Switch to Simplified Chinese</source>
        <translation type="vanished">切换到简体中文</translation>
    </message>
    <message>
        <source>Traditional Chinese</source>
        <translation type="vanished">繁體中文</translation>
    </message>
    <message>
        <source>Switch to Traditional Chinese</source>
        <translation type="vanished">切換到繁體中文</translation>
    </message>
    <message>
        <source>Russian</source>
        <translation type="vanished">Русский</translation>
    </message>
    <message>
        <source>Switch to Russian</source>
        <translation type="vanished">Переключиться на русский</translation>
    </message>
    <message>
        <source>Korean</source>
        <translation type="vanished">한국어</translation>
    </message>
    <message>
        <source>Switch to Korean</source>
        <translation type="vanished">한국어로 전환</translation>
    </message>
    <message>
        <source>Switch to Japanese</source>
        <translation type="vanished">日本語に切り替える</translation>
    </message>
    <message>
        <source>French</source>
        <translation type="vanished">français</translation>
    </message>
    <message>
        <source>Switch to French</source>
        <translation type="vanished">Passer au français</translation>
    </message>
    <message>
        <source>Spanish</source>
        <translation type="vanished">español</translation>
    </message>
    <message>
        <source>Switch to Spanish</source>
        <translation type="vanished">Cambiar a español</translation>
    </message>
    <message>
        <source>Light</source>
        <translation type="vanished">밝게</translation>
    </message>
    <message>
        <source>Switch to light theme</source>
        <translation type="vanished">밝은 테마로 전환</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation type="vanished">어둡게</translation>
    </message>
    <message>
        <source>Switch to dark theme</source>
        <translation type="vanished">어두운 테마로 전환</translation>
    </message>
    <message>
        <source>Display help</source>
        <translation type="vanished">도움말 표시</translation>
    </message>
    <message>
        <source>Check Update</source>
        <translation type="vanished">업데이트 확인</translation>
    </message>
    <message>
        <source>Check for updates</source>
        <translation type="vanished">업데이트 확인</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">정보</translation>
    </message>
    <message>
        <source>Display about dialog</source>
        <translation type="vanished">정보 대화 상자 표시</translation>
    </message>
    <message>
        <source>About Qt</source>
        <translation type="vanished">Qt 정보</translation>
    </message>
    <message>
        <source>Display about Qt dialog</source>
        <translation type="vanished">Qt 정보 대화 상자 표시</translation>
    </message>
    <message>
        <source>PrintScreen saved to %1</source>
        <translation type="vanished">스크린 샷 저장 위치: %1</translation>
    </message>
    <message>
        <source>Save Screenshot</source>
        <translation type="vanished">스크린 샷 저장</translation>
    </message>
    <message>
        <source>Image Files (*.jpg)</source>
        <translation type="vanished">이미지 파일 (*.jpg)</translation>
    </message>
    <message>
        <source>Screenshot saved to %1</source>
        <translation type="vanished">스크린 샷 저장 위치: %1</translation>
    </message>
    <message>
        <source>Save Session Export</source>
        <translation type="vanished">세션 내보내기 저장</translation>
    </message>
    <message>
        <source>Text Files (*.txt);;HTML Files (*.html)</source>
        <translation type="vanished">텍스트 파일 (*.txt);;HTML 파일 (*.html)</translation>
    </message>
    <message>
        <source>Text Files (*.txt)</source>
        <translation type="vanished">텍스트 파일 (*.txt)</translation>
    </message>
    <message>
        <source>HTML Files (*.html)</source>
        <translation type="vanished">HTML 파일 (*.html)</translation>
    </message>
    <message>
        <source>Session Export saved to %1</source>
        <translation type="vanished">세션 내보내기 저장 위치: %1</translation>
    </message>
    <message>
        <source>Session Export failed to save to %1</source>
        <translation type="vanished">세션 내보내기 저장 위치: %1</translation>
    </message>
    <message>
        <source>Select a directory</source>
        <translation type="vanished">디렉터리 선택</translation>
    </message>
    <message>
        <source>Select a bookmark</source>
        <translation type="vanished">북마크 선택</translation>
    </message>
    <message>
        <source>Are you sure to clean all bookmark?</source>
        <translation type="vanished">모든 북마크를 지우시겠습니까?</translation>
    </message>
    <message>
        <source>Port</source>
        <translation type="vanished">포트</translation>
    </message>
    <message>
        <source>Video background is enabled, please enable animation in global options (more system resources) or change background image.</source>
        <translation type="vanished">비디오 배경이 활성화되었습니다. 전역 옵션에서 애니메이션을 활성화하거나(시스템 리소스가 더 필요함) 배경 이미지를 변경하십시오.</translation>
    </message>
    <message>
        <source>Session information get failed.</source>
        <translation type="vanished">세션 정보 가져오기 실패.</translation>
    </message>
    <message>
        <source>Serial - </source>
        <translation type="vanished">시리얼 - </translation>
    </message>
    <message>
        <source>Serial</source>
        <translation type="vanished">시리얼</translation>
    </message>
    <message>
        <source>Raw - </source>
        <translation type="vanished">Raw - </translation>
    </message>
    <message>
        <source>Raw</source>
        <translation type="vanished">Raw</translation>
    </message>
    <message>
        <source>NamePipe - </source>
        <translation type="vanished">네임 파이프 - </translation>
    </message>
    <message>
        <source>NamePipe</source>
        <translation type="vanished">네임 파이프</translation>
    </message>
    <message>
        <source>Local Shell</source>
        <translation type="vanished">로컬 쉘</translation>
    </message>
    <message>
        <source>Local Shell - </source>
        <translation type="vanished">로컬 쉘 - </translation>
    </message>
    <message>
        <source>Are you sure to disconnect this session?</source>
        <translation type="vanished">이 세션을 연결 해제하시겠습니까?</translation>
    </message>
    <message>
        <source>Global Shortcuts:</source>
        <translation type="vanished">전역 단축키:</translation>
    </message>
    <message>
        <source>show/hide menu bar</source>
        <translation type="vanished">메뉴 바 표시/숨기기</translation>
    </message>
    <message>
        <source>connect to LocalShell</source>
        <translation type="vanished">로컬 쉘에 연결</translation>
    </message>
    <message>
        <source>clone current session</source>
        <translation type="vanished">현재 세션 복제</translation>
    </message>
    <message>
        <source>switch ui to STD mode</source>
        <translation type="vanished">STD 모드로 UI 전환</translation>
    </message>
    <message>
        <source>switch ui to MINI mode</source>
        <translation type="vanished">MINI 모드로 UI 전환</translation>
    </message>
    <message>
        <source>switch to previous session</source>
        <translation type="vanished">이전 세션으로 전환</translation>
    </message>
    <message>
        <source>switch to next session</source>
        <translation type="vanished">다음 세션으로 전환</translation>
    </message>
    <message>
        <source>switch to session [num]</source>
        <translation type="vanished">세션 [num]으로 전환</translation>
    </message>
    <message>
        <source>Go to line start</source>
        <translation type="vanished">줄 시작으로 이동</translation>
    </message>
    <message>
        <source>Go to line end</source>
        <translation type="vanished">줄 끝으로 이동</translation>
    </message>
    <message>
        <source>There are sessions that have not yet been unlocked, please unlock them first.</source>
        <translation type="vanished">아직 잠금 해제되지 않은 세션이 있습니다. 먼저 잠금을 해제하십시오.</translation>
    </message>
    <message>
        <source>Are you sure to quit?</source>
        <translation type="vanished">종료하시겠습니까?</translation>
    </message>
</context>
<context>
    <name>NetScanWindow</name>
    <message>
        <location filename="../src/netscanwindow/netscanwindow.ui" line="14"/>
        <source>NetScan</source>
        <translation>넷스캔</translation>
    </message>
</context>
<context>
    <name>OneStepWindow</name>
    <message>
        <source>Port</source>
        <translation type="obsolete">포트</translation>
    </message>
    <message>
        <source>UserName</source>
        <translation type="obsolete">사용자 이름</translation>
    </message>
</context>
<context>
    <name>PluginInfoWindow</name>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.ui" line="14"/>
        <source>Plugin Info</source>
        <translation>플러그인 정보</translation>
    </message>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.ui" line="28"/>
        <source>API version</source>
        <translation>API 버전</translation>
    </message>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.ui" line="38"/>
        <source>Install plugin</source>
        <translation>플러그인 설치</translation>
    </message>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="40"/>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="86"/>
        <source>Name</source>
        <translation>이름</translation>
    </message>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="40"/>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="86"/>
        <source>Version</source>
        <translation>버전</translation>
    </message>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="40"/>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="86"/>
        <source>API Version</source>
        <translation>API 버전</translation>
    </message>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="40"/>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="86"/>
        <source>Enable</source>
        <translation>사용</translation>
    </message>
</context>
<context>
    <name>PluginViewerHomeWidget</name>
    <message>
        <location filename="../src/pluginviewerwidget/pluginviewerhomewidget.cpp" line="18"/>
        <source>Welcome to use</source>
        <translation>QuardCRT 플러그인 시스템을</translation>
    </message>
    <message>
        <location filename="../src/pluginviewerwidget/pluginviewerhomewidget.cpp" line="20"/>
        <source>QuardCRT plugin system!</source>
        <translation>사용해 주셔서 감사합니다!</translation>
    </message>
</context>
<context>
    <name>PluginViewerWidget</name>
    <message>
        <location filename="../src/pluginviewerwidget/pluginviewerwidget.cpp" line="37"/>
        <source>Home</source>
        <translation>홈</translation>
    </message>
</context>
<context>
    <name>QCustomFileSystemModel</name>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="155"/>
        <source>Directory</source>
        <translation>디렉터리</translation>
    </message>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="155"/>
        <source>File</source>
        <translation>파일</translation>
    </message>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="159"/>
        <source>Loading...</source>
        <translation>로딩 중...</translation>
    </message>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="184"/>
        <source>Name</source>
        <translation>이름</translation>
    </message>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="186"/>
        <source>Type</source>
        <translation>종류</translation>
    </message>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="188"/>
        <source>Size</source>
        <translation>크기</translation>
    </message>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="190"/>
        <source>Last Modified</source>
        <translation>마지막 수정</translation>
    </message>
</context>
<context>
    <name>QKeychain::DeletePasswordJobPrivate</name>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="173"/>
        <source>Could not open keystore</source>
        <translation>키스토어를 열 수 없습니다</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="179"/>
        <source>Could not remove private key from keystore</source>
        <translation>키스토어에서 개인 키를 제거할 수 없습니다</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="584"/>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="592"/>
        <source>Unknown error</source>
        <translation>알 수 없는 오류</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="613"/>
        <source>Could not open wallet: %1; %2</source>
        <translation>월렛을 열 수 없습니다: %1; %2</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="104"/>
        <source>Password entry not found</source>
        <translation>암호 항목을 찾을 수 없습니다</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="108"/>
        <source>Could not decrypt data</source>
        <translation>데이터를 복호화할 수 없습니다</translation>
    </message>
</context>
<context>
    <name>QKeychain::Job</name>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="29"/>
        <source>No error</source>
        <translation>오류 없음</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="31"/>
        <source>The specified item could not be found in the keychain</source>
        <translation>지정된 항목을 키스토어에서 찾을 수 없습니다</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="33"/>
        <source>User canceled the operation</source>
        <translation>사용자가 작업을 취소했습니다</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="35"/>
        <source>User interaction is not allowed</source>
        <translation>사용자 상호 작용이 허용되지 않습니다</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="37"/>
        <source>No keychain is available. You may need to restart your computer</source>
        <translation>사용 가능한 키스토어가 없습니다. 컴퓨터를 다시 시작해야 할 수도 있습니다</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="39"/>
        <source>The user name or passphrase you entered is not correct</source>
        <translation>입력한 사용자 이름 또는 암호가 올바르지 않습니다</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="41"/>
        <source>A cryptographic verification failure has occurred</source>
        <translation>암호화 검증 실패가 발생했습니다</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="43"/>
        <source>Function or operation not implemented</source>
        <translation>기능 또는 작업이 구현되지 않았습니다</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="45"/>
        <source>I/O error</source>
        <translation>입출력 오류</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="47"/>
        <source>Already open with with write permission</source>
        <translation>쓰기 권한으로 이미 열려 있습니다</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="49"/>
        <source>Invalid parameters passed to a function</source>
        <translation>함수에 잘못된 매개변수가 전달되었습니다</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="51"/>
        <source>Failed to allocate memory</source>
        <translation>메모리 할당에 실패했습니다</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="53"/>
        <source>Bad parameter or invalid state for operation</source>
        <translation>잘못된 매개변수 또는 작업에 대한 잘못된 상태입니다</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="55"/>
        <source>An internal component failed</source>
        <translation>내부 구성 요소가 실패했습니다</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="57"/>
        <source>The specified item already exists in the keychain</source>
        <translation>지정된 항목이 이미 키스토어에 있습니다</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="59"/>
        <source>Unable to decode the provided data</source>
        <translation>제공된 데이터를 디코딩할 수 없습니다</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="62"/>
        <source>Unknown error</source>
        <translation>알 수 없는 오류</translation>
    </message>
</context>
<context>
    <name>QKeychain::JobPrivate</name>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="294"/>
        <source>Unknown error</source>
        <translation>알 수 없는 오류</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="541"/>
        <source>Access to keychain denied</source>
        <translation>키스토어에 접근할 수 없습니다</translation>
    </message>
</context>
<context>
    <name>QKeychain::PlainTextStore</name>
    <message>
        <location filename="../lib/qtkeychain/plaintextstore.cpp" line="65"/>
        <source>Could not store data in settings: access error</source>
        <translation>설정에 데이터를 저장할 수 없습니다: 접근 오류</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/plaintextstore.cpp" line="67"/>
        <source>Could not store data in settings: format error</source>
        <translation>설정에 데이터를 저장할 수 없습니다: 형식 오류</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/plaintextstore.cpp" line="85"/>
        <source>Could not delete data from settings: access error</source>
        <translation>설정에서 데이터를 삭제할 수 없습니다: 접근 오류</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/plaintextstore.cpp" line="87"/>
        <source>Could not delete data from settings: format error</source>
        <translation>설정에서 데이터를 삭제할 수 없습니다: 형식 오류</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/plaintextstore.cpp" line="104"/>
        <source>Entry not found</source>
        <translation>항목을 찾을 수 없습니다</translation>
    </message>
</context>
<context>
    <name>QKeychain::ReadPasswordJobPrivate</name>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="52"/>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="392"/>
        <source>Entry not found</source>
        <translation>항목을 찾을 수 없습니다</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="60"/>
        <source>Could not open keystore</source>
        <translation>키스토어를 열 수 없습니다</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="68"/>
        <source>Could not retrieve private key from keystore</source>
        <translation>키스토어에서 개인 키를 검색할 수 없습니다</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="75"/>
        <source>Could not create decryption cipher</source>
        <translation>복호화 암호를 생성할 수 없습니다</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="204"/>
        <source>D-Bus is not running</source>
        <translation>D-Bus가 실행되고 있지 않습니다</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="213"/>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="223"/>
        <source>Unknown error</source>
        <translation>알 수 없는 오류</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="315"/>
        <source>No keychain service available</source>
        <translation>사용 가능한 키스토어 서비스가 없습니다</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="317"/>
        <source>Could not open wallet: %1; %2</source>
        <translation>월렛을 열 수 없습니다: %1; %2</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="362"/>
        <source>Access to keychain denied</source>
        <translation>키스토어에 접근할 수 없습니다</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="383"/>
        <source>Could not determine data type: %1; %2</source>
        <translation>데이터 유형을 결정할 수 없습니다: %1; %2</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="401"/>
        <source>Unsupported entry type &apos;Map&apos;</source>
        <translation>지원되지 않는 항목 유형 &apos;Map&apos;</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="404"/>
        <source>Unknown kwallet entry type &apos;%1&apos;</source>
        <translation>알 수 없는 kwallet 항목 유형 &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="32"/>
        <source>Password entry not found</source>
        <translation>암호 항목을 찾을 수 없습니다</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="36"/>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="139"/>
        <source>Could not decrypt data</source>
        <translation>데이터를 복호화할 수 없습니다</translation>
    </message>
</context>
<context>
    <name>QKeychain::WritePasswordJobPrivate</name>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="95"/>
        <source>Could not open keystore</source>
        <translation>키스토어를 열 수 없습니다</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="124"/>
        <source>Could not create private key generator</source>
        <translation>개인 키 생성기를 만들 수 없습니다</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="131"/>
        <source>Could not generate new private key</source>
        <translation>새 개인 키를 생성할 수 없습니다</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="139"/>
        <source>Could not retrieve private key from keystore</source>
        <translation>키스토어에서 개인 키를 검색할 수 없습니다</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="147"/>
        <source>Could not create encryption cipher</source>
        <translation>암호화 암호를 생성할 수 없습니다</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="155"/>
        <source>Could not encrypt data</source>
        <translation>데이터를 암호화할 수 없습니다</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="444"/>
        <source>D-Bus is not running</source>
        <translation>D-Bus가 실행되고 있지 않습니다</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="454"/>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="481"/>
        <source>Unknown error</source>
        <translation>알 수 없는 오류</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="500"/>
        <source>Could not open wallet: %1; %2</source>
        <translation>월렛을 열 수 없습니다: %1; %2</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="78"/>
        <source>Credential size exceeds maximum size of %1</source>
        <translation>자격 증명 크기가 최대 크기 %1을(를) 초과합니다</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="87"/>
        <source>Credential key exceeds maximum size of %1</source>
        <translation>자격 증명 키가 최대 크기 %1을(를) 초과합니다</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="92"/>
        <source>Writing credentials failed: Win32 error code %1</source>
        <translation>자격 증명 쓰기 실패: Win32 오류 코드 %1</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="162"/>
        <source>Encryption failed</source>
        <translation>암호화 실패</translation>
    </message>
</context>
<context>
    <name>QMessageBox</name>
    <message>
        <location filename="../lib/qtermwidget/TerminalDisplay.cpp" line="2949"/>
        <source>Show Details...</source>
        <translation>세부 정보 표시...</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../lib/qtermwidget/ColorScheme.cpp" line="267"/>
        <location filename="../lib/qtermwidget/ColorScheme.cpp" line="282"/>
        <source>Un-named Color Scheme</source>
        <translation>이름 없는 색상 구성표</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/ColorScheme.cpp" line="453"/>
        <source>Accessible Color Scheme</source>
        <translation>접근 가능한 색상 구성표</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/Filter.cpp" line="549"/>
        <source>Open Link</source>
        <translation>링크 열기</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/Filter.cpp" line="550"/>
        <source>Copy Link Address</source>
        <translation>링크 주소 복사</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/Filter.cpp" line="554"/>
        <source>Send Email To...</source>
        <translation>이메일 보내기...</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/Filter.cpp" line="555"/>
        <source>Copy Email Address</source>
        <translation>이메일 주소 복사</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/Filter.cpp" line="559"/>
        <source>Open Path</source>
        <translation>경로 열기</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/Filter.cpp" line="560"/>
        <source>Copy Path</source>
        <translation>경로 복사</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="254"/>
        <source>Access to keychain denied</source>
        <translation>키스토어에 접근할 수 없습니다</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="256"/>
        <source>No keyring daemon</source>
        <translation>키링 데몬 없음</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="258"/>
        <source>Already unlocked</source>
        <translation>이미 잠금 해제됨</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="260"/>
        <source>No such keyring</source>
        <translation>그런 키링 없음</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="262"/>
        <source>Bad arguments</source>
        <translation>잘못된 인수</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="264"/>
        <source>I/O error</source>
        <translation>입출력 오류</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="266"/>
        <source>Cancelled</source>
        <translation>취소됨</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="268"/>
        <source>Keyring already exists</source>
        <translation>키링이 이미 있습니다</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="270"/>
        <source>No match</source>
        <translation>일치하는 것 없음</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="275"/>
        <source>Unknown error</source>
        <translation>알 수 없는 오류</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/libsecret.cpp" line="120"/>
        <source>Entry not found</source>
        <translation>항목을 찾을 수 없습니다</translation>
    </message>
</context>
<context>
    <name>QTermWidget</name>
    <message>
        <location filename="../lib/qtermwidget/qtermwidget.cpp" line="350"/>
        <source>Color Scheme Error</source>
        <translation>색상 구성표 오류</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/qtermwidget.cpp" line="351"/>
        <source>Cannot load color scheme: %1</source>
        <translation>색상 구성표를 불러올 수 없습니다: %1</translation>
    </message>
</context>
<context>
    <name>QuickConnectWindow</name>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="30"/>
        <source>Protocol</source>
        <translation>프로토콜</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="43"/>
        <source>Serial</source>
        <translation>시리얼</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="48"/>
        <source>Local Shell</source>
        <translation>로컬 쉘</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="58"/>
        <source>Named Pipe</source>
        <translation>네임 파이프</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="86"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="69"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="166"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="224"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="253"/>
        <source>Hostname</source>
        <translation>호스트 이름</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="109"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="70"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="167"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="225"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="254"/>
        <source>Port</source>
        <translation>포트</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="125"/>
        <source>WebSocket</source>
        <translation>웹 소켓</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="133"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="233"/>
        <source>None</source>
        <translation>없음</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="138"/>
        <source>Insecure</source>
        <translation>보안되지 않음</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="143"/>
        <source>Secure</source>
        <translation>보안됨</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="161"/>
        <source>Username</source>
        <translation>사용자 이름</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="171"/>
        <source>Password</source>
        <translation>비밀번호</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="188"/>
        <source>DataBits</source>
        <translation>데이터 비트</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="225"/>
        <source>Parity</source>
        <translation>패리티</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="238"/>
        <source>Odd</source>
        <translation>홀수</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="243"/>
        <source>Even</source>
        <translation>짝수</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="261"/>
        <source>StopBits</source>
        <translation>정지 비트</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="313"/>
        <source>Save session</source>
        <translation>세션 저장</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="323"/>
        <source>Open in tab</source>
        <translation>탭에서 열기</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="33"/>
        <source>Quick Connect</source>
        <translation>빠른 연결</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="91"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="188"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="246"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="275"/>
        <source>e.g. 127.0.0.1</source>
        <translation>예: 127.0.0.1</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="99"/>
        <source>Port Name</source>
        <translation>포트 이름</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="100"/>
        <source>Baud Rate</source>
        <translation>전송 속도</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="109"/>
        <source>e.g. 110, 300, 600, 1200, 2400, 
4800, 9600, 14400, 19200, 38400, 
56000, 57600, 115200, 128000, 256000, 
460800, 921600</source>
        <translation>예: 110, 300, 600, 1200, 2400,
4800, 9600, 14400, 19200, 38400,
56000, 57600, 115200, 128000, 256000,
460800, 921600</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="141"/>
        <source>Command</source>
        <translation>명령</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="162"/>
        <source>e.g. /bin/bash</source>
        <translation>예: /bin/bash</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="195"/>
        <source>Pipe Name</source>
        <translation>파이프 이름</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="217"/>
        <source>e.g. \\\.\pipe\namedpipe</source>
        <translation>예: \\\.\pipe\namedpipe</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="219"/>
        <source>e.g. /tmp/socket</source>
        <translation>예: /tmp/socket</translation>
    </message>
</context>
<context>
    <name>SFTPmenuBookmarkWidget</name>
    <message>
        <location filename="../src/sftpwindow/sftpmenubookmarkwidget.ui" line="17"/>
        <source>BookMarkName</source>
        <translation>북마크 이름</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpmenubookmarkwidget.ui" line="35"/>
        <source>LocalPath</source>
        <translation>로컬 경로</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpmenubookmarkwidget.ui" line="62"/>
        <source>RemotePath</source>
        <translation>원격 경로</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpmenubookmarkwidget.cpp" line="31"/>
        <source>Open Directory</source>
        <translation>디렉터리 열기</translation>
    </message>
</context>
<context>
    <name>SFTPmenuBookmarkWindow</name>
    <message>
        <source>Bookmark</source>
        <translation type="vanished">북마크</translation>
    </message>
    <message>
        <source>BookMarkName</source>
        <translation type="vanished">북마크 이름</translation>
    </message>
    <message>
        <source>LocalPath</source>
        <translation type="vanished">로컬 경로</translation>
    </message>
    <message>
        <source>RemotePath</source>
        <translation type="vanished">원격 경로</translation>
    </message>
    <message>
        <source>Open Directory</source>
        <translation type="vanished">디렉터리 열기</translation>
    </message>
</context>
<context>
    <name>SearchBar</name>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.ui" line="14"/>
        <source>SearchBar</source>
        <translation>검색 바</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.ui" line="20"/>
        <source>X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.ui" line="32"/>
        <source>Find:</source>
        <translation>찾기:</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.ui" line="42"/>
        <source>&lt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.ui" line="54"/>
        <source>&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.ui" line="66"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.cpp" line="40"/>
        <location filename="../lib/qtermwidget/SearchBar.cpp" line="130"/>
        <source>Match case</source>
        <translation>대소문자 구분</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.cpp" line="46"/>
        <location filename="../lib/qtermwidget/SearchBar.cpp" line="131"/>
        <source>Regular expression</source>
        <translation>정규식</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.cpp" line="50"/>
        <location filename="../lib/qtermwidget/SearchBar.cpp" line="132"/>
        <source>Highlight all matches</source>
        <translation>모든 일치 항목 강조 표시</translation>
    </message>
</context>
<context>
    <name>SessionManagerTreeModel</name>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="133"/>
        <source>Telnet</source>
        <translation>텔넷</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="135"/>
        <source>Serial</source>
        <translation>시리얼</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="137"/>
        <source>Shell</source>
        <translation>쉘</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="139"/>
        <source>Raw</source>
        <translation>Raw</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="141"/>
        <source>NamePipe</source>
        <translation>네임 파이프</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="269"/>
        <source>Name</source>
        <translation>이름</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="271"/>
        <source>Kind</source>
        <translation>종류</translation>
    </message>
</context>
<context>
    <name>SessionManagerTreeView</name>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="39"/>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="74"/>
        <source>Session</source>
        <translation>세션</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="84"/>
        <source>Connect Terminal</source>
        <translation>터미널 연결</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="89"/>
        <source>Connect in New Window</source>
        <translation>새 창에서 연결</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="96"/>
        <source>Connect in New Tab Group</source>
        <translation>새 탭 그룹에서 연결</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="103"/>
        <source>Delete</source>
        <translation>삭제</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="109"/>
        <source>Properties</source>
        <translation>속성</translation>
    </message>
</context>
<context>
    <name>SessionManagerWidget</name>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagerwidget.ui" line="43"/>
        <source>Session Manager</source>
        <translation>세션 관리자</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagerwidget.cpp" line="67"/>
        <source>Filter by folder/session name</source>
        <translation>폴더/세션 이름으로 필터링</translation>
    </message>
</context>
<context>
    <name>SessionOptionsGeneralWidget</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsgeneralwidget.ui" line="25"/>
        <source>Name</source>
        <translation>이름</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsgeneralwidget.ui" line="45"/>
        <source>Protocol</source>
        <translation>프로토콜</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsgeneralwidget.ui" line="58"/>
        <source>Serial</source>
        <translation>시리얼</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsgeneralwidget.ui" line="63"/>
        <source>Local Shell</source>
        <translation>로컬 쉘</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsgeneralwidget.ui" line="73"/>
        <source>Named Pipe</source>
        <translation>네임 파이프</translation>
    </message>
</context>
<context>
    <name>SessionOptionsLocalShellProperties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionslocalshellproperties.ui" line="19"/>
        <source>Command</source>
        <translation>명령</translation>
    </message>
</context>
<context>
    <name>SessionOptionsLocalShellState</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionslocalshellstate.cpp" line="33"/>
        <source>Name</source>
        <translation>이름</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionslocalshellstate.ui" line="25"/>
        <source>State</source>
        <translation>상태</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionslocalshellstate.ui" line="51"/>
        <source>Process Tree</source>
        <translation>프로세스 트리</translation>
    </message>
</context>
<context>
    <name>SessionOptionsNamePipeProperties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsnamepipeproperties.ui" line="25"/>
        <source>PipeName</source>
        <translation>파이프 이름</translation>
    </message>
</context>
<context>
    <name>SessionOptionsRawProperties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsrawproperties.ui" line="25"/>
        <source>Hostname</source>
        <translation>호스트 이름</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsrawproperties.ui" line="45"/>
        <source>Port</source>
        <translation>포트</translation>
    </message>
</context>
<context>
    <name>SessionOptionsSerialProperties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="25"/>
        <source>Port Name</source>
        <translation>포트 이름</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="45"/>
        <source>Baud Rate</source>
        <translation>전송 속도</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="72"/>
        <source>DataBits</source>
        <translation>데이터 비트</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="109"/>
        <source>Parity</source>
        <translation>패리티</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="117"/>
        <source>None</source>
        <translation>없음</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="122"/>
        <source>Odd</source>
        <translation>홀수</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="127"/>
        <source>Even</source>
        <translation>짝수</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="145"/>
        <source>StopBits</source>
        <translation>정지 비트</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.cpp" line="28"/>
        <source>e.g. 110, 300, 600, 1200, 2400, 
4800, 9600, 14400, 19200, 38400, 
56000, 57600, 115200, 128000, 256000, 
460800, 921600</source>
        <translation>예: 110, 300, 600, 1200, 2400,
4800, 9600, 14400, 19200, 38400,
56000, 57600, 115200, 128000, 256000,
460800, 921600</translation>
    </message>
</context>
<context>
    <name>SessionOptionsSsh2Properties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsssh2properties.ui" line="25"/>
        <source>Hostname</source>
        <translation>호스트 이름</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsssh2properties.ui" line="41"/>
        <source>Port</source>
        <translation>포트</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsssh2properties.ui" line="65"/>
        <source>UserName</source>
        <translation>사용자 이름</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsssh2properties.ui" line="81"/>
        <source>Password</source>
        <translation>비밀번호</translation>
    </message>
</context>
<context>
    <name>SessionOptionsTelnetProperties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionstelnetproperties.ui" line="19"/>
        <source>Hostname</source>
        <translation>호스트 이름</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionstelnetproperties.ui" line="33"/>
        <source>Port</source>
        <translation>포트</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionstelnetproperties.ui" line="55"/>
        <source>None</source>
        <translation>없음</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionstelnetproperties.ui" line="60"/>
        <source>Insecure</source>
        <translation>보안되지 않음</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionstelnetproperties.ui" line="65"/>
        <source>Secure</source>
        <translation>보안됨</translation>
    </message>
</context>
<context>
    <name>SessionOptionsVNCProperties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsvncproperties.ui" line="25"/>
        <source>Hostname</source>
        <translation>호스트 이름</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsvncproperties.ui" line="41"/>
        <source>Port</source>
        <translation>포트</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsvncproperties.ui" line="65"/>
        <source>Password</source>
        <translation>비밀번호</translation>
    </message>
</context>
<context>
    <name>SessionOptionsWindow</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionswindow.ui" line="14"/>
        <source>Session Options</source>
        <translation>세션 옵션</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionswindow.cpp" line="122"/>
        <source>General</source>
        <translation>일반</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionswindow.cpp" line="122"/>
        <source>Properties</source>
        <translation>속성</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionswindow.cpp" line="122"/>
        <source>State</source>
        <translation>상태</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionswindow.cpp" line="133"/>
        <source>Name</source>
        <translation>이름</translation>
    </message>
</context>
<context>
    <name>SessionsWindow</name>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="141"/>
        <source>Telnet Error</source>
        <translation>텔넷 오류</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="141"/>
        <source>Telnet error:
%1.</source>
        <translation>텔넷 오류:
%1.</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="168"/>
        <source>Serial Error</source>
        <translation>시리얼 오류</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="168"/>
        <source>Serial error:
%1.</source>
        <translation>시리얼 오류:
%1.</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="203"/>
        <source>Raw Socket Error</source>
        <translation>Raw 소켓 오류</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="203"/>
        <source>Raw Socket error:
%1.</source>
        <translation>Raw 소켓 오류:
%1.</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="234"/>
        <source>Name Pipe Error</source>
        <translation>네임 파이프 오류</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="234"/>
        <source>Name Pipe error:
%1.</source>
        <translation>네임 파이프 오류:
%1.</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="246"/>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="275"/>
        <source>SSH2 Error</source>
        <translation>SSH2 오류</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="246"/>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="275"/>
        <source>SSH2 error:
%1.</source>
        <translation>SSH2 오류:
%1.</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="434"/>
        <source>Start Local Shell</source>
        <translation>로컬 쉘 시작</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="434"/>
        <source>Cannot start local shell:
%1.</source>
        <translation>로컬 쉘을 시작할 수 없습니다:
%1.</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="573"/>
        <source>Save log...</source>
        <translation>로그 저장...</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="574"/>
        <source>log files (*.log)</source>
        <translation>로그 파일 (*.log)</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="578"/>
        <source>Save log</source>
        <translation>로그 저장</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="578"/>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="613"/>
        <source>Cannot write file %1:
%2.</source>
        <translation>%1 파일을 쓸 수 없습니다:
%2.</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="608"/>
        <source>Save Raw log...</source>
        <translation>Raw 로그 저장...</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="609"/>
        <source>binary files (*.bin)</source>
        <translation>바이너리 파일 (*.bin)</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="613"/>
        <source>Save Raw log</source>
        <translation>Raw 로그 저장</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="685"/>
        <source>Unlock Session</source>
        <translation>세션 잠금 해제</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="685"/>
        <source>Wrong password!</source>
        <translation>잘못된 비밀번호!</translation>
    </message>
</context>
<context>
    <name>SftpWindow</name>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.ui" line="14"/>
        <source>sftp</source>
        <translation>SFTP</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.ui" line="72"/>
        <source>local</source>
        <translation>로컬</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.ui" line="130"/>
        <source>remote</source>
        <translation>원격</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="46"/>
        <source>Bookmarks</source>
        <translation>북마크</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="47"/>
        <source>Add Bookmark</source>
        <translation>북마크 추가</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="48"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="67"/>
        <source>Edit Bookmark</source>
        <translation>북마크 편집</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="49"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="84"/>
        <source>Remove Bookmark</source>
        <translation>북마크 제거</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="67"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="84"/>
        <source>Bookmark Name:</source>
        <translation>북마크 이름:</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="109"/>
        <source>Warning</source>
        <translation>경고</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="109"/>
        <source>Bookmark Name can not be empty!</source>
        <translation>북마크 이름은 비워둘 수 없습니다!</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="196"/>
        <source>No task!</source>
        <translation>작업 없음!</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="200"/>
        <source>All tasks finished!</source>
        <translation>모든 작업이 완료되었습니다!</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="205"/>
        <source>task %1/%2</source>
        <translation>작업 %1/%2</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="223"/>
        <source>Open Directory</source>
        <translation>디렉터리 열기</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="246"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="412"/>
        <source>Show/Hide Files</source>
        <translation>파일 표시/숨기기</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="250"/>
        <source>Upload</source>
        <translation>업로드</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="301"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="476"/>
        <source>Open</source>
        <translation>열기</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="305"/>
        <source>Open in System File Manager</source>
        <translation>시스템 파일 관리자에서 열기</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="310"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="482"/>
        <source>refresh</source>
        <translation>새로고침</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="315"/>
        <source>Upload All</source>
        <translation>모두 업로드</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="359"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="535"/>
        <source>Cancel Selection</source>
        <translation>선택 취소</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="416"/>
        <source>Download</source>
        <translation>다운로드</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="456"/>
        <source>File exists</source>
        <translation>파일이 존재합니다</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="456"/>
        <source>File %1 already exists. Do you want to overwrite it?</source>
        <translation>%1 파일이 이미 존재합니다. 덮어쓰시겠습니까?</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="487"/>
        <source>Download All</source>
        <translation>모두 다운로드</translation>
    </message>
</context>
<context>
    <name>StartTftpSeverWindow</name>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.ui" line="14"/>
        <source>Start TFTP Sever</source>
        <translation>TFTP 서버 시작</translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.ui" line="32"/>
        <source>The TFTP server uses local directories for uploading and downloading files. Please enter this information below.</source>
        <translation>TFTP 서버는 파일을 업로드하고 다운로드하기 위해 로컬 디렉터리를 사용합니다. 아래에 이 정보를 입력하십시오.</translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.ui" line="65"/>
        <source>TFTP Port</source>
        <translation>TFTP 포트</translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.ui" line="104"/>
        <source>Upload directory</source>
        <translation>업로드 디렉터리</translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.ui" line="143"/>
        <source>Download directory</source>
        <translation>다운로드 디렉터리</translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.cpp" line="38"/>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.cpp" line="45"/>
        <source>Open Directory</source>
        <translation>디렉터리 열기</translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.cpp" line="77"/>
        <source>Warning</source>
        <translation>경고</translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.cpp" line="77"/>
        <source>Please select a valid directory!</source>
        <translation>유효한 디렉터리를 선택하십시오!</translation>
    </message>
</context>
<context>
    <name>UndoStack</name>
    <message>
        <location filename="../lib/qhexedit/commands.cpp" line="115"/>
        <source>Inserting %1 bytes</source>
        <translation>%1 바이트 삽입</translation>
    </message>
    <message>
        <location filename="../lib/qhexedit/commands.cpp" line="137"/>
        <source>Delete %1 chars</source>
        <translation>%1 문자 삭제</translation>
    </message>
    <message>
        <location filename="../lib/qhexedit/commands.cpp" line="162"/>
        <source>Overwrite %1 chars</source>
        <translation>%1 문자 덮어쓰기</translation>
    </message>
</context>
<context>
    <name>keyMapManager</name>
    <message>
        <location filename="../src/keymapmanager/keymapmanager.ui" line="14"/>
        <source>keyMapManager</source>
        <translation>키 맵 관리자</translation>
    </message>
    <message>
        <location filename="../src/keymapmanager/keymapmanager.ui" line="20"/>
        <source>KeyBinding</source>
        <translation>키 바인딩</translation>
    </message>
</context>
</TS>
