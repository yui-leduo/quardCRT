<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>AddTabButton</name>
    <message>
        <location filename="../lib/QtFancyTabWidget/addtabbutton.cpp" line="29"/>
        <source>New tab</source>
        <translation>Новая вкладка</translation>
    </message>
</context>
<context>
    <name>CentralWidget</name>
    <message>
        <location filename="../src/mainwindow.ui" line="101"/>
        <location filename="../src/mainwindow.cpp" line="1123"/>
        <source>Tool Bar</source>
        <translation>Панель инструментов</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="191"/>
        <location filename="../src/mainwindow.cpp" line="194"/>
        <location filename="../src/mainwindow.cpp" line="197"/>
        <location filename="../src/mainwindow.cpp" line="325"/>
        <location filename="../src/mainwindow.cpp" line="341"/>
        <location filename="../src/mainwindow.cpp" line="358"/>
        <location filename="../src/mainwindow.cpp" line="371"/>
        <location filename="../src/mainwindow.cpp" line="546"/>
        <location filename="../src/mainwindow.cpp" line="2552"/>
        <location filename="../src/mainwindow.cpp" line="2747"/>
        <location filename="../src/mainwindow.cpp" line="3115"/>
        <location filename="../src/mainwindow.cpp" line="3451"/>
        <location filename="../src/mainwindow.cpp" line="3456"/>
        <source>Warning</source>
        <translation>Предупреждение</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="191"/>
        <source>TFTP server bind error!</source>
        <translation>Ошибка привязки TFTP сервера!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="194"/>
        <source>TFTP server file error!</source>
        <translation>Ошибка файла TFTP сервера!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="197"/>
        <source>TFTP server network error!</source>
        <translation>Ошибка сети TFTP сервера!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="286"/>
        <source>Unlock Session</source>
        <translation>Разблокировать сессию</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="287"/>
        <source>Unlock current session</source>
        <translation>Разблокировать текущую сессию</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="293"/>
        <source>Move to another Tab</source>
        <translation>Переместить в другую вкладку</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="294"/>
        <source>Move to current session to another tab group</source>
        <translation>Переместить текущую сессию в другую группу вкладок</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="303"/>
        <source>Floating Window</source>
        <translation>Плавающее окно</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="304"/>
        <source>Floating current session to a new window</source>
        <translation>Плавающая текущая сессия в новом окне</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="311"/>
        <source>Copy Path</source>
        <translation>Копировать путь</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="312"/>
        <source>Copy current session working folder path</source>
        <translation>Копировать путь рабочего каталога текущей сессии</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="325"/>
        <location filename="../src/mainwindow.cpp" line="341"/>
        <location filename="../src/mainwindow.cpp" line="358"/>
        <source>No working folder!</source>
        <translation>Нет рабочего каталога!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="327"/>
        <source>Add Path to Bookmark</source>
        <translation>Добавить путь в закладки</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="328"/>
        <source>Add current session working folder path to bookmark</source>
        <translation>Добавить путь рабочего каталога текущей сессии в закладки</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="343"/>
        <source>Open Working Folder</source>
        <translation>Открыть рабочий каталог</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="344"/>
        <source>Open current session working folder in system file manager</source>
        <translation>Открыть рабочий каталог текущей сессии в файловом менеджере системы</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="362"/>
        <source>Open SFTP</source>
        <translation>Открыть SFTP</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="363"/>
        <source>Open SFTP in a new window</source>
        <translation>Открыть SFTP в новом окне</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="371"/>
        <source>No SFTP channel!</source>
        <translation>Нет канала SFTP!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="378"/>
        <source>Save Session</source>
        <translation>Сохранить сессию</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="379"/>
        <source>Save current session to session manager</source>
        <translation>Сохранить текущую сессию в менеджере сессий</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="389"/>
        <source>Enter Session Name</source>
        <translation>Введите имя сессии</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="390"/>
        <source>The session already exists, please rename the new session or cancel saving.</source>
        <translation>Сессия уже существует, переименуйте новую сессию или отмените сохранение.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="396"/>
        <source>Properties</source>
        <translation>Свойства</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="397"/>
        <source>Show current session properties</source>
        <translation>Показать свойства текущей сессии</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="417"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="418"/>
        <source>Close current session</source>
        <translation>Закрыть текущую сессию</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="423"/>
        <source>Close Others</source>
        <translation>Закрыть другие</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="424"/>
        <source>Close other sessions</source>
        <translation>Закрыть другие сессии</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="434"/>
        <source>Close to the Right</source>
        <translation>Закрыть справа</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="435"/>
        <source>Close sessions to the right</source>
        <translation>Закрыть сессии справа</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="443"/>
        <source>Close All</source>
        <translation>Закрыть все</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="444"/>
        <source>Close all sessions</source>
        <translation>Закрыть все сессии</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="546"/>
        <source>Session properties error!</source>
        <translation>Ошибка свойств сессии!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="566"/>
        <location filename="../src/mainwindow.cpp" line="684"/>
        <location filename="../src/mainwindow.cpp" line="2455"/>
        <source>Ready</source>
        <translation>Готово</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="777"/>
        <source>Highlight/Unhighlight</source>
        <translation>Выделить/Снять выделение</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="778"/>
        <source>Highlight/Unhighlight selected text</source>
        <translation>Выделить/Снять выделение выделенного текста</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="838"/>
        <source>Google Translate</source>
        <translation>Google Translate</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="839"/>
        <location filename="../src/mainwindow.cpp" line="852"/>
        <location filename="../src/mainwindow.cpp" line="864"/>
        <source>Translate selected text</source>
        <translation>Перевести выделенный текст</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="851"/>
        <source>Baidu Translate</source>
        <translation>Baidu Translate</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="863"/>
        <source>Microsoft Translate</source>
        <translation>Microsoft Translate</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="924"/>
        <source>Back to Main Window</source>
        <translation>Вернуться в главное окно</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="995"/>
        <location filename="../src/mainwindow.cpp" line="1019"/>
        <source>Session Manager</source>
        <translation>Менеджер сессий</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="996"/>
        <source>Plugin</source>
        <translation>Плагин</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="998"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="999"/>
        <source>Edit</source>
        <translation>Правка</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1000"/>
        <source>View</source>
        <translation>Вид</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1001"/>
        <source>Options</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1002"/>
        <source>Transfer</source>
        <translation>Передача</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1003"/>
        <source>Script</source>
        <translation>Скрипт</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1004"/>
        <source>Bookmark</source>
        <translation>Закладка</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1005"/>
        <source>Tools</source>
        <translation>Инструменты</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1006"/>
        <source>Window</source>
        <translation>Окно</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1007"/>
        <source>Language</source>
        <translation>Язык</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1008"/>
        <source>Theme</source>
        <translation>Тема</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1009"/>
        <location filename="../src/mainwindow.cpp" line="1229"/>
        <location filename="../src/mainwindow.cpp" line="3415"/>
        <source>Help</source>
        <translation>Помощь</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1011"/>
        <source>New Window</source>
        <translation>Новое окно</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1013"/>
        <source>Open a new window &lt;Ctrl+Shift+N&gt;</source>
        <translation>Открыть новое окно &lt;Ctrl+Shift+N&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1015"/>
        <source>Connect...</source>
        <translation>Подключиться...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1017"/>
        <source>Connect to a host &lt;Alt+C&gt;</source>
        <translation>Подключиться к хосту &lt;Alt+C&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1021"/>
        <source>Go to the Session Manager &lt;Alt+M&gt;</source>
        <translation>Перейти в менеджер сессий &lt;Alt+M&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1023"/>
        <source>Quick Connect...</source>
        <translation>Быстрое подключение...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1025"/>
        <source>Quick Connect to a host &lt;Alt+Q&gt;</source>
        <translation>Быстрое подключение к хосту &lt;Alt+Q&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1027"/>
        <source>Connect in Tab/Tile...</source>
        <translation>Подключиться во вкладке/плитке...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1028"/>
        <source>Connect to a host in a new tab &lt;Alt+B&gt;</source>
        <translation>Подключиться к хосту в новой вкладке &lt;Alt+B&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1030"/>
        <source>Connect Local Shell</source>
        <translation>Подключиться к локальному shell</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1032"/>
        <source>Connect to a local shell &lt;Alt+T&gt;</source>
        <translation>Подключиться к локальному shell &lt;Alt+T&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1034"/>
        <source>Reconnect</source>
        <translation>Переподключиться</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1036"/>
        <source>Reconnect current session</source>
        <translation>Переподключить текущую сессию</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1037"/>
        <source>Reconnect All</source>
        <translation>Переподключить все</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1038"/>
        <source>Reconnect all sessions &lt;Alt+A&gt;</source>
        <translation>Переподключить все сессии &lt;Alt+A&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1040"/>
        <source>Disconnect</source>
        <translation>Отключиться</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1042"/>
        <source>Disconnect current session</source>
        <translation>Отключить текущую сессию</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1043"/>
        <location filename="../src/mainwindow.cpp" line="1044"/>
        <source>Enter host &lt;Alt+R&gt; to connect</source>
        <translation>Введите хост &lt;Alt+R&gt; для подключения</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1045"/>
        <source>Disconnect All</source>
        <translation>Отключить все</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1046"/>
        <source>Disconnect all sessions</source>
        <translation>Отключить все сессии</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1047"/>
        <source>Clone Session</source>
        <translation>Клонировать сессию</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1048"/>
        <source>Clone current session &lt;Ctrl+Shift+T&gt;</source>
        <translation>Клонировать текущую сессию &lt;Ctrl+Shift+T&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1050"/>
        <source>Lock Session</source>
        <translation>Блокировка сессии</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1051"/>
        <source>Lock/Unlock current session</source>
        <translation>Блокировать/Разблокировать текущую сессию</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1052"/>
        <source>Log Session</source>
        <translation>Журнал сессии</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1053"/>
        <source>Create a log file for current session</source>
        <translation>Создать файл журнала для текущей сессии</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1054"/>
        <source>Raw Log Session</source>
        <translation>Сырой журнал сессии</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1055"/>
        <source>Create a raw log file for current session</source>
        <translation>Создать сырой файл журнала для текущей сессии</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1056"/>
        <source>Hex View</source>
        <translation>Шестнадцатеричный просмотр</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1057"/>
        <source>Show/Hide Hex View for current session</source>
        <translation>Показать/Скрыть шестнадцатеричный просмотр для текущей сессии</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1058"/>
        <source>Exit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1059"/>
        <source>Quit the application</source>
        <translation>Выйти из приложения</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1061"/>
        <source>Copy</source>
        <translation>Копировать</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1064"/>
        <source>Copy the selected text to the clipboard &lt;Command+C&gt;</source>
        <translation>Копировать выделенный текст в буфер обмена &lt;Command+C&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1067"/>
        <source>Copy the selected text to the clipboard &lt;Ctrl+Ins&gt;</source>
        <translation>Копировать выделенный текст в буфер обмена &lt;Ctrl+Ins&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1070"/>
        <source>Paste</source>
        <translation>Вставить</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1073"/>
        <source>Paste the clipboard text to the current session &lt;Command+V&gt;</source>
        <translation>Вставить текст из буфера обмена в текущую сессию &lt;Command+V&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1076"/>
        <source>Paste the clipboard text to the current session &lt;Shift+Ins&gt;</source>
        <translation>Вставить текст из буфера обмена в текущую сессию &lt;Shift+Ins&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1079"/>
        <source>Copy and Paste</source>
        <translation>Копировать и вставить</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1080"/>
        <source>Copy the selected text to the clipboard and paste to the current session</source>
        <translation>Копировать выделенный текст в буфер обмена и вставить в текущую сессию</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1081"/>
        <source>Select All</source>
        <translation>Выделить все</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1083"/>
        <source>Select all text in the current session &lt;Ctrl+Shift+A&gt;</source>
        <translation>Выделить весь текст в текущей сессии &lt;Ctrl+Shift+A&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1085"/>
        <source>Find...</source>
        <translation>Найти...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1087"/>
        <source>Find text in the current session &lt;Ctrl+F&gt;</source>
        <translation>Найти текст в текущей сессии &lt;Ctrl+F&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1089"/>
        <source>Print Screen</source>
        <translation>Печать экрана</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1091"/>
        <source>Print current screen</source>
        <translation>Печать текущего экрана</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1092"/>
        <source>Screen Shot</source>
        <translation>Снимок экрана</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1094"/>
        <source>Screen shot current screen &lt;Alt+P&gt;</source>
        <translation>Снимок текущего экрана &lt;Alt+P&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1096"/>
        <source>Session Export</source>
        <translation>Экспорт сессии</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1098"/>
        <source>Export current session to a file &lt;Alt+O&gt;</source>
        <translation>Экспортировать текущую сессию в файл &lt;Alt+O&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1100"/>
        <source>Clear Scrollback</source>
        <translation>Очистить буфер прокрутки</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1101"/>
        <source>Clear the contents of the scrollback rows</source>
        <translation>Очистить содержимое строк буфера прокрутки</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1102"/>
        <source>Clear Screen</source>
        <translation>Очистить экран</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1103"/>
        <source>Clear the contents of the current screen</source>
        <translation>Очистить содержимое текущего экрана</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1104"/>
        <source>Clear Screen and Scrollback</source>
        <translation>Очистить экран и буфер прокрутки</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1105"/>
        <source>Clear the contents of the screen and scrollback</source>
        <translation>Очистить содержимое экрана и буфера прокрутки</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1106"/>
        <source>Reset</source>
        <translation>Сбросить</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1107"/>
        <source>Reset terminal emulator</source>
        <translation>Сбросить эмулятор терминала</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1109"/>
        <source>Zoom In</source>
        <translation>Увеличить</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1111"/>
        <source>Zoom In &lt;Ctrl+&quot;=&quot;&gt;</source>
        <translation>Увеличить &lt;Ctrl+&quot;=&quot;&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1113"/>
        <source>Zoom Out</source>
        <translation>Уменьшить</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1115"/>
        <source>Zoom Out &lt;Ctrl+&quot;-&quot;&gt;</source>
        <translation>Уменьшить &lt;Ctrl+&quot;-&quot;&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1117"/>
        <location filename="../src/mainwindow.cpp" line="1119"/>
        <source>Zoom Reset</source>
        <translation>Сбросить масштаб</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1120"/>
        <source>Menu Bar</source>
        <translation>Меню</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1121"/>
        <source>Show/Hide Menu Bar &lt;Alt+U&gt;</source>
        <translation>Показать/Скрыть меню &lt;Alt+U&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1124"/>
        <source>Show/Hide Tool Bar</source>
        <translation>Показать/Скрыть панель инструментов</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1125"/>
        <source>Status Bar</source>
        <translation>Строка состояния</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1126"/>
        <source>Show/Hide Status Bar</source>
        <translation>Показать/Скрыть строку состояния</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1127"/>
        <source>Command Window</source>
        <translation>Окно команд</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1128"/>
        <source>Show/Hide Command Window</source>
        <translation>Показать/Скрыть окно команд</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1129"/>
        <source>Connect Bar</source>
        <translation>Панель подключения</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1130"/>
        <source>Show/Hide Connect Bar</source>
        <translation>Показать/Скрыть панель подключения</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1131"/>
        <source>Side Window</source>
        <translation>Боковое окно</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1132"/>
        <source>Show/Hide Side Window</source>
        <translation>Показать/Скрыть боковое окно</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1133"/>
        <source>Windows Transparency</source>
        <translation>Прозрачность окон</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1134"/>
        <source>Enable/Disable alpha transparency</source>
        <translation>Включить/Выключить альфа прозрачность</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1135"/>
        <source>Vertical Scroll Bar</source>
        <translation>Вертикальная полоса прокрутки</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1136"/>
        <source>Show/Hide Vertical Scroll Bar</source>
        <translation>Показать/Скрыть вертикальную полосу прокрутки</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1137"/>
        <source>Allways On Top</source>
        <translation>Поверх всех окон</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1138"/>
        <source>Show window always on top</source>
        <translation>Показать окно поверх всех окон</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1139"/>
        <source>Full Screen</source>
        <translation>Полный экран</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1140"/>
        <source>Toggle between full screen and normal mode &lt;Alt+Enter&gt;</source>
        <translation>Переключиться между полноэкранным и нормальным режимом &lt;Alt+Enter&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1143"/>
        <source>Session Options...</source>
        <translation>Настройки сессии...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1145"/>
        <source>Configure session options</source>
        <translation>Настроить параметры сессии</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1146"/>
        <source>Global Options...</source>
        <translation>Глобальные настройки...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1148"/>
        <source>Configure global options</source>
        <translation>Настроить глобальные параметры</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1149"/>
        <source>Real-time Save Options</source>
        <translation>Настройки сохранения в реальном времени</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1150"/>
        <source>Real-time save session options and global options</source>
        <translation>Настройки сохранения сессии и глобальные настройки в реальном времени</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1151"/>
        <source>Save Settings Now</source>
        <translation>Сохранить настройки сейчас</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1152"/>
        <source>Save options configuration now</source>
        <translation>Сохранить конфигурацию настроек сейчас</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1154"/>
        <source>Send ASCII...</source>
        <translation>Отправить ASCII...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1155"/>
        <source>Send ASCII file</source>
        <translation>Отправить файл ASCII</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1156"/>
        <source>Receive ASCII...</source>
        <translation>Принять ASCII...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1157"/>
        <source>Receive ASCII file</source>
        <translation>Принять файл ASCII</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1158"/>
        <source>Send Binary...</source>
        <translation>Отправить бинарный файл...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1159"/>
        <source>Send Binary file</source>
        <translation>Отправить бинарный файл</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1160"/>
        <source>Send Xmodem...</source>
        <translation>Отправить Xmodem...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1161"/>
        <source>Send a file using Xmodem</source>
        <translation>Отправить файл, используя Xmodem</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1162"/>
        <source>Receive Xmodem...</source>
        <translation>Принять Xmodem...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1163"/>
        <source>Receive a file using Xmodem</source>
        <translation>Принять файл, используя Xmodem</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1164"/>
        <source>Send Ymodem...</source>
        <translation>Отправить Ymodem...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1165"/>
        <source>Send a file using Ymodem</source>
        <translation>Отправить файл, используя Ymodem</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1166"/>
        <source>Receive Ymodem...</source>
        <translation>Принять Ymodem...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1167"/>
        <source>Receive a file using Ymodem</source>
        <translation>Принять файл, используя Ymodem</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1168"/>
        <source>Zmodem Upload List...</source>
        <translation>Список загрузки Zmodem...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1169"/>
        <source>Display Zmodem file upload list</source>
        <translation>Показать список загрузки файлов Zmodem</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1170"/>
        <source>Start Zmodem Upload</source>
        <translation>Начать загрузку Zmodem</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1171"/>
        <source>Start Zmodem file upload</source>
        <translation>Начать загрузку файла Zmodem</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1172"/>
        <source>Start TFTP Server</source>
        <translation>Запустить TFTP сервер</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1173"/>
        <source>Start/Stop the TFTP server</source>
        <translation>Запустить/Остановить TFTP сервер</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1175"/>
        <source>Run...</source>
        <translation>Запустить...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1176"/>
        <source>Run a script</source>
        <translation>Запустить скрипт</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1177"/>
        <source>Cancel</source>
        <translation>Отменить</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1178"/>
        <source>Cancel script execution</source>
        <translation>Отменить выполнение скрипта</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1179"/>
        <source>Start Recording Script</source>
        <translation>Начать запись скрипта</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1180"/>
        <source>Start recording script</source>
        <translation>Начать запись скрипта</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1181"/>
        <source>Stop Recording Script...</source>
        <translation>Остановить запись скрипта...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1182"/>
        <source>Stop recording script</source>
        <translation>Остановить запись скрипта</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1183"/>
        <source>Cancel Recording Script</source>
        <translation>Отменить запись скрипта</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1184"/>
        <source>Cancel recording script</source>
        <translation>Отменить запись скрипта</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1186"/>
        <source>Add Bookmark</source>
        <translation>Добавить закладку</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1187"/>
        <source>Add a bookmark</source>
        <translation>Добавить закладку</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1188"/>
        <location filename="../src/mainwindow.cpp" line="2350"/>
        <source>Remove Bookmark</source>
        <translation>Удалить закладку</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1189"/>
        <source>Remove a bookmark</source>
        <translation>Удалить закладку</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1190"/>
        <location filename="../src/mainwindow.cpp" line="2377"/>
        <source>Clean All Bookmark</source>
        <translation>Очистить все закладки</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1191"/>
        <source>Clean all bookmark</source>
        <translation>Очистить все закладки</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1193"/>
        <source>Keymap Manager</source>
        <translation>Менеджер клавиатурных схем</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1194"/>
        <source>Display keymap editor</source>
        <translation>Показать редактор клавиатурных схем</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1195"/>
        <source>Create Public Key...</source>
        <translation>Создать публичный ключ...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1196"/>
        <source>Create a public key</source>
        <translation>Создать публичный ключ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1197"/>
        <source>Publickey Manager</source>
        <translation>Менеджер публичных ключей</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1198"/>
        <source>Display publickey manager</source>
        <translation>Показать менеджер публичных ключей</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1242"/>
        <source>Laboratory</source>
        <translation>Лаборатория</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1245"/>
        <location filename="../src/mainwindow.cpp" line="2395"/>
        <source>SSH Scanning</source>
        <translation>Сканирование SSH</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1246"/>
        <source>Display SSH scanning dialog</source>
        <translation>Показать диалог сканирования SSH</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3399"/>
        <source>Version</source>
        <translation>Версия</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3401"/>
        <source>Commit</source>
        <translation>Коммит</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3403"/>
        <source>Date</source>
        <translation>Дата</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3405"/>
        <source>Author</source>
        <translation>Автор</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3407"/>
        <source>Website</source>
        <translation>Вебсайт</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1200"/>
        <source>Tab</source>
        <translation>Вкладка</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1201"/>
        <source>Arrange sessions in tabs</source>
        <translation>Расположить сессии во вкладках</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1202"/>
        <source>Tile</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1203"/>
        <source>Arrange sessions in non-overlapping tiles</source>
        <translation>Расположить сессии в неперекрывающихся плитках</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1204"/>
        <source>Cascade</source>
        <translation>Каскад</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1205"/>
        <source>Arrange sessions to overlap each other</source>
        <translation>Расположить сессии так, чтобы они перекрывали друг друга</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1207"/>
        <source>Simplified Chinese</source>
        <translation>简体中文</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1208"/>
        <source>Switch to Simplified Chinese</source>
        <translation>切换到简体中文</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1209"/>
        <source>Traditional Chinese</source>
        <translation>繁體中文</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1210"/>
        <source>Switch to Traditional Chinese</source>
        <translation>切換到繁體中文</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1211"/>
        <source>Russian</source>
        <translation>Русский</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1212"/>
        <source>Switch to Russian</source>
        <translation>Переключиться на русский</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1213"/>
        <source>Korean</source>
        <translation>한국어</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1214"/>
        <source>Switch to Korean</source>
        <translation>한국어로 전환</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1215"/>
        <source>Japanese</source>
        <translation>日本語</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1216"/>
        <source>Switch to Japanese</source>
        <translation>日本語に切り替える</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1217"/>
        <source>French</source>
        <translation>français</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1218"/>
        <source>Switch to French</source>
        <translation>Passer au français</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1219"/>
        <source>Spanish</source>
        <translation>español</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1220"/>
        <source>Switch to Spanish</source>
        <translation>Cambiar a español</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1221"/>
        <source>English</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1222"/>
        <source>Switch to English</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1224"/>
        <source>Light</source>
        <translation>Светлая</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1225"/>
        <source>Switch to light theme</source>
        <translation>Переключиться на светлую тему</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1226"/>
        <source>Dark</source>
        <translation>Темная</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1227"/>
        <source>Switch to dark theme</source>
        <translation>Переключиться на темную тему</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1231"/>
        <source>Display help</source>
        <translation>Показать справку</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1232"/>
        <source>Check Update</source>
        <translation>Проверить обновления</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1234"/>
        <source>Check for updates</source>
        <translation>Проверить наличие обновлений</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1235"/>
        <location filename="../src/mainwindow.cpp" line="3397"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1237"/>
        <source>Display about dialog</source>
        <translation>Показать диалоговое окно &quot;О программе&quot;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1238"/>
        <source>About Qt</source>
        <translation>О Qt</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1240"/>
        <source>Display about Qt dialog</source>
        <translation>Показать диалоговое окно &quot;О Qt&quot;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1247"/>
        <source>Plugin Info</source>
        <translation>Информация о плагине</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1248"/>
        <source>Display plugin information dialog</source>
        <translation>Показать диалоговое окно информации о плагине</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2134"/>
        <source>PrintScreen saved to %1</source>
        <translation>Снимок экрана сохранен в %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2146"/>
        <source>Save Screenshot</source>
        <translation>Сохранить снимок экрана</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2146"/>
        <source>Image Files (*.jpg)</source>
        <translation>Файлы изображений (*.jpg)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2151"/>
        <source>Screenshot saved to %1</source>
        <translation>Снимок экрана сохранен в %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2162"/>
        <source>Save Session Export</source>
        <translation>Сохранить экспорт сессии</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2162"/>
        <source>Text Files (*.txt);;HTML Files (*.html)</source>
        <translation>Текстовые файлы (*.txt);;HTML файлы (*.html)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2165"/>
        <source>Text Files (*.txt)</source>
        <translation>Текстовые файлы (*.txt)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2168"/>
        <source>HTML Files (*.html)</source>
        <translation>HTML файлы (*.html)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2177"/>
        <source>Session Export saved to %1</source>
        <translation>Экспорт сессии сохранен в %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2179"/>
        <source>Session Export failed to save to %1</source>
        <translation>Экспорт сессии не удалось сохранить в %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2323"/>
        <source>Select a directory</source>
        <translation>Выберите каталог</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2350"/>
        <source>Select a bookmark</source>
        <translation>Выберите закладку</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2377"/>
        <source>Are you sure to clean all bookmark?</source>
        <translation>Вы уверены, что хотите очистить все закладки?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2395"/>
        <source>Port</source>
        <translation>Порт</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2552"/>
        <source>Video background is enabled, please enable animation in global options (more system resources) or change background image.</source>
        <translation>Включен видеофон, пожалуйста, включите анимацию в глобальных настройках (больше системных ресурсов) или измените фоновое изображение.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2747"/>
        <source>Session information get failed.</source>
        <translation>Не удалось получить информацию о сессии.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2830"/>
        <source>Telnet - </source>
        <translation>Телнет - </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2831"/>
        <source>Telnet</source>
        <translation>Телнет</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2863"/>
        <source>Serial - </source>
        <translation>Серийный порт - </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2864"/>
        <source>Serial</source>
        <translation>Серийный порт</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2895"/>
        <source>Raw - </source>
        <translation>Raw - </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2896"/>
        <source>Raw</source>
        <translation>Raw</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2927"/>
        <source>NamePipe - </source>
        <translation>Именованный канал - </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2928"/>
        <source>NamePipe</source>
        <translation>Именованный канал</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2996"/>
        <location filename="../src/mainwindow.cpp" line="3000"/>
        <source>Local Shell</source>
        <translation>Локальная оболочка</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2998"/>
        <source>Local Shell - </source>
        <translation>Локальная оболочка - </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3115"/>
        <source>Are you sure to disconnect this session?</source>
        <translation>Вы уверены, что хотите отключить эту сессию?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3417"/>
        <source>Global Shortcuts:</source>
        <translation>Глобальные ярлыки:</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3418"/>
        <source>show/hide menu bar</source>
        <translation>показать/скрыть меню</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3419"/>
        <source>connect to LocalShell</source>
        <translation>подключиться к локальной оболочке</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3420"/>
        <source>clone current session</source>
        <translation>клонировать текущую сессию</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3421"/>
        <source>switch ui to STD mode</source>
        <translation>переключить интерфейс в режим STD</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3422"/>
        <source>switch ui to MINI mode</source>
        <translation>переключить интерфейс в режим MINI</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3423"/>
        <source>switch to previous session</source>
        <translation>переключиться на предыдущую сессию</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3424"/>
        <source>switch to next session</source>
        <translation>переключиться на следующую сессию</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3425"/>
        <source>switch to session [num]</source>
        <translation>переключиться на сессию [num]</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3426"/>
        <source>Go to line start</source>
        <translation>Перейти в начало строки</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3427"/>
        <source>Go to line end</source>
        <translation>Перейти в конец строки</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3451"/>
        <source>There are sessions that have not yet been unlocked, please unlock them first.</source>
        <translation>Есть сессии, которые еще не были разблокированы, пожалуйста, сначала разблокируйте их.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3456"/>
        <source>Are you sure to quit?</source>
        <translation>Вы уверены, что хотите выйти?</translation>
    </message>
</context>
<context>
    <name>CommandWidget</name>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="35"/>
        <source>Send commands to active session</source>
        <translation>Отправить команды в активную сессию</translation>
    </message>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="71"/>
        <source>ASCII</source>
        <translation>ASCII</translation>
    </message>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="84"/>
        <source>HEX</source>
        <translation>HEX</translation>
    </message>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="104"/>
        <source>time</source>
        <translation>время</translation>
    </message>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="117"/>
        <source>ms</source>
        <translation>мс</translation>
    </message>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="135"/>
        <source>Auto Send</source>
        <translation>Автоотправка</translation>
    </message>
    <message>
        <location filename="../src/commandwidget/commandwidget.ui" line="142"/>
        <source>Send</source>
        <translation>Отправить</translation>
    </message>
</context>
<context>
    <name>EmptyTabWidget</name>
    <message>
        <location filename="../src/sessiontab/sessiontab.cpp" line="69"/>
        <source>No session</source>
        <translation>Нет сессии</translation>
    </message>
</context>
<context>
    <name>GlobalOptionsAdvancedWidget</name>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="25"/>
        <source>Config File</source>
        <translation>Файл конфигурации</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="56"/>
        <source>Translate Service</source>
        <translation>Сервис перевода</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="67"/>
        <source>Google Translate</source>
        <translation>Google Translate</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="72"/>
        <source>Baidu Translate</source>
        <translation>Baidu Translate</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="77"/>
        <source>Microsoft Translate</source>
        <translation>Microsoft Translate</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="87"/>
        <source>Terminal background support animation</source>
        <translation>Анимация фона терминала</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="94"/>
        <source>NativeUI(Effective after restart)</source>
        <translation>NativeUI(Действует после перезапуска)</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsadvancedwidget.ui" line="101"/>
        <source>Github Copilot</source>
        <translation>Github Copilot</translation>
    </message>
</context>
<context>
    <name>GlobalOptionsAppearanceWidget</name>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="17"/>
        <source>Color Schemes</source>
        <translation>Цветовые схемы</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="27"/>
        <source>Font</source>
        <translation>Шрифт</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="45"/>
        <source>Series</source>
        <translation>Серия</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="65"/>
        <source>Size</source>
        <translation>Размер</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="93"/>
        <source>Background image</source>
        <translation>Фоновое изображение</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="115"/>
        <source>Clear</source>
        <translation>Очистить</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="124"/>
        <source>Background mode</source>
        <translation>Режим фона</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="131"/>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="143"/>
        <source>Stretch</source>
        <translation>Растянуть</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="138"/>
        <source>None</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="148"/>
        <source>Zoom</source>
        <translation>Масштабировать</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="153"/>
        <source>Fit</source>
        <translation>Подогнать</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="158"/>
        <source>Center</source>
        <translation>Центрировать</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="163"/>
        <source>Tile</source>
        <translation>Замостить</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsappearancewidget.ui" line="171"/>
        <source>Background opacity</source>
        <translation>Прозрачность фона</translation>
    </message>
</context>
<context>
    <name>GlobalOptionsGeneralWidget</name>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="19"/>
        <source>New tab mode</source>
        <translation>Режим новой вкладки</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="33"/>
        <source>New session</source>
        <translation>Новая сессия</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="38"/>
        <source>Clone session</source>
        <translation>Клонировать сессию</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="43"/>
        <source>LocalShell session</source>
        <translation>Локальная оболочка</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="55"/>
        <source>New tab workpath</source>
        <translation>Рабочий каталог новой вкладки</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="84"/>
        <source>Tab title mode</source>
        <translation>Режим заголовка вкладки</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="98"/>
        <source>Brief</source>
        <translation>Краткий</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="103"/>
        <source>Full</source>
        <translation>Полный</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="108"/>
        <source>Scroll</source>
        <translation>Прокрутка</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="126"/>
        <source>Tab Title Width</source>
        <translation>Ширина заголовка вкладки</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="155"/>
        <source>Tab Preview</source>
        <translation>Предпросмотр вкладки</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsgeneralwidget.ui" line="171"/>
        <source>Preview Width</source>
        <translation>Ширина предпросмотра</translation>
    </message>
</context>
<context>
    <name>GlobalOptionsTerminalWidget</name>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="17"/>
        <source>Scrollback lines</source>
        <translation>Количество строк прокрутки</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="34"/>
        <source>Cursor Shape</source>
        <translation>Форма курсора</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="42"/>
        <source>Block</source>
        <translation>Блок</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="47"/>
        <source>Underline</source>
        <translation>Подчеркивание</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="52"/>
        <source>IBeam</source>
        <translation>IBeam</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="60"/>
        <source>Cursor Blink</source>
        <translation>Мигание курсора</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionsterminalwidget.ui" line="70"/>
        <source>Word Characters</source>
        <translation>Символы слова</translation>
    </message>
</context>
<context>
    <name>GlobalOptionsWindow</name>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.ui" line="14"/>
        <source>Global Options</source>
        <translation>Глобальные настройки</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="164"/>
        <source>Select Background Image</source>
        <translation>Выберите фоновое изображение</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="164"/>
        <source>Image Files (*.png *.jpg *.jpeg *.bmp *.gif);;Video Files (*.mp4 *.avi *.mkv *.mov)</source>
        <translation>Файлы изображений (*.png *.jpg *.jpeg *.bmp *.gif);;Файлы видео (*.mp4 *.avi *.mkv *.mov)</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="202"/>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="207"/>
        <source>Information</source>
        <translation>Информация</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="202"/>
        <source>This feature needs more system resources, please use it carefully!</source>
        <translation>Эта функция требует больше системных ресурсов, используйте ее осторожно!</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="207"/>
        <source>This feature is not implemented yet!</source>
        <translation>Эта функция еще не реализована!</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="229"/>
        <source>General</source>
        <translation>Общие</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="229"/>
        <source>Appearance</source>
        <translation>Внешний вид</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="229"/>
        <source>Terminal</source>
        <translation>Терминал</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="229"/>
        <source>Window</source>
        <translation>Окно</translation>
    </message>
    <message>
        <location filename="../src/globaloptions/globaloptionswindow.cpp" line="229"/>
        <source>Advanced</source>
        <translation>Дополнительно</translation>
    </message>
</context>
<context>
    <name>GlobalOptionsWindowWidget</name>
    <message>
        <location filename="../src/globaloptions/globaloptionswindowwidget.ui" line="17"/>
        <source>Transparent window</source>
        <translation>Прозрачное окно</translation>
    </message>
</context>
<context>
    <name>HexViewWindow</name>
    <message>
        <source>ASCII Text...</source>
        <translation type="vanished">ASCII текст...</translation>
    </message>
    <message>
        <source>clear</source>
        <translation type="vanished">очистить</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.ui" line="20"/>
        <source>Hex View</source>
        <translation>Шестнадцатеричный просмотр</translation>
    </message>
    <message>
        <source>Information</source>
        <translation type="vanished">Информация</translation>
    </message>
    <message>
        <source>Will send Hex:
</source>
        <translation type="vanished">Будет отправлено Hex:
</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="71"/>
        <source>Copy</source>
        <translation>Копировать</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="79"/>
        <source>Copy Hex</source>
        <translation>Копировать Hex</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="87"/>
        <source>Dump</source>
        <translation>Дамп</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="91"/>
        <source>Save As</source>
        <translation>Сохранить как</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="91"/>
        <source>Binary File (*.bin)</source>
        <translation>Бинарный файл (*.bin)</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="99"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="99"/>
        <source>Failed to save file!</source>
        <translation>Не удалось сохранить файл!</translation>
    </message>
    <message>
        <location filename="../src/hexviewwindow/hexviewwindow.cpp" line="104"/>
        <source>Clear</source>
        <translation>Очистить</translation>
    </message>
</context>
<context>
    <name>Konsole::Session</name>
    <message>
        <location filename="../lib/qtermwidget/Session.cpp" line="317"/>
        <source>Bell in session &apos;%1&apos;</source>
        <translation>Сигнал в сессии &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>Konsole::TerminalDisplay</name>
    <message>
        <location filename="../lib/qtermwidget/TerminalDisplay.cpp" line="1383"/>
        <source>Size: XXX x XXX</source>
        <translation>Размер: XXX x XXX</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/TerminalDisplay.cpp" line="1395"/>
        <source>Size: %1 x %2</source>
        <translation>Размер: %1 x %2</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/TerminalDisplay.cpp" line="2942"/>
        <source>Paste multiline text</source>
        <translation>Вставить многострочный текст</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/TerminalDisplay.cpp" line="2943"/>
        <source>Are you sure you want to paste this text?</source>
        <translation>Вы уверены, что хотите вставить этот текст?</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/TerminalDisplay.cpp" line="3426"/>
        <source>&lt;qt&gt;Output has been &lt;a href=&quot;http://en.wikipedia.org/wiki/Flow_control&quot;&gt;suspended&lt;/a&gt; by pressing Ctrl+S.  Press &lt;b&gt;Ctrl+Q&lt;/b&gt; to resume.&lt;/qt&gt;</source>
        <translation>&lt;qt&gt;Вывод был &lt;a href=&quot;http://en.wikipedia.org/wiki/Flow_control&quot;&gt;приостановлен&lt;/a&gt; нажатием Ctrl+S. Нажмите &lt;b&gt;Ctrl+Q&lt;/b&gt; для продолжения.&lt;/qt&gt;</translation>
    </message>
</context>
<context>
    <name>Konsole::Vt102Emulation</name>
    <message>
        <location filename="../lib/qtermwidget/Vt102Emulation.cpp" line="1113"/>
        <source>No keyboard translator available.  The information needed to convert key presses into characters to send to the terminal is missing.</source>
        <translation>Нет доступного переводчика клавиатуры. Недостающая информация для преобразования нажатий клавиш в символы для отправки в терминал.</translation>
    </message>
</context>
<context>
    <name>LockSessionWindow</name>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.ui" line="14"/>
        <source>Lock Session</source>
        <translation>Блокировка сессии</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.ui" line="20"/>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="84"/>
        <source>Enter the password that will be used to unlock the session:</source>
        <translation>Введите пароль, который будет использоваться для разблокировки сессии:</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.ui" line="32"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.ui" line="50"/>
        <source>Reenter Password</source>
        <translation>Повторите пароль</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.ui" line="68"/>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="85"/>
        <source>Lock all sessions</source>
        <translation>Заблокировать все сессии</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.ui" line="75"/>
        <source>Lock all sessions in tab group</source>
        <translation>Заблокировать все сессии в группе вкладок</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="49"/>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="55"/>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="66"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="49"/>
        <source>Passwords do not match!</source>
        <translation>Пароли не совпадают!</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="55"/>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="66"/>
        <source>Password cannot be empty!</source>
        <translation>Пароль не может быть пустым!</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="98"/>
        <source>Enter the password that was used to lock the session:</source>
        <translation>Введите пароль, который использовался для блокировки сессии:</translation>
    </message>
    <message>
        <location filename="../src/locksessionwindow/locksessionwindow.cpp" line="99"/>
        <source>Unlock all sessions</source>
        <translation>Разблокировать все сессии</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Tool Bar</source>
        <translation type="vanished">Панель инструментов</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="vanished">Предупреждение</translation>
    </message>
    <message>
        <source>TFTP server bind error!</source>
        <translation type="vanished">Ошибка привязки TFTP сервера!</translation>
    </message>
    <message>
        <source>TFTP server file error!</source>
        <translation type="vanished">Ошибка файла TFTP сервера!</translation>
    </message>
    <message>
        <source>TFTP server network error!</source>
        <translation type="vanished">Ошибка сети TFTP сервера!</translation>
    </message>
    <message>
        <source>Unlock Session</source>
        <translation type="vanished">Разблокировать сессию</translation>
    </message>
    <message>
        <source>Move to another Tab</source>
        <translation type="vanished">Переместить в другую вкладку</translation>
    </message>
    <message>
        <source>Floating Window</source>
        <translation type="vanished">Плавающее окно</translation>
    </message>
    <message>
        <source>Copy Path</source>
        <translation type="vanished">Копировать путь</translation>
    </message>
    <message>
        <source>No working folder!</source>
        <translation type="vanished">Нет рабочего каталога!</translation>
    </message>
    <message>
        <source>Add Path to Bookmark</source>
        <translation type="vanished">Добавить путь в закладки</translation>
    </message>
    <message>
        <source>Open Working Folder</source>
        <translation type="vanished">Открыть рабочий каталог</translation>
    </message>
    <message>
        <source>Save Session</source>
        <translation type="vanished">Сохранить сессию</translation>
    </message>
    <message>
        <source>Enter Session Name</source>
        <translation type="vanished">Введите имя сессии</translation>
    </message>
    <message>
        <source>The session already exists, please rename the new session or cancel saving.</source>
        <translation type="vanished">Сессия уже существует, переименуйте новую сессию или отмените сохранение.</translation>
    </message>
    <message>
        <source>Properties</source>
        <translation type="vanished">Свойства</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Закрыть</translation>
    </message>
    <message>
        <source>Close Others</source>
        <translation type="vanished">Закрыть другие</translation>
    </message>
    <message>
        <source>Close to the Right</source>
        <translation type="vanished">Закрыть справа</translation>
    </message>
    <message>
        <source>Close All</source>
        <translation type="vanished">Закрыть все</translation>
    </message>
    <message>
        <source>Highlight/Unhighlight</source>
        <translation type="vanished">Выделить/Снять выделение</translation>
    </message>
    <message>
        <source>Ready</source>
        <translation type="vanished">Готово</translation>
    </message>
    <message>
        <source>Open SFTP</source>
        <translation type="vanished">Открыть SFTP</translation>
    </message>
    <message>
        <source>No SFTP channel!</source>
        <translation type="vanished">Нет канала SFTP!</translation>
    </message>
    <message>
        <source>Session properties error!</source>
        <translation type="vanished">Ошибка свойств сессии!</translation>
    </message>
    <message>
        <source>Google Translate</source>
        <translation type="vanished">Google Translate</translation>
    </message>
    <message>
        <source>Baidu Translate</source>
        <translation type="vanished">Baidu Translate</translation>
    </message>
    <message>
        <source>Microsoft Translate</source>
        <translation type="vanished">Microsoft Translate</translation>
    </message>
    <message>
        <source>Back to Main Window</source>
        <translation type="vanished">Вернуться в главное окно</translation>
    </message>
    <message>
        <source>Session Manager</source>
        <translation type="vanished">Менеджер сессий</translation>
    </message>
    <message>
        <source>File</source>
        <translation type="vanished">Файл</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="vanished">Правка</translation>
    </message>
    <message>
        <source>View</source>
        <translation type="vanished">Вид</translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="vanished">Настройки</translation>
    </message>
    <message>
        <source>Transfer</source>
        <translation type="vanished">Передача</translation>
    </message>
    <message>
        <source>Script</source>
        <translation type="vanished">Скрипт</translation>
    </message>
    <message>
        <source>Bookmark</source>
        <translation type="vanished">Закладка</translation>
    </message>
    <message>
        <source>Tools</source>
        <translation type="vanished">Инструменты</translation>
    </message>
    <message>
        <source>Window</source>
        <translation type="vanished">Окно</translation>
    </message>
    <message>
        <source>Language</source>
        <translation type="vanished">Язык</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation type="vanished">Тема</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">Помощь</translation>
    </message>
    <message>
        <source>New Window</source>
        <translation type="vanished">Новое окно</translation>
    </message>
    <message>
        <source>Open a new window &lt;Ctrl+Shift+N&gt;</source>
        <translation type="vanished">Открыть новое окно &lt;Ctrl+Shift+N&gt;</translation>
    </message>
    <message>
        <source>Connect...</source>
        <translation type="vanished">Подключиться...</translation>
    </message>
    <message>
        <source>Connect to a host &lt;Alt+C&gt;</source>
        <translation type="vanished">Подключиться к хосту &lt;Alt+C&gt;</translation>
    </message>
    <message>
        <source>Go to the Session Manager &lt;Alt+M&gt;</source>
        <translation type="vanished">Перейти в менеджер сессий &lt;Alt+M&gt;</translation>
    </message>
    <message>
        <source>Quick Connect...</source>
        <translation type="vanished">Быстрое подключение...</translation>
    </message>
    <message>
        <source>Quick Connect to a host &lt;Alt+Q&gt;</source>
        <translation type="vanished">Быстрое подключение к хосту &lt;Alt+Q&gt;</translation>
    </message>
    <message>
        <source>Connect in Tab/Tile...</source>
        <translation type="vanished">Подключиться во вкладке/плитке...</translation>
    </message>
    <message>
        <source>Connect to a host in a new tab &lt;Alt+B&gt;</source>
        <translation type="vanished">Подключиться к хосту в новой вкладке &lt;Alt+B&gt;</translation>
    </message>
    <message>
        <source>Connect Local Shell</source>
        <translation type="vanished">Подключиться к локальному shell</translation>
    </message>
    <message>
        <source>Connect to a local shell &lt;Alt+T&gt;</source>
        <translation type="vanished">Подключиться к локальному shell &lt;Alt+T&gt;</translation>
    </message>
    <message>
        <source>Reconnect</source>
        <translation type="vanished">Переподключиться</translation>
    </message>
    <message>
        <source>Reconnect current session</source>
        <translation type="vanished">Переподключить текущую сессию</translation>
    </message>
    <message>
        <source>Reconnect All</source>
        <translation type="vanished">Переподключить все</translation>
    </message>
    <message>
        <source>Reconnect all sessions &lt;Alt+A&gt;</source>
        <translation type="vanished">Переподключить все сессии &lt;Alt+A&gt;</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation type="vanished">Отключиться</translation>
    </message>
    <message>
        <source>Disconnect current session</source>
        <translation type="vanished">Отключить текущую сессию</translation>
    </message>
    <message>
        <source>Enter host &lt;Alt+R&gt; to connect</source>
        <translation type="vanished">Введите хост &lt;Alt+R&gt; для подключения</translation>
    </message>
    <message>
        <source>Disconnect All</source>
        <translation type="vanished">Отключить все</translation>
    </message>
    <message>
        <source>Disconnect all sessions</source>
        <translation type="vanished">Отключить все сессии</translation>
    </message>
    <message>
        <source>Clone Session</source>
        <translation type="vanished">Клонировать сессию</translation>
    </message>
    <message>
        <source>Clone current session &lt;Ctrl+Shift+T&gt;</source>
        <translation type="vanished">Клонировать текущую сессию &lt;Ctrl+Shift+T&gt;</translation>
    </message>
    <message>
        <source>Lock Session</source>
        <translation type="vanished">Блокировка сессии</translation>
    </message>
    <message>
        <source>Lock/Unlock current session</source>
        <translation type="vanished">Блокировать/Разблокировать текущую сессию</translation>
    </message>
    <message>
        <source>Log Session</source>
        <translation type="vanished">Журнал сессии</translation>
    </message>
    <message>
        <source>Create a log file for current session</source>
        <translation type="vanished">Создать файл журнала для текущей сессии</translation>
    </message>
    <message>
        <source>Raw Log Session</source>
        <translation type="vanished">Сырой журнал сессии</translation>
    </message>
    <message>
        <source>Create a raw log file for current session</source>
        <translation type="vanished">Создать сырой файл журнала для текущей сессии</translation>
    </message>
    <message>
        <source>Hex View</source>
        <translation type="vanished">Шестнадцатеричный просмотр</translation>
    </message>
    <message>
        <source>Show/Hide Hex View for current session</source>
        <translation type="vanished">Показать/Скрыть шестнадцатеричный просмотр для текущей сессии</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="vanished">Выход</translation>
    </message>
    <message>
        <source>Quit the application</source>
        <translation type="vanished">Выйти из приложения</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation type="vanished">Копировать</translation>
    </message>
    <message>
        <source>Copy the selected text to the clipboard &lt;Command+C&gt;</source>
        <translation type="vanished">Копировать выделенный текст в буфер обмена &lt;Command+C&gt;</translation>
    </message>
    <message>
        <source>Copy the selected text to the clipboard &lt;Ctrl+Ins&gt;</source>
        <translation type="vanished">Копировать выделенный текст в буфер обмена &lt;Ctrl+Ins&gt;</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation type="vanished">Вставить</translation>
    </message>
    <message>
        <source>Paste the clipboard text to the current session &lt;Command+V&gt;</source>
        <translation type="vanished">Вставить текст из буфера обмена в текущую сессию &lt;Command+V&gt;</translation>
    </message>
    <message>
        <source>Paste the clipboard text to the current session &lt;Shift+Ins&gt;</source>
        <translation type="vanished">Вставить текст из буфера обмена в текущую сессию &lt;Shift+Ins&gt;</translation>
    </message>
    <message>
        <source>Copy and Paste</source>
        <translation type="vanished">Копировать и вставить</translation>
    </message>
    <message>
        <source>Copy the selected text to the clipboard and paste to the current session</source>
        <translation type="vanished">Копировать выделенный текст в буфер обмена и вставить в текущую сессию</translation>
    </message>
    <message>
        <source>Select All</source>
        <translation type="vanished">Выделить все</translation>
    </message>
    <message>
        <source>Select all text in the current session &lt;Ctrl+Shift+A&gt;</source>
        <translation type="vanished">Выделить весь текст в текущей сессии &lt;Ctrl+Shift+A&gt;</translation>
    </message>
    <message>
        <source>Find...</source>
        <translation type="vanished">Найти...</translation>
    </message>
    <message>
        <source>Find text in the current session &lt;Ctrl+F&gt;</source>
        <translation type="vanished">Найти текст в текущей сессии &lt;Ctrl+F&gt;</translation>
    </message>
    <message>
        <source>Print Screen</source>
        <translation type="vanished">Печать экрана</translation>
    </message>
    <message>
        <source>Print current screen</source>
        <translation type="vanished">Печать текущего экрана</translation>
    </message>
    <message>
        <source>Screen Shot</source>
        <translation type="vanished">Снимок экрана</translation>
    </message>
    <message>
        <source>Screen shot current screen &lt;Alt+P&gt;</source>
        <translation type="vanished">Снимок текущего экрана &lt;Alt+P&gt;</translation>
    </message>
    <message>
        <source>Session Export</source>
        <translation type="vanished">Экспорт сессии</translation>
    </message>
    <message>
        <source>Export current session to a file &lt;Alt+O&gt;</source>
        <translation type="vanished">Экспортировать текущую сессию в файл &lt;Alt+O&gt;</translation>
    </message>
    <message>
        <source>Clear Scrollback</source>
        <translation type="vanished">Очистить буфер прокрутки</translation>
    </message>
    <message>
        <source>Clear the contents of the scrollback rows</source>
        <translation type="vanished">Очистить содержимое строк буфера прокрутки</translation>
    </message>
    <message>
        <source>Clear Screen</source>
        <translation type="vanished">Очистить экран</translation>
    </message>
    <message>
        <source>Clear the contents of the current screen</source>
        <translation type="vanished">Очистить содержимое текущего экрана</translation>
    </message>
    <message>
        <source>Clear Screen and Scrollback</source>
        <translation type="vanished">Очистить экран и буфер прокрутки</translation>
    </message>
    <message>
        <source>Clear the contents of the screen and scrollback</source>
        <translation type="vanished">Очистить содержимое экрана и буфера прокрутки</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation type="vanished">Сбросить</translation>
    </message>
    <message>
        <source>Reset terminal emulator</source>
        <translation type="vanished">Сбросить эмулятор терминала</translation>
    </message>
    <message>
        <source>Zoom In</source>
        <translation type="vanished">Увеличить</translation>
    </message>
    <message>
        <source>Zoom In &lt;Ctrl+&quot;=&quot;&gt;</source>
        <translation type="vanished">Увеличить &lt;Ctrl+&quot;=&quot;&gt;</translation>
    </message>
    <message>
        <source>Zoom Out</source>
        <translation type="vanished">Уменьшить</translation>
    </message>
    <message>
        <source>Zoom Out &lt;Ctrl+&quot;-&quot;&gt;</source>
        <translation type="vanished">Уменьшить &lt;Ctrl+&quot;-&quot;&gt;</translation>
    </message>
    <message>
        <source>Zoom Reset</source>
        <translation type="vanished">Сбросить масштаб</translation>
    </message>
    <message>
        <source>Menu Bar</source>
        <translation type="vanished">Меню</translation>
    </message>
    <message>
        <source>Show/Hide Menu Bar &lt;Alt+U&gt;</source>
        <translation type="vanished">Показать/Скрыть меню &lt;Alt+U&gt;</translation>
    </message>
    <message>
        <source>Show/Hide Tool Bar</source>
        <translation type="vanished">Показать/Скрыть панель инструментов</translation>
    </message>
    <message>
        <source>Status Bar</source>
        <translation type="vanished">Строка состояния</translation>
    </message>
    <message>
        <source>Show/Hide Status Bar</source>
        <translation type="vanished">Показать/Скрыть строку состояния</translation>
    </message>
    <message>
        <source>Command Window</source>
        <translation type="vanished">Окно команд</translation>
    </message>
    <message>
        <source>Show/Hide Command Window</source>
        <translation type="vanished">Показать/Скрыть окно команд</translation>
    </message>
    <message>
        <source>Connect Bar</source>
        <translation type="vanished">Панель подключения</translation>
    </message>
    <message>
        <source>Show/Hide Connect Bar</source>
        <translation type="vanished">Показать/Скрыть панель подключения</translation>
    </message>
    <message>
        <source>Side Window</source>
        <translation type="vanished">Боковое окно</translation>
    </message>
    <message>
        <source>Show/Hide Side Window</source>
        <translation type="vanished">Показать/Скрыть боковое окно</translation>
    </message>
    <message>
        <source>Windows Transparency</source>
        <translation type="vanished">Прозрачность окон</translation>
    </message>
    <message>
        <source>Enable/Disable alpha transparency</source>
        <translation type="vanished">Включить/Выключить альфа прозрачность</translation>
    </message>
    <message>
        <source>Vertical Scroll Bar</source>
        <translation type="vanished">Вертикальная полоса прокрутки</translation>
    </message>
    <message>
        <source>Show/Hide Vertical Scroll Bar</source>
        <translation type="vanished">Показать/Скрыть вертикальную полосу прокрутки</translation>
    </message>
    <message>
        <source>Allways On Top</source>
        <translation type="vanished">Поверх всех окон</translation>
    </message>
    <message>
        <source>Show window always on top</source>
        <translation type="vanished">Показать окно поверх всех окон</translation>
    </message>
    <message>
        <source>Full Screen</source>
        <translation type="vanished">Полный экран</translation>
    </message>
    <message>
        <source>Toggle between full screen and normal mode &lt;Alt+Enter&gt;</source>
        <translation type="vanished">Переключиться между полноэкранным и нормальным режимом &lt;Alt+Enter&gt;</translation>
    </message>
    <message>
        <source>Session Options...</source>
        <translation type="vanished">Настройки сессии...</translation>
    </message>
    <message>
        <source>Configure session options</source>
        <translation type="vanished">Настроить параметры сессии</translation>
    </message>
    <message>
        <source>Global Options...</source>
        <translation type="vanished">Глобальные настройки...</translation>
    </message>
    <message>
        <source>Configure global options</source>
        <translation type="vanished">Настроить глобальные параметры</translation>
    </message>
    <message>
        <source>Real-time Save Options</source>
        <translation type="vanished">Настройки сохранения в реальном времени</translation>
    </message>
    <message>
        <source>Real-time save session options and global options</source>
        <translation type="vanished">Настройки сохранения сессии и глобальные настройки в реальном времени</translation>
    </message>
    <message>
        <source>Save Settings Now</source>
        <translation type="vanished">Сохранить настройки сейчас</translation>
    </message>
    <message>
        <source>Save options configuration now</source>
        <translation type="vanished">Сохранить конфигурацию настроек сейчас</translation>
    </message>
    <message>
        <source>Send ASCII...</source>
        <translation type="vanished">Отправить ASCII...</translation>
    </message>
    <message>
        <source>Send ASCII file</source>
        <translation type="vanished">Отправить файл ASCII</translation>
    </message>
    <message>
        <source>Receive ASCII...</source>
        <translation type="vanished">Принять ASCII...</translation>
    </message>
    <message>
        <source>Receive ASCII file</source>
        <translation type="vanished">Принять файл ASCII</translation>
    </message>
    <message>
        <source>Send Binary...</source>
        <translation type="vanished">Отправить бинарный файл...</translation>
    </message>
    <message>
        <source>Send Binary file</source>
        <translation type="vanished">Отправить бинарный файл</translation>
    </message>
    <message>
        <source>Send Xmodem...</source>
        <translation type="vanished">Отправить Xmodem...</translation>
    </message>
    <message>
        <source>Send a file using Xmodem</source>
        <translation type="vanished">Отправить файл, используя Xmodem</translation>
    </message>
    <message>
        <source>Receive Xmodem...</source>
        <translation type="vanished">Принять Xmodem...</translation>
    </message>
    <message>
        <source>Receive a file using Xmodem</source>
        <translation type="vanished">Принять файл, используя Xmodem</translation>
    </message>
    <message>
        <source>Send Ymodem...</source>
        <translation type="vanished">Отправить Ymodem...</translation>
    </message>
    <message>
        <source>Send a file using Ymodem</source>
        <translation type="vanished">Отправить файл, используя Ymodem</translation>
    </message>
    <message>
        <source>Receive Ymodem...</source>
        <translation type="vanished">Принять Ymodem...</translation>
    </message>
    <message>
        <source>Receive a file using Ymodem</source>
        <translation type="vanished">Принять файл, используя Ymodem</translation>
    </message>
    <message>
        <source>Zmodem Upload List...</source>
        <translation type="vanished">Список загрузки Zmodem...</translation>
    </message>
    <message>
        <source>Display Zmodem file upload list</source>
        <translation type="vanished">Показать список загрузки файлов Zmodem</translation>
    </message>
    <message>
        <source>Start Zmodem Upload</source>
        <translation type="vanished">Начать загрузку Zmodem</translation>
    </message>
    <message>
        <source>Start Zmodem file upload</source>
        <translation type="vanished">Начать загрузку файла Zmodem</translation>
    </message>
    <message>
        <source>Start TFTP Server</source>
        <translation type="vanished">Запустить TFTP сервер</translation>
    </message>
    <message>
        <source>Start/Stop the TFTP server</source>
        <translation type="vanished">Запустить/Остановить TFTP сервер</translation>
    </message>
    <message>
        <source>Run...</source>
        <translation type="vanished">Запустить...</translation>
    </message>
    <message>
        <source>Run a script</source>
        <translation type="vanished">Запустить скрипт</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Отменить</translation>
    </message>
    <message>
        <source>Cancel script execution</source>
        <translation type="vanished">Отменить выполнение скрипта</translation>
    </message>
    <message>
        <source>Start Recording Script</source>
        <translation type="vanished">Начать запись скрипта</translation>
    </message>
    <message>
        <source>Start recording script</source>
        <translation type="vanished">Начать запись скрипта</translation>
    </message>
    <message>
        <source>Stop Recording Script...</source>
        <translation type="vanished">Остановить запись скрипта...</translation>
    </message>
    <message>
        <source>Stop recording script</source>
        <translation type="vanished">Остановить запись скрипта</translation>
    </message>
    <message>
        <source>Cancel Recording Script</source>
        <translation type="vanished">Отменить запись скрипта</translation>
    </message>
    <message>
        <source>Cancel recording script</source>
        <translation type="vanished">Отменить запись скрипта</translation>
    </message>
    <message>
        <source>Add Bookmark</source>
        <translation type="vanished">Добавить закладку</translation>
    </message>
    <message>
        <source>Add a bookmark</source>
        <translation type="vanished">Добавить закладку</translation>
    </message>
    <message>
        <source>Remove Bookmark</source>
        <translation type="vanished">Удалить закладку</translation>
    </message>
    <message>
        <source>Remove a bookmark</source>
        <translation type="vanished">Удалить закладку</translation>
    </message>
    <message>
        <source>Clean All Bookmark</source>
        <translation type="vanished">Очистить все закладки</translation>
    </message>
    <message>
        <source>Clean all bookmark</source>
        <translation type="vanished">Очистить все закладки</translation>
    </message>
    <message>
        <source>Keymap Manager</source>
        <translation type="vanished">Менеджер клавиатурных схем</translation>
    </message>
    <message>
        <source>Display keymap editor</source>
        <translation type="vanished">Показать редактор клавиатурных схем</translation>
    </message>
    <message>
        <source>Create Public Key...</source>
        <translation type="vanished">Создать публичный ключ...</translation>
    </message>
    <message>
        <source>Create a public key</source>
        <translation type="vanished">Создать публичный ключ</translation>
    </message>
    <message>
        <source>Publickey Manager</source>
        <translation type="vanished">Менеджер публичных ключей</translation>
    </message>
    <message>
        <source>Display publickey manager</source>
        <translation type="vanished">Показать менеджер публичных ключей</translation>
    </message>
    <message>
        <source>SSH Scanning</source>
        <translation type="vanished">Сканирование SSH</translation>
    </message>
    <message>
        <source>Display SSH scanning dialog</source>
        <translation type="vanished">Показать диалог сканирования SSH</translation>
    </message>
    <message>
        <source>Tab</source>
        <translation type="vanished">Вкладка</translation>
    </message>
    <message>
        <source>Arrange sessions in tabs</source>
        <translation type="vanished">Расположить сессии во вкладках</translation>
    </message>
    <message>
        <source>Tile</source>
        <translation type="vanished">Плитка</translation>
    </message>
    <message>
        <source>Arrange sessions in non-overlapping tiles</source>
        <translation type="vanished">Расположить сессии в неперекрывающихся плитках</translation>
    </message>
    <message>
        <source>Cascade</source>
        <translation type="vanished">Каскад</translation>
    </message>
    <message>
        <source>Arrange sessions to overlap each other</source>
        <translation type="vanished">Расположить сессии так, чтобы они перекрывали друг друга</translation>
    </message>
    <message>
        <source>Japanese</source>
        <translation type="vanished">日本語</translation>
    </message>
    <message>
        <source>Simplified Chinese</source>
        <translation type="vanished">简体中文</translation>
    </message>
    <message>
        <source>Switch to Simplified Chinese</source>
        <translation type="vanished">切换到简体中文</translation>
    </message>
    <message>
        <source>Traditional Chinese</source>
        <translation type="vanished">繁體中文</translation>
    </message>
    <message>
        <source>Switch to Traditional Chinese</source>
        <translation type="vanished">切換到繁體中文</translation>
    </message>
    <message>
        <source>Russian</source>
        <translation type="vanished">Русский</translation>
    </message>
    <message>
        <source>Switch to Russian</source>
        <translation type="vanished">Переключиться на русский</translation>
    </message>
    <message>
        <source>Korean</source>
        <translation type="vanished">한국어</translation>
    </message>
    <message>
        <source>Switch to Korean</source>
        <translation type="vanished">한국어로 전환</translation>
    </message>
    <message>
        <source>Switch to Japanese</source>
        <translation type="vanished">日本語に切り替える</translation>
    </message>
    <message>
        <source>French</source>
        <translation type="vanished">français</translation>
    </message>
    <message>
        <source>Switch to French</source>
        <translation type="vanished">Passer au français</translation>
    </message>
    <message>
        <source>Spanish</source>
        <translation type="vanished">español</translation>
    </message>
    <message>
        <source>Switch to Spanish</source>
        <translation type="vanished">Cambiar a español</translation>
    </message>
    <message>
        <source>Light</source>
        <translation type="vanished">Светлая</translation>
    </message>
    <message>
        <source>Switch to light theme</source>
        <translation type="vanished">Переключиться на светлую тему</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation type="vanished">Темная</translation>
    </message>
    <message>
        <source>Switch to dark theme</source>
        <translation type="vanished">Переключиться на темную тему</translation>
    </message>
    <message>
        <source>Display help</source>
        <translation type="vanished">Показать справку</translation>
    </message>
    <message>
        <source>Check Update</source>
        <translation type="vanished">Проверить обновления</translation>
    </message>
    <message>
        <source>Check for updates</source>
        <translation type="vanished">Проверить наличие обновлений</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">О программе</translation>
    </message>
    <message>
        <source>Display about dialog</source>
        <translation type="vanished">Показать диалоговое окно &quot;О программе&quot;</translation>
    </message>
    <message>
        <source>About Qt</source>
        <translation type="vanished">О Qt</translation>
    </message>
    <message>
        <source>Display about Qt dialog</source>
        <translation type="vanished">Показать диалоговое окно &quot;О Qt&quot;</translation>
    </message>
    <message>
        <source>PrintScreen saved to %1</source>
        <translation type="vanished">Снимок экрана сохранен в %1</translation>
    </message>
    <message>
        <source>Save Screenshot</source>
        <translation type="vanished">Сохранить снимок экрана</translation>
    </message>
    <message>
        <source>Image Files (*.jpg)</source>
        <translation type="vanished">Файлы изображений (*.jpg)</translation>
    </message>
    <message>
        <source>Screenshot saved to %1</source>
        <translation type="vanished">Снимок экрана сохранен в %1</translation>
    </message>
    <message>
        <source>Save Session Export</source>
        <translation type="vanished">Сохранить экспорт сессии</translation>
    </message>
    <message>
        <source>Text Files (*.txt);;HTML Files (*.html)</source>
        <translation type="vanished">Текстовые файлы (*.txt);;HTML файлы (*.html)</translation>
    </message>
    <message>
        <source>Text Files (*.txt)</source>
        <translation type="vanished">Текстовые файлы (*.txt)</translation>
    </message>
    <message>
        <source>HTML Files (*.html)</source>
        <translation type="vanished">HTML файлы (*.html)</translation>
    </message>
    <message>
        <source>Session Export saved to %1</source>
        <translation type="vanished">Экспорт сессии сохранен в %1</translation>
    </message>
    <message>
        <source>Session Export failed to save to %1</source>
        <translation type="vanished">Экспорт сессии не удалось сохранить в %1</translation>
    </message>
    <message>
        <source>Select a directory</source>
        <translation type="vanished">Выберите каталог</translation>
    </message>
    <message>
        <source>Select a bookmark</source>
        <translation type="vanished">Выберите закладку</translation>
    </message>
    <message>
        <source>Are you sure to clean all bookmark?</source>
        <translation type="vanished">Вы уверены, что хотите очистить все закладки?</translation>
    </message>
    <message>
        <source>Port</source>
        <translation type="vanished">Порт</translation>
    </message>
    <message>
        <source>Video background is enabled, please enable animation in global options (more system resources) or change background image.</source>
        <translation type="vanished">Включен видеофон, пожалуйста, включите анимацию в глобальных настройках (больше системных ресурсов) или измените фоновое изображение.</translation>
    </message>
    <message>
        <source>Session information get failed.</source>
        <translation type="vanished">Не удалось получить информацию о сессии.</translation>
    </message>
    <message>
        <source>Telnet - </source>
        <translation type="vanished">Телнет - </translation>
    </message>
    <message>
        <source>Telnet</source>
        <translation type="vanished">Телнет</translation>
    </message>
    <message>
        <source>Serial - </source>
        <translation type="vanished">Серийный порт - </translation>
    </message>
    <message>
        <source>Serial</source>
        <translation type="vanished">Серийный порт</translation>
    </message>
    <message>
        <source>Raw - </source>
        <translation type="vanished">Raw - </translation>
    </message>
    <message>
        <source>Raw</source>
        <translation type="vanished">Raw</translation>
    </message>
    <message>
        <source>NamePipe - </source>
        <translation type="vanished">Именованный канал - </translation>
    </message>
    <message>
        <source>NamePipe</source>
        <translation type="vanished">Именованный канал</translation>
    </message>
    <message>
        <source>Local Shell</source>
        <translation type="vanished">Локальная оболочка</translation>
    </message>
    <message>
        <source>Local Shell - </source>
        <translation type="vanished">Локальная оболочка - </translation>
    </message>
    <message>
        <source>Are you sure to disconnect this session?</source>
        <translation type="vanished">Вы уверены, что хотите отключить эту сессию?</translation>
    </message>
    <message>
        <source>Global Shortcuts:</source>
        <translation type="vanished">Глобальные ярлыки:</translation>
    </message>
    <message>
        <source>show/hide menu bar</source>
        <translation type="vanished">показать/скрыть меню</translation>
    </message>
    <message>
        <source>connect to LocalShell</source>
        <translation type="vanished">подключиться к локальной оболочке</translation>
    </message>
    <message>
        <source>clone current session</source>
        <translation type="vanished">клонировать текущую сессию</translation>
    </message>
    <message>
        <source>switch ui to STD mode</source>
        <translation type="vanished">переключить интерфейс в режим STD</translation>
    </message>
    <message>
        <source>switch ui to MINI mode</source>
        <translation type="vanished">переключить интерфейс в режим MINI</translation>
    </message>
    <message>
        <source>switch to previous session</source>
        <translation type="vanished">переключиться на предыдущую сессию</translation>
    </message>
    <message>
        <source>switch to next session</source>
        <translation type="vanished">переключиться на следующую сессию</translation>
    </message>
    <message>
        <source>switch to session [num]</source>
        <translation type="vanished">переключиться на сессию [num]</translation>
    </message>
    <message>
        <source>Go to line start</source>
        <translation type="vanished">Перейти в начало строки</translation>
    </message>
    <message>
        <source>Go to line end</source>
        <translation type="vanished">Перейти в конец строки</translation>
    </message>
    <message>
        <source>There are sessions that have not yet been unlocked, please unlock them first.</source>
        <translation type="vanished">Есть сессии, которые еще не были разблокированы, пожалуйста, сначала разблокируйте их.</translation>
    </message>
    <message>
        <source>Are you sure to quit?</source>
        <translation type="vanished">Вы уверены, что хотите выйти?</translation>
    </message>
</context>
<context>
    <name>NetScanWindow</name>
    <message>
        <location filename="../src/netscanwindow/netscanwindow.ui" line="14"/>
        <source>NetScan</source>
        <translation>Сканирование сети</translation>
    </message>
</context>
<context>
    <name>OneStepWindow</name>
    <message>
        <source>Port</source>
        <translation type="obsolete">Порт</translation>
    </message>
    <message>
        <source>UserName</source>
        <translation type="obsolete">Имя пользователя</translation>
    </message>
</context>
<context>
    <name>PluginInfoWindow</name>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.ui" line="14"/>
        <source>Plugin Info</source>
        <translation>Информация о плагине</translation>
    </message>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.ui" line="28"/>
        <source>API version</source>
        <translation>Версия API</translation>
    </message>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.ui" line="38"/>
        <source>Install plugin</source>
        <translation>Установить плагин</translation>
    </message>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="40"/>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="86"/>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="40"/>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="86"/>
        <source>Version</source>
        <translation>Версия</translation>
    </message>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="40"/>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="86"/>
        <source>API Version</source>
        <translation>Версия API</translation>
    </message>
    <message>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="40"/>
        <location filename="../src/plugininfowindow/plugininfowindow.cpp" line="86"/>
        <source>Enable</source>
        <translation>Включить</translation>
    </message>
</context>
<context>
    <name>PluginViewerHomeWidget</name>
    <message>
        <location filename="../src/pluginviewerwidget/pluginviewerhomewidget.cpp" line="18"/>
        <source>Welcome to use</source>
        <translation>Добро пожаловать в</translation>
    </message>
    <message>
        <location filename="../src/pluginviewerwidget/pluginviewerhomewidget.cpp" line="20"/>
        <source>QuardCRT plugin system!</source>
        <translation>систему плагинов QuardCRT!</translation>
    </message>
</context>
<context>
    <name>PluginViewerWidget</name>
    <message>
        <location filename="../src/pluginviewerwidget/pluginviewerwidget.cpp" line="37"/>
        <source>Home</source>
        <translation>Домашняя страница</translation>
    </message>
</context>
<context>
    <name>QCustomFileSystemModel</name>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="155"/>
        <source>Directory</source>
        <translation>Каталог</translation>
    </message>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="155"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="159"/>
        <source>Loading...</source>
        <translation>Загрузка...</translation>
    </message>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="184"/>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="186"/>
        <source>Type</source>
        <translation>Тип</translation>
    </message>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="188"/>
        <source>Size</source>
        <translation>Размер</translation>
    </message>
    <message>
        <location filename="../lib/qcustomfilesystemmodel/qcustomfilesystemmodel.cpp" line="190"/>
        <source>Last Modified</source>
        <translation>Последнее изменение</translation>
    </message>
</context>
<context>
    <name>QKeychain::DeletePasswordJobPrivate</name>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="173"/>
        <source>Could not open keystore</source>
        <translation>Не удалось открыть хранилище ключей</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="179"/>
        <source>Could not remove private key from keystore</source>
        <translation>Не удалось удалить закрытый ключ из хранилища ключей</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="584"/>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="592"/>
        <source>Unknown error</source>
        <translation>Неизвестная ошибка</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="613"/>
        <source>Could not open wallet: %1; %2</source>
        <translation>Не удалось открыть кошелек: %1; %2</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="104"/>
        <source>Password entry not found</source>
        <translation>Запись пароля не найдена</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="108"/>
        <source>Could not decrypt data</source>
        <translation>Не удалось расшифровать данные</translation>
    </message>
</context>
<context>
    <name>QKeychain::Job</name>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="29"/>
        <source>No error</source>
        <translation>Нет ошибки</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="31"/>
        <source>The specified item could not be found in the keychain</source>
        <translation>Указанный элемент не может быть найден в хранилище ключей</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="33"/>
        <source>User canceled the operation</source>
        <translation>Пользователь отменил операцию</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="35"/>
        <source>User interaction is not allowed</source>
        <translation>Взаимодействие с пользователем не разрешено</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="37"/>
        <source>No keychain is available. You may need to restart your computer</source>
        <translation>Нет доступного хранилища ключей. Возможно, вам нужно перезапустить компьютер</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="39"/>
        <source>The user name or passphrase you entered is not correct</source>
        <translation>Введенное вами имя пользователя или пароль неверны</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="41"/>
        <source>A cryptographic verification failure has occurred</source>
        <translation>Произошла ошибка криптографической проверки</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="43"/>
        <source>Function or operation not implemented</source>
        <translation>Функция или операция не реализована</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="45"/>
        <source>I/O error</source>
        <translation>Ошибка ввода-вывода</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="47"/>
        <source>Already open with with write permission</source>
        <translation>Уже открыто с разрешением на запись</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="49"/>
        <source>Invalid parameters passed to a function</source>
        <translation>Недопустимые параметры, переданные функции</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="51"/>
        <source>Failed to allocate memory</source>
        <translation>Не удалось выделить память</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="53"/>
        <source>Bad parameter or invalid state for operation</source>
        <translation>Неверный параметр или недопустимое состояние для операции</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="55"/>
        <source>An internal component failed</source>
        <translation>Внутренний компонент не сработал</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="57"/>
        <source>The specified item already exists in the keychain</source>
        <translation>Указанный элемент уже существует в хранилище ключей</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="59"/>
        <source>Unable to decode the provided data</source>
        <translation>Не удалось декодировать предоставленные данные</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_apple.mm" line="62"/>
        <source>Unknown error</source>
        <translation>Неизвестная ошибка</translation>
    </message>
</context>
<context>
    <name>QKeychain::JobPrivate</name>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="294"/>
        <source>Unknown error</source>
        <translation>Неизвестная ошибка</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="541"/>
        <source>Access to keychain denied</source>
        <translation>Доступ к хранилищу ключей запрещен</translation>
    </message>
</context>
<context>
    <name>QKeychain::PlainTextStore</name>
    <message>
        <location filename="../lib/qtkeychain/plaintextstore.cpp" line="65"/>
        <source>Could not store data in settings: access error</source>
        <translation>Не удалось сохранить данные в настройках: ошибка доступа</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/plaintextstore.cpp" line="67"/>
        <source>Could not store data in settings: format error</source>
        <translation>Не удалось сохранить данные в настройках: ошибка формата</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/plaintextstore.cpp" line="85"/>
        <source>Could not delete data from settings: access error</source>
        <translation>Не удалось удалить данные из настроек: ошибка доступа</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/plaintextstore.cpp" line="87"/>
        <source>Could not delete data from settings: format error</source>
        <translation>Не удалось удалить данные из настроек: ошибка формата</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/plaintextstore.cpp" line="104"/>
        <source>Entry not found</source>
        <translation>Запись не найдена</translation>
    </message>
</context>
<context>
    <name>QKeychain::ReadPasswordJobPrivate</name>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="52"/>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="392"/>
        <source>Entry not found</source>
        <translation>Запись не найдена</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="60"/>
        <source>Could not open keystore</source>
        <translation>Не удалось открыть хранилище ключей</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="68"/>
        <source>Could not retrieve private key from keystore</source>
        <translation>Не удалось получить закрытый ключ из хранилища ключей</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="75"/>
        <source>Could not create decryption cipher</source>
        <translation>Не удалось создать шифр дешифрования</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="204"/>
        <source>D-Bus is not running</source>
        <translation>D-Bus не запущен</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="213"/>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="223"/>
        <source>Unknown error</source>
        <translation>Неизвестная ошибка</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="315"/>
        <source>No keychain service available</source>
        <translation>Нет доступных служб хранилища ключей</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="317"/>
        <source>Could not open wallet: %1; %2</source>
        <translation>Не удалось открыть кошелек: %1; %2</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="362"/>
        <source>Access to keychain denied</source>
        <translation>Доступ к хранилищу ключей запрещен</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="383"/>
        <source>Could not determine data type: %1; %2</source>
        <translation>Не удалось определить тип данных: %1; %2</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="401"/>
        <source>Unsupported entry type &apos;Map&apos;</source>
        <translation>Неподдерживаемый тип записи &apos;Map&apos;</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="404"/>
        <source>Unknown kwallet entry type &apos;%1&apos;</source>
        <translation>Неизвестный тип записи kwallet &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="32"/>
        <source>Password entry not found</source>
        <translation>Запись пароля не найдена</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="36"/>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="139"/>
        <source>Could not decrypt data</source>
        <translation>Не удалось расшифровать данные</translation>
    </message>
</context>
<context>
    <name>QKeychain::WritePasswordJobPrivate</name>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="95"/>
        <source>Could not open keystore</source>
        <translation>Не удалось открыть хранилище ключей</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="124"/>
        <source>Could not create private key generator</source>
        <translation>Не удалось создать генератор закрытых ключей</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="131"/>
        <source>Could not generate new private key</source>
        <translation>Не удалось сгенерировать новый закрытый ключ</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="139"/>
        <source>Could not retrieve private key from keystore</source>
        <translation>Не удалось получить закрытый ключ из хранилища ключей</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="147"/>
        <source>Could not create encryption cipher</source>
        <translation>Не удалось создать шифр шифрования</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_android.cpp" line="155"/>
        <source>Could not encrypt data</source>
        <translation>Не удалось зашифровать данные</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="444"/>
        <source>D-Bus is not running</source>
        <translation>D-Bus не запущен</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="454"/>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="481"/>
        <source>Unknown error</source>
        <translation>Неизвестная ошибка</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="500"/>
        <source>Could not open wallet: %1; %2</source>
        <translation>Не удалось открыть кошелек: %1; %2</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="78"/>
        <source>Credential size exceeds maximum size of %1</source>
        <translation>Размер учетных данных превышает максимальный размер %1</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="87"/>
        <source>Credential key exceeds maximum size of %1</source>
        <translation>Ключ учетных данных превышает максимальный размер %1</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="92"/>
        <source>Writing credentials failed: Win32 error code %1</source>
        <translation>Не удалось записать учетные данные: код ошибки Win32 %1</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_win.cpp" line="162"/>
        <source>Encryption failed</source>
        <translation>Шифрование не удалось</translation>
    </message>
</context>
<context>
    <name>QMessageBox</name>
    <message>
        <location filename="../lib/qtermwidget/TerminalDisplay.cpp" line="2949"/>
        <source>Show Details...</source>
        <translation>Показать подробности...</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../lib/qtermwidget/ColorScheme.cpp" line="267"/>
        <location filename="../lib/qtermwidget/ColorScheme.cpp" line="282"/>
        <source>Un-named Color Scheme</source>
        <translation>Безымянная цветовая схема</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/ColorScheme.cpp" line="453"/>
        <source>Accessible Color Scheme</source>
        <translation>Доступная цветовая схема</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/Filter.cpp" line="549"/>
        <source>Open Link</source>
        <translation>Открыть ссылку</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/Filter.cpp" line="550"/>
        <source>Copy Link Address</source>
        <translation>Копировать адрес ссылки</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/Filter.cpp" line="554"/>
        <source>Send Email To...</source>
        <translation>Отправить письмо на...</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/Filter.cpp" line="555"/>
        <source>Copy Email Address</source>
        <translation>Копировать адрес электронной почты</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/Filter.cpp" line="559"/>
        <source>Open Path</source>
        <translation>Открыть путь</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/Filter.cpp" line="560"/>
        <source>Copy Path</source>
        <translation>Копировать путь</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="254"/>
        <source>Access to keychain denied</source>
        <translation>Доступ к хранилищу ключей запрещен</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="256"/>
        <source>No keyring daemon</source>
        <translation>Нет демона кольцевых ключей</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="258"/>
        <source>Already unlocked</source>
        <translation>Уже разблокировано</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="260"/>
        <source>No such keyring</source>
        <translation>Нет такого кольцевого ключа</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="262"/>
        <source>Bad arguments</source>
        <translation>Неверные аргументы</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="264"/>
        <source>I/O error</source>
        <translation>Ошибка ввода-вывода</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="266"/>
        <source>Cancelled</source>
        <translation>Отменено</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="268"/>
        <source>Keyring already exists</source>
        <translation>Кольцевой ключ уже существует</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="270"/>
        <source>No match</source>
        <translation>Нет совпадений</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/keychain_unix.cpp" line="275"/>
        <source>Unknown error</source>
        <translation>Неизвестная ошибка</translation>
    </message>
    <message>
        <location filename="../lib/qtkeychain/libsecret.cpp" line="120"/>
        <source>Entry not found</source>
        <translation>Запись не найдена</translation>
    </message>
</context>
<context>
    <name>QTermWidget</name>
    <message>
        <location filename="../lib/qtermwidget/qtermwidget.cpp" line="350"/>
        <source>Color Scheme Error</source>
        <translation>Ошибка цветовой схемы</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/qtermwidget.cpp" line="351"/>
        <source>Cannot load color scheme: %1</source>
        <translation>Не удается загрузить цветовую схему: %1</translation>
    </message>
</context>
<context>
    <name>QuickConnectWindow</name>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="30"/>
        <source>Protocol</source>
        <translation>Протокол</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="43"/>
        <source>Serial</source>
        <translation>Серийный порт</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="48"/>
        <source>Local Shell</source>
        <translation>Локальная оболочка</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="58"/>
        <source>Named Pipe</source>
        <translation>Именованный канал</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="86"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="69"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="166"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="224"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="253"/>
        <source>Hostname</source>
        <translation>Имя хоста</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="109"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="70"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="167"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="225"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="254"/>
        <source>Port</source>
        <translation>Порт</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="125"/>
        <source>WebSocket</source>
        <translation>WebSocket</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="133"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="233"/>
        <source>None</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="138"/>
        <source>Insecure</source>
        <translation>Небезопасный</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="143"/>
        <source>Secure</source>
        <translation>Безопасный</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="161"/>
        <source>Username</source>
        <translation>Имя пользователя</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="171"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="188"/>
        <source>DataBits</source>
        <translation>Биты данных</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="225"/>
        <source>Parity</source>
        <translation>Четность</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="238"/>
        <source>Odd</source>
        <translation>Нечетный</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="243"/>
        <source>Even</source>
        <translation>Четный</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="261"/>
        <source>StopBits</source>
        <translation>Стоповые биты</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="313"/>
        <source>Save session</source>
        <translation>Сохранить сессию</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.ui" line="323"/>
        <source>Open in tab</source>
        <translation>Открыть во вкладке</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="33"/>
        <source>Quick Connect</source>
        <translation>Быстрое подключение</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="91"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="188"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="246"/>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="275"/>
        <source>e.g. 127.0.0.1</source>
        <translation>например, 127.0.0.1</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="99"/>
        <source>Port Name</source>
        <translation>Имя порта</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="100"/>
        <source>Baud Rate</source>
        <translation>Скорость передачи</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="109"/>
        <source>e.g. 110, 300, 600, 1200, 2400, 
4800, 9600, 14400, 19200, 38400, 
56000, 57600, 115200, 128000, 256000, 
460800, 921600</source>
        <translation>например, 110, 300, 600, 1200, 2400,
4800, 9600, 14400, 19200, 38400,
56000, 57600, 115200, 128000, 256000,
460800, 921600</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="141"/>
        <source>Command</source>
        <translation>Команда</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="162"/>
        <source>e.g. /bin/bash</source>
        <translation>например, /bin/bash</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="195"/>
        <source>Pipe Name</source>
        <translation>Имя канала</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="217"/>
        <source>e.g. \\\.\pipe\namedpipe</source>
        <translation>например, \\\.\pipe\namedpipe</translation>
    </message>
    <message>
        <location filename="../src/quickconnectwindow/quickconnectwindow.cpp" line="219"/>
        <source>e.g. /tmp/socket</source>
        <translation>например, /tmp/socket</translation>
    </message>
</context>
<context>
    <name>SFTPmenuBookmarkWidget</name>
    <message>
        <location filename="../src/sftpwindow/sftpmenubookmarkwidget.ui" line="17"/>
        <source>BookMarkName</source>
        <translation>Имя закладки</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpmenubookmarkwidget.ui" line="35"/>
        <source>LocalPath</source>
        <translation>Локальный путь</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpmenubookmarkwidget.ui" line="62"/>
        <source>RemotePath</source>
        <translation>Удаленный путь</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpmenubookmarkwidget.cpp" line="31"/>
        <source>Open Directory</source>
        <translation>Открыть каталог</translation>
    </message>
</context>
<context>
    <name>SFTPmenuBookmarkWindow</name>
    <message>
        <source>Bookmark</source>
        <translation type="vanished">Закладка</translation>
    </message>
    <message>
        <source>BookMarkName</source>
        <translation type="vanished">Имя закладки</translation>
    </message>
    <message>
        <source>LocalPath</source>
        <translation type="vanished">Локальный путь</translation>
    </message>
    <message>
        <source>RemotePath</source>
        <translation type="vanished">Удаленный путь</translation>
    </message>
    <message>
        <source>Open Directory</source>
        <translation type="vanished">Открыть каталог</translation>
    </message>
</context>
<context>
    <name>SearchBar</name>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.ui" line="14"/>
        <source>SearchBar</source>
        <translation>Панель поиска</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.ui" line="20"/>
        <source>X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.ui" line="32"/>
        <source>Find:</source>
        <translation>Найти:</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.ui" line="42"/>
        <source>&lt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.ui" line="54"/>
        <source>&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.ui" line="66"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.cpp" line="40"/>
        <location filename="../lib/qtermwidget/SearchBar.cpp" line="130"/>
        <source>Match case</source>
        <translation>Учитывать регистр</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.cpp" line="46"/>
        <location filename="../lib/qtermwidget/SearchBar.cpp" line="131"/>
        <source>Regular expression</source>
        <translation>Регулярное выражение</translation>
    </message>
    <message>
        <location filename="../lib/qtermwidget/SearchBar.cpp" line="50"/>
        <location filename="../lib/qtermwidget/SearchBar.cpp" line="132"/>
        <source>Highlight all matches</source>
        <translation>Подсвечивать все совпадения</translation>
    </message>
</context>
<context>
    <name>SessionManagerTreeModel</name>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="133"/>
        <source>Telnet</source>
        <translation>Телнет</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="135"/>
        <source>Serial</source>
        <translation>Серийный порт</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="137"/>
        <source>Shell</source>
        <translation>Оболочка</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="139"/>
        <source>Raw</source>
        <translation>Raw</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="141"/>
        <source>NamePipe</source>
        <translation>Именованный канал</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="269"/>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreemodel.cpp" line="271"/>
        <source>Kind</source>
        <translation>Вид</translation>
    </message>
</context>
<context>
    <name>SessionManagerTreeView</name>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="39"/>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="74"/>
        <source>Session</source>
        <translation>Сессия</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="84"/>
        <source>Connect Terminal</source>
        <translation>Подключить терминал</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="89"/>
        <source>Connect in New Window</source>
        <translation>Подключить в новом окне</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="96"/>
        <source>Connect in New Tab Group</source>
        <translation>Подключить в новой группе вкладок</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="103"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagertreeview.cpp" line="109"/>
        <source>Properties</source>
        <translation>Свойства</translation>
    </message>
</context>
<context>
    <name>SessionManagerWidget</name>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagerwidget.ui" line="43"/>
        <source>Session Manager</source>
        <translation>Менеджер сессий</translation>
    </message>
    <message>
        <location filename="../src/sessionmanagerwidget/sessionmanagerwidget.cpp" line="67"/>
        <source>Filter by folder/session name</source>
        <translation>Фильтровать по имени папки/сессии</translation>
    </message>
</context>
<context>
    <name>SessionOptionsGeneralWidget</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsgeneralwidget.ui" line="25"/>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsgeneralwidget.ui" line="45"/>
        <source>Protocol</source>
        <translation>Протокол</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsgeneralwidget.ui" line="58"/>
        <source>Serial</source>
        <translation>Серийный порт</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsgeneralwidget.ui" line="63"/>
        <source>Local Shell</source>
        <translation>Локальная оболочка</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsgeneralwidget.ui" line="73"/>
        <source>Named Pipe</source>
        <translation>Именованный канал</translation>
    </message>
</context>
<context>
    <name>SessionOptionsLocalShellProperties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionslocalshellproperties.ui" line="19"/>
        <source>Command</source>
        <translation>Команда</translation>
    </message>
</context>
<context>
    <name>SessionOptionsLocalShellState</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionslocalshellstate.cpp" line="33"/>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionslocalshellstate.ui" line="25"/>
        <source>State</source>
        <translation>Состояние</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionslocalshellstate.ui" line="51"/>
        <source>Process Tree</source>
        <translation>Дерево процессов</translation>
    </message>
</context>
<context>
    <name>SessionOptionsNamePipeProperties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsnamepipeproperties.ui" line="25"/>
        <source>PipeName</source>
        <translation>Имя канала</translation>
    </message>
</context>
<context>
    <name>SessionOptionsRawProperties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsrawproperties.ui" line="25"/>
        <source>Hostname</source>
        <translation>Имя хоста</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsrawproperties.ui" line="45"/>
        <source>Port</source>
        <translation>Порт</translation>
    </message>
</context>
<context>
    <name>SessionOptionsSerialProperties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="25"/>
        <source>Port Name</source>
        <translation>Имя порта</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="45"/>
        <source>Baud Rate</source>
        <translation>Скорость передачи</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="72"/>
        <source>DataBits</source>
        <translation>Биты данных</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="109"/>
        <source>Parity</source>
        <translation>Четность</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="117"/>
        <source>None</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="122"/>
        <source>Odd</source>
        <translation>Нечетный</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="127"/>
        <source>Even</source>
        <translation>Четный</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.ui" line="145"/>
        <source>StopBits</source>
        <translation>Стоповые биты</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsserialproperties.cpp" line="28"/>
        <source>e.g. 110, 300, 600, 1200, 2400, 
4800, 9600, 14400, 19200, 38400, 
56000, 57600, 115200, 128000, 256000, 
460800, 921600</source>
        <translation>например, 110, 300, 600, 1200, 2400,
4800, 9600, 14400, 19200, 38400,
56000, 57600, 115200, 128000, 256000,
460800, 921600</translation>
    </message>
</context>
<context>
    <name>SessionOptionsSsh2Properties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsssh2properties.ui" line="25"/>
        <source>Hostname</source>
        <translation>Имя хоста</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsssh2properties.ui" line="41"/>
        <source>Port</source>
        <translation>Порт</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsssh2properties.ui" line="65"/>
        <source>UserName</source>
        <translation>Имя пользователя</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsssh2properties.ui" line="81"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
</context>
<context>
    <name>SessionOptionsTelnetProperties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionstelnetproperties.ui" line="19"/>
        <source>Hostname</source>
        <translation>Имя хоста</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionstelnetproperties.ui" line="33"/>
        <source>Port</source>
        <translation>Порт</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionstelnetproperties.ui" line="55"/>
        <source>None</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionstelnetproperties.ui" line="60"/>
        <source>Insecure</source>
        <translation>Небезопасный</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionstelnetproperties.ui" line="65"/>
        <source>Secure</source>
        <translation>Безопасный</translation>
    </message>
</context>
<context>
    <name>SessionOptionsVNCProperties</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsvncproperties.ui" line="25"/>
        <source>Hostname</source>
        <translation>Имя хоста</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsvncproperties.ui" line="41"/>
        <source>Port</source>
        <translation>Порт</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionsvncproperties.ui" line="65"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
</context>
<context>
    <name>SessionOptionsWindow</name>
    <message>
        <location filename="../src/sessionoptions/sessionoptionswindow.ui" line="14"/>
        <source>Session Options</source>
        <translation>Настройки сессии</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionswindow.cpp" line="122"/>
        <source>General</source>
        <translation>Общие</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionswindow.cpp" line="122"/>
        <source>Properties</source>
        <translation>Свойства</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionswindow.cpp" line="122"/>
        <source>State</source>
        <translation>Состояние</translation>
    </message>
    <message>
        <location filename="../src/sessionoptions/sessionoptionswindow.cpp" line="133"/>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
</context>
<context>
    <name>SessionsWindow</name>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="141"/>
        <source>Telnet Error</source>
        <translation>Ошибка Telnet</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="141"/>
        <source>Telnet error:
%1.</source>
        <translation>Ошибка Telnet:
%1.</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="168"/>
        <source>Serial Error</source>
        <translation>Ошибка серийного порта</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="168"/>
        <source>Serial error:
%1.</source>
        <translation>Ошибка серийного порта:
%1.</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="203"/>
        <source>Raw Socket Error</source>
        <translation>Ошибка Raw Socket</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="203"/>
        <source>Raw Socket error:
%1.</source>
        <translation>Ошибка Raw Socket:
%1.</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="234"/>
        <source>Name Pipe Error</source>
        <translation>Ошибка именованного канала</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="234"/>
        <source>Name Pipe error:
%1.</source>
        <translation>Ошибка именованного канала:
%1.</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="246"/>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="275"/>
        <source>SSH2 Error</source>
        <translation>Ошибка SSH2</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="246"/>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="275"/>
        <source>SSH2 error:
%1.</source>
        <translation>Ошибка SSH2:
%1.</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="434"/>
        <source>Start Local Shell</source>
        <translation>Запустить локальную оболочку</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="434"/>
        <source>Cannot start local shell:
%1.</source>
        <translation>Не удалось запустить локальную оболочку:
%1.</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="573"/>
        <source>Save log...</source>
        <translation>Сохранить журнал...</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="574"/>
        <source>log files (*.log)</source>
        <translation>файлы журналов (*.log)</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="578"/>
        <source>Save log</source>
        <translation>Сохранить журнал</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="578"/>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="613"/>
        <source>Cannot write file %1:
%2.</source>
        <translation>Не удалось записать файл %1:
%2.</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="608"/>
        <source>Save Raw log...</source>
        <translation>Сохранить Raw журнал...</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="609"/>
        <source>binary files (*.bin)</source>
        <translation>бинарные файлы (*.bin)</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="613"/>
        <source>Save Raw log</source>
        <translation>Сохранить Raw журнал</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="685"/>
        <source>Unlock Session</source>
        <translation>Разблокировать сессию</translation>
    </message>
    <message>
        <location filename="../src/sessionswindow/sessionswindow.cpp" line="685"/>
        <source>Wrong password!</source>
        <translation>Неверный пароль!</translation>
    </message>
</context>
<context>
    <name>SftpWindow</name>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.ui" line="14"/>
        <source>sftp</source>
        <translation>sftp</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.ui" line="72"/>
        <source>local</source>
        <translation>локальный</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.ui" line="130"/>
        <source>remote</source>
        <translation>удаленный</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="46"/>
        <source>Bookmarks</source>
        <translation>Закладки</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="47"/>
        <source>Add Bookmark</source>
        <translation>Добавить закладку</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="48"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="67"/>
        <source>Edit Bookmark</source>
        <translation>Редактировать закладку</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="49"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="84"/>
        <source>Remove Bookmark</source>
        <translation>Удалить закладку</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="67"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="84"/>
        <source>Bookmark Name:</source>
        <translation>Имя закладки:</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="109"/>
        <source>Warning</source>
        <translation>Предупреждение</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="109"/>
        <source>Bookmark Name can not be empty!</source>
        <translation>Имя закладки не может быть пустым!</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="196"/>
        <source>No task!</source>
        <translation>Нет задач!</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="200"/>
        <source>All tasks finished!</source>
        <translation>Все задачи завершены!</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="205"/>
        <source>task %1/%2</source>
        <translation>задача %1/%2</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="223"/>
        <source>Open Directory</source>
        <translation>Открыть каталог</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="246"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="412"/>
        <source>Show/Hide Files</source>
        <translation>Показать/скрыть файлы</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="250"/>
        <source>Upload</source>
        <translation>Загрузить</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="301"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="476"/>
        <source>Open</source>
        <translation>Открыть</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="305"/>
        <source>Open in System File Manager</source>
        <translation>Открыть в файловом менеджере системы</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="310"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="482"/>
        <source>refresh</source>
        <translation>обновить</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="315"/>
        <source>Upload All</source>
        <translation>Загрузить все</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="359"/>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="535"/>
        <source>Cancel Selection</source>
        <translation>Отменить выбор</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="416"/>
        <source>Download</source>
        <translation>Скачать</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="456"/>
        <source>File exists</source>
        <translation>Файл существует</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="456"/>
        <source>File %1 already exists. Do you want to overwrite it?</source>
        <translation>Файл %1 уже существует. Вы хотите перезаписать его?</translation>
    </message>
    <message>
        <location filename="../src/sftpwindow/sftpwindow.cpp" line="487"/>
        <source>Download All</source>
        <translation>Скачать все</translation>
    </message>
</context>
<context>
    <name>StartTftpSeverWindow</name>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.ui" line="14"/>
        <source>Start TFTP Sever</source>
        <translation>Запустить TFTP сервер</translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.ui" line="32"/>
        <source>The TFTP server uses local directories for uploading and downloading files. Please enter this information below.</source>
        <translation>TFTP сервер использует локальные каталоги для загрузки и скачивания файлов. Пожалуйста, введите эту информацию ниже.</translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.ui" line="65"/>
        <source>TFTP Port</source>
        <translation>TFTP порт</translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.ui" line="104"/>
        <source>Upload directory</source>
        <translation>Каталог загрузки</translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.ui" line="143"/>
        <source>Download directory</source>
        <translation>Каталог скачивания</translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.cpp" line="38"/>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.cpp" line="45"/>
        <source>Open Directory</source>
        <translation>Открыть каталог</translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.cpp" line="77"/>
        <source>Warning</source>
        <translation>Предупреждение</translation>
    </message>
    <message>
        <location filename="../src/starttftpseverwindow/starttftpseverwindow.cpp" line="77"/>
        <source>Please select a valid directory!</source>
        <translation>Пожалуйста, выберите действительный каталог!</translation>
    </message>
</context>
<context>
    <name>UndoStack</name>
    <message>
        <location filename="../lib/qhexedit/commands.cpp" line="115"/>
        <source>Inserting %1 bytes</source>
        <translation>Вставка %1 байт</translation>
    </message>
    <message>
        <location filename="../lib/qhexedit/commands.cpp" line="137"/>
        <source>Delete %1 chars</source>
        <translation>Удаление %1 символов</translation>
    </message>
    <message>
        <location filename="../lib/qhexedit/commands.cpp" line="162"/>
        <source>Overwrite %1 chars</source>
        <translation>Перезапись %1 символов</translation>
    </message>
</context>
<context>
    <name>keyMapManager</name>
    <message>
        <location filename="../src/keymapmanager/keymapmanager.ui" line="14"/>
        <source>keyMapManager</source>
        <translation>Менеджер карты клавиш</translation>
    </message>
    <message>
        <location filename="../src/keymapmanager/keymapmanager.ui" line="20"/>
        <source>KeyBinding</source>
        <translation>Привязка клавиш</translation>
    </message>
</context>
</TS>
